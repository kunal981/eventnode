//
//  OnlineOnlyInviteEmailGuest.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class OnlineOnlyInviteEmailGuest
{
    func emailMessage(eventId: String, eventTitle: String,  hostName: String, type: String, imageUrl: String, url:String)-> String
{

     var file = "Online_Only_Invite_Email_Guest.html"

    
    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    println(base64String)
    
    let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    
    let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

    
     var path = documentDirectory.stringByAppendingPathComponent(file)

     var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!

     message = message.stringByReplacingOccurrencesOfString("<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"http://dcy86hdr5o800.cloudfront.net/image1-online.jpg\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", withString:"<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"\(imageUrl)\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", options: nil, range: nil)

     message = message.stringByReplacingOccurrencesOfString("Sharon tucker", withString:"\(hostName)", options: nil, range: nil)
   
     message = message.stringByReplacingOccurrencesOfString("Asia trip 2014", withString:"\(eventTitle)", options: nil, range: nil)
    
    message = message.stringByReplacingOccurrencesOfString("geetika", withString:"\(hostName)", options: nil, range: nil)
    
     message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center text-decoration: none;\" href=\"#\" target=\"_blank\">Join Event</a></td>", withString:"<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Join Event</a></td>", options: nil, range: nil)
    
  return message
    
    
    
 }
}