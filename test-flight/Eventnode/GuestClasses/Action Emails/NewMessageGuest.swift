//
//  NewMessageGuest.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class NewMessageGuest
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, messageText: String, url:String )-> String
{

var file = "new_message_guest.html"

var path = documentDirectory.stringByAppendingPathComponent(file)

    
    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    println(base64String)
    
    let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    
    let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!

   message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRRRRRRRRRosy's Baby Shower", withString: "\(eventTitle)", options: nil, range: nil)


message = message.stringByReplacingOccurrencesOfString("SSSSSSSSSharon Tucker sent a group message for the event: <br><b>RRRRRRRRosy’s Baby Shower", withString:"\(hostName) sent a group message for the event: <br><b>\(eventTitle)", options: nil, range: nil)
    
    
    message = message.stringByReplacingOccurrencesOfString("HHHHHHHHHHHHHHHHHey guys, how are you?", withString:"\(messageText)", options: nil, range: nil)
    
    message = message.stringByReplacingOccurrencesOfString("<a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Event</a>", withString:"<a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Go to Event</a>", options: nil, range: nil)
    
   
    
   

return message
}
}