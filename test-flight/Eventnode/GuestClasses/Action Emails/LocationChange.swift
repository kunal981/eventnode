//
//  LocationChange.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class LocationChange
{
    func emailMessage(eventId: String, eventTitle: String,  locationString: String, hostName: String, type: String, url:String  )-> String
{

var file = "location_change.html"

var path = documentDirectory.stringByAppendingPathComponent(file)

    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    println(base64String)

    let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    
    let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    

var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!

message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRRRRRRRosy's Baby Shower", withString: "\(eventTitle)", options: nil, range: nil)

if type == "rsvp"
{
    message = message.stringByReplacingOccurrencesOfString("SSSSSSSSSSSSSSSSSSharon Tucker", withString: "\(hostName)", options: nil, range: nil)
    
    message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRRRooooooooooooooooooosy's Baby Shower", withString: "\(eventTitle)", options: nil, range: nil)
    
    message = message.stringByReplacingOccurrencesOfString("33333333333333444 Ssssssssssssspectrum, Irvine, CA 9261999999999998", withString:"\(locationString)", options: nil, range: nil)
    
    message = message.stringByReplacingOccurrencesOfString("deeeeeeeeeeeeeeeeppppppppppppppplllllllllllllliiiiiiiiinnnnnnnkkkkkkkkkkk", withString:"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)", options: nil, range: nil)
    
}


return message
}
}