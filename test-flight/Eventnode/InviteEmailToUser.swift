//
//  InviteEmailToUser.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class InviteEmailToUser
{
    func emailMessage(inviteCode: String, eventTitle: String, dateString: String, timeString: String, locationString: String, hostName: String, type: String)-> String
    {
        
        var file = "Invite_User.html"
        
        var path = documentDirectory.stringByAppendingPathComponent(file)
        
        
        
        var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!
        
        message = message.stringByReplacingOccurrencesOfString("<span style='font-size: 28px; font-weight:600;'>Naz Merritt </span><span style='font-size:28px;font-weight: 300;'>invited you to the event,</span><span style='font-size: 28px;font-weight: 600;'> Geetika's Baby Shower</span></div>", withString: "<span style='font-size: 28px; font-weight:600;'>\(hostName) </span><span style='font-size:28px;font-weight: 300;'>invited you to the event,</span><span style='font-size: 28px;font-weight: 600;'> \(eventTitle)</span></div>", options: nil, range: nil)
        
        if type == "rsvp"
        {
            message = message.stringByReplacingOccurrencesOfString("<span style='font-size:16px; line-height:24px;'><span style='font-weight: 300;'>Date:</span><span style='font-weight: 500;padding-left:10px;'>Sunday, November 1, 2015</span></span></div><div style=\"font-size: 12px; line-height: 18px;color: #555555;font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif;text-align: left;line-height: 18px\"><span style='font-size:16px; line-height:24px;'><span style='font-weight: 300;'>Time:</span><span style='font-weight: 500;padding-left:10px;'>6:30PM</span></span></div><div style=\"font-size: 12px; line-height: 18px;color: #555555;font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif;text-align: left;line-height: 18px\"><span style='font-size:16px; line-height:24px;'><span style='font-weight: 300;'>Location:</span><span style='font-weight: 500;padding-left:10px;'>3444 Spectrum, Irvine, CA 92618</span></span></div>", withString: "<span style=\"font-size:16px; line-height:24px;\"><span style='font-weight: 300;'>Date:</span><span style='font-weight: 500;padding-left:10px;'>\(dateString)</span></span></div><div style=\"font-size: 12px; line-height: 18px;color: #555555;font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif;text-align: left;line-height: 18px\"><span style='font-size:16px; line-height:24px;'><span style='font-weight: 300;'>Time:</span><span style='font-weight: 500;padding-left:10px;'>\(timeString)</span></span></div><div style=\"font-size: 12px; line-height: 18px;color: #555555;font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif;text-align: left;line-height: 18px\"><span style='font-size:16px; line-height:24px;'><span style='font-weight: 300;'>Location:</span><span style='font-weight: 500;padding-left:10px;'>\(locationString)</span></span></div>", options: nil, range: nil)
        }
        else
        {
            message = message.stringByReplacingOccurrencesOfString("<span style='font-size:16px; line-height:24px;'><span style='font-weight: 300;'>Date:</span><span style='font-weight: 500;padding-left:10px;'>Sunday, November 1, 2015</span></span></div><div style=\"font-size: 12px; line-height: 18px;color: #555555;font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif;text-align: left;line-height: 18px\"><span style='font-size:16px; line-height:24px;'><span style='font-weight: 300;'>Time:</span><span style='font-weight: 500;padding-left:10px;'>6:30PM</span></span></div><div style=\"font-size: 12px; line-height: 18px;color: #555555;font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif;text-align: left;line-height: 18px\"><span style='font-size:16px; line-height:24px;'><span style='font-weight: 300;'>Location:</span><span style='font-weight: 500;padding-left:10px;'>3444 Spectrum, Irvine, CA 92618</span></span></div>", withString: "", options: nil, range: nil)

        }
        
        return message
    }
}