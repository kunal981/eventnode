//
//  BetaViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 10/8/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class BetaViewController: UIViewController
{
    
    @IBOutlet weak var betaCode: UITextField!
    
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderSubView: UIView!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.view.addSubview(wakeUpImageView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        var loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        loadingMessage.textAlignment = .Center
        loaderSubView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        self.loaderView.hidden = true
        
        if var isBetaCodeEntered = NSUserDefaults.standardUserDefaults().valueForKey("isBetaCodeEntered") as? String
        {
            if isBetaCodeEntered == "yes"
            {
                let customLoginScreen = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
                self.navigationController?.pushViewController(customLoginScreen, animated: true)
                
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
     {
        betaCode.resignFirstResponder()
     }

    @IBAction func goButtonClicked(sender: AnyObject)
    {
        
        betaCode.resignFirstResponder()
        loaderView.hidden = false
        
        
        var betaQuery = PFQuery(className: "BetaCode")
        betaQuery.whereKey("InvitationCode", equalTo: betaCode.text)
        
        ParseOperations.instance.fetchData(betaQuery, target: self, successSelector: "fetchBetaDataSuccess:", successSelectorParameters: nil, errorSelector:"fetchBetaDataError:", errorSelectorParameters: nil)
    }
    
    func fetchBetaDataSuccess(timer:NSTimer)
    {
        
        loaderView.hidden = true
        var fetchedBetaObjects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if var betaCodes = fetchedBetaObjects
        {
            if betaCodes.count == 0
            {
                println("Unknown Beta Code: \(betaCode.text)")
                NSUserDefaults.standardUserDefaults().setObject("no", forKey: "isBetaCodeEntered")
                
                var refreshAlert = UIAlertController(title: "Unknown Code", message: "Please enter a valid invitation code. If you don't have one and would like to participate in private beta, please request for an early access.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in}))
                
                betaCode.text = ""
                
                presentViewController(refreshAlert, animated: true, completion: nil)
                
            }
            else
            {
                AnalyticsModel.instance.logBetaCodeSuccess(betaCode.text)
                
                NSUserDefaults.standardUserDefaults().setObject("yes", forKey: "isBetaCodeEntered")
                
                let loginScreen = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
                self.navigationController?.pushViewController(loginScreen, animated: true)
                
            }

        }
        
    }
    
    func fetchBetaDataError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        // Log a metric to track all parse errors ?
    }
    
    
    @IBAction func earlyAccesButton(sender: AnyObject)
    {
        AnalyticsModel.instance.logEarlyAccessRequestMetric();
        let openLink = NSURL(string : "http://signup.eventnode.co/signup")
        UIApplication.sharedApplication().openURL(openLink!)
        
    }
}
