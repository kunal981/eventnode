//
//  FeedbackViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 5/5/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    @IBOutlet var evenNameText : UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        evenNameText.becomeFirstResponder()
        // Do any additional setup after loading the view.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func keyboardWillShow(sender: NSNotification) {
        evenNameText.frame = CGRectMake(16 , 237, evenNameText.frame.width, evenNameText.frame.height - 175)
    }
    func keyboardWillHide(sender: NSNotification) {
        evenNameText.frame = CGRectMake(16 , 237, evenNameText.frame.width, evenNameText.frame.height + 175)
    }
    
    
    @IBAction func viewTapped(sender : AnyObject) {
        evenNameText.resignFirstResponder()
    }
    
    
    @IBAction func eventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func sharedEventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertViewController") as! AlertViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }

}
