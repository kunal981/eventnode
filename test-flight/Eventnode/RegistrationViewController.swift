

import UIKit

class RegistrationViewController: UIViewController,UITextFieldDelegate,UIAlertViewDelegate
{
    
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var backButton: UIImageView!
    
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var wakeupView: UIImageView!
    @IBOutlet weak var wakeUpView: UIImageView!
    var email = NSString()
    
    var user = PFUser()
    
    var stringCompare = NSString()
    var alert = UIAlertView()
    
    var goToNext: Bool = true
    
    var deepLinkEmail = ""
    var deepLinkObjectId = ""
    var eventCreatorObjectId = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        self.view.addSubview(wakeUpImageView)


        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        var loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        loaderSubView.layer.cornerRadius = 10
        
        self.wakeUpView.hidden = true
        self.loaderView.hidden = true
        
        fullNameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        fullNameTextField.layer.cornerRadius = 3
        emailTextField.layer.cornerRadius = 3
        passwordTextField.layer.cornerRadius = 3
        passwordTextField.secureTextEntry = true
        
        emailTextField.keyboardType = UIKeyboardType.EmailAddress
        
        fullNameTextField.tag = 1
        emailTextField.tag = 2
        passwordTextField.tag = 3
        textFieldPadding()
        
        
        alert.delegate = self
        alert.title = "Alert"
        alert.message = "Enter username"
        alert.addButtonWithTitle("Ok")
        
        
        if deepLinkEmail == "noemail"
        {
            emailTextField.text = ""
        }
        else
        {
            emailTextField.text = deepLinkEmail
            
        }

        
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    override func  touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        
        var touch = touches.first as! UITouch
        
        fullNameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
    }
    
    @IBAction func termsAndConditionbtn(sender: AnyObject)
    {
         let termsAndCondition = self.storyboard!.instantiateViewControllerWithIdentifier("TermsAndConditions") as! TermsAndConditionViewController
        self.navigationController?.pushViewController(termsAndCondition, animated: false)
        
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        if textField == emailTextField
        {
            //emailTextField.text = emailTextField.text.lowercaseString
        }
        return true
        
    }
    
    
    @IBAction func sIgnUpButton(sender: AnyObject)
    {
        stringCompare = passwordTextField.text
        if fullNameTextField.text == "" || emailTextField.text == "" || passwordTextField.text == ""
        {
            goToNext = false
            var alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Please Fill Require Fields"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
        else if !isValidEmail(emailTextField.text)
        {
            goToNext = false
            
            var alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Enter Valid EmailId"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
            
        }
        else if stringCompare.length < 7
        {
            goToNext = false
            var alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Enter Strong Password"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
        else
        {
            userRegister()
        }
    }
    
    
    func userRegister()
    {
        wakeUpView.hidden = true
        loaderView.hidden = false
        var user = PFUser()
        
        var lowerEmail = emailTextField.text.lowercaseString
        
        var isTrimmed = true
        
        var emailId = lowerEmail as NSString
        
        while(isTrimmed)
        {
            
            var startSpace = emailId.substringWithRange(NSRange(location: 0, length: 1))
            
            var endSpace = emailId.substringWithRange(NSRange(location: emailId.length-1, length: 1))
            
            if startSpace == " " || endSpace == " "
            {
                if startSpace == " "
                {
                    emailId = emailId.stringByReplacingCharactersInRange(NSRange(location: 0, length: 1), withString: "")
                }
                if endSpace == " "
                {
                    emailId = emailId.stringByReplacingCharactersInRange(NSRange(location: emailId.length-1, length: 1), withString: "")
                }
            }
            else
            {
                isTrimmed = false
            }
        }
        
        self.loaderView.hidden = true
        println("email:\(emailId)")
        
        lowerEmail = emailId as String
        
        
        user.username = lowerEmail
        user.email = lowerEmail
        
        user.password = passwordTextField.text
        user["fullUserName"] = fullNameTextField.text
        user["isFacebookLogin"] =  false
        user["hasPassword"] =  true
        
        println(user.email!)
        
        let query = PFQuery(className: "LinkedAccounts")
        query.whereKey("emailId", equalTo: user.email!)
        query.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            
            if let users = users as? [PFObject]
            {
                println(users)
                if users.count == 0
                {
                    self.createUser(user)
                }
                else
                {
                    if users[0]["isEmailVerified"] as? Bool == true
                    {
                        self.goToNext = false
                        self.loaderView.hidden = true
                        var alert1 = UIAlertView()
                        alert1.title = "Error"
                        alert1.delegate = self
                        alert1.message = "This email id is already linked with a different eventnode account. If you want to use it, unlink it first from that account."
                        alert1.addButtonWithTitle("Ok")
                        alert1.show()
                        println("email id linked with some other account")
                    }
                    else
                    {
                        users[0].deleteInBackgroundWithBlock{
                            (success: Bool, error: NSError?) -> Void in
                            if (success)
                            {
                                self.createUser(user)
                            }
                            else
                            {
                                self.goToNext = false
                                self.loaderView.hidden = true
                                var alert1 = UIAlertView()
                                alert1.title = "Error"
                                alert1.delegate = self
                                alert1.message = "Unknown error occured"
                                alert1.addButtonWithTitle("Ok")
                                alert1.show()
                            }
                        }
                    }
                }
            }
            else
            {
                self.goToNext = false
                self.loaderView.hidden = true
                var alert1 = UIAlertView()
                alert1.title = "Error"
                alert1.delegate = self
                alert1.message = "Unknown error occured"
                alert1.addButtonWithTitle("Ok")
                alert1.show()
            }
        }
    }
    
    
    func createUser(user: PFUser)
    {
        user.signUpInBackgroundWithBlock
            { (succeeded: Bool, error: NSError?) -> Void in
                if let error = error
                {
                    self.goToNext = false
                    self.loaderView.hidden = true
                    //let errorString = error.userInfo?["error"] as? NSString
                    //println("(\(error.localizedDescription))")
                    var alert1 = UIAlertView()
                    alert1.title = "Error"
                    if error.code == 202
                    {
                        alert1.message = "Email id \(self.emailTextField.text!) is already taken. Please try with a different email id."
                    }
                    else
                    {
                        alert1.message = "Something went wrong. Please try again later."
                    }
                    //alert1.message = error.localizedDescription
                    alert1.addButtonWithTitle("Ok")
                    alert1.show()
                } else
                {
                    /*self.goToNext = true
                    self.loaderView.hidden = true
                    var alert1 = UIAlertView()
                    alert1.title = "Alert"
                    alert1.delegate = self
                    alert1.message = "Register Succesfully"
                    alert1.addButtonWithTitle("Ok")
                    alert1.show()
                    println("Register Succesfully")*/
                    
                    let fileManager = NSFileManager.defaultManager()
                    var imagePath = "\(documentDirectory)/profilePic.png"
                    
                    if (fileManager.fileExistsAtPath(imagePath))
                    {
                        var error:NSErrorPointer = NSErrorPointer()

                        fileManager.removeItemAtPath(imagePath, error: error)

                        if error != nil
                        {
                            println(error.debugDescription)
                        }
                    }

                    NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")


                    NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
                    NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                    NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isNormalLogin")
                    NSUserDefaults.standardUserDefaults().setObject(self.passwordTextField.text!, forKey: "password")
                    NSUserDefaults.standardUserDefaults().setObject(self.fullNameTextField.text!, forKey: "fullUserName")
                    
                    AnalyticsModel.instance.identifyNewUserForAnalytics(user.email!, isFbUser: false, name: self.fullNameTextField.text!, user: user)
                    SupportModel.instance.initializeWithUsernameAndEmail(self.fullNameTextField.text!, email: user.email!)
                    
                    isFacebookLogin = false
                    hasPassword = true
                    
                    
                    showInviteCodePopup = true
                    
                    //self.updateDeviceToken(user.objectId!)
                    self.updateNotificationSettingsWithDeviceToken(user, userObjectId: user.objectId!)
                    
                    var localNotification = UILocalNotification()
                    
                    localNotification.fireDate = NSDate()
                    
                    localNotification.alertBody = "Welcome to eventnode, we love that you choose us. Don’t wait for a special occasion, everyday can be an event with eventnode."
                    
                    // localNotification.applicationIconBadgeNumber = 100
                    
                    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                    

                    
                    //let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteCodeViewController") as! InviteCodeViewController
                    
                    if self.deepLinkEmail != ""
                    {
                        let SharedVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
                        
                        SharedVC.deepLinkEmail = self.deepLinkEmail
                        SharedVC.deepLinkObjectId = self.deepLinkObjectId
                        SharedVC.eventCreatorObjectId = self.eventCreatorObjectId
                        
                        self.navigationController?.pushViewController(SharedVC, animated: true)
                        
                    }
                    else
                    {
                        
                        self.navigateToNextScreen(self.fullNameTextField.text!)
                        /*let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
                        self.navigationController?.pushViewController(eventVC, animated: false)*/
                        
                    }

                    //eventVC.backOrPop = true
                    
                    println("\(PFUser.currentUser())")
                    
                }
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if buttonIndex == 0
        {
            if goToNext
            {
                let logInView = self.storyboard!.instantiateViewControllerWithIdentifier("logInWithEmail") as! LogInWithEmailViewController
                
                logInView.deepLinkEmail = self.deepLinkEmail
                logInView.deepLinkObjectId = self.deepLinkObjectId
                logInView.eventCreatorObjectId = self.eventCreatorObjectId
                
                self.navigationController?.pushViewController(logInView, animated: false)
            }
        }
    }
    
    
    func isValidEmail(emailid: NSString)->Bool
    {
        var isValid = true
        
        var isTrimmed = true
        
        var emailId = emailid
        
        while(isTrimmed)
        {
            
            var startSpace = emailId.substringWithRange(NSRange(location: 0, length: 1))
            
            var endSpace = emailId.substringWithRange(NSRange(location: emailId.length-1, length: 1))
            
            if startSpace == " " || endSpace == " "
            {
                if startSpace == " "
                {
                    emailId = emailId.stringByReplacingCharactersInRange(NSRange(location: 0, length: 1), withString: "")
                }
                if endSpace == " "
                {
                    emailId = emailId.stringByReplacingCharactersInRange(NSRange(location: emailId.length-1, length: 1), withString: "")
                }
            }
            else
            {
                isTrimmed = false
            }
        }

        
        if !emailId.containsString(" ")
        {
            var atRateSplitArray = emailId.componentsSeparatedByString("@")
            
            if(atRateSplitArray.count>=2)
            {
                for component in atRateSplitArray
                {
                    if component as! String == ""
                    {
                        isValid = false
                    }
                }
                
                if(isValid)
                {
                    var dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                    
                    if(dotSplitArray.count>=2)
                    {
                        for component in dotSplitArray
                        {
                            if component as! String == ""
                            {
                                isValid = false
                            }
                        }
                    }
                    else
                    {
                        isValid = false
                    }
                }
            }
            else
            {
                isValid = false
            }
            
        }
        else
        {
            isValid = false
        }
        
        return isValid
    }
    
    func textFieldPadding()
    {
        let paddingViewForName = UIView(frame: CGRectMake(0, 0, 15, self.fullNameTextField.frame.height))
        fullNameTextField.leftView = paddingViewForName
        fullNameTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewForEmail = UIView(frame: CGRectMake(0, 0, 15, self.emailTextField.frame.height))
        emailTextField.leftView = paddingViewForEmail
        emailTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewForPassword = UIView(frame: CGRectMake(0, 0, 15, self.passwordTextField.frame.height))
        passwordTextField.leftView = paddingViewForPassword
        passwordTextField.leftViewMode = UITextFieldViewMode.Always
    }
    
    
    func navigateToNextScreen(userName: String) {
        countMyEvents()
    }
    
    func countMyEvents() {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            
            let myEventsPredicate = NSPredicate(format: "eventCreatorObjectId = '\(currentUserId)'")
            
            var myEventsQuery = PFQuery(className:"Events", predicate: myEventsPredicate)
            
            myEventsQuery.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(myEventsQuery, target: self, successSelector: "fetchMyEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchMyEventsError:", errorSelectorParameters:nil)
        }
    }
    
    func fetchMyEventsSuccess(timer:NSTimer) {
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if (objects!.count > 0) {
            
            self.loaderView.hidden = true
            self.wakeUpView.hidden = true
            
            var myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            self.navigationController?.pushViewController(myEventsVC, animated: false)
        } else {
            countMySharedEvents()
        }
    }
    
    func fetchMyEventsError(timer:NSTimer) {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        println("Unable to fetch myEvents count. What to do here ?")
        checkOfflineData()
    }
    
    func countMySharedEvents() {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            let mySharedEventsPredicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
            
            var mySharedEventsQuery = PFQuery(className:"Invitations", predicate: mySharedEventsPredicate)
            
            mySharedEventsQuery.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(mySharedEventsQuery, target: self, successSelector: "fetchMySharedEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchMySharedEventsError:", errorSelectorParameters:nil)
        }
    }
    
    func fetchMySharedEventsSuccess(timer:NSTimer) {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        self.loaderView.hidden = true
        self.wakeUpView.hidden = true
        
        if (objects!.count > 0) {
            var mySharedEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
            self.navigationController?.pushViewController(mySharedEventsVC, animated: false)
        } else {
            print("Navigating to OnboardingVC")
            let onboardingVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
            //onboardingVC.firstName = userName
            self.navigationController?.pushViewController(onboardingVC, animated: false)
        }
    }
    
    func fetchMySharedEventsError(timer:NSTimer) {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        println("Unable to fetch mySharedEvents count. What to do here ?")
        checkOfflineData()
    }
    
    
    func checkOfflineData()
    {
        self.loaderView.hidden = true
        self.wakeUpView.hidden = true
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            var resultSetEventCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "eventCreatorObjectId = ? ", whereFields: [currentUserId])
            
            resultSetEventCount.next()
            
            var totalEventCount = Int(resultSetEventCount.intForColumn("count"))
            resultSetEventCount.close()
            
            if totalEventCount > 0
            {
                var myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                self.navigationController?.pushViewController(myEventsVC, animated: false)
            }
            else
            {
                var resultSetSharedEventCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "objectId IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = ?  )", whereFields: [currentUserId])
                
                resultSetSharedEventCount.next()
                
                var totalSharedEventCount = Int(resultSetSharedEventCount.intForColumn("count"))
                resultSetSharedEventCount.close()
                if totalSharedEventCount > 0 {
                    var mySharedEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
                    self.navigationController?.pushViewController(mySharedEventsVC, animated: false)
                } else {
                    print("Navigating to OnboardingVC")
                    let onboardingVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
                    //onboardingVC.firstName = userName
                    self.navigationController?.pushViewController(onboardingVC, animated: false)
                }
            }
            
        }
    }
    
    
    func updateDeviceToken(userObjectId: String!)
    {
        let installation = PFInstallation.currentInstallation()
        var user: PFUser = PFUser.currentUser()!
        
        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = user["inviteNotification"] as! Bool
        installation["hostActivityNotification"] = user["inviteNotification"] as! Bool
        installation["guestActivityNotification"] = user["inviteNotification"] as! Bool
        
        installation.saveInBackground()
    }
    
    func updateNotificationSettingsWithDeviceToken(user: PFUser!, userObjectId: String!)
    {
        var installation = PFInstallation.currentInstallation()
        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = true
        installation["hostActivityNotification"] = true
        installation["guestActivityNotification"] = true
        
        user["inviteEmail"] = true
        user["hostActivityEmail"] = true
        user["guestActivityEmail"] = true
        user["inviteNotification"] = true
        user["hostActivityNotification"] = true
        user["guestActivityNotification"] = true
        
        installation.saveInBackground()
        user.saveInBackground()
        
    }
    
    
}


