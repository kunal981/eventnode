//
//  LogInWithEmailViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class LogInWithEmailViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordtextField: UITextField!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var wakeUpView: UIImageView!

    var deepLinkEmail = ""
    var deepLinkObjectId = ""
    var eventCreatorObjectId = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        emailTextfield.delegate = self
        passwordtextField.delegate = self
        emailTextfield.layer.cornerRadius = 3
        passwordtextField.layer.cornerRadius = 3
        passwordtextField.secureTextEntry = true
        textFieldpadding()
        loaderSubView.layer.cornerRadius = 5
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        var loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        
        emailTextfield.keyboardType = UIKeyboardType.EmailAddress
        
        if deepLinkEmail == "noemail"
        {
            emailTextfield.text = ""
        }
        else
        {
            emailTextfield.text = deepLinkEmail

        }
        
        
        self.wakeUpView.hidden = true
        self.loaderView.hidden = true
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        emailTextfield.resignFirstResponder()
        passwordtextField.resignFirstResponder()
    }
    
    
    @IBAction func ForgotPasswordButton(sender: AnyObject)
    {
        let forgotPasswordView = self.storyboard!.instantiateViewControllerWithIdentifier("forgotpasswordView") as! forgotPasswordViewController
        self.navigationController?.pushViewController(forgotPasswordView, animated: false)
    }
    
    
    @IBAction func logInWithEmail(sender: AnyObject)
    {
        
        
        if emailTextfield.text == "" || passwordtextField.text == nil
        {
            var alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Please Fill The Require Fields"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
            
        }
            
        else
        {
            
            self.wakeUpView.hidden = true
            self.loaderView.hidden = false
            
            
            PFUser.logInWithUsernameInBackground(emailTextfield.text, password:passwordtextField.text)
                {
                    (user: PFUser?, error: NSError?) -> Void in
                    if let user = user
                    {
                        
                        hasPassword = true
                        
                        println("user found")
                        self.loaderView.hidden = true
                        self.wakeUpView.hidden = true
                        
                        
                        let pwd = self.passwordtextField.text
                        let userName = user["fullUserName"] as! String
                        println(user["fullUserName"])
                        let email = self.emailTextfield.text as String
                        
                        
                        NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                        
                        
                        NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
                        NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                        NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isNormalLogin")
                        NSUserDefaults.standardUserDefaults().setObject(pwd, forKey: "password")
                        NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                        
                        AnalyticsModel.instance.identifyExistingUserForAnalytics(user.email!, isFbUser: false, name: (user["fullUserName"] as! String), user: user)
                        SupportModel.instance.initializeWithUsernameAndEmail((user["fullUserName"] as! String), email: user.email!)
                        
                        let s3BucketName = "eventnodepublicpics"
                        let fileName = "profilePic.png"
                        
                        let downloadFilePath = documentDirectory.stringByAppendingPathComponent(fileName)
                        let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                        
                        let downloadRequest = AWSS3TransferManagerDownloadRequest()
                        downloadRequest.bucket = s3BucketName
                        println("\(user.objectId!)/profilePic/profilePic.png")
                        downloadRequest.key  = "\(user.objectId!)/profilePic/profilePic.png"
                        downloadRequest.downloadingFileURL = downloadingFileURL
                        
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        
                        
                        transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                            
                            if (task.error != nil){
                                if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                                    switch (task.error.code) {
                                    case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                        break;
                                    case AWSS3TransferManagerErrorType.Paused.rawValue:
                                        break;
                                        
                                    default:
                                        println("error downloading")
                                        break;
                                    }
                                } else {
                                    // Unknown error.
                                    println("error downloading")
                                }
                            }
                            
                            if (task.result != nil) {
                                println("downloading successfull")
                                
                            }
                            
                            return nil
                            
                        })
                        
                        
                        // TODO(geetikak): This will always be a non FB user. Correct ?
                        isFacebookLogin = user["isFacebookLogin"] as! Bool
                        
                        self.updateDeviceToken(user.objectId!)
                        
                        if self.deepLinkEmail != ""
                        {
                            let SharedVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
                            
                            SharedVC.deepLinkEmail = self.deepLinkEmail
                            SharedVC.deepLinkObjectId = self.deepLinkObjectId
                            SharedVC.eventCreatorObjectId = self.eventCreatorObjectId
                            
                            self.navigationController?.pushViewController(SharedVC, animated: true)
                            
                        }
                        else
                        {
                            
                            self.navigateToNextScreen(user["fullUserName"] as! String)
                            
                            /*let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
                            self.navigationController?.pushViewController(eventVC, animated: false)*/
                            
                        }
                        
                        println("\(PFUser.currentUser())")
                        
                    }
                        
                        
                    else
                    {
                        self.loaderView.hidden = true
                        self.wakeUpView.hidden = true
                        println("logIn failed\(error!.code)")
                        println("logIn failed\(error!.localizedDescription)")
                        var alert1 = UIAlertView()
                        alert1.title = "Alert"
                        
                        if error!.code == 101
                        {
                            alert1.message = "Incorrect Username or Password. Please try again."
                        }
                        else
                        {
                            alert1.message = "Something went wrong. Please try again later."
                        }
                        
                        //alert1.message = "\(error!.localizedDescription)"
                        alert1.addButtonWithTitle("Ok")
                        alert1.show()
                        
                    }
            }
        }
    }
    

    func navigateToNextScreen(userName: String) {
        countMyEvents()
    }
    
    func countMyEvents() {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            
            let myEventsPredicate = NSPredicate(format: "eventCreatorObjectId = '\(currentUserId)'")
            
            var myEventsQuery = PFQuery(className:"Events", predicate: myEventsPredicate)
            
            myEventsQuery.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(myEventsQuery, target: self, successSelector: "fetchMyEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchMyEventsError:", errorSelectorParameters:nil)
        }
    }
    
    func fetchMyEventsSuccess(timer:NSTimer) {
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if (objects!.count > 0) {
            
            self.loaderView.hidden = true
            self.wakeUpView.hidden = true
            
            var myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            self.navigationController?.pushViewController(myEventsVC, animated: false)
        } else {
            countMySharedEvents()
        }
    }
    
    func fetchMyEventsError(timer:NSTimer) {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        println("Unable to fetch myEvents count. What to do here ?")
        checkOfflineData()
    }
    
    func countMySharedEvents() {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            let mySharedEventsPredicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
            
            var mySharedEventsQuery = PFQuery(className:"Invitations", predicate: mySharedEventsPredicate)
            
            mySharedEventsQuery.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(mySharedEventsQuery, target: self, successSelector: "fetchMySharedEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchMySharedEventsError:", errorSelectorParameters:nil)
        }
    }
    
    func fetchMySharedEventsSuccess(timer:NSTimer) {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        self.loaderView.hidden = true
        self.wakeUpView.hidden = true
        
        if (objects!.count > 0) {
            var mySharedEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
            self.navigationController?.pushViewController(mySharedEventsVC, animated: false)
        } else {
            print("Navigating to OnboardingVC")
            let onboardingVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
            //onboardingVC.firstName = userName
            self.navigationController?.pushViewController(onboardingVC, animated: false)
        }
    }
    
    func fetchMySharedEventsError(timer:NSTimer) {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        println("Unable to fetch mySharedEvents count. What to do here ?")
        checkOfflineData()
    }
    
    
    func checkOfflineData()
    {
        self.loaderView.hidden = true
        self.wakeUpView.hidden = true
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            var resultSetEventCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "eventCreatorObjectId = ? ", whereFields: [currentUserId])
            
            resultSetEventCount.next()
            
            var totalEventCount = Int(resultSetEventCount.intForColumn("count"))
            resultSetEventCount.close()
            
            if totalEventCount > 0
            {
                var myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                self.navigationController?.pushViewController(myEventsVC, animated: false)
            }
            else
            {
                var resultSetSharedEventCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "objectId IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = ?  )", whereFields: [currentUserId])
                
                resultSetSharedEventCount.next()
                
                var totalSharedEventCount = Int(resultSetSharedEventCount.intForColumn("count"))
                resultSetSharedEventCount.close()
                if totalSharedEventCount > 0 {
                    var mySharedEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
                    self.navigationController?.pushViewController(mySharedEventsVC, animated: false)
                } else {
                    print("Navigating to OnboardingVC")
                    let onboardingVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
                    //onboardingVC.firstName = userName
                    self.navigationController?.pushViewController(onboardingVC, animated: false)
                }
            }
            
        }
    }
    
    
    func updateDeviceToken(userObjectId: String!)
    {
        let installation = PFInstallation.currentInstallation()
        var user: PFUser = PFUser.currentUser()!
        
        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = user["inviteNotification"] as! Bool
        installation["hostActivityNotification"] = user["inviteNotification"] as! Bool
        installation["guestActivityNotification"] = user["inviteNotification"] as! Bool
        
        installation.saveInBackground()
    }
    
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func textFieldpadding()
    {
        let paddingViewForEmail = UIView(frame: CGRectMake(0, 0, 15, self.emailTextfield.frame.height))
        emailTextfield.leftView = paddingViewForEmail
        emailTextfield.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewForPassword = UIView(frame: CGRectMake(0, 0, 15, self.passwordtextField.frame.height))
        passwordtextField.leftView = paddingViewForPassword
        passwordtextField.leftViewMode = UITextFieldViewMode.Always
        
    }
    
}
