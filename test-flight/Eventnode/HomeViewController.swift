//
//  HomeViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/23/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MobileCoreServices

class HomeViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

   @IBOutlet var imageToSave: UIImageView!
   @IBOutlet var loaderView : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        indicator.center = view.center
        loaderView.addSubview(indicator)
        indicator.startAnimating()
        //indicator.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func eventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewControllerOne") as! settingViewControllerOne
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }
    
    @IBAction func sharedEventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertViewController") as! AlertViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
    @IBAction func getStartedButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        loaderView.hidden=false
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            //println("Button capture")
            
            var imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
            loaderView.hidden=true
        }
        else
        {
            NSLog("failed")
            loaderView.hidden=true
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {
        NSLog("Success.")
        //imageToSave.image = image
        //sets the selected image to image view
        let data = UIImagePNGRepresentation(image)
        println(data.length)

        let adjustPhotoVC = self.storyboard!.instantiateViewControllerWithIdentifier("AdjustPhotoViewController") as! AdjustPhotoViewController

        adjustPhotoVC.imageData = image
            
            //println(image)
            
        self.navigationController?.pushViewController(adjustPhotoVC, animated: false)

        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        NSLog("picker cancel.")
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
}
