//
//  EventViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/23/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import Foundation
import MobileCoreServices

var currentEvent:PFObject!
var isUpdated:Bool! = true

var myEvents = [PFObject]()

class EventViewController: UIViewController, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    var redirect: Bool! = false
    
    var isRedirected: Bool! = true
    
    @IBOutlet var tableView : UITableView!
    
    @IBOutlet var loaderView : UIView!

    @IBOutlet var loaderSubView : UIView!
    
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var sadSmily: UIImageView!
    
    var loadingMessage = UILabel()
    var currentUserId = ""
    var fullUserName: String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        showInviteCodePopup = false
        
        //println()
        
        //https://bnc.lt/PYyc/7GJIhWwIPn
        /*
        var data = [
            "deepLinkType": "createEvent"
        ]
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            //
            if error == nil
            {
                var eventCreationURL = url! as String
                
                println("urlllllllllll: \(eventCreationURL)")
            }
            
        })
        */
        
        
        /*var isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "1", whereFields: [])
        if isDeleted {
            Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }

        
        var isDeleted1 = ModelManager.instance.deleteTableData("EventImages", whereString: "1", whereFields: [])
        
        if isDeleted1 {
            Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        
        
        var isDeleted2 = ModelManager.instance.deleteTableData("Invitations", whereString: "1", whereFields: [])
        
        if isDeleted2 {
            Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }

        
        var isDeleted3 = ModelManager.instance.deleteTableData("Notifications", whereString: "1", whereFields: [])
        
        if isDeleted3 {
            Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }*/
        
        loaderSubView.addSubview(loadingMessage)
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
            var isExtraEventDataDeleted = ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId IN (SELECT objectId FROM Events WHERE eventCreatorObjectId != '\(self.currentUserId)' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)')) ", whereFields: [])
            if isExtraEventDataDeleted
            {
                isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Events", whereString: "eventCreatorObjectId != '\(self.currentUserId)' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)')", whereFields: [])
                
                if isExtraEventDataDeleted
                {
                    println("Extra data deleted successfully")
                    
                    isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Invitations", whereString: "userObjectId != '\(self.currentUserId)' AND eventObjectId NOT IN (SELECT objectId FROM Events WHERE eventCreatorObjectId = '\(self.currentUserId)')", whereFields: [])
                    
                    if isExtraEventDataDeleted
                    {
                        println("Extra data deleted successfully")
                        
                        isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Notifications", whereString: "receiverId != '\(self.currentUserId)'", whereFields: [])
                        
                    }
                    
                }
            }
        }
        
        tableView.separatorColor = UIColor.clearColor()
        
        sadSmily.frame.size.height = 40*self.view.frame.width/320
        sadSmily.frame.size.width = 40*self.view.frame.width/320
        
        sadSmily.frame.origin.x = (self.view.frame.width/2)-(20*self.view.frame.width/320)
        
        if let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as? String
        {
            self.fullUserName = fullUserName
            
        }
        
        self.view.addSubview(wakeUpImageView)
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        indicator.startAnimating()
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: date)
        let hour = components.hour
        let minute = components.minute
        let day = components.day
        let month = components.month
        let year = components.year

        println("\(hour)h,\(minute)m,\(day)d,\(month)mt,\(year)y")

        // Do any additional setup after loading the view.
        if(redirect == true)
        {
            redirect = false
            let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
            self.navigationController?.pushViewController(eventPhototsVC, animated: false)
        }

        var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("refreshContent"), userInfo: nil, repeats: true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        /*if UIApplication.sharedApplication().applicationState == UIApplicationState.Active
        {
            msg = "active"
        }
        
        if UIApplication.sharedApplication().applicationState == UIApplicationState.Background
        {
            msg = "background"
        }
        
        if UIApplication.sharedApplication().applicationState == UIApplicationState.Inactive
        {
            msg = "inactive"
        }
        
        
        var refreshAlert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)*/
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            if(redirect == true)
            {
                redirect = false
                let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
                self.navigationController?.pushViewController(eventPhototsVC, animated: false)
            }
            else
            {
                
                if(isUpdated == true)
                {
                    isUpdated = false
                    
                    isRedirected = false
                    tableView.estimatedRowHeight=240
                }
                
                deleteData()
                downloadData()
                refreshList()
                
                if (myEvents.count > 0)
                {
                    updateData()
                }
                
            }
        }
    }
    
    // MARK: - loader
    func showLoader(message: String)
    {
        loadingMessage.text = "\(message)"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        loaderView.hidden = false
    }

    func refreshList()
    {

        isEventDataUpDated = false

        /*var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "eventCreatorObjectId = '\(self.currentUserId)'", whereFields: [])

        resultSetCount.next()

        var eventCount = resultSetCount.intForColumn("count")

        resultSetCount.close()


        if(eventCount>0)
        {*/
            var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "eventCreatorObjectId = '\(self.currentUserId)' GROUP BY objectId ORDER BY eventId DESC", whereFields: [])

            myEvents = []

            if (resultSet != nil)
            {
                while resultSet.next() {
                    
                    var userevent = PFObject(className: "Events")
                    
                    userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                    userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                    userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                    var isRSVP = resultSet.stringForColumn("isRSVP")
                    
                    if isRSVP == "0"
                    {
                        userevent["isRSVP"] = false
                    }
                    else
                    {
                        userevent["isRSVP"] = true
                        
                        userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                        userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                        userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                        userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
                        userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                        
                        userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                    }
                    userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                    userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                    
                    userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
                    
                    userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                    userevent["frameX"] = resultSet.doubleForColumn("frameX")
                    userevent["frameY"] = resultSet.doubleForColumn("frameY")
                    userevent["senderName"] = resultSet.stringForColumn("senderName")
                    userevent["socialSharingURL"] = resultSet.stringForColumn("socialSharingURL")
                    
                    userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                    userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                    
                    if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil)
                    {
                        userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                        userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                        userevent.objectId = resultSet.stringForColumn("objectId")
                    }
                    
                    var isPosted = resultSet.stringForColumn("isPosted")
                    
                    if isPosted == "0"
                    {
                        userevent["isPosted"] = false
                    }
                    else
                    {
                        userevent["isPosted"] = true
                    }
                    
                    userevent["isUploading"] = false
                    
                    myEvents.append(userevent)
                    
                    println(userevent["isPosted"]!)
                }
            }
            
            resultSet.close()
            
            self.tableView.reloadData()
            
            if myEvents.count == 0
            {
                blankView.hidden = false
            }
            else
            {
                blankView.hidden = true
            }
            
        /*}
        else
        {
            var query = PFQuery(className:"Events")
            query.whereKey("eventCreatorObjectId", equalTo:"\(currentUserId)")
            query.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllEventsError:", errorSelectorParameters:nil)
            
        }*/
    }
    
    
    func downloadData()
    {
        var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "eventCreatorObjectId = '\(self.currentUserId)' ORDER BY eventId DESC", whereFields: [])
        
        var eventObjectIds: Array<String>
        
        eventObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                eventObjectIds.append(resultSet.stringForColumn("objectId"))
            }
        }
        
        resultSet.close()
        
        var eventObjectIdsString = "','".join(eventObjectIds)
        
        var predicate = NSPredicate()
        
        var eventObjectIdsStringPredicate = ""
        
        if eventObjectIdsString != ""
        {
            eventObjectIdsStringPredicate = "NOT (objectId IN {'\(eventObjectIdsString)'}) AND eventCreatorObjectId = '\(self.currentUserId)'"
        }
        else
        {
            eventObjectIdsStringPredicate = "eventCreatorObjectId = '\(self.currentUserId)'"
        }
        
        println(eventObjectIdsStringPredicate)
        
        predicate = NSPredicate(format: eventObjectIdsStringPredicate)
        
        var query = PFQuery(className:"Events", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllEventsError:", errorSelectorParameters:nil)
    }
    
    
    func updateData()
    {
        let updatePredicate = NSPredicate(format: "eventCreatorObjectId = '\(currentUserId)' AND isUpdated = true")
        
        var updateQuery = PFQuery(className:"Events", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchEventUpdatesSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventUpdatesError:", errorSelectorParameters:nil)
    }
    
    
    func deleteData()
    {
        let updatePredicate = NSPredicate(format: "eventCreatorObjectId = '\(currentUserId)'")
        
        var updateQuery = PFQuery(className:"Events", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchExistingEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchExistingEventsError:", errorSelectorParameters:nil)
    }

    
    func fetchExistingEventsSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects
        {
            
            var i = 0
            
            var existingEventObjectIds: Array<String>
            existingEventObjectIds = []
            
            for eventObject in fetchedobjects
            {
                existingEventObjectIds.append(eventObject.objectId!)
            }
            
            var existingEventObjectIdsString = "','".join(existingEventObjectIds)
            
            
            var whereQuery = ""
            
            if existingEventObjectIdsString != ""
            {
                whereQuery = "eventCreatorObjectId = '\(currentUserId)' AND objectId NOT IN ('\(existingEventObjectIdsString)') AND objectId != '' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(currentUserId)')"
            }
            else
            {
                whereQuery = "eventCreatorObjectId = '\(currentUserId)' AND objectId != '' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(currentUserId)')"
            }
            
            var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["objectId"], whereString: whereQuery, whereFields: [])
            
            
            var nonExistingEventObjectIds: Array<String>
            nonExistingEventObjectIds = []
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    nonExistingEventObjectIds.append(resultSet.stringForColumn("objectId"))
                }
            }
            
            resultSet.close()
            
            var nonExistingEventObjectIdsString = "','".join(nonExistingEventObjectIds)
            
            if nonExistingEventObjectIdsString != ""
            {
                ModelManager.instance.deleteTableData("Events", whereString: "objectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("Invitations", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("PostLikes", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventComments", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                refreshList()
            }
        }
    }
    
    
    func fetchExistingEventsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func fetchEventUpdatesSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            for eventObject in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                tblFields["eventImage"] = eventObject["eventImage"] as? String
                tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                
                var frameX = eventObject["frameX"] as! CGFloat
                var frameY = eventObject["frameY"] as! CGFloat
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                tblFields["senderName"] = eventObject["senderName"] as? String
                
                tblFields["eventFolder"] = eventObject["eventFolder"] as? String
                
                if(eventObject["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if eventObject["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                        println(date)
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if eventObject["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                        println(date)
                        tblFields["eventEndDateTime"] = date
                        println(tblFields["eventEndDateTime"])
                    }
                    
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    
                    var eventLatitude = eventObject["eventLatitude"] as! Double
                    var eventLongitude = eventObject["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                }
                
                tblFields["isPosted"] = "1"
                
                tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                
                var date = ""
                
                if eventObject.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                    println(date)
                    tblFields["createdAt"] = date
                }
                
                if eventObject.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                    println(date)
                    tblFields["updatedAt"] = date
                }
                
                var eventTimezoneOffset = eventObject["eventTimezoneOffset"] as! Int
                
                tblFields["eventTimezoneOffset"] = "\(eventTimezoneOffset)"
                
                var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [eventObject.objectId!])
                
                fetchedobjects[i]["isUpdated"] = false
                i++
                
            }
            
            refreshList()
            
            PFObject.saveAllInBackground(fetchedobjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    
                }
                else
                {
                    
                }
            }
            
        }
    }
    
    func fetchEventUpdatesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    func refreshContent()
    {
        if isEventDataUpDated
        {
            refreshList()
        }
    }
    
    func stringToDate(dateString: String)->NSDate
    {
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        var date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return myEvents.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        
        return tableView.frame.width*(3/4)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("MyEventsTableViewCell", forIndexPath: indexPath) as! MyEventsTableViewCell
        
        let row = indexPath.row
        
        for view in cell.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        var eventTitleText: String = (myEvents[row]["eventTitle"] as? String)!

        eventTitleText = prefix(eventTitleText, 1).capitalizedString + suffix(eventTitleText, count(eventTitleText) - 1)
        
        var eventImageView = UIImageView()
        
        eventImageView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height)
        
        cell.contentView.addSubview(eventImageView)

        var eventImageOverlayView = UIView()

        eventImageOverlayView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height)
        
        eventImageOverlayView.backgroundColor = UIColor.blackColor()
        eventImageOverlayView.alpha = 0.5
        
        cell.contentView.addSubview(eventImageOverlayView)
        
        
        var eventTitleView = UITextView()
        
        eventTitleView.frame = CGRectMake(0.053125*cell.contentView.frame.width,0.5333*cell.contentView.frame.height, 0.665625*cell.contentView.frame.width, 0.126*cell.contentView.frame.height)
        
        
        
        eventTitleView.text = eventTitleText
        
        eventTitleView.frame.size.height = eventTitleView.sizeThatFits(eventTitleView.bounds.size).height
        
        eventTitleView.textColor = UIColor.whiteColor()
        eventTitleView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        eventTitleView.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)
        eventTitleView.editable = false

        cell.contentView.addSubview(eventTitleView)

        if eventTitleView.contentSize.height > eventTitleView.frame.height {
            eventTitleView.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)
        }
        
        var pdate: NSDate!
        
        if myEvents[row]["isRSVP"] as! Bool == true
        {
            var eventStartDate = myEvents[row]["eventStartDateTime"] as! NSDate
            
            var startTimeStamp = Int64(eventStartDate.timeIntervalSince1970)
            var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
            var eventTimezoneOffset = myEvents[row]["eventTimezoneOffset"] as! Int
            
            var timeStampToBeShown = Int64(startTimeStamp-timezoneOffset+eventTimezoneOffset)
            
            pdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))
            
            //pdate = myEvents[row]["eventStartDateTime"] as! NSDate

            println(pdate)
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: pdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour >= 12)
            {
                if(scomponents.hour > 12)
                {
                    shour = scomponents.hour-12
                }
                else
                {
                    shour = 12
                }
                
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            var startDate = "\(monthsArray[smonth-1]) \(sday), \(shour):\(sminute) \(sam)"
            
            var eventDateView = UITextView()
            
            //println("senderName height: \(senderNameView.frame.height)")
            
            eventDateView.frame = CGRectMake(0.053125*cell.contentView.frame.width, eventTitleView.frame.origin.y+eventTitleView.frame.height , 0.665625*cell.contentView.frame.width, 0.11*cell.contentView.frame.height)
            
            eventDateView.text = startDate
            eventDateView.textColor = UIColor.whiteColor()
            eventDateView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
            eventDateView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            eventDateView.editable = false
            
            cell.contentView.addSubview(eventDateView)
        }
        
        
        var eventShareButton = UIButton()
        eventShareButton.frame = CGRectMake(0.78875*cell.contentView.frame.width, 0.5650*cell.contentView.frame.height,0.06*cell.contentView.frame.width+0.15*cell.contentView.frame.width,0.075*cell.contentView.frame.height+0.08333*cell.contentView.frame.height)
        
        eventShareButton.tag = indexPath.row
        
        var eventShareImageView = UIImageView()
        
        eventShareImageView.frame = CGRectMake(0.78875*cell.contentView.frame.width, 0.5650*cell.contentView.frame.height,0.06*cell.contentView.frame.width, 0.075*cell.contentView.frame.height)
        
        eventShareButton.addTarget(self, action: "eventShareButton:", forControlEvents:.TouchUpInside)
        
        
        eventShareImageView.image = UIImage(named:"group_manage.png")
        
        cell.contentView.addSubview(eventShareImageView)
        
        
        var totalCount = 0
        
        var typeofevent = myEvents[row]["isRSVP"] as! Bool
        
        println("type of event \(typeofevent)")
        
        if myEvents[row]["isRSVP"] as! Bool == true
        {
            var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["SUM(noOfChilds) as childs, SUM(noOfAdults) as adults"], whereString: "attendingStatus = 'yes' AND eventObjectId = ? ", whereFields: [myEvents[row].objectId!])
            
            resultSetCount.next()
            
            totalCount = Int(resultSetCount.intForColumn("childs")) + Int(resultSetCount.intForColumn("adults"))
            
            resultSetCount.close()
        }
        else
        {
            var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["count(*) as count"], whereString: "userObjectId != '' AND eventObjectId = ? ", whereFields: [myEvents[row].objectId!])
            
            resultSetCount.next()
            
            totalCount = Int(resultSetCount.intForColumn("count"))
            
            resultSetCount.close()
        }
    
        var eventShareCount = UILabel()
        
        eventShareCount.frame = CGRectMake(0.8625*cell.contentView.frame.width, 0.5650*cell.contentView.frame.height, 0.15*cell.contentView.frame.width, 0.08333*cell.contentView.frame.height)
        
        println(totalCount)
        
        eventShareCount.text = totalCount.description
        eventShareCount.textColor = UIColor.lightGrayColor()
        
        eventShareCount.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        
        cell.contentView.addSubview(eventShareCount)
        
        var eventCellOverlayView = UIView()
        
        eventCellOverlayView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height)
        
        eventCellOverlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        cell.contentView.addSubview(eventCellOverlayView)
        
        var loaderCellView = UIView()
        
        let cellIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderCellView.addSubview(cellIndicator)
        
        loaderCellView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height)
        
        loaderCellView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        
        cellIndicator.frame = CGRectMake((cell.contentView.frame.width/2)-(cell.contentView.frame.width*(25/320)/2), (cell.contentView.frame.height/2)-(cell.contentView.frame.width*(25/320)/2), cell.contentView.frame.width*(25/320), cell.contentView.frame.width*(25/320))
        
        var loadingMessage = UILabel()
        loadingMessage.text = "Downloading..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name:"AvenirNext-Regular", size: 9.0)
        loadingMessage.textAlignment = .Center
        loaderCellView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , cellIndicator.frame.origin.y+(cell.contentView.frame.width*(25/320)+10), cell.contentView.frame.width, 20)
        cellIndicator.startAnimating()
        
        cell.contentView.addSubview(loaderCellView)
        
        var eventImageFile = myEvents[row]["eventImage"] as! String
        
        var eventFolder = myEvents[row]["eventFolder"] as! String
        
        var eventImagePath = "\(documentDirectory)/\(eventImageFile)"
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath(eventImagePath)) {
            var image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
            eventImageView.image = image

            loaderCellView.hidden = true
        }
        else
        {
            let s3BucketName = "eventnodepublicpics"
            let fileName = eventImageFile

            let downloadFilePath = documentDirectory.stringByAppendingPathComponent(fileName)
            let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)

            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest.bucket = s3BucketName
            downloadRequest.key  = "\(eventFolder)\(fileName)"
            downloadRequest.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            
                        
            //transferManager.download(downloadRequest)
           
            /*transferManager.download(downloadRequest).continueWithSuccessBlock({
                (task: AWSTask!) -> AWSTask! in
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
                    eventImageView.image = image
                    loaderCellView.hidden = true
                })
                return nil
            })*/
            
            
            
            transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                
                if (task.error != nil){
                    if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                        switch (task.error.code) {
                        case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                            break;
                        case AWSS3TransferManagerErrorType.Paused.rawValue:
                            break;
                            
                        default:
                            println("error downloading")
                            break;
                        }
                    } else {
                        // Unknown error.
                        println("error downloading")
                    }
                }
                
                if (task.result != nil) {
                    println("downloading successfull")
                    
                    var image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
                    eventImageView.image = image
                    loaderCellView.hidden = true
                    
                }
                
                return nil
                
            })
            
        }
        
        var eventOriginalImageFile = myEvents[row]["originalEventImage"] as! String
        
        var eventOriginalImagePath = "\(documentDirectory)/\(eventOriginalImageFile)"
        let managerOriginal = NSFileManager.defaultManager()
        
        if (managerOriginal.fileExistsAtPath(eventOriginalImagePath) != true)
        {
            let s3OriginalBucketName = "eventnodepublicpics"
            let fileOriginalName = eventOriginalImageFile
            
            let downloadOriginalFilePath = documentDirectory.stringByAppendingPathComponent(fileOriginalName)
            let downloadingOriginalFileURL = NSURL.fileURLWithPath(downloadOriginalFilePath)
            
            let downloadOriginalRequest = AWSS3TransferManagerDownloadRequest()
            downloadOriginalRequest.bucket = s3OriginalBucketName
            downloadOriginalRequest.key  = "\(eventFolder)\(fileOriginalName)"
            downloadOriginalRequest.downloadingFileURL = downloadingOriginalFileURL
            
            let transferOriginalManager = AWSS3TransferManager.defaultS3TransferManager()
            
            
            transferOriginalManager.download(downloadOriginalRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                
                if (task.error != nil){
                    if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                        switch (task.error.code) {
                        case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                            break;
                        case AWSS3TransferManagerErrorType.Paused.rawValue:
                            break;
                            
                        default:
                            println("error downloading")
                            break;
                        }
                    } else {
                        // Unknown error.
                        println("error downloading")
                    }
                }
                
                if (task.result != nil) {
                    println("downloading successfull")

                }
                
                return nil
                
            })

            
        }
        
        if( myEvents[indexPath.row]["isPosted"] as! Bool == false)
        {
            var uploadButton = UIButton()
            
            uploadButton.frame = CGRectMake(30, cell.contentView.frame.height-45, 30, 30)
            
            uploadButton.setImage(UIImage(named: "upload.png"), forState: UIControlState.Normal)
            
            uploadButton.tag = indexPath.row
            uploadButton.addTarget(self, action:"uploadEvent:",forControlEvents: UIControlEvents.TouchUpInside)
            
            var notUploadedButton = UIButton()
            
            notUploadedButton.frame = CGRectMake(75, cell.contentView.frame.height-38, 23, 23)
            
            notUploadedButton.setImage(UIImage(named: "not_uploaded.png"), forState: UIControlState.Normal)
            
            notUploadedButton.tag = indexPath.row
            notUploadedButton.addTarget(self, action:"notUploaded:",forControlEvents: UIControlEvents.TouchUpInside)
            
            
            cell.contentView.addSubview(uploadButton)
            cell.contentView.addSubview(notUploadedButton)
        }
        
        cell.contentView.addSubview(eventShareButton)
        
        return cell
    }

    
    func eventShareButton(sender:UIButton)
    {
        if myEvents[sender.tag]["isRSVP"] as! Bool == true

        {
            let manageVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageGuestViewController") as! ManageGuestViewController
            
            currentEvent = myEvents[sender.tag]
            
            self.navigationController?.pushViewController(manageVC, animated: false)
        }
        else
        {
            let friendsVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageFreindsViewController") as! ManageFreindsViewController
            
            currentEvent = myEvents[sender.tag]
            
            self.navigationController?.pushViewController(friendsVC, animated: false)
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {

        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            var refreshAlert = UIAlertController(title: "Delete Event", message: "This Event will be deleted now. This cannot be undone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction!) in

                self.showLoader("Deleting event")

                var eventToBeDeleted = myEvents[indexPath.row]

                
                if( myEvents[indexPath.row]["isPosted"] as! Bool == false && myEvents[indexPath.row].objectId == nil)
                {
                    var isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "eventId=?", whereFields: [eventToBeDeleted["eventId"]!])
                    if isDeleted
                    {
                        if eventToBeDeleted.objectId != nil
                        {
                            ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId=?", whereFields: [eventToBeDeleted.objectId!])
                        }
                    }
                    
                    myEvents.removeAtIndex(indexPath.row)
                    
                    self.loaderView.hidden = true
                    
                    
                    tableView.reloadData()
                }
                else
                {
                    var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("deleteEventFromParse:"), userInfo: eventToBeDeleted, repeats: false)
                }
            }))
            
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if( myEvents[indexPath.row]["isPosted"] as! Bool == true && myEvents[indexPath.row].objectId != nil)
        {
            let manager = NSFileManager.defaultManager()
            
            var eventImageFile = myEvents[indexPath.row]["eventImage"] as! String
            
            var eventImagePath = "\(documentDirectory)/\(eventImageFile)"
            
            var eventOriginalImageFile = myEvents[indexPath.row]["originalEventImage"] as! String
            
            var eventOriginalImagePath = "\(documentDirectory)/\(eventOriginalImageFile)"
            
            if (manager.fileExistsAtPath(eventOriginalImagePath) && manager.fileExistsAtPath(eventImagePath))
            {
                
                isPostUpdated = true
                        
                myEventData.removeAll()
                        
                currentEvent = myEvents[indexPath.row]
                
                let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
                self.navigationController?.pushViewController(eventPhototsVC, animated: true)
            }
            else
            {
//                var refreshAlert = UIAlertController(title: "Error", message: "Please wait while the images for this event are downloading.", preferredStyle: UIAlertControllerStyle.Alert)
//                
//                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
//                    
//                }))
//                
//                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }

        }
        else
        {
            
        }
    }
    
    func uploadEvent(sender: UIButton)
    {
        self.loaderView.hidden = false
        
        
        if myEvents[sender.tag].objectId != nil
        {
            myEvents[sender.tag]["isNew"] = false
        }
        else
        {
            myEvents[sender.tag]["isNew"] = true
        }
        
        sender.enabled = false
        
        var originalEventLogoFile = myEvents[sender.tag]["originalEventImage"] as! String
        
        var eventLogoFile = myEvents[sender.tag]["eventImage"] as! String
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        var eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(eventLogoFile))
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()

        uploadRequest.bucket = "eventnodepublicpics"
        uploadRequest.key =  "\(currentUserId)/eventProfileImages/\(eventLogoFile)"
        uploadRequest.body = eventLogoFileUrl
        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
        upload(uploadRequest, isOriginal: false, insertedId: myEvents[sender.tag]["eventId"] as! Int, eventToBeUploaded: myEvents[sender.tag])
        
    }
    
    func notUploaded(sender: UIButton)
    {
        self.loaderView.hidden = true
        
        var refreshAlert = UIAlertController(title: "Not Uploaded to Cloud", message: "If you just created this and there’s internet connection, then it’s uploading in the background. If not, try uploading manually.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in

        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    func uploadFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int, postToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            println("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                        println("upload() failed: [\(error)]")
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                    println("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                println("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.createFirstPost(postToBeUploaded, insertedId: insertedId)
                    
                })
            }
            return nil
        }
    }
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int, eventToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                            println("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                        println("upload() failed: [\(error)]")
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                    println("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                println("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    
                    if(isOriginal == true){
                        
                        self.createEvent(eventToBeUploaded, insertedId: insertedId)

                    }
                    else
                    {
                        println("cropped image uploaded. uploading original image now....")
                        
                        
                        var originalEventFile = eventToBeUploaded["originalEventImage"] as! String
                        
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        var originalEventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(originalEventFile))
                        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        

                        uploadRequest.bucket = "eventnodepublicpics"
                        uploadRequest.key =  "\(self.currentUserId)/eventProfileImages/\(originalEventFile)"
                        uploadRequest.body = originalEventLogoFileUrl
                        
                        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead

                        
                        self.upload(uploadRequest, isOriginal: true, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                    }
                    
                })
            }
            return nil
        }
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        var normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int, eventToBeUploaded: PFObject){
        var refreshAlert = UIAlertController(title: "Error", message: "There’s bad or no internet connection. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            self.loaderView.hidden=true
            self.tableView.reloadData()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction!) in
            self.loaderView.hidden=false
            self.upload(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    
    func internetErrorForFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int){
        
    }
    
    func createFirstPost(eventObject:PFObject, insertedId: Int){
        
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createFirstPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createFirstPostError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    
    func createEvent(eventObject:PFObject, insertedId: Int)
    {
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createEventSuccess:", successSelectorParameters: insertedId, errorSelector: "createEventError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    func createFirstPostSuccess(timer:NSTimer)
    {
        self.loaderView.hidden=true
        isPostUpdated = true
        
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        var postId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            println(date)
            tblFields["updatedAt"] = date
        }
        
        println("postId: \(postId)")
        
        var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            println("Record Updated Successfully")
            println("eventImage")

        } else {
            println("Record not Updated Successfully")

        }

    }
    

    func createFirstPostError(timer:NSTimer)
    {
        self.loaderView.hidden = true
    }
    
    
    func createEventSuccess(timer:NSTimer)
    {
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var eventId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            println(date)
            tblFields["updatedAt"] = date
        }
        println(eventId)
        
        var data = [
            "eventObjectId": eventObject.objectId!,
            "emailId": "noemail",
            "eventCreatorId": "\(currentUserId)"
        ]
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                println(url!)
                tblFields["socialSharingURL"] = url! as String
                var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
                
                eventObject["socialSharingURL"] = url! as String
                
                eventObject.saveInBackground()
            }
            
        })
        
        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
        if isUpdated {
            println("Record Updated Successfully")
            println("event")
            
            var localNotification = UILocalNotification()
            
            localNotification.fireDate = NSDate()
            
            localNotification.alertBody = "Congrats you just created an event. Don’t forget to invite your friends & guests."
            
            // localNotification.applicationIconBadgeNumber = 100
            
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            

            
            refreshList()
            
            if eventObject["isNew"] as! Bool == true
            {
                
                var tblFieldsPost: Dictionary! = [String: String]()
                
                var originalFileName = eventObject["originalEventImage"] as? String
                
                var originalImageData = UIImage(named: documentDirectory.stringByAppendingPathComponent(originalFileName!))
                println(originalImageData?.size.height)
                tblFieldsPost["postData"] = originalFileName
                tblFieldsPost["isApproved"] = "0"
                tblFieldsPost["postHeight"] = "\(originalImageData!.size.height)"
                tblFieldsPost["postWidth"] = "\(originalImageData!.size.width)"
                tblFieldsPost["postType"] = "image"
                tblFieldsPost["eventObjectId"] = "\(eventObject.objectId!)"
                tblFieldsPost["eventFolder"] = "\(self.currentUserId)/\(eventObject.objectId!)/"
                
                var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFieldsPost)
                if insertedId>0
                {
                    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                    var originalEventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(originalFileName!))
                    let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                    
                    let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(originalImageData!), 0.5)

                    originaldata.writeToURL(originalEventLogoFileUrl!, atomically: true)
                    uploadRequest.bucket = "eventnode1"
                    uploadRequest.key =  "\(self.currentUserId)/\(eventObject.objectId)/\(originalFileName)"
                    uploadRequest.body = originalEventLogoFileUrl
                    
                    var myFirstPost = PFObject(className:"EventImages")
                    myFirstPost["postData"] = originalFileName
                    myFirstPost["postHeight"] = originalImageData!.size.height
                    myFirstPost["postWidth"] = originalImageData!.size.height
                    myFirstPost["postType"] = "image"
                    myFirstPost["eventObjectId"] = eventObject.objectId!
                    myFirstPost["eventFolder"] = "\(self.currentUserId)/\(eventObject.objectId!)/"
                    
                    
                    self.uploadFirstPost(uploadRequest, insertedId: insertedId, postToBeUploaded: myFirstPost)
                }
                else
                {
                    println("Post not created Successfully.")
                }
            }
            
        } else {
            println("Record not Updated Successfully")
        }
        
        isUpdated = true

        println(eventObject.objectId)
        
        currentEvent = eventObject;
    }
    
    func createEventError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        var errorObject: NSArray = timer.userInfo?.valueForKey("external") as! NSArray
        var eventObject: PFObject = errorObject[0] as! PFObject
        
        var insertedId: Int = errorObject[1] as! Int
        

        println("error occured \(error.description)")
        
        self.loaderView.hidden=true
        
        var refreshAlert = UIAlertController(title: "Error", message: "There’s bad or no internet connection. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            self.refreshList()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction!) in
            self.loaderView.hidden=false
            self.createEvent(eventObject, insertedId: insertedId)
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
    
    func fetchAllEventsSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")

        if var fetchedobjects = objects {

            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for eventObject in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                tblFields["eventImage"] = eventObject["eventImage"] as? String
                tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                
                var frameX = eventObject["frameX"] as! CGFloat
                var frameY = eventObject["frameY"] as! CGFloat
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                tblFields["senderName"] = eventObject["senderName"] as? String
                
                tblFields["eventFolder"] = eventObject["eventFolder"] as? String

                
                if(eventObject["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"

                    var date = ""
                    if eventObject["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                        println(date)
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if eventObject["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                        println(date)
                        tblFields["eventEndDateTime"] = date
                        println(tblFields["eventEndDateTime"])
                    }
                    
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    
                    var eventLatitude = eventObject["eventLatitude"] as! Double
                    var eventLongitude = eventObject["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                    
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                }
                
                tblFields["objectId"] = eventObject.objectId
                tblFields["isPosted"] = "1"
                
                var eventTimezoneOffset = eventObject["eventTimezoneOffset"] as! Int
                
                tblFields["eventTimezoneOffset"] = "\(eventTimezoneOffset)"
                
                tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                
                var date = ""
                
                if eventObject.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                    println(date)
                    tblFields["createdAt"] = date
                }
                
                if eventObject.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                    println(date)
                    tblFields["updatedAt"] = date
                }
                
                var insertedId = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                
                if insertedId>0
                {
                    println("Record inserted at \(insertedId).")
                }
                else
                {
                    self.loaderView.hidden = true
                    println("Error in inserting record.")
                }
                
                fetchedEventObjectIds.append(eventObject.objectId!)
                
                i++
            }
            
            refreshList()
            
            var eventObjectIdsString = "','".join(fetchedEventObjectIds)
            println(eventObjectIdsString)
            if eventObjectIdsString != ""
            {
                println(eventObjectIdsString)
                
                let predicate = NSPredicate(format: "eventObjectId IN {'\(eventObjectIdsString)'}")
                
                var query = PFQuery(className:"EventImages", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
            }

            
        }
    }
    
    func fetchAllEventsError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    

    func fetchAllPostsSuccess(timer:NSTimer)
    {
        
        //self.loaderView.hidden = true
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        isPostUpdated = false
        println("Successfully retrieved \(objects!.count) posts.")
        
        if var fetchedobjects = objects {
            //self.loaderView.hidden=true
            for post in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["postData"] = post["postData"] as? String
                
                tblFields["isApproved"] = "0"
                
                var postHeight = post["postHeight"] as! CGFloat
                var postWidth = post["postWidth"] as! CGFloat
                
                tblFields["postHeight"] = "\(postHeight)"
                tblFields["postWidth"] = "\(postWidth)"
                tblFields["eventObjectId"] = post["eventObjectId"] as? String
                
                tblFields["eventFolder"] = post["eventFolder"] as? String
                
                tblFields["postType"] = post["postType"] as? String
                
                tblFields["objectId"] = post.objectId
                tblFields["isPosted"] = "1"
                tblFields["isRead"] = "0"
                
                var date = ""
                
                if post.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.createdAt)!)
                    println(date)
                    tblFields["createdAt"] = date
                }
                
                if post.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.updatedAt)!)
                    println(date)
                    tblFields["updatedAt"] = date
                }
                var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
            }
            
            self.refreshList()
        }
    }
    
    func fetchAllPostsError(timer:NSTimer)
    {
        //self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func deleteEventFromParse(timer: NSTimer)
    {
        if MyReachability.isConnectedToNetwork()
        {
            var eventToBeDeleted: PFObject = timer.userInfo as! PFObject
            ParseOperations.instance.deleteData(eventToBeDeleted, target: self, successSelector: "deleteEventSuccess:", successSelectorParameters: nil, errorSelector: "deleteEventError:", errorSelectorParameters:nil)
        }
        else
        {
            
            self.loaderView.hidden=true
            
            var refreshAlert = UIAlertController(title: "Error", message: "Event cannot be deleted as you seem to be offline. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    func deleteEventSuccess(timer:NSTimer)
    {
        var eventToBeDeleted: PFObject = timer.userInfo?.valueForKey("internal") as! PFObject
        
        
        var isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "eventId=?", whereFields: [eventToBeDeleted["eventId"]!])
        
        if isDeleted
        {
            if eventToBeDeleted.objectId != nil
            {
                ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId=?", whereFields: [eventToBeDeleted.objectId!])
            }
        }
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(eventToBeDeleted.objectId!)'")
        
        var invitationQuery = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(invitationQuery, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: eventToBeDeleted, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        ModelManager.instance.deleteTableData("Events", whereString: "objectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        ModelManager.instance.deleteTableData("Invitations", whereString: "eventObjectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        ModelManager.instance.deleteTableData("PostLikes", whereString: "eventObjectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        ModelManager.instance.deleteTableData("EventComments", whereString: "eventObjectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        refreshList()
        
        var eventFolder = eventToBeDeleted["eventFolder"] as! String
        var eventFile = eventToBeDeleted["eventImage"] as! String

        self.loaderView.hidden = true
        
        var deleteRequest = AWSS3DeleteObjectRequest()
        
        
        deleteRequest.bucket = "eventnodepublicpics"
        deleteRequest.key = "\(eventFolder)\(eventFile)"
        
        var s3 = AWSS3.defaultS3()

        s3.deleteObject(deleteRequest).continueWithBlock {
            (task: AWSTask!) -> AnyObject! in
            
            if(task.error != nil){

                println("not deleted cropped")
                
            }else{
                
                println("deleted cropped")
                
                var eventOriginalFile = eventToBeDeleted["originalEventImage"] as! String
                
                var deleteRequestOriginal = AWSS3DeleteObjectRequest()
                deleteRequestOriginal.bucket = "eventnodepublicpics"
                deleteRequestOriginal.key = "\(eventFolder)\(eventOriginalFile)"
                
                var s3Original = AWSS3.defaultS3()
                
                s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                    (task: AWSTask!) -> AnyObject! in
                    
                    if(task.error != nil){
                        println("not deleted original")
                    }else{
                        println("deleted original")
                    }
                    return nil
                }
                
            }
            return nil
        }

        var query = PFQuery(className:"EventImages")
        query.whereKey("eventObjectId", equalTo:eventToBeDeleted.objectId!)
        query.orderByDescending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventsAfterDeleteOriginalSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventsAfterDeleteOriginalError:", errorSelectorParameters:nil)
        
        self.refreshList()

    }
    
    func deleteEventError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        var refreshAlert = UIAlertController(title: "Error", message: "Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.loaderView.hidden=true
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
        println("error occured \(error.description)")
    }
    
    func fetchEventsAfterDeleteOriginalSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) posts.")
        if var objects = objects {
            self.loaderView.hidden=true

            for post in objects
            {
                var postType = post["postType"] as! String
                
                if(postType == "image" || postType == "video")
                {
                    
                    var postFolder = post["eventFolder"] as! String
                    var postImage = post["postData"] as! String
                    
                    var deleteRequestOriginal = AWSS3DeleteObjectRequest()
                    deleteRequestOriginal.bucket = "eventnode1"
                    deleteRequestOriginal.key = "\(postFolder)\(postImage)"
                    
                    var s3Original = AWSS3.defaultS3()
                    
                    s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                        (task: AWSTask!) -> AnyObject! in

                        if(task.error != nil){
                            println("not deleted post")
                        }else{
                            println("deleted post")
                        }
                        
                        return nil
                    }
                }
                
            }
        }
    }
    
    func fetchEventsAfterDeleteOriginalError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        self.loaderView.hidden=true
        println("Error: \(error) \(error.userInfo!)")
    }
   
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        var eventToBeDeleted = timer.userInfo?.valueForKey("external") as! PFObject
        
        var fetchedUserEmailIds: Array<String>
        fetchedUserEmailIds = []

        
        var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        var eventTitle = eventToBeDeleted["eventTitle"] as! String
        
        var notificationObjects = [PFObject]()
        
        var notifMessage = ""
        
        if eventToBeDeleted["isRSVP"] as! Bool == true
        {
            notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
        }
        else
        {
            notifMessage = "\(fullUserName) deleted the event, \(eventTitle)"
        }

        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            
            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    var notificationObject = PFObject(className: "Notifications")
                    fetchedUserEmailIds.append(invitation["emailId"] as! String)
                    
                    notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                    notificationObject["notificationImage"] = "profilePic.png"
                    notificationObject["senderId"] = currentUserId
                    notificationObject["receiverId"] = invitation["userObjectId"] as! String
                    notificationObject["notificationActivityMessage"] = notifMessage
                    notificationObject["eventObjectId"] = eventToBeDeleted.objectId!
                    notificationObject["notificationType"] = "eventdeleted"
                  
                    notificationObjects.append(notificationObject)
                }
            }
            
            
            PFObject.deleteAllInBackground(fetchedobjects)
            
            var eventCreatorObjectId = currentUserId
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //var eventTitle = currentEvent["eventTitle"] as! String
            var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            
            
            var createdAt = ""
            
            var fetchedUserObjectIdsString = "','".join(fetchedUserObjectIds)
            
            
            if fetchedUserObjectIdsString != ""
            {
                let predicate = NSPredicate(format: "userObjectId IN {'\(fetchedUserObjectIdsString)'} AND userObjectId != '\(currentUserId)' AND hostActivityNotification = true ")
                
                PFObject.saveAllInBackground(notificationObjects)
                
                
                var eventCreatorId = eventToBeDeleted["eventCreatorObjectId"] as! String
                let data = [
                    "alert" : "\(notifMessage)",
                    "notifType" :  "eventdeleted",
                    "eventObjectId": eventToBeDeleted.objectId!  ,
                    "eventCreatorId" : "\(eventCreatorId)"
                ]
                

                var urlString = String()
                
                Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                    
                    if error == nil
                    {
                        println(url!)
                        
                        urlString = url!
                        
                    }
                    
                    var sendEmailObject = SendEmail()
                    
                    var deleteEmail = DeleteEvent()
                    var emailMessage = ""
                    
                    if eventToBeDeleted["isRSVP"] as! Bool == true
                    {
                        emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "rsvp",url:urlString)
                    }
                    else
                    {
                        emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "online",url:urlString)
                    }
                    
                    
                    for email in fetchedUserEmailIds
                    {
                        sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                    }
                    
                    
                })

                
                let query = PFInstallation.queryWithPredicate(predicate)
                
                let push = PFPush()
                push.setQuery(query)
                push.setData(data)
                push.sendPushInBackground()
            }
        }
    }
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }

    @IBAction func sharedEventButtonClicked(sender : AnyObject){
        
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
        
        /*let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        
        alertVC.haveData = true
        alertVC.alertEventObjectId = "J2Be4nYQA3"
        alertVC.alertNotificationType = "invitation"
        
        self.navigationController?.pushViewController(alertVC, animated: false)*/
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject){
        
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewControllerOne") as! settingViewControllerOne
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }
    
    @IBAction func eventItemButtonClicked(sender : AnyObject){
        
        let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
        self.navigationController?.pushViewController(eventPhototsVC, animated: true)
    }
    
    @IBAction func getStartedButtonClicked(sender : AnyObject){
        // Navigate to EventDetailsViewController to initiate createEvent.
        let eventDetailsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        // TODO(geetikak): What does animated to true do ?
        
        eventDetailsVC.isAfterImage = false
        
        self.navigationController?.pushViewController(eventDetailsVC, animated: true)
    }
}
