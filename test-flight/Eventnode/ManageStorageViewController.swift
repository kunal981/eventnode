//
//  ManageStorageViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class ManageStorageViewController: UIViewController {

    @IBOutlet var priceButton1 : UIButton!
    @IBOutlet var priceButton2 : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        priceButton1.layer.cornerRadius = 5.0
        priceButton1.layer.masksToBounds = true
        priceButton1.layer.borderColor = UIColor( red: 14/255, green: 156/255, blue:226/255, alpha: 0.3 ).CGColor
        priceButton1.layer.borderWidth = 2.0
        
        priceButton2.layer.cornerRadius = 5.0
        priceButton2.layer.masksToBounds = true
        priceButton2.layer.borderColor = UIColor( red: 14/255, green: 156/255, blue:226/255, alpha: 0.3 ).CGColor
        priceButton2.layer.borderWidth = 2.0
        // Do any additional setup after loading the view.
        //14,156,226
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func eventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func sharedEventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertViewController") as! AlertViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }

}
