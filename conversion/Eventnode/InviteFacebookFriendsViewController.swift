//
//  InviteFacebookFriendsViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 7/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class InviteFacebookFriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var eventnodeContactsTable: UITableView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorMessage: UITextView!
    
    var registeredContacts: Array<NSDictionary>!
    var registeredContactsOriginal: Array<NSDictionary>!
    
    var registeredInviteStatus: Array<Bool>!
    var registeredInviteStatusOriginal: Array<Bool>!
    
    var contactFbIds: Array<String> = []
    
    var contactDetails: Array<NSDictionary> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        contactFbIds = []
        contactDetails = []
        
        registeredContacts = []
        registeredInviteStatus = []
        
        registeredContactsOriginal = []
        registeredInviteStatusOriginal = []
        
        
        eventnodeContactsTable.separatorColor = UIColor.clearColor()
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        errorMessage.attributedText = NSAttributedString(string:errorMessage.text, attributes:attributes)
        errorMessage.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        errorMessage.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        errorMessage.textAlignment = .Center
        
        if facebookFriends.count == 0
        {
            errorView.hidden = false
        }
        else
        {
            errorView.hidden = true
            
            for friend in facebookFriends {
                let valueDict = friend as NSDictionary
                var name = valueDict.valueForKey("name") as! String
                let id = valueDict.valueForKey("id") as! String
                
                contactFbIds.append(id)
                //self.contactDetails.append(emailDetail)
            }
            self.reloadContacts()
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func reloadContacts()
    {
        
        //println(currentUserId)
        
        var existingFriends: Array<NSDictionary>!
        
        existingFriends = []
        
        registeredContacts = []
        registeredInviteStatus = []
        
        registeredContactsOriginal = []
        registeredInviteStatusOriginal = []
        
        var query = PFUser.query()
        query?.whereKey("facebookId", containedIn: contactFbIds)
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                print("Successfully retrieved \(objects!.count) contacts.")
                
                if let objects = objects as? [PFUser] {
                    
                    if objects.count == 0
                    {
                        self.errorView.hidden = false
                    }
                    else
                    {
                        self.errorView.hidden = true
                    }
                    
                    for object in objects {
                        if let emailVerified = object["emailVerified"] as? Bool
                        {
                            /*if(emailVerified)
                            {*/
                            var existingEmailId: NSDictionary! = ["userObjectId": object.objectId!, "email": object.email!, "facebookId": object["facebookId"]!]
                            existingFriends.append(existingEmailId)
                            //}
                            print(object.email!)
                            
                        }
                    }
                    
                    for emailDetail in facebookFriends
                    {
                        
                        //println(email)
                        var existingEmailIdDetails: NSDictionary!
                        var emailCount = 0
                        var emailStatus = false
                        for email in existingFriends
                        {
                            if emailDetail["id"] as! String == email["facebookId"] as! String
                            {
                                emailStatus = true
                                
                                existingEmailIdDetails = ["userObjectId": email["userObjectId"] as! String, "email": email["email"] as! String,  "contactName":emailDetail["name"] as! String, "facebookId": emailDetail["id"] as! String, "contactIndex":self.registeredContacts.count]
                                
                                var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["count(*) as count"], whereString: "userObjectId = ? AND eventObjectId = ? ", whereFields: [email["userObjectId"] as! String, currentEvent.objectId as String!])
                                
                                resultSetCount.next()
                                
                                emailCount = Int(resultSetCount.intForColumn("count"))
                                
                                resultSetCount.close()
                                
                                break
                            }
                        }
                        
                        
                        if emailStatus == true
                        {
                            //foundIndex = emailDetail["contactIndex"] as! Int
                            
                            self.registeredContacts.append(existingEmailIdDetails)
                            self.registeredContactsOriginal.append(existingEmailIdDetails)
                            
                            if emailCount>0
                            {
                                self.registeredInviteStatus.append(true)
                                self.registeredInviteStatusOriginal.append(true)
                            }
                            else
                            {
                                self.registeredInviteStatus.append(false)
                                self.registeredInviteStatusOriginal.append(false)
                            }
                            //
                            
                            //existingEmailIndexes.append(foundIndex)
                            
                        }
                        
                        
                        
                    }
                    
                    self.eventnodeContactsTable.reloadData()
                    
                }
            } else {
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return registeredContacts.count
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        return 60
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let row = indexPath.row
        
        var cellIdentifier: String! = ""
        
        cellIdentifier = "FacebookFriendsTableViewCell"
        
        let cell: FacebookFriendsTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? FacebookFriendsTableViewCell
        
        
        tableView.separatorColor = UIColor.clearColor()
        
        
        for view in cell!.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        let imageView = UIImageView()
        imageView.frame = CGRectMake(16, 16, 28, 28)
        
        let fbId = registeredContacts[row]["facebookId"] as! String
        
        print(fbId)
        //var url:NSURL = NSURL(string:"https://graph.facebook.com/\(fbId)/picture?type=square")!
        //var data:NSData = NSData(contentsOfURL: url)!
        
        if let url = NSURL(string: "https://graph.facebook.com/\(fbId)/picture?type=square") {
            if let data = NSData(contentsOfURL: url){
                imageView.contentMode = UIViewContentMode.ScaleAspectFit
                imageView.image = UIImage(data: data)
            }
        }
        
        imageView.layer.masksToBounds = true;
        imageView.layer.cornerRadius = 14
        
        let nameLabel = UILabel()
        nameLabel.frame = CGRectMake(52, 20, self.view.frame.width-140, 20)
        //nameLabel.numberOfLines = 2
        nameLabel.text = registeredContacts[row]["contactName"] as? String
        nameLabel.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        nameLabel.adjustsFontSizeToFitWidth = true
        
        
        let sendButton = UIButton()
        sendButton.frame = CGRectMake(self.view.frame.width-140+60+27, 17, 26, 26)
        
        
        if registeredInviteStatus[row]
        {
            sendButton.setImage(UIImage(named: "check-box.png"), forState: UIControlState.Normal)
        }
        else
        {
            sendButton.setImage(UIImage(named: "checkbox.png"), forState: UIControlState.Normal)
        }
        
        
        sendButton.addTarget(self, action:"sendInvitationToParseUser:",forControlEvents: UIControlEvents.TouchUpInside)
        sendButton.tag = row
        
        
        cell?.contentView.addSubview(imageView)
        cell?.contentView.addSubview(nameLabel)
        //cell?.contentView.addSubview(emailLabel)
        cell?.contentView.addSubview(sendButton)
        
        //check-round-grey.png
        //check-box.png
        
        //cell?.textLabel!.text = registeredContacts[row]["contactName"] as? String
        
        cell?.selectionStyle = .None
        
        return cell!
        
    }
    
    func sendInvitationToParseUser(sender: UIButton)
    {
        print("parse")
        sender.enabled = false
        if !registeredInviteStatus[sender.tag]
        {
            let inviteObject: PFObject = PFObject(className: "Invitations")
            
            let userObjectId = registeredContacts[sender.tag]["userObjectId"] as! String
            let email = registeredContacts[sender.tag]["email"] as! String
            
            
            inviteObject["invitedName"] = registeredContacts[sender.tag]["contactName"] as? String
            inviteObject["eventObjectId"] = currentEvent.objectId!
            inviteObject["isApproved"] = true
            inviteObject["userObjectId"] = userObjectId
            inviteObject["emailId"] = email
            inviteObject["attendingStatus"] = ""
            inviteObject["invitationType"] = "facebook"
            
            inviteObject["isUpdated"] = false
            inviteObject["noOfChilds"] = 0
            inviteObject["noOfAdults"] = 0
            inviteObject["invitationNote"] = ""
            inviteObject["isEventUpdated"] = false
            
            inviteObject["isEventStreamUpdated"] = false
            inviteObject["isTextUpdated"] = false
            
            var suppliedParameters: Dictionary<String, String>! = Dictionary()
            
            suppliedParameters["invitationType"] = "parse"
            suppliedParameters["invitationNo"] = "\(sender.tag)"
            
            
            ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "createInvitationSuccess:", successSelectorParameters: suppliedParameters, errorSelector: "createInvitationError:", errorSelectorParameters:"parse")
        }
    }
    
    func createInvitationSuccess(timer: NSTimer)
    {
        let invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var recievedParameters: Dictionary<String,String> = (timer.userInfo?.valueForKey("external") as? Dictionary)!
        
        let type = recievedParameters["invitationType"] as String!
        
        let invitationNo = recievedParameters["invitationNo"] as String!
        
        print(type)
        
        let email = invitation["emailId"] as! String
        
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = invitation.objectId!
        tblFields["invitedName"] = invitation["invitedName"] as? String
        tblFields["userObjectId"] = invitation["userObjectId"] as? String
        tblFields["attendingStatus"] = ""
        tblFields["invitationType"] = "facebook"
        tblFields["needsContentApprovel"] = "0"
        
        var date = ""
        
        if invitation.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((invitation.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
        }
        
        if invitation.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((invitation.updatedAt)!)
            print(date)
            tblFields["updatedAt"] = date
        }
        
        tblFields["isPosted"] = "1"
        tblFields["emailId"] = "\(email)"
        tblFields["eventObjectId"] = "\(currentEvent.objectId!)"
        
        tblFields["isApproved"] = "1"
        
        tblFields["isUpdated"] = "1"
        tblFields["noOfChilds"] = "0"
        tblFields["noOfAdults"] = "0"
        tblFields["invitationNote"] = ""
        tblFields["isEventUpdated"] = "0"
        
        
        
        var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
        
        //if insertedId>0
        
        let inviteCode = currentEvent.objectId!
        let eventTitle = currentEvent["eventTitle"] as! String
        var dateString = ""
        var timeString = ""
        var locationString = ""
        let hostName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        var eventType = "online"
        
        if currentEvent["isRSVP"] as! Bool
        {
            
            let sdate = currentEvent["eventStartDateTime"] as! NSDate
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            let sweekday = scomponents.weekday
            
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            
            eventType = "rsvp"
            
            dateString = "\(weekDaysArray[sweekday-1]) \(monthsArray[smonth-1]) \(sday), \(syear)"
            timeString = "\(shour):\(sminute) \(sam)"
            
            print(sweekday)
            
            locationString = currentEvent["eventLocation"] as! String!
        }
        
        var message = ""
        
        
        var inviteUserEmail = InviteEmailToUser()
        
        let eventFolder = currentEvent["eventFolder"] as! String
        let eventImage = currentEvent["eventImage"] as! String
        
        let userObjectId = invitation["userObjectId"] as! String
        
        var notifMessage = ""
        let currentUserId =  NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        
        
        if eventType == "rsvp"
        {
            notifMessage = "\(hostName) invited you to the event, \(eventTitle). Please respond to the invitation."
        }
        else
        {
            notifMessage = "\(hostName) shared the event, \(eventTitle) with you. Check it out."
        }
        
        let invitedName = invitation["invitedName"] as! String
        let isApproved = invitation["isApproved"] as! Bool
        //var userObjectId = invitation["userObjectId"] as! String
        let emailId = invitation["emailId"] as! String
        let attendingStatus = invitation["attendingStatus"] as! String
        let invitationType = invitation["invitationType"] as! String
        let isUpdated = invitation["isUpdated"] as! Bool
        let noOfChilds = invitation["noOfChilds"] as! Int
        let noOfAdults = invitation["noOfAdults"] as! Int
        let invitationNote = invitation["invitationNote"] as! String
        let isEventUpdated = invitation["isEventUpdated"] as! Bool
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let createdAt = dateFormatter.stringFromDate((invitation.createdAt)!)
        
        let updatedAt = dateFormatter.stringFromDate((invitation.updatedAt)!)
        
        let eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        
        let data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "invitation",
            "objectId" :  invitation.objectId!,
            "eventObjectId": currentEvent.objectId!,
            "invitedName" : "\(invitedName)",
            "isUpdated" : "\(isUpdated)",
            "isEventUpdated": "\(isEventUpdated)",
            "isApproved": "\(isApproved)",
            "userObjectId" : "\(userObjectId)",
            "emailId": "\(emailId)",
            "attendingStatus" : "\(attendingStatus)",
            "invitationType" : "\(invitationType)",
            "noOfChilds": "\(noOfChilds)",
            "noOfAdults": "\(noOfAdults)",
            "invitationNote": "\(invitationNote)",
            "createdAt": "\(createdAt)",
            "updatedAt": "\(updatedAt)",
            "eventCreatorId" : "\(eventCreatorId)"
        ]
        
        var urlString = String()
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                print(url!)
                
                urlString = url!
                
                if eventType == "rsvp"
                {
                    
                    let eventLatitude = currentEvent["eventLatitude"] as! Double
                    let eventLongitude = currentEvent["eventLongitude"] as! Double
                    
                    let inviteUserEmail = InPerson()
                    message = inviteUserEmail.emailMessage(inviteCode, eventTitle: eventTitle, dateString: dateString, timeString: timeString, locationString: locationString, hostName: hostName, type: "rsvp", latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                    
                }
                else
                {
                    let inviteUserEmail = OnlineOnlyInviteEmailGuest()
                    
                    message = inviteUserEmail.emailMessage(inviteCode, eventTitle:eventTitle, hostName:hostName, type:"online", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                }
                
                
                let sendNonInvitationEmailsObject = SendNonInvitationEmails()
                
                sendNonInvitationEmailsObject.sendEmail("\(hostName) invited you to the event, \(eventTitle)", message: message, emails: [email])
                
            }
            
        })
        
        
        //message = inviteUserEmail.emailMessage(inviteCode, eventTitle: eventTitle, dateString: dateString, timeString: timeString, locationString: locationString, hostName: hostName, type: eventType)
        
        
        
        if Int(invitationNo)! >= 0
        {
            registeredInviteStatus[Int(invitationNo)!] = true
            registeredInviteStatusOriginal[registeredContacts[Int(invitationNo)!]["contactIndex"] as! Int] = true
        }
        eventnodeContactsTable.reloadData()
        
        
        
        
        
        let notificationObject = PFObject(className: "Notifications")
        notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
        notificationObject["notificationImage"] = "profilePic.png"
        notificationObject["senderId"] = currentUserId
        notificationObject["receiverId"] = userObjectId
        notificationObject["notificationActivityMessage"] = notifMessage
        notificationObject["eventObjectId"] = currentEvent.objectId!
        notificationObject["notificationType"] = "invitation"
        
        let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)' AND inviteNotification = true ")
        
        
        notificationObject.saveInBackground()
        
        let query = PFInstallation.queryWithPredicate(predicate)
        
        let push = PFPush()
        push.setQuery(query)
        push.setData(data)
        push.sendPushInBackground()
        
        
        //let message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!
        
        //println("text: \(text2)")
        
        
        /*var sns = AWSSES.defaultSES()
        
        var messageBody = AWSSESContent()
        var subject = AWSSESContent()
        var body = AWSSESBody()
        
        
        
        subject.data = "\(hostName) invited you to the event, \(eventTitle)"
        
        //messageBody.data = "\(senderName) invited you to the event, \(eventTitle). Your invitation code is \(invitation.objectId!)"
        
        messageBody.data = message
        
        //body.text = messageBody
        body.html = messageBody
        
        var theMessage = AWSSESMessage()
        theMessage.subject = subject
        
        theMessage.body = body
        
        //email = "dimpal1990@gmail.com"
        
        var destination = AWSSESDestination()
        destination.toAddresses = [email]
        
        var send = AWSSESSendEmailRequest()
        send.source = "noreply@eventnode.co"
        send.destination = destination
        send.message = theMessage
        send.returnPath = "noreply@eventnode.co"
        
        sns.sendEmail(send).continueWithBlock {(task: AnyObject!) -> AWSTask! in
        
        if task.error != nil
        {
        println(task.error.debugDescription)
        }
        else
        {
        println("success")
        }
        
        return nil
        }*/
    }
    
    func createInvitationError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
        showLinkfacebookView = false
        
        self.navigationController?.popViewControllerAnimated(true)
        
        //let linkedAccounts = self.storyboard!.instantiateViewControllerWithIdentifier("LinkedAccounts") as! LinkedAccountsViewController
        //self.navigationController?.pushViewController(linkedAccounts, animated: false)
        
    }
    
}
/*

2015-07-28 23:48:55.236 eventnode[9579:475132] requesting access...
Jul 28 23:48:55 Krishnas-MacBook-Pro.local eventnode[9579] <Error>: CGImageCreate: invalid image size: 0 x 0.
2015-07-28 23:49:07.489 eventnode[9579:475207] *** Assertion failure in void _UIPerformResizeOfTextViewForTextContainer(NSLayoutManager *, UIView<NSTextContainerView> *, NSTextContainer *, NSUInteger)(), /SourceCache/UIFoundation_Sim/UIFoundation-376.14/UIFoundation/TextSystem/NSLayoutManager_Private.m:1547
2015-07-28 23:49:07.490 eventnode[9579:475207] <NSATSTypesetter: 0x7f835357a110>: Exception Only run on the main thread! raised during typesetting layout manager <NSLayoutManager: 0x7f835601aef0>
1 containers, text backing has 0 characters
Currently holding 0 glyphs.
Glyph tree contents:  0 characters, 0 glyphs, 1 nodes, 64 node bytes, 0 storage bytes, 64 total bytes, 0.00 bytes per character, 0.00 bytes per glyph
Layout tree contents:  0 characters, 0 glyphs, 0 laid glyphs, 0 laid line fragments, 1 nodes, 64 node bytes, 0 storage bytes, 64 total bytes, 0.00 bytes per character, 0.00 bytes per glyph, 0.00 laid glyphs per laid line fragment, 0.00 bytes per laid line fragment
, glyph range {0 0}. Ignoring...
2015-07-28 23:49:07.490 eventnode[9579:475207] *** Assertion failure in void _UIPerformResizeOfTextViewForTextContainer(NSLayoutManager *, UIView<NSTextContainerView> *, NSTextContainer *, NSUInteger)(), /SourceCache/UIFoundation_Sim/UIFoundation-376.14/UIFoundation/TextSystem/NSLayoutManager_Private.m:1547
2015-07-28 23:49:07.491 eventnode[9579:475207] <NSATSTypesetter: 0x7f835357a110>: Exception Only run on the main thread! raised during typesetting layout manager <NSLayoutManager: 0x7f835601aef0>
1 containers, text backing has 0 characters
Currently holding 0 glyphs.
Glyph tree contents:  0 characters, 0 glyphs, 1 nodes, 64 node bytes, 0 storage bytes, 64 total bytes, 0.00 bytes per character, 0.00 bytes per glyph
Layout tree contents:  0 characters, 0 glyphs, 0 laid glyphs, 0 laid line fragments, 1 nodes, 64 node bytes, 0 storage bytes, 64 total bytes, 0.00 bytes per character, 0.00 bytes per glyph, 0.00 laid glyphs per laid line fragment, 0.00 bytes per laid line fragment
, glyph range {0 0}. Ignoring...
2015-07-28 23:49:07.491 eventnode[9579:475207] *** Assertion failure in void _UIPerformResizeOfTextViewForTextContainer(NSLayoutManager *, UIView<NSTextContainerView> *, NSTextContainer *, NSUInteger)(), /SourceCache/UIFoundation_Sim/UIFoundation-376.14/UIFoundation/TextSystem/NSLayoutManager_Private.m:1547
2015-07-28 23:49:07.497 eventnode[9579:475207] *** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'Only run on the main thread!'
*** First throw call stack:
(
0   CoreFoundation                      0x000000010f52dc65 __exceptionPreprocess + 165
1   libobjc.A.dylib                     0x000000010e92fbb7 objc_exception_throw + 45
2   CoreFoundation                      0x000000010f52daca +[NSException raise:format:arguments:] + 106
3   Foundation                          0x000000010e544a57 -[NSAssertionHandler handleFailureInFunction:file:lineNumber:description:] + 169
4   UIFoundation                        0x0000000116222633 -[NSLayoutManager(NSPrivate) _resizeTextViewForTextContainer:] + 401
5   UIFoundation                        0x000000011622235d -[NSLayoutManager(NSPrivate) _recalculateUsageForTextContainerAtIndex:] + 2424
6   UIFoundation                        0x0000000116259886 -[NSLayoutManager textStorage:edited:range:changeInLength:invalidatedRange:] + 753
7   UIFoundation                        0x000000011627fdfa -[NSTextStorage _notifyEdited:range:changeInLength:invalidatedRange:] + 152
8   UIFoundation                        0x000000011627f932 -[NSTextStorage processEditing] + 367
9   UIFoundation                        0x000000011627f570 -[NSTextStorage endEditing] + 82
10  UIKit                               0x0000000110511eae -[UITextView setAttributedText:] + 229
11  UIKit                               0x00000001105113d9 -[UITextView initWithCoder:] + 574
12  UIKit                               0x0000000110235956 UINibDecoderDecodeObjectForValue + 705
13  UIKit                               0x0000000110235b25 UINibDecoderDecodeObjectForValue + 1168
14  UIKit                               0x000000011023568c -[UINibDecoder decodeObjectForKey:] + 276
15  UIKit                               0x000000010fe6f010 -[UIView initWithCoder:] + 850
16  UIKit                               0x0000000110235956 UINibDecoderDecodeObjectForValue + 705
17  UIKit                               0x000000011023568c -[UINibDecoder decodeObjectForKey:] + 276
18  UIKit                               0x00000001100e3e18 -[UIRuntimeConnection initWithCoder:] + 109
19  UIKit                               0x00000001102f22ba -[UIRuntimeOutletCollectionConnection initWithCoder:] + 47
20  UIKit                               0x0000000110235956 UINibDecoderDecodeObjectForValue + 705
21  UIKit                               0x0000000110235b25 UINibDecoderDecodeObjectForValue + 1168
22  UIKit                               0x000000011023568c -[UINibDecoder decodeObjectForKey:] + 276
23  UIKit                               0x00000001100e32e7 -[UINib instantiateWithOwner:options:] + 990
24  UIKit                               0x000000010ff3b6d8 -[UIViewController _loadViewFromNibNamed:bundle:] + 242
25  UIKit                               0x000000010ff3bcc8 -[UIViewController loadView] + 109
26  UIKit                               0x000000010ff3bf39 -[UIViewController loadViewIfRequired] + 75
27  UIKit                               0x000000010ff6bfdb -[UINavigationController _layoutViewController:] + 44
28  UIKit                               0x000000010ff6c525 -[UINavigationController _updateScrollViewFromViewController:toViewController:] + 216
29  UIKit                               0x000000010ff6c624 -[UINavigationController _startTransition:fromViewController:toViewController:] + 92
30  UIKit                               0x000000010ff6d408 -[UINavigationController _startDeferredTransitionIfNeeded:] + 523
31  UIKit                               0x000000010ff6dece -[UINavigationController __viewWillLayoutSubviews] + 43
32  UIKit                               0x00000001100b86d5 -[UILayoutContainerView layoutSubviews] + 202
33  UIKit                               0x000000010fe8b9eb -[UIView(CALayerDelegate) layoutSublayersOfLayer:] + 536
34  QuartzCore                          0x000000010db2ded2 -[CALayer layoutSublayers] + 146
35  QuartzCore                          0x000000010db226e6 _ZN2CA5Layer16layout_if_neededEPNS_11TransactionE + 380
36  QuartzCore                          0x000000010db22556 _ZN2CA5Layer28layout_and_display_if_neededEPNS_11TransactionE + 24
37  QuartzCore                          0x000000010da8e86e _ZN2CA7Context18commit_transactionEPNS_11TransactionE + 242
38  QuartzCore                          0x000000010da8fa22 _ZN2CA11Transaction6commitEv + 462
39  QuartzCore                          0x000000010da8fc99 _ZN2CA11Transaction14release_threadEPv + 199
40  libsystem_pthread.dylib             0x00000001150c772a _pthread_tsd_cleanup + 86
41  libsystem_pthread.dylib             0x00000001150c7451 _pthread_exit + 117
42  libsystem_pthread.dylib             0x00000001150c66cd _pthread_wqthread + 879
43  libsystem_pthread.dylib             0x00000001150c440d start_wqthread + 13
)
libc++abi.dylib: terminating with uncaught exception of type NSException
(lldb)

*/