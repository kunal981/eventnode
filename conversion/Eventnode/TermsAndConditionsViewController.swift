//
//  TermsAndConditionsViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 5/4/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func logoutButtonClicked(sender : AnyObject){
        self.navigationController?.popToRootViewControllerAnimated(false)
    }
    

}
