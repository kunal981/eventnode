//
//  AboutUsViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        let attributes = [NSParagraphStyleAttributeName : style]
        
        textView.attributedText = NSAttributedString(string: textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func termsAndConditionsButton(sender: AnyObject)
    {
        let termsAndCondition = self.storyboard!.instantiateViewControllerWithIdentifier("TermsAndConditions") as! TermsAndConditionViewController
        self.navigationController?.pushViewController(termsAndCondition, animated: false)
    }
    
    @IBAction func PrivacyPolicybutton(sender: AnyObject)
    {
        let privacyPolicy = self.storyboard!.instantiateViewControllerWithIdentifier("PrivacyPolicy") as!
        PrivacyPolicyViewController
        
        self.navigationController?.pushViewController(privacyPolicy, animated: false)
    }
    
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
