//
//  CustomLoginViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 10/8/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MobileCoreServices

class CustomLoginViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    @IBOutlet weak var imageLoaderView: UIView!
    
    @IBOutlet weak var tAndCText: UITextView!
    
    @IBOutlet weak var facebookLoginButton: UIButton!
    
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var hostName: UILabel!
    @IBOutlet weak var evenTtitle: UILabel!
    var resultdict = NSDictionary()
    var deepLinkEmail = ""
    var deepLinkObjectId = ""
    var eventCreatorObjectId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.view.addSubview(wakeUpImageView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        let loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        loadingMessage.textAlignment = .Center
        loaderSubView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        self.loaderView.hidden = false
        
        NSUserDefaults.standardUserDefaults().setObject("yes", forKey: "isBetaCodeEntered")
        
        let cellIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        imageLoaderView.addSubview(cellIndicator)
        
        imageLoaderView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        
        cellIndicator.frame = CGRectMake((imageLoaderView.frame.width/2)-(imageLoaderView.frame.width*(25/320)/2), (imageLoaderView.frame.height/2)-(imageLoaderView.frame.width*(25/320)/2), imageLoaderView.frame.width*(25/320), imageLoaderView.frame.width*(25/320))
        
        /*var eventImageOverlayView = UIView()
        
        eventImageOverlayView.frame = CGRectMake(0, 0, imageLoaderView.frame.width, imageLoaderView.frame.height)
        
        eventImageOverlayView.backgroundColor = UIColor.blackColor()
        eventImageOverlayView.alpha = 0.5
        
        imageLoaderView.addSubview(eventImageOverlayView)*/
        
        
        let imageLoadingMessage = UILabel()
        imageLoadingMessage.text = "Downloading..."
        imageLoadingMessage.textColor = UIColor.whiteColor()
        imageLoadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        imageLoadingMessage.numberOfLines = 2
        imageLoadingMessage.font = UIFont(name:"AvenirNext-Regular", size: 12.0)
        imageLoadingMessage.textAlignment = .Center
        imageLoaderView.addSubview(imageLoadingMessage)
        imageLoadingMessage.frame = CGRectMake(0 , cellIndicator.frame.origin.y+(imageLoaderView.frame.width*(25/320)+10), imageLoaderView.frame.width, 20)
        cellIndicator.startAnimating()
        
        
        
        //registerWithemail.backgroundColor = UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0)
        
        
        let updatePredicate = NSPredicate(format: "objectId = '\(deepLinkObjectId)'")
        
        let updateQuery = PFQuery(className:"Events", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchEventDetailsSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventDetailsError:", errorSelectorParameters:nil)
        
        // TODO(geetikak): What does isNormalLogin signify ?
        if let isNormalLogin = NSUserDefaults.standardUserDefaults().objectForKey("isNormalLogin") as? String
        {
            var isLoggedIn = "No"
            
            if let loggedInStatus = NSUserDefaults.standardUserDefaults().objectForKey("isLoggedIn") as? String
            {
                isLoggedIn = loggedInStatus
            }
            
            if isNormalLogin == "Yes" && isLoggedIn == "Yes"
            {
                var email = ""
                var password = ""
                
                if let savedEmail = NSUserDefaults.standardUserDefaults().objectForKey("email") as? String
                {
                    email = savedEmail
                }
                
                if let savedPassword = NSUserDefaults.standardUserDefaults().objectForKey("password") as? String
                {
                    password = savedPassword
                }
                
                doNormalLogin(email, password: password)
            }
            else
            {
                // TODO(geetikak): what is the point of calling loginAutomatically ? It takes care of only FB user.
                // but this code path is reachable when isNormalLogin is true and isLoggedIn is false.
                // Is that a scenario that we need to handle ?
                loginAutomaticaly()
            }
        }
        else
        {
            loginAutomaticaly()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchEventDetailsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            for eventObject in fetchedobjects
            {
                
                let senderName = eventObject["senderName"] as! String
                
                hostName.text = "\(senderName) invited you to"
                
                hostName.textColor = UIColor.whiteColor()
                hostName.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
                hostName.textAlignment = .Center
                
                let eventTitle = eventObject["eventTitle"] as! String
                
                evenTtitle.text = eventTitle
                evenTtitle.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
                evenTtitle.textColor = UIColor.whiteColor()
                evenTtitle.textAlignment = .Center
                
                let eventImageFile = eventObject["eventImage"] as! String
                
                let eventFolder = eventObject["eventFolder"] as! String
                
                
                let s3BucketName = "eventnodepublicpics"
                let fileName = eventImageFile
                
                let downloadFilePath = "\(documentDirectory)/\(fileName)"
                
                let managerOriginal = NSFileManager.defaultManager()
                
                if (managerOriginal.fileExistsAtPath(downloadFilePath) != true)
                {
                    let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                    
                    let downloadRequest = AWSS3TransferManagerDownloadRequest()
                    downloadRequest.bucket = s3BucketName
                    downloadRequest.key  = "\(eventFolder)\(fileName)"
                    downloadRequest.downloadingFileURL = downloadingFileURL
                    
                    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                    
                    transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                        
                        if (task.error != nil){
                            if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                                switch (task.error.code) {
                                case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                    break;
                                case AWSS3TransferManagerErrorType.Paused.rawValue:
                                    break;
                                    
                                default:
                                    self.imageLoaderView.hidden = true
                                    print("error downloading")
                                    break;
                                }
                            } else {
                                // Unknown error.
                                self.imageLoaderView.hidden = true
                                print("error downloading")
                            }
                        }
                        
                        if (task.result != nil) {
                            print("downloading successfull")
                            self.imageLoaderView.hidden = true
                            let image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
                            self.eventImageView.image = image
                        }
                        
                        return nil
                        
                    })
                    
                }
                else
                {
                    let image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
                    self.eventImageView.image = image
                    self.imageLoaderView.hidden = true
                }
            }
            
        }
    }
    
    func fetchEventDetailsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    
    func doNormalLogin(email: String, password: String)
    {
        PFUser.logInWithUsernameInBackground(email, password: password)
            {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user
                {
                    
                    hasPassword = true
                    
                    print("user found")
                    
                    print(user["emailVerified"] as! Bool)
                    
                    NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                    
                    
                    NSUserDefaults.standardUserDefaults().setObject(email, forKey: "email")
                    NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                    NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isNormalLogin")
                    NSUserDefaults.standardUserDefaults().setObject(password, forKey: "password")
                    NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                    
                    
                    let s3BucketName = "eventnodepublicpics"
                    let fileName = "profilePic.png"
                    
                    let downloadFilePath = "\(documentDirectory)/\(fileName)"
                    let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                    
                    let downloadRequest = AWSS3TransferManagerDownloadRequest()
                    downloadRequest.bucket = s3BucketName
                    print("\(user.objectId!)/profilePic/profilePic.png")
                    downloadRequest.key  = "\(user.objectId!)/profilePic/profilePic.png"
                    downloadRequest.downloadingFileURL = downloadingFileURL
                    
                    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                    
                    
                    transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                        
                        if (task.error != nil){
                            if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                                switch (task.error.code) {
                                case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                    break;
                                case AWSS3TransferManagerErrorType.Paused.rawValue:
                                    break;
                                    
                                default:
                                    print("error downloading")
                                    break;
                                }
                            } else {
                                // Unknown error.
                                print("error downloading")
                            }
                        }
                        
                        if (task.result != nil) {
                            
                            print("downloading successfull")
                            
                        }
                        
                        return nil
                        
                    })
                    
                    
                    isFacebookLogin = user["isFacebookLogin"] as! Bool
                    
                    // TODO(geetikak): Should we log a user logged in metric here ?
                    
                    self.facebookLoginButton.hidden=false
                    self.tAndCText.hidden=false
                    
                    if isFacebookLogin
                    {
                        self.fetchFacebookFriends()
                    }
                    
                    
                    self.updateDeviceToken(user.objectId!)
                    
                    self.navigateToNextScreen(user["fullUserName"] as! String)
                    
                    print("\(PFUser.currentUser())")
                }
                else
                {
                    
                    //self.wakeUpView.hidden = true
                    self.loaderView.hidden = true
                    
                    self.facebookLoginButton.hidden=false
                    self.tAndCText.hidden=false
                    print("Could not login");
                }
                
        }
        
    }
    
    
    @IBAction func loginWithfacebookButton(sender: AnyObject)
    {
        
        
        
        
        
        let accessToken = FBSDKAccessToken.currentAccessToken() // Use existing access token.
        
        if(accessToken != nil)
        {
            // TODO(geetikak): Understand why there would be an access token at this point ?
            // If we did, we should have been automatically logged in.
            print(accessToken)
            PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken, block: {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user {
                    let user_id = user.objectId!
                    print("\(user_id)")
                    self.loaderView.hidden = true
                    //self.wakeUpView.hidden = true
                    
                    if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
                    {
                        print(currentUserId)
                        
                        isFacebookLogin = true
                        
                        // TODO(geetikak): Confirm that this is always an existing user and cannot be a new FB User Signup.
                        if (user.isNew) {
                            AnalyticsModel.instance.identifyNewUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                        } else {
                            AnalyticsModel.instance.identifyExistingUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                        }
                        //pX2x64NbCa
                        if let password = user["hasPassword"] as? Bool
                        {
                            
                            NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                            NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
                            
                            NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
                            NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                            
                            NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                            NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
                            
                            hasPassword = password
                            
                            
                            self.fetchFacebookFriends()
                            
                            self.updateDeviceToken(user.objectId!)
                            
                            self.navigateToNextScreen(user["fullUserName"] as! String)
                        }
                        else
                        {
                            
                            
                            let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                            
                            let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                            connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                                if error != nil {
                                    print(error)
                                } else
                                {
                                    
                                    self.resultdict = result as! NSDictionary
                                    
                                    let fbId = self.resultdict.valueForKey("id") as! String
                                    print("\(fbId)")
                                    print(self.resultdict)
                                    
                                    
                                    let emailData = self.resultdict.valueForKey("email") as! String
                                    
                                    let usernameData = self.resultdict.valueForKey("first_name") as! String
                                    let lastnameData = self.resultdict.valueForKey("last_name") as! String
                                    
                                    self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user , isSignedUp: false)
                                    
                                }
                            })
                            
                            connection.start()
                            
                        }
                        
                    }
                    else
                    {
                        print("Uh oh. There was an error logging in.")
                        self.openFacebookLogin()
                    }
                    
                } else {
                    print("Uh oh. There was an error logging in.")
                    self.openFacebookLogin()
                }
            })
        }
        else
        {
            self.openFacebookLogin()
        }
        
        
        
    }
    
    func fetchFacebookFriends()
    {
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me/friends?fields=name,email", parameters: nil)
        
        let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
        connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
            if error != nil {
                //handle error
                print(error)
            } else
            {
                //println(result)
                facebookFriends = result.valueForKey("data") as! Array<NSDictionary!>
                print(facebookFriends)
            }
        })
        
        connection.start()
    }
    
    func updateDeviceToken(userObjectId: String!)
    {
        let installation = PFInstallation.currentInstallation()
        let user: PFUser = PFUser.currentUser()!
        
        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = user["inviteNotification"] as! Bool
        installation["hostActivityNotification"] = user["inviteNotification"] as! Bool
        installation["guestActivityNotification"] = user["inviteNotification"] as! Bool
        
        
        
        installation.saveInBackground()
    }
    
    
    func updateNotificationSettingsWithDeviceToken(userObjectId: String!)
    {
        let installation = PFInstallation.currentInstallation()
        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = true
        installation["hostActivityNotification"] = true
        installation["guestActivityNotification"] = true
        
        let user: PFUser = PFUser.currentUser()!
        
        user["inviteEmail"] = true
        user["hostActivityEmail"] = true
        user["guestActivityEmail"] = true
        user["inviteNotification"] = true
        user["hostActivityNotification"] = true
        user["guestActivityNotification"] = true
        
        installation.saveInBackground()
        user.saveInBackground()
        
    }
    
    func navigateToNextScreen(userName: String)
    {
        
        let SharedVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        
        SharedVC.deepLinkEmail = deepLinkEmail
        SharedVC.deepLinkObjectId = deepLinkObjectId
        SharedVC.eventCreatorObjectId = eventCreatorObjectId
        
        self.navigationController?.pushViewController(SharedVC, animated: true)
    }
    
    
    func openFacebookLogin(){
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile", "email", "user_friends"], block: {
            (user: PFUser?, error: NSError?) -> Void in
            
            if let user = user {
                if user.isNew {
                    
                    
                    print("User signed up and logged in through Facebook!")
                    self.updateNotificationSettingsWithDeviceToken(user.objectId!)
                    
                    showInviteCodePopup = true
                    
                    // TODO(geetikak): The next piece of code until connection.start() seems to be same for the else() condition. Refactor and Simplify.
                    let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                    
                    let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                        if error != nil {
                            //handle error
                            print(error)
                        } else
                        {
                            
                            self.resultdict = result as! NSDictionary
                            
                            let fbId = self.resultdict.valueForKey("id") as! String
                            print("\(fbId)")
                            print(self.resultdict)
                            
                            
                            let emailData = self.resultdict.valueForKey("email") as! String
                            
                            let usernameData = self.resultdict.valueForKey("first_name") as! String
                            let lastnameData = self.resultdict.valueForKey("last_name") as! String
                            
                            
                            
                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user, isSignedUp: true )
                            
                            // New FB User SignUp.
                            AnalyticsModel.instance.identifyNewUserForAnalytics(emailData, isFbUser: true, name: usernameData + " " + lastnameData, user: user)
                            
                            
                        }
                    })
                    connection.start()
                }
                else
                {
                    let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                    
                    let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                        if error != nil {
                            print(error)
                        } else
                        {
                            self.resultdict = result as! NSDictionary
                            
                            let fbId = self.resultdict.valueForKey("id") as! String
                            print("\(fbId)")
                            print(self.resultdict)
                            
                            
                            let emailData = self.resultdict.valueForKey("email") as! String
                            
                            let usernameData = self.resultdict.valueForKey("first_name") as! String
                            let lastnameData = self.resultdict.valueForKey("last_name") as! String
                            
                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user, isSignedUp:false)
                            
                            AnalyticsModel.instance.identifyExistingUserForAnalytics(emailData, isFbUser: true, name: usernameData + " " + lastnameData, user: user)
                        }
                    })
                    
                    connection.start()
                }
            }
            else
            {
                
                //self.wakeUpView.hidden = true
                self.loaderView.hidden = true
                print("Uh oh. The user cancelled the Facebook login.")
                NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isLoggedIn")
            }
        })
    }
    
    func checkExistance(email: String, facebookId: String, firstName: String, lastName: String, userObject: PFUser, isSignedUp: Bool )
    {
        print(email)
        let query = PFUser.query()
        query!.whereKey("email", equalTo: email)
        query!.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            print(users!.count)
            if let users = users as? [PFObject]
            {
                print(users)
                if users.count == 0 || self.isValidEmail(userObject.username!)
                {
                    let query = PFQuery(className: "LinkedAccounts")
                    query.whereKey("email", equalTo: email)
                    query.findObjectsInBackgroundWithBlock {
                        (users: [AnyObject]?, error: NSError?) -> Void in
                        
                        if let users = users as? [PFObject]
                        {
                            print(users)
                            if users.count == 0
                            {
                                let fbId = facebookId
                                print("\(fbId)")
                                
                                
                                let emailData = email
                                
                                let usernameData = firstName
                                let lastnameData = lastName
                                
                                self.loaderView.hidden = false
                                let s3BucketName = "eventnodepublicpics"
                                let fileName = "profilePic.png"
                                
                                let downloadFilePath = "\(documentDirectory)/\(fileName)"
                                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                                
                                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                                downloadRequest.bucket = s3BucketName
                                print("\(userObject.objectId!)/profilePic/profilePic.png")
                                downloadRequest.key  = "\(userObject.objectId!)/profilePic/profilePic.png"
                                downloadRequest.downloadingFileURL = downloadingFileURL
                                
                                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                                
                                
                                transferManager.download(downloadRequest).continueWithBlock {
                                    (task: AWSTask!) -> AnyObject! in
                                    
                                    if task.error != nil
                                    {
                                        print("Error downloading")
                                        print(task.error.description)
                                        
                                        let url:NSURL = NSURL(string:"https://graph.facebook.com/\(fbId)/picture?type=square")!
                                        let data:NSData = NSData(contentsOfURL: url)!
                                        
                                        
                                        let fileManager = NSFileManager.defaultManager()
                                        
                                        let filePathToWrite = "\(documentDirectory)/profilePic.png"
                                        
                                        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
                                        
                                        self.uploadProfilePic()
                                        self.loaderView.hidden = true
                                        
                                    }
                                    else
                                    {
                                        self.loaderView.hidden = true
                                        print("successfull")
                                    }
                                    
                                    return nil
                                }
                                
                                let user: PFUser = userObject
                                
                                user["isFacebookLogin"] =  true
                                user["facebookId"] =  fbId
                                
                                if let hasSetPassword = user["hasPassword"] as? Bool
                                {
                                    user["hasPassword"] =  hasSetPassword
                                    hasPassword = hasSetPassword
                                    if !hasPassword
                                    {
                                        user.email = emailData
                                        user.username = emailData
                                    }
                                }
                                else
                                {
                                    user["hasPassword"] =  false
                                    hasPassword = false
                                    user.email = emailData
                                    user.username = emailData
                                }
                                
                                
                                // user["emailVerified"] = true
                                //println("\(isFacebookLogin)")
                                
                                print("\(user.email)")
                                
                                user["fullUserName"] = "\(usernameData) \(lastnameData)"
                                
                                user.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                                    if success
                                    {
                                        if isSignedUp
                                        {
                                            self.updateDeviceToken(user.objectId!)
                                            
                                            let localNotification = UILocalNotification()
                                            
                                            localNotification.fireDate = NSDate()
                                            
                                            localNotification.alertBody = "Welcome to eventnode, we love that you chose us. Don’t wait for a special occasion, everyday can be an event with eventnode."
                                            
                                            // localNotification.applicationIconBadgeNumber = 100
                                            
                                            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                                        }
                                        
                                        self.setFacebookUser(user, usernameData: usernameData, lastnameData: lastnameData, emailData: emailData)
                                        
                                    }
                                    else
                                    {
                                        //self.wakeUpView.hidden = true
                                        self.loaderView.hidden = true
                                        print(error?.localizedDescription)
                                    }
                                })
                            }
                            else
                            {
                                self.deleteUser(userObject, userToLogin: users[0] as! PFUser, usernameData: firstName, lastnameData: lastName, emailData: email)
                            }
                        }
                    }
                    
                    
                }
                else
                {
                    self.deleteUser(userObject, userToLogin: users[0] as! PFUser, usernameData: firstName, lastnameData: lastName, emailData: email)
                }
            }
            else
            {
                // self.wakeUpView.hidden = true
                self.loaderView.hidden = true
            }
            
        }
    }
    
    func isValidEmail(emailid: NSString)->Bool
    {
        
        var isValid = true
        
        if #available(iOS 8.0, *) {
            if !emailid.containsString(" ")
            {
                var atRateSplitArray = emailid.componentsSeparatedByString("@")
                
                if(atRateSplitArray.count>=2)
                {
                    for component in atRateSplitArray
                    {
                        if component == ""
                        {
                            isValid = false
                        }
                    }
                    
                    if(isValid)
                    {
                        let dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                        
                        if(dotSplitArray.count>=2)
                        {
                            for component in dotSplitArray
                            {
                                if component == ""
                                {
                                    isValid = false
                                }
                            }
                        }
                        else
                        {
                            isValid = false
                        }
                    }
                }
                else
                {
                    isValid = false
                }
                
            }
            else
            {
                isValid = false
            }
        } else {
            // Fallback on earlier versions
        }
        
        return isValid
    }
    
    func uploadProfilePic()
    {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        let profilePicUrl = NSURL(fileURLWithPath: "\(documentDirectory)/profilePic.png")
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        
        
        
        
        let pickedImage = UIImage(named: "\(documentDirectory)/profilePic.png")
        
        //profileImageView.image = pickedImage
        
        let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(pickedImage!), 0.5)
        
        var error:NSErrorPointer = NSErrorPointer()
        
        let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        //NSFileManager.defaultManager().removeItemAtPath(documentDirectory.stringByAppendingPathComponent("profilePic.png"), error: error)
        uploadRequest.bucket = "eventnodepublicpics"
        uploadRequest.key =  "\(currentUserId)/profilePic/profilePic.png"
        uploadRequest.body = profilePicUrl
        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
        
        transferManager.upload(uploadRequest)
        
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage
    {
        
        if image.imageOrientation == UIImageOrientation.Up
        {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    func deleteUser(user: PFUser, userToLogin: PFUser, usernameData: String, lastnameData: String, emailData: String)
    {
        print(user)
        
        PFFacebookUtils.unlinkUserInBackground(user, block: { (success:Bool, error:NSError?) -> Void in
            print("deleted")
            
            if #available(iOS 8.0, *)
            {
                
                var refreshAlert = UIAlertController(title: "Error", message: "There's an existing Eventnode account using your facebook email address. Either link your facebook account with that Eventnode account or change the email address in that Eventnode account.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                    self.loaderView.hidden=true
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
                
                
            } else {
                // Fallback on earlier versions
            }
            /*}*/
        });
    }
    
    
    func setFacebookUser(user: PFUser, usernameData: String, lastnameData: String, emailData: String)
    {
        isFacebookLogin = true
        //hasPassword = (user["hasPassword"] as? Bool)!
        
        print("data is updated")
        
        NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
        NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
        NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
        NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
        
        if hasPassword
        {
            NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
            NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setObject(emailData, forKey: "email")
            NSUserDefaults.standardUserDefaults().setObject("\(usernameData) \(lastnameData)", forKey: "fullUserName")
        }
        NSUserDefaults.standardUserDefaults().setObject(emailData, forKey: "facebookEmail")
        NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "facebookName")
        
        self.fetchFacebookFriends()
        
        self.updateDeviceToken(user.objectId!)
        
        self.navigateToNextScreen(user["fullUserName"] as! String)
    }
    
    func loginAutomaticaly()
    {
        
        let accessToken = FBSDKAccessToken.currentAccessToken() // Use existing access token.
        
        if(accessToken != nil)
        {
            print(accessToken)
            PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken, block: {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user {
                    let user_id = user.objectId!
                    print("\(user_id)")
                    
                    if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
                    {
                        print(currentUserId)
                        if let isLoggedIn = NSUserDefaults.standardUserDefaults().objectForKey("isLoggedIn") as? String
                        {
                            print(isLoggedIn)
                            if isLoggedIn == "Yes"
                            {
                                isFacebookLogin = true
                                
                                // TODO(geetikak): Confirm this is always existing user and remove the isNew condition.
                                if (user.isNew) {
                                    AnalyticsModel.instance.identifyNewUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                                } else {
                                    AnalyticsModel.instance.identifyExistingUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                                }
                                
                                if let password = user["hasPassword"] as? Bool
                                {
                                    // Why would FB user have password ? It's a feature. You can turn it on in EventNode Settings.
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                                    NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
                                    NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
                                    NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                                    
                                    hasPassword = password
                                    
                                    self.fetchFacebookFriends()
                                    
                                    self.updateDeviceToken(user.objectId!)
                                    
                                    self.navigateToNextScreen(user["fullUserName"] as! String)
                                }
                                else
                                {
                                    
                                    let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                                    
                                    let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                                        if error != nil {
                                            print(error)
                                        } else
                                        {
                                            
                                            self.resultdict = result as! NSDictionary
                                            
                                            let fbId = self.resultdict.valueForKey("id") as! String
                                            print("\(fbId)")
                                            print(self.resultdict)
                                            
                                            
                                            let emailData = self.resultdict.valueForKey("email") as! String
                                            
                                            let usernameData = self.resultdict.valueForKey("first_name") as! String
                                            let lastnameData = self.resultdict.valueForKey("last_name") as! String
                                            
                                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user ,isSignedUp: false)
                                            
                                        }
                                    })
                                    
                                    connection.start()
                                    
                                }
                                
                            }
                        }
                    }
                    self.facebookLoginButton.hidden=false
                    //self.facebookSignupButton.hidden=false
                    //self.facebookSignupLabel.hidden=false
                    self.tAndCText.hidden=false
                    
                    /*let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(homeVC, animated: true)*/
                    
                } else {
                    print("Uh oh. There was an error logging in.")
                    
                    //self.wakeUpView.hidden = true
                    self.loaderView.hidden = true
                    self.facebookLoginButton.hidden=false
                    //self.facebookSignupButton.hidden=false
                    //self.facebookSignupLabel.hidden=false
                    self.tAndCText.hidden=false
                }
            })
        }
        else
        {
            
            //self.wakeUpView.hidden = true
            self.loaderView.hidden = true
            self.facebookLoginButton.hidden=false
            //facebookSignupButton.hidden=false
            //facebookSignupLabel.hidden=false
            self.tAndCText.hidden=false
        }
        
    }
    
    
    @IBAction func registerButtonClicked(sender: AnyObject)
    {
        let registerView = self.storyboard!.instantiateViewControllerWithIdentifier("RegistrationView") as! RegistrationViewController
        
        registerView.deepLinkEmail = deepLinkEmail
        registerView.deepLinkObjectId = deepLinkObjectId
        registerView.eventCreatorObjectId = deepLinkObjectId
        
        self.navigationController?.pushViewController(registerView, animated: true)
    }
    @IBAction func loginButtonClicked(sender: AnyObject)
    {
        let logInView = self.storyboard!.instantiateViewControllerWithIdentifier("logInWithEmail") as! LogInWithEmailViewController
        print("log in button pressed")
        
        logInView.deepLinkEmail = deepLinkEmail
        logInView.deepLinkObjectId = deepLinkObjectId
        logInView.eventCreatorObjectId = deepLinkObjectId
        
        self.navigationController?.pushViewController(logInView, animated: true)
    }
    @IBAction func termsButtonclicked(sender: AnyObject)
    {
        let termsAndCondition = self.storyboard!.instantiateViewControllerWithIdentifier("TermsAndConditions") as! TermsAndConditionViewController
        self.navigationController?.pushViewController(termsAndCondition, animated: false)
    }
}
