//
//  AnalyticsModel.swift
//  Eventnode
//
//  Created by Chandra Kilaru on 9/7/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation

let model = AnalyticsModel()

class AnalyticsModel: NSObject
{
    let mixPanelToken: String = "9437360151c5da3a9564edd618761ac0"
    let mixpanel: Mixpanel
    
    override init() {
        // Initialize Mixpanel library.
        Mixpanel.sharedInstanceWithToken(mixPanelToken)
        self.mixpanel = Mixpanel.sharedInstance()
    }

    class var instance: AnalyticsModel {
        return model
    }
    
    func logAppLaunchEvent() {
        mixpanel.track("App_launch")
        print("App Delegate MP DistinctId: " + mixpanel.distinctId)
    }
    
    func logNewEventEvent(ownerId: String, isRsvp: Bool) {
        mixpanel.track("New_Event", properties: [
            "isRSVP": isRsvp,
            "eventOwnerId": ownerId
        ])
    }
    
    
    func identifyExistingUserForAnalytics(email: String, isFbUser: Bool, name: String, user: PFUser) {
        print("VC: identifyExistingUserForAnalytics MP DistinctId: " + mixpanel.distinctId)
        print("VC: identifyExistingUserForAnalytics user ObjectId: " + user.objectId!)
        mixpanel.identify(user.objectId)
        let date = NSDate()
        mixpanel.people.set([
            "$email": email,    // only special properties need the $
            "$name": name,    // only special properties need the $
            "$last_login": date, // properties can be dates...
            "isFacebookUser": isFbUser,        // feel free to define your own properties
            "parseUserId": user.objectId as String!
            ]);
        mixpanel.track("Existing_User_Login", properties: [
            "email": email,
            "name": name,
            "last_login": date,
            "isFacebookUser": isFbUser,
            "parseUserId": user.objectId as String!
            ])
    }
    
    func identifyNewUserForAnalytics(email: String, isFbUser: Bool, name: String, user: PFUser) {
        print("VC: identifyNewUserForAnalytics MP DistinctId: " + mixpanel.distinctId)
        print("VC: identifyNewUserForAnalytics user ObjectId: " + user.objectId!)
        mixpanel.createAlias(user.objectId, forDistinctID: mixpanel.distinctId)
        mixpanel.identify(mixpanel.distinctId)
        let date = NSDate()
        mixpanel.people.set([
            "$email": email,    // only special properties need the $
            "$name": name,    // only special properties need the $
            "$created": date,   // properties can be dates...
            "isFacebookUser": isFbUser,        // feel free to define your own properties
            "parseUserId": user.objectId as String!
            ]);
        mixpanel.track("New_User", properties: [
            "email": email,
            "name": name,
            "created": date,
            "isFacebookUser": isFbUser,
            "parseUserId": user.objectId as String!
            ])
    }
}