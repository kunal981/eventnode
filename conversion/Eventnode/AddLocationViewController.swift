//
//  AddLocationViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/29/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AddLocationViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var mapView: MKMapView!
    var imageData: UIImage!
    
    @IBOutlet weak var leftPaddingView: UIView!
    @IBOutlet weak var rightPaddingView: UIView!
    @IBOutlet weak var searchButtonView: UIView!
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var searchText: UITextField!
    
    //var matchingItems: [MKMapItem] = [MKMapItem]()
    //var matchingItems: NSArray!=[]
    
    let regionRadius: CLLocationDistance = 1000
    
    var locationManager = CLLocationManager()
    
    var eventTitle:String!
    
    var newAnnotation = MKPointAnnotation()
    
    var searchedItems: NSArray!=[]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let lat = newEvent["eventLatitude"] as! Double
        let long = newEvent["eventLongitude"] as! Double
        
        if(lat == 0 || long == 0)
        {
            
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
        }
        else
        {
            let latitude = lat as CLLocationDegrees
            let longitude = long as CLLocationDegrees
            
            print(latitude)
            print(longitude)
            
            let newLocation = CLLocation(latitude: latitude, longitude: longitude)
            
            getAddressByLocation(newLocation, isCentered: true)
        }
        
        let uilpgr = UILongPressGestureRecognizer(target: self, action: "action:")
        
        uilpgr.minimumPressDuration = 0.5
        
        mapView.addGestureRecognizer(uilpgr)
        
        // Do any additional setup after loading the view.
        /*let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)*/
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func saveLocation(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func action(gestureRecognizer:UIGestureRecognizer)
    {
        let touchPoint = gestureRecognizer.locationInView(self.mapView)
        let newCoordinate:CLLocationCoordinate2D = mapView.convertPoint(touchPoint, toCoordinateFromView: self.mapView)
        
        self.mapView.removeAnnotation(self.newAnnotation)
        
        let newAnnotation = MKPointAnnotation()
        newAnnotation.coordinate = newCoordinate
        
        self.newAnnotation = newAnnotation
        
        let newLocation:CLLocation = CLLocation(latitude: newCoordinate.latitude, longitude: newCoordinate.longitude)
        
        getAddressByLocation(newLocation,isCentered: false)
        
        print(self.newAnnotation.title)
        
        newEvent["eventLatitude"] = newLocation.coordinate.latitude
        newEvent["eventLongitude"] = newLocation.coordinate.longitude
        
        mapView.addAnnotation(self.newAnnotation)
        
        //self.mapView.addAnnotation(self.newAnnotation)
    }
    
    
    func locationManager(manager:CLLocationManager, didUpdateLocations locations:[CLLocation]){
        let userLocation:CLLocation = locations[0]
        print(userLocation)
        
        newEvent["eventLatitude"] = userLocation.coordinate.latitude
        newEvent["eventLongitude"] = userLocation.coordinate.longitude
        
        //println(userLocation.coordinate.latitude)
        
        self.locationManager.stopUpdatingLocation()
        
        getAddressByLocation(userLocation,isCentered: true)
        
    }
    
    
    func locationManager(manager:CLLocationManager, didFailWithError error:NSError){
        
        print("\(error.localizedDescription)")
        let defaultLocation:CLLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        
        newEvent["eventLatitude"] = defaultLocation.coordinate.latitude
        newEvent["eventLongitude"] = defaultLocation.coordinate.longitude
        
        getAddressByLocation(defaultLocation,isCentered: true)
    }
    
    
    func getAddressByLocation(userLocation: CLLocation, isCentered: Bool)
    {
        
        CLGeocoder().reverseGeocodeLocation(userLocation, completionHandler: {(placemarks, error) in
            if error != nil
            {
                newEvent["eventLocation"] = ""
            }
            else
            {
                
                let p = CLPlacemark(placemark: placemarks![0])
                
                let placeArray = placemarks ?? []
                
                var subThoroughfare:String
                
                if(p.subThoroughfare != nil && p.subThoroughfare != "nil" )
                {
                    subThoroughfare = p.subThoroughfare!
                }
                else
                {
                    subThoroughfare = ""
                }
                
                
                
                var eventLocation:String = "\(subThoroughfare), \(p.thoroughfare), \(p.subLocality), \(p.locality), \(p.subAdministrativeArea), \(p.administrativeArea), \(p.postalCode), \(p.country)"
                
                
                
                
                newEvent["eventLocation"] = eventLocation
                //var eventDescription:String = newEvent["eventDescription"] as! String
                
                //self.mapView.removeAnnotation(self.newAnnotation)
                
                /*
                -(void) mapView: (MKMapView *)
                mapView.didDeflectAnnotationView: (MKNotationView *)
                view{
                [mapView deflectAnnotation:view.Annotartion, Animated false]
                
                }
                */
                
                self.newAnnotation.title = "\(eventLocation)"
                
                
                
                if(isCentered == true)
                {
                    self.newAnnotation.coordinate = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
                    
                    self.mapView.addAnnotation(self.newAnnotation)
                    
                    self.mapView.selectAnnotation(self.newAnnotation, animated: false)
                    
                    self.centerMapOnLocation(userLocation)
                }
                
                
            }
        })
    }
    
    
    @IBAction func textFieldReturn(sender: AnyObject) {
        
        searchText.resignFirstResponder()
        
        self.performSearch()
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        searchText.resignFirstResponder()
        self.performSearch()
        return true
    }
    
    func performSearch() {
        
        //self.searchTable.hidden = false
        
//        let textForSearch = searchText.text
//        print(textForSearch!)
        
        let urlPath = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(searchText.text!)&key=\(googleApiKey)"
        
        
      //  let url:NSURL = NSURL(string: urlPath.stringByReplacingOccurrencesOfString(" ", withString: "%20"))!
        
        print(urlPath)
        
        let url: NSURL = NSURL(string: urlPath.stringByReplacingOccurrencesOfString(" ", withString:"%20", options: NSStringCompareOptions.LiteralSearch, range: nil))!
        
        let session = NSURLSession.sharedSession()
        
        
        let task = session.dataTaskWithURL(url)
            {
            (data:NSData?, response:NSURLResponse?, error:NSError?) in
            
            do
            {
                 let response = (try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    self.searchedItems = response["results"] as! NSArray
                    print(response["results"])
                    
                    self.searchTable.reloadData()
                    
                    if(self.searchedItems.count>0)
                        {
                            self.searchTable.hidden = false
                        }
                    else
                        {
                            self.searchTable.hidden = true
                        }
                });

                
                
            }
            catch let caught as NSError
            {
                 print(error!.localizedDescription)
            } catch
            {
                // Something else happened.
                
            }
        }
        
//        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
//            
//            if error != nil
//            {
//                // If there is an error in the web request, print it to the console
//                print(error!.localizedDescription)
//            }
//            //println(data)
//            
//            
//            var err: NSError?
//            
//            
//            var response = (try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
//            
//            
//            if err != nil
//            {
//                // If there is an error parsing JSON, print it to the console
//                print("JSON Error \(err!.localizedDescription)")
//            }
//            
//            dispatch_async(dispatch_get_main_queue(), {
//                
//                self.searchedItems = response["results"] as! NSArray
//                print(response["results"])
//                
//                self.searchTable.reloadData()
//                
//                if(self.searchedItems.count>0)
//                {
//                    self.searchTable.hidden = false
//                }
//                else
//                {
//                    self.searchTable.hidden = true
//                }
//            });
//            
//        })
        task.resume()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return searchedItems.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MyPlacesSearchCell
        
        let row = indexPath.row
        
        let obj = searchedItems[indexPath.row] as! NSDictionary
        print(obj["formatted_address"])
        let lname = obj["name"] as! NSString
        let laddress = obj["formatted_address"] as! NSString
        
        if laddress.lowercaseString.rangeOfString(lname.lowercaseString) != nil {
            cell.locationTitle.text = "\(laddress)"
        }
        else
        {
            cell.locationTitle.text = "\(lname), \(laddress)"
        }
        
        cell.eventFocusLocation.tag = indexPath.row
        
        return cell
    }
    
    
    
    /*func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
    //getAddressByLocation(searchedLocation,isCentered: true)
    
    
    
    }*/
    
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    @IBAction func focusLocationButtonClicked(sender : AnyObject){
        mapView.removeAnnotations(mapView.annotations)
        self.searchTable.hidden = true
        let searchedItem = searchedItems[sender.tag] as! NSDictionary
        
        let geometry = searchedItem["geometry"] as! NSDictionary
        
        let loc = geometry["location"] as! NSDictionary
        
        let lat = loc["lat"] as! Double
        let lng = loc["lng"] as! Double
        
        let latitude = lat as CLLocationDegrees
        let longitude = lng as CLLocationDegrees
        
        let searchedLocation = CLLocation(latitude: latitude, longitude: longitude)
        
        newEvent["eventLatitude"] = searchedLocation.coordinate.latitude
        newEvent["eventLongitude"] = searchedLocation.coordinate.longitude
        
        let lname = searchedItem["name"] as! NSString
        let laddress = searchedItem["formatted_address"] as! NSString
        
        if laddress.lowercaseString.rangeOfString(lname.lowercaseString) != nil {
            self.newAnnotation.title = "\(laddress)"
            newEvent["eventLocation"] = "\(laddress)"
        }
        else
        {
            self.newAnnotation.title = "\(lname), \(laddress)"
            newEvent["eventLocation"] = "\(lname), \(laddress)"
        }
        
        
        //self.newAnnotation.subtitle = newEvent["eventDescription"] as! String
        
        self.newAnnotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        
        self.mapView.addAnnotation(self.newAnnotation)
        
        self.mapView.selectAnnotation(self.newAnnotation, animated: false)
        
        self.centerMapOnLocation(searchedLocation)
    }
    
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventVC.imageData=imageData
        self.navigationController?.pushViewController(eventVC, animated: true)*/
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    @IBAction func saveButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventVC.imageData=imageData
        self.navigationController?.pushViewController(eventVC, animated: true)*/
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let taps = touches as NSSet
        let touch = taps.allObjects[0] as! UITouch
        let touchLocation = touch.locationInView(self.view)
        
        if (!(CGRectContainsPoint(searchTable.frame, touchLocation) || (CGRectContainsPoint(leftPaddingView.frame, touchLocation)) || CGRectContainsPoint(rightPaddingView.frame, touchLocation) || CGRectContainsPoint(searchText.frame, touchLocation) || CGRectContainsPoint(searchButtonView.frame, touchLocation))) {
            searchText.resignFirstResponder()
            searchTable.hidden = true
        }
        
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
