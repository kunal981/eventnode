//
//  EventSettingsViewController.swift
//  Eventnode
//
//  Created by brst on 7/31/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import Crashlytics

var isLocationUpdated: Bool!
var isDateUpdated:  Bool!


class EventSettingsViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var deleteEvent: UIView!
    
    @IBOutlet weak var showDate: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var editEventTitle: UITextField!
    
    @IBOutlet weak var invitationPreview: UIView!
    @IBOutlet weak var noteFromhost: UIView!
    @IBOutlet weak var location: UIView!
    @IBOutlet weak var dateandTime: UIView!
    
    @IBOutlet var loaderView: UIView!
    @IBOutlet var loaderSubView: UIView!
    @IBOutlet weak var eventTitleTextField: UITextField!
    var loadingMessage = UILabel()
    var startDate = NSString()
    var showPicker = false
    let tapRec = UITapGestureRecognizer()
    var isToBeEdit = false
    var isToBeBack = false
    
    var currentUserId: String!
    
    var datePickerView : UIDatePicker! = UIDatePicker()
    //    var datePickerView  : UIDatePicker! = UIDatePicker(frame(CGRectMake,0, 330,self.scrollView.frame.size.width,230))
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        //var arrayndex = ["1","2","3","4"]
        
        //arrayndex.insert("4", atIndex: 7)
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        isLocationUpdated = false
        isDateUpdated = false
        
        
        self.view.addSubview(wakeUpImageView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        loadingMessage.text = "Saving..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        loadingMessage.textAlignment = .Center
        loaderSubView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        self.loaderView.hidden = true
        
        
        isUpdated = false
        
        eventTitleTextField.delegate = self
        
        print(currentEvent["eventTitle"] as! String)
        eventTitleTextField.text = currentEvent["eventTitle"]  as! String
        
        
        if var eventStartDate = currentEvent["eventStartDateTime"] as? NSDate
        {
            var startTimeStamp = Int64(eventStartDate.timeIntervalSince1970)
            var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
            
            print(currentEvent["eventTimezoneOffset"] as! Int)
            
            var eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
            
            var timeStampToBeShown = Int64(startTimeStamp-timezoneOffset+eventTimezoneOffset)
            
            //october 22 2015, 8:30 pm
            
            var sdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))
            
            showDate.text = getFormatedStringFromDate(sdate)
            
        }
        else
        {
            showDate.text = ""
        }
        
        
        showDate.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
        showDate.textAlignment = .Right
        
        
        //var sdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))
        
        /*let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
        shour = scomponents.hour-12
        sam = "PM"
        }
        else
        {
        shour = scomponents.hour
        sam = "AM"
        if(scomponents.hour==0)
        {
        shour = 12
        }
        }
        
        var sminute = "\(scomponents.minute)"
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        if(scomponents.minute<10)
        {
        sminute="0\(sminute)"
        }
        
        
        var startDate = "\(monthsArray[smonth-1]) \(sday), \(syear) - \(shour):\(sminute) \(sam)"*/
        
        
        let isRsvp: Bool = currentEvent["isRSVP"] as! Bool
        
        if( isRsvp == false )
        {
            
            datePickerView.hidden = true
            noteFromhost.hidden = true
            location.hidden = true
            invitationPreview.hidden = true
            dateandTime.hidden = true
            /* eventDescriptionButton.hidden = true
            eventLocationButton.hidden = true
            eventDateTimeButton.hidden = true
            deleteEventButton.frame = CGRectMake(((self.view.frame.width/2)-(deleteEventButton.frame.width/2)), deleteEventButton.frame.origin.y, deleteEventButton.frame.width, deleteEventButton.frame.height)*/
        }
        else
        {
            
            print(currentEvent["eventStartDateTime"])
            datePickerView  = UIDatePicker(frame: CGRectMake(0,240*(self.view.frame.height/568), self.view.frame.size.width,230*(self.view.frame.height/568)))
            
            let sDate = currentEvent["eventStartDateTime"] as? NSDate
            
            //          showDate.hidden = true
            
            datePickerView.datePickerMode = UIDatePickerMode.DateAndTime
            datePickerView.backgroundColor = UIColor(red: 225/255, green: 241/255, blue: 249/255, alpha: 1.0)
            //datePickerView.backgroundColor = UIColor(red: 233/255, green: 217/255, blue: 133/255, alpha: 1.0)
            datePickerView.setDate(sDate!, animated: true)
            
            datePickerView.minuteInterval = 15
            //          datePickerView.set
            
            datePickerView.addTarget(self, action: Selector("dateChanged:"), forControlEvents: UIControlEvents.ValueChanged)
            
            self.scrollView.addSubview(datePickerView)
            datePickerView.hidden = true
            
            
            datePickerView.minimumDate = NSDate()
            
            
            
            
        }
        
        
        
        tapRec.addTarget(self, action: "tapped")
        scrollView.addGestureRecognizer(tapRec)
        scrollView.userInteractionEnabled = true
        
        /*let button = UIButton()
        button.frame = CGRectMake(20, 50, 100, 30)
        button.setTitle("Crash", forState: UIControlState.Normal)
        button.addTarget(self, action: "crashButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        view.addSubview(button)*/
    }
    
    
    func showLoader(message: String)
    {
        loadingMessage.text = "\(message)"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        loaderView.hidden = false
    }
    
    
    func dateChanged(sender: UIDatePicker)
    {
        let timeFormatter = NSDateFormatter()
        timeFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        startDate = timeFormatter.stringFromDate(datePickerView.date)
        currentEvent["eventStartDateTime"] = datePickerView.date
        
        showDate.text = getFormatedStringFromDate(datePickerView.date)
        showDate.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
        showDate.textAlignment = .Right
        
        isUpdated = true
        isDateUpdated = true
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tapped()
    {
        
        eventTitleTextField.resignFirstResponder()
        
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        eventTitleTextField.resignFirstResponder()
        eventTitleTextField.userInteractionEnabled = false
    }
    
    
    @IBAction func editEventTitle(sender: AnyObject)
    {
        //handler(UIDatePicker())
        
        if isToBeEdit
        {
            eventTitleTextField.userInteractionEnabled = false
            eventTitleTextField.resignFirstResponder()
            isToBeEdit = false
        }
        else
        {
            
            eventTitleTextField.userInteractionEnabled = true
            eventTitleTextField.becomeFirstResponder()
            isToBeEdit = true
            
        }
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        return true
    }
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        currentEvent["eventTitle"] = eventTitleTextField.text!
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool{
        
        print(string)
        
        //\u200B
        var _char = string.cStringUsingEncoding(NSUTF8StringEncoding)
        
        
        
        if(string=="")
        {
            print("sdsdfs__\(_char?.count)")
        }
        
        // eventTitleLabel.text = eventTitle.text
        eventTitleTextField.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        // eventTitleTextField.textAlignment =
        //eventTitleTextField.font = UIFont(name: "Monsterrat - Regular", size: 15.0)
        
        if(eventTitleTextField.text! != "")
        {
            if eventTitleTextField.text!.characters.count < 30 {
                print("eerfre")
                
                // println("")
                isUpdated = true
                currentEvent["eventTitle"] = eventTitleTextField.text!
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            
            return true
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        eventTitleTextField.resignFirstResponder()
        return true
    }
    
    
    
    @IBAction func eventImages(sender: AnyObject)
    {
        let eventImage = self.storyboard!.instantiateViewControllerWithIdentifier("ChangeEventImageViewController") as! ChangeEventImageViewController
        
        self.navigationController?.pushViewController(eventImage, animated: false)
        
        
    }
    
    
    @IBAction func noteFromHost(sender: AnyObject) {
        
        let updateDescription = self.storyboard!.instantiateViewControllerWithIdentifier("UpdateDescriptionViewController") as! UpdateDescriptionViewController
        self.navigationController?.pushViewController(updateDescription, animated: false)
        
    }
    
    @IBAction func locationbtn(sender: AnyObject)
    {
        let addLocationVC = self.storyboard!.instantiateViewControllerWithIdentifier("UpdateLocationViewController") as! UpdateLocationViewController
        addLocationVC.eventTitle = eventTitleTextField.text
        self.navigationController?.pushViewController(addLocationVC, animated: false)
    }
    
    
    @IBAction func Date(sender: AnyObject)
    {
        
        
        if showPicker
        {
            
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height - 85)
            self.datePickerView.hidden = true
            //            showDate.hidden = true
            self.invitationPreview.frame.origin.y = self.invitationPreview.frame.origin.y  - 225
            self.deleteEvent.frame.origin.y = self.deleteEvent.frame.origin.y - 225
            showPicker = false
        }
        else
        {
            //            showDate.hidden = false
            //showDate.text = self.sDate as String
            showPicker = true
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height + self.datePickerView.frame.size.height - 85)
            self.datePickerView.hidden = false
            self.invitationPreview.frame.origin.y = self.invitationPreview.frame.origin.y + 225
            self.deleteEvent.frame.origin.y = self.deleteEvent.frame.origin.y + 225
            
            
            
            showDate.text = getFormatedStringFromDate(datePickerView.date)
            showDate.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
            showDate.textAlignment = .Right
            
            dateChanged(datePickerView)
            
        }
    }
    
    @IBAction func previewInviteBtn(sender: AnyObject)
    {
        let eventPreviewVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPreviewViewController") as! EventPreviewViewController
        //eventPreviewVC.imageData=imageData
        eventPreviewVC.eventTitle = currentEvent["eventTitle"] as! String
        eventPreviewVC.eventObject = currentEvent
        self.navigationController?.pushViewController(eventPreviewVC, animated: true)
    }
    
    @IBAction func deleteEventBtn(sender: AnyObject)
    {
        let refreshAlert = UIAlertController(title: "Delete Event", message: "This Event will be deleted now. This cannot be undone.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction) in
            
            self.showLoader("Deleting Event")
            
            
            var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("deleteEventFromParse:"), userInfo: currentEvent, repeats: false)
            
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
        isToBeBack = true
        
        if isUpdated == true
        {
            var refreshAlert = UIAlertController(title: "Discard Changes", message: "You have pending changes. If you do not save these changes they will be discarded. Do you still want to continue?", preferredStyle: UIAlertControllerStyle.Alert)
            
            
            refreshAlert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action: UIAlertAction) in
                self.saveData()
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Discard", style: .Default, handler: { (action: UIAlertAction!) in
                self.navigationController?.popViewControllerAnimated(false)
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
            
        }
        else
        {
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    @IBAction func saveButton(sender: UIButton)
    {
        isToBeBack = false
        saveData()
    }
    
    
    func saveData()
    {
        var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
        
        print(timezoneOffset)
        
        currentEvent["eventTimezoneOffset"] = timezoneOffset as? Int
         print(currentEvent["eventTimezoneOffset"])
        
        var title = eventTitleTextField.text!
        title = title.stringByReplacingOccurrencesOfString(" ", withString: "", options: [], range: nil)
        
        eventTitleTextField.resignFirstResponder()
        
        if title != ""
        {
            loaderView.hidden = false
            
            if(isUpdated == true)
            {
                currentEvent["eventTitle"] = eventTitleTextField.text!
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventTitleTextField.text!
                
                
                tblFields["isPosted"] = "0"
                
                if(currentEvent["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if currentEvent["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
                        print(date)
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if currentEvent["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((currentEvent["eventEndDateTime"] as? NSDate)!)
                        print(date)
                        tblFields["eventEndDateTime"] = date
                        print(tblFields["eventEndDateTime"])
                    }
                    
                    tblFields["eventDescription"] = currentEvent["eventDescription"] as? String
                    let eventLatitude = currentEvent["eventLatitude"] as! Double
                    let eventLongitude = currentEvent["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = currentEvent["eventLocation"] as? String
                    
                    //loaderView.hidden = true
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                    
                    //loaderView.hidden = true
                }
                
                var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
                
                tblFields["eventTimezoneOffset"] = "\(timezoneOffset)"
                
                
                
                let isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [currentEvent.objectId!])
                
                if isUpdated
                {
                    //showLoader("Updating Event")
                    
                    var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("uploadEventWithoutImage:"), userInfo: currentEvent["eventId"] as! Int, repeats: false)
                }
                else
                {
                    self.loaderView.hidden = true
                    Util.invokeAlertMethod("", strBody: "Error in updating record.", delegate: nil)
                    
                }
                //loaderView.hidden = true
            }
            else
            {
                loaderView.hidden = true
            }
        }
        else
        {
            Util.invokeAlertMethod("", strBody: "Please enter event title.", delegate: nil)
        }
    }
    
    
    func updateEvent()
    {
        currentEvent["isUpdated"] = true
        ParseOperations.instance.saveData(currentEvent, target: self, successSelector: "updateEventSuccess:", successSelectorParameters: nil, errorSelector: "updateEventError:", errorSelectorParameters:currentEvent)
    }
    
    func uploadEventWithoutImage(timer: NSTimer)
    {
        var insertedId = timer.userInfo as! Int
        
        if MyReachability.isConnectedToNetwork()
        {
            updateEvent()
        }
        else
        {
            isPostUpdated = true
            
            //self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    func displayLoader(message: String )
    {
        self.loadingMessage.text = "\(message)"
        self.loadingMessage.textColor = UIColor.whiteColor()
        self.loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.loadingMessage.numberOfLines = 2
        self.loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        self.loadingMessage.textAlignment = .Center
        self.loaderView.hidden = false
    }
    
    
    
    func updateEventSuccess(timer:NSTimer)
    {
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        self.loaderView.hidden=true
        var tblFields: Dictionary! = [String: String]()
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            print(date)
            tblFields["updatedAt"] = date
        }
        
        
        let data = [
            "eventObjectId": eventObject.objectId!,
            "emailId": "noemail",
            "eventCreatorId": "\(currentUserId)"
        ]
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            //
            if error == nil
            {
                print(url!)
                tblFields["socialSharingURL"] = url! as String
                var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [currentEvent["eventId"] as! Int])
                
                eventObject["socialSharingURL"] = url! as String
                
                eventObject.saveInBackground()
            }
            
        })
        
        
        isUpdated = false
        
        var isUpdated1 = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [currentEvent.objectId!])
        
        print(currentEvent.objectId)
        
        let predicate = NSPredicate(format: "eventObjectId IN {'\(currentEvent.objectId!)'}")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventInvitationsError:", errorSelectorParameters: nil)
        
        if isToBeBack == true
        {
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    func updateEventError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        
        //self.navigationController?.popViewControllerAnimated(false)
        
        print("error")
    }
    
    
    
    func fetchEventInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedObjects = objects {
            
            var i = 0
            
            for object in fetchedObjects
            {
                fetchedObjects[i]["isEventUpdated"] = true
                i++
            }
            
            PFObject.saveAllInBackground(fetchedObjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    
                }
                else
                {
                    
                }
            }
        }
        
        let predicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)'")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: "update", errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
    }
    
    
    
    
    func fetchEventInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let actionType = timer.userInfo?.valueForKey("external") as! String
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            
            var notificationObjects = [PFObject]()
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            
            var fetchedUserEmailIds: Array<String!>
            fetchedUserEmailIds = []
            
            let eventTitle = currentEvent["eventTitle"] as! String
            
            let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            
            var notifMessage = ""
            
            //var emailMessage = ""
            
            /*if actionType == "update"
            {
            if isDateUpdated == true || isLocationUpdated == true
            {
            if isDateUpdated == true && isLocationUpdated == true
            {
            notifMessage = "\(fullUserName) changed the date & time and location for the event, \(eventTitle)"
            }
            else if isDateUpdated == true
            {
            notifMessage = "\(fullUserName) changed the date & time for the event, \(eventTitle)"
            }
            else
            {
            notifMessage = "\(fullUserName) changed the location for the event, \(eventTitle)"
            }
            }
            }
            else
            {
            
            if currentEvent["isRSVP"] as! Bool == true
            {
            notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
            }
            else
            {
            notifMessage = "\(fullUserName) deleted the event, \(eventTitle)"
            }
            
            }*/
            
            let eventCreatorObjectId = currentUserId
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            let createdAt = ""
            let updatedAt = ""
            
            let fetchedUserObjectIdsString = fetchedUserObjectIds.joinWithSeparator("','")
            
            let predicate = NSPredicate(format: "userObjectId IN {'\(fetchedUserObjectIdsString)'} AND userObjectId != '\(currentUserId)'")
            
            if actionType == "update"
            {
                if isDateUpdated == true || isLocationUpdated == true
                {
                    var notifMessage = ""
                    
                    if isDateUpdated == true && isLocationUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the date & time and location for the event, \(eventTitle)"
                    }
                    else if isDateUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the date & time for the event, \(eventTitle)"
                    }
                    else
                    {
                        notifMessage = "\(fullUserName) changed the location for the event, \(eventTitle)"
                    }
                    
                    var userObjectid = String()
                    var eventCreatorId = ""
                    for invitation in fetchedobjects
                    {
                        if invitation["userObjectId"] as! String != ""
                        {
                            fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                            fetchedUserEmailIds.append(invitation["emailId"] as! String)
                            
                            let notificationObject = PFObject(className: "Notifications")
                            notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                            notificationObject["notificationImage"] = "profilePic.png"
                            notificationObject["senderId"] = currentUserId
                            userObjectid = invitation["userObjectId"] as! String
                            notificationObject["receiverId"] = invitation["userObjectId"] as! String
                            notificationObject["notificationActivityMessage"] = notifMessage
                            notificationObject["eventObjectId"] = currentEvent.objectId!
                            if actionType == "update"
                            {
                                notificationObject["notificationType"] = "eventdataupdated"
                            }
                            else
                            {
                                notificationObject["notificationType"] = "eventdeleted"
                            }
                            
                            notificationObjects.append(notificationObject)
                        }
                    }
                    
                    PFObject.saveAllInBackground(notificationObjects)
                    
                    let locationString = currentEvent["eventLocation"] as! String
                    
                    let date = currentEvent["eventStartDateTime"] as! NSDate
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    let eventStartDateTime = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
                    
                    let eventDescription = currentEvent["eventDescription"] as! String
                    
                    let eventLatitude = currentEvent["eventLatitude"] as! Double
                    let eventLongitude = currentEvent["eventLongitude"] as! Double
                    
                    let isRSVP = currentEvent["isRSVP"] as! Bool
                    let frameX = currentEvent["frameX"] as! Double
                    let frameY = currentEvent["frameY"] as! Double
                    let eventFolder = currentEvent["eventFolder"] as! String
                    let eventImage = currentEvent["eventImage"] as! String
                    let originalEventImage = currentEvent["originalEventImage"] as! String
                    
                    var eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
                    
                    
                    eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
                    
                    print(currentEvent["socialSharingURL"])
                    
                    let socialSharingURL = currentEvent["socialSharingURL"] as? String
                    
                    var data = [
                        "alert" : "\(notifMessage)",
                        "notifType" :  "eventdataupdated",
                        "eventTitle": "\(eventTitle)",
                        "eventCreatorObjectId": "\(currentUserId)",
                        "eventImage": "\(eventImage)",
                        "originalEventImage": "\(originalEventImage)",
                        "eventFolder": "\(eventFolder)",
                        "emailId": "",
                        "frameX": "\(frameX)",
                        "frameY": "\(frameY)",
                        "eventTimezoneOffset": "\(eventTimezoneOffset)",
                        "eventLatitude": "\(eventLatitude)",
                        "eventLongitude": "\(eventLongitude)",
                        "senderName": "\(fullUserName)",
                        "isRSVP": "\(isRSVP)",
                        "socialSharingURL": "\(socialSharingURL)",
                        "eventStartDateTime": "\(eventStartDateTime)",
                        "eventDescription": "\(eventDescription)",
                        "eventLocation": "\(locationString)",
                        "eventObjectId": "\(currentEvent.objectId!)",
                        "createdAt": "\(createdAt)",
                        "updatedAt": "\(updatedAt)",
                        "eventCreatorId" : "\(eventCreatorId)"
                    ]
                    
                    
                    
                    if isDateUpdated == true && isLocationUpdated == true
                    {
                        for (var i = 0; i < fetchedUserEmailIds.count; i++  )
                        {
                            
                            data["emailId"] = fetchedUserEmailIds[i]
                            
                            sendEventLocationUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i])
                            sendEventDateUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i])
                            
                        }
                    }
                    else if isDateUpdated == true
                    {
                        var urlString = ""
                        for (var i = 0; i < fetchedUserEmailIds.count; i++  )
                        {
                            
                            data["emailId"] = fetchedUserEmailIds[i]
                            
                            sendEventDateUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i])
                        }
                        
                    }
                    else
                    {
                        
                        var urlString = String()
                        for (var i = 0; i < fetchedUserEmailIds.count; i++  )
                        {
                            
                            data["emailId"] = fetchedUserEmailIds[i]
                            
                            sendEventLocationUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i])
                            
                            
                            
                        }
                        
                    }
                    
                    
                    
                    
                    let query = PFInstallation.queryWithPredicate(predicate)
                    
                    let push = PFPush()
                    push.setQuery(query)
                    push.setData(data)
                    push.sendPushInBackground()
                    
                    isDateUpdated = false
                    isLocationUpdated = false
                    
                }
            }
            else
            {
                
                var notifMessage = ""
                
                
                if currentEvent["isRSVP"] as! Bool == true
                {
                    notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
                }
                else
                {
                    notifMessage = "\(fullUserName) deleted the event, \(eventTitle)"
                }
                
                for invitation in fetchedobjects
                {
                    if invitation["userObjectId"] as! String != ""
                    {
                        fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                        fetchedUserEmailIds.append(invitation["emailId"] as! String)
                        
                        let notificationObject = PFObject(className: "Notifications")
                        notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                        notificationObject["notificationImage"] = "profilePic.png"
                        notificationObject["senderId"] = currentUserId
                        notificationObject["receiverId"] = invitation["userObjectId"] as! String
                        notificationObject["notificationActivityMessage"] = notifMessage
                        notificationObject["eventObjectId"] = currentEvent.objectId!
                        
                        if actionType == "update"
                        {
                            notificationObject["notificationType"] = "eventdataupdated"
                        }
                        else
                        {
                            notificationObject["notificationType"] = "eventdeleted"
                        }
                        
                        notificationObjects.append(notificationObject)
                    }
                }
                
                PFObject.saveAllInBackground(notificationObjects)
                
                let creatorId = currentEvent["eventCreatorObjectId"] as! String
                
                let data = [
                    "alert" : "\(notifMessage)",
                    "notifType" :  "eventdeleted",
                    "eventObjectId" : currentEvent.objectId!  ,
                    "eventCreatorId":"\(creatorId)"
                ]
                
                var urlString = String()
                
                Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                    
                    if error == nil
                    {
                        print(url!)
                        
                        urlString = url!
                        
                        let sendEmailObject = SendEmail()
                        
                        let deleteEmail = DeleteEvent()
                        
                        let emailMessage = deleteEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "rsvp",url:urlString)
                        
                        for email in fetchedUserEmailIds
                        {
                            sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                        }
                    }
                    
                })
                
                
                
                
                
                let query = PFInstallation.queryWithPredicate(predicate)
                
                let push = PFPush()
                push.setQuery(query)
                push.setData(data)
                push.sendPushInBackground()
            }
        }
    }
    
    
    func sendEventLocationUpdateEmail(notifMessage: String!, emailId: String!)
        
    {
        
        var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        var locationString = currentEvent["eventLocation"] as! String
        
        //var date = currentEvent["eventStartDateTime"] as! NSDate
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        var eventStartDateTime = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
        
        var eventDescription = currentEvent["eventDescription"] as! String
        
        var eventLatitude = currentEvent["eventLatitude"] as! Double
        var eventLongitude = currentEvent["eventLongitude"] as! Double
        
        var isRSVP = currentEvent["isRSVP"] as! Bool
        var frameX = currentEvent["frameX"] as! Double
        var frameY = currentEvent["frameY"] as! Double
        var eventFolder = currentEvent["eventFolder"] as! String
        var eventImage = currentEvent["eventImage"] as! String
        var originalEventImage = currentEvent["originalEventImage"] as! String
        
        var eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        
        var socialSharingURL = currentEvent["socialSharingURL"] as! String
        var eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
        
        var data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "eventdataupdated",
            "eventTitle": "\(eventTitle)",
            "eventCreatorObjectId": "\(currentUserId)",
            "eventImage": "\(eventImage)",
            "originalEventImage": "\(originalEventImage)",
            "eventFolder": "\(eventFolder)",
            "emailId": "",
            "frameX": "\(frameX)",
            "frameY": "\(frameY)",
            "eventLatitude": "\(eventLatitude)",
            "eventLongitude": "\(eventLongitude)",
            "senderName": "\(fullUserName)",
            "isRSVP": "\(isRSVP)",
            "eventTimezoneOffset": "\(eventTimezoneOffset)",
            "socialSharingURL": "\(socialSharingURL)",
            "eventStartDateTime": "\(eventStartDateTime)",
            "eventDescription": "\(eventDescription)",
            "eventLocation": "\(locationString)",
            "eventObjectId": "\(currentEvent.objectId!)",
            "createdAt": "",
            "updatedAt": "",
            "eventCreatorId" : "\(eventCreatorId)"
        ]
        
        
        
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                print(url!)
                
                let urlString = url!
                
                let sendEmailObject = SendEmail()
                
                let locationEmail = LocationChange()
                
                let emailMessage = locationEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, locationString: locationString, hostName: fullUserName, type: "rsvp",url:urlString)
                
                
                sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [emailId])
            }
            
        })
    }
    
    
    func sendEventDateUpdateEmail(notifMessage: String!, emailId: String!)
    {
        var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        var locationString = currentEvent["eventLocation"] as! String
        
        var eventLatestTitle = currentEvent["eventTitle"] as! String
        
        var date = currentEvent["eventStartDateTime"] as! NSDate
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        var eventStartDateTime = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
        
        var eventDescription = currentEvent["eventDescription"] as! String
        
        var eventLatitude = currentEvent["eventLatitude"] as! Double
        var eventLongitude = currentEvent["eventLongitude"] as! Double
        
        var isRSVP = currentEvent["isRSVP"] as! Bool
        var frameX = currentEvent["frameX"] as! Double
        var frameY = currentEvent["frameY"] as! Double
        var eventFolder = currentEvent["eventFolder"] as! String
        var eventImage = currentEvent["eventImage"] as! String
        var originalEventImage = currentEvent["originalEventImage"] as! String
        
        var eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        
        var socialSharingURL = currentEvent["socialSharingURL"] as! String
        var eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
        
        var data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "eventdataupdated",
            "eventTitle": "\(eventTitle)",
            "eventCreatorObjectId": "\(currentUserId)",
            "eventImage": "\(eventImage)",
            "originalEventImage": "\(originalEventImage)",
            "eventFolder": "\(eventFolder)",
            "emailId": "",
            "frameX": "\(frameX)",
            "frameY": "\(frameY)",
            "eventLatitude": "\(eventLatitude)",
            "eventLongitude": "\(eventLongitude)",
            "senderName": "\(fullUserName)",
            "isRSVP": "\(isRSVP)",
            "eventTimezoneOffset": "\(eventTimezoneOffset)",
            "socialSharingURL": "\(socialSharingURL)",
            "eventStartDateTime": "\(eventStartDateTime)",
            "eventDescription": "\(eventDescription)",
            "eventLocation": "\(locationString)",
            "eventObjectId": "\(currentEvent.objectId!)",
            "createdAt": "",
            "updatedAt": "",
            "eventCreatorId" : "\(eventCreatorId)"
        ]
        
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                print(url!)
                var urlString = url!
                
                var dateEmail = DateChange()
                
                var emailMessage = dateEmail.emailMessage(currentEvent.objectId!, eventTitle: eventLatestTitle, dateString: self.dateStringFromNSDate(date), timeString: self.timeStringFromNSDate(date), hostName: fullUserName, type: "rsvp",url:urlString)
                
                var sendEmailObject = SendEmail()
                
                sendEmailObject.sendEmail("Update – \(eventLatestTitle)", message: emailMessage, emails: [emailId])
            }
            
        })
    }
    
    
    /*
    
    if actionType == "update"
    {
    if isDateUpdated == true || isLocationUpdated == true
    {
    if isDateUpdated == true && isLocationUpdated == true
    {
    notifMessage = "\(fullUserName) changed the date & time and location for the event, \(eventTitle)"
    }
    else if isDateUpdated == true
    {
    
    
    var dateEmail = DateChange()
    
    emailMessage = dateEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, dateString: dateStringFromNSDate(date), timeString: timeStringFromNSDate(date), hostName: fullUserName, type: "rsvp")
    
    var sendEmailObject = SendEmail()
    
    notifMessage = "\(fullUserName) changed the date & time for the event, \(eventTitle)"
    
    sendEmailObject.sendEmail(notifMessage, message: emailMessage, emails: [String])
    
    
    }
    else
    {
    notifMessage = "\(fullUserName) changed the location for the event, \(eventTitle)"
    }
    }
    }
    else
    {
    
    if currentEvent["isRSVP"] as! Bool == true
    {
    notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
    }
    else
    {
    notifMessage = "\(fullUserName) deleted the event, \(eventTitle)"
    }
    
    }
    
    
    */
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    func dateStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        let sweekday = scomponents.weekday
        
        let dateString = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        
        return dateString
    }
    
    
    func timeStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            shour = scomponents.hour-12
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        let timeString = "\(shour):\(sminute) \(sam)"
        
        return timeString
    }
    
    
    func deleteEventFromParse(timer: NSTimer)
    {
        if MyReachability.isConnectedToNetwork()
        {
            ParseOperations.instance.deleteData(currentEvent, target: self, successSelector: "deleteEventSuccess:", successSelectorParameters: nil, errorSelector: "deleteEventError:", errorSelectorParameters:nil)
        }
        else
        {
            self.loaderView.hidden = true
            
            let refreshAlert = UIAlertController(title: "No internet connection", message: "Event cannot be deleted as you seem to be offline. Please check your networks settings.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Go Back", style: .Default, handler: { (action: UIAlertAction) in
                
                let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                self.navigationController?.pushViewController(eventVC, animated: true)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
            
            
            
        }
    }
    
    
    func deleteEventSuccess(timer:NSTimer)
    {
        let eventToBeDeleted: PFObject = timer.userInfo?.valueForKey("internal") as! PFObject
        
        self.loaderView.hidden = true
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)'")
        
        let invitationQuery = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(invitationQuery, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: "delete", errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        
        let imageFolder = currentEvent["eventFolder"] as! String
        
        let imageName = currentEvent["eventImage"] as! String
        
        let deleteRequest = AWSS3DeleteObjectRequest()
        deleteRequest.bucket = "eventnodepublicpics"
        deleteRequest.key = "\(imageFolder)\(imageName)"
        
        let s3 = AWSS3.defaultS3()
        
        s3.deleteObject(deleteRequest).continueWithBlock {
            (task: AWSTask!) -> AnyObject! in
            
            if(task.error != nil)
            {
                
            }else{
                
                let imageOriginalName = currentEvent["originalEventImage"] as! String
                
                let deleteRequestOriginal = AWSS3DeleteObjectRequest()
                deleteRequestOriginal.bucket = "eventnodepublicpics"
                deleteRequestOriginal.key = "\(imageFolder)\(imageOriginalName)"
                
                let s3Original = AWSS3.defaultS3()
                
                s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                    (task: AWSTask!) -> AnyObject! in
                    
                    if(task.error != nil){
                        
                    }else{
                        
                    }
                    return nil
                }
                
            }
            return nil
        }
        
        
        isUpdated = true
        
        let isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "objectId=?", whereFields: [currentEvent.objectId!])
        
        if isDeleted
        {
            ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId=?", whereFields: [currentEvent.objectId!])
        }
        
        
        let query = PFQuery(className:"EventImages")
        query.whereKey("eventObjectId", equalTo:eventToBeDeleted.objectId!)
        query.orderByDescending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventsAfterDeleteOriginalSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventsAfterDeleteOriginalError:", errorSelectorParameters:nil)
        
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: true)
    }
    
    func deleteEventError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        let refreshAlert = UIAlertController(title: "Error", message: "Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
            self.loaderView.hidden=true
        }))
        
        
        
        print("error occured \(error.description)")
    }
    
    
    func fetchEventsAfterDeleteOriginalSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) posts.")
        if let objects = objects {
            self.loaderView.hidden=true
            
            for post in objects
            {
                let postType = post["postType"] as! String
                
                if(postType == "image" || postType == "video")
                {
                    
                    let postFolder = post["eventFolder"] as! String
                    let postImage = post["postData"] as! String
                    
                    let deleteRequestOriginal = AWSS3DeleteObjectRequest()
                    deleteRequestOriginal.bucket = "eventnode1"
                    deleteRequestOriginal.key = "\(postFolder)\(postImage)"
                    
                    let s3Original = AWSS3.defaultS3()
                    
                    s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                        (task: AWSTask!) -> AnyObject! in
                        
                        if(task.error != nil){
                            print("not deleted post")
                            
                        }else{
                            print("deleted post")
                            
                            
                            
                            
                        }
                        return nil
                    }
                }
                
            }
        }
    }
    
    func fetchEventsAfterDeleteOriginalError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        self.loaderView.hidden=true
        print("Error: \(error) \(error.userInfo)")
    }
    
    
    func getFormatedStringFromDate(sdate: NSDate) -> String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour > 12)
        {
            shour = scomponents.hour-12
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        
        let startDate = "\(monthsArray[smonth-1]) \(sday) \(syear),\(shour):\(sminute) \(sam)"
        return startDate
    }
    
}
