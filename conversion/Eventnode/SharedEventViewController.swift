//
//  SharedEventViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit


var mySharedEvents = [PFObject]()
var currentSharedEvent:PFObject!

class SharedEventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var inviteBoardingTextView: UITextView!
    @IBOutlet weak var invitedBoardingScreen: UIView!
    @IBOutlet var inviteCode: UIButton!
    
    var currentUserId: String!
    var deepLinkObjectId = String()
    var deepLinkEmail = ""
    var eventCreatorObjectId = ""
    
    var isUpdateComplete = false
    var isDownloadComplete = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //createInvitation()
        self.view.addSubview(wakeUpImageView)
        
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        //indicator.frame = (loaderSubView.frame.width/2) - (indicator.frame.width/2)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Loading..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loaderSubView.layer.cornerRadius = 10.0
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        //self.wakeUpView.hidden = false
        self.loaderView.hidden = true
        
        
        /*var isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "1", whereFields: [])
        if isDeleted {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        
        
        var isDeleted1 = ModelManager.instance.deleteTableData("EventImages", whereString: "1", whereFields: [])
        
        if isDeleted1 {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        
        
        var isDeleted2 = ModelManager.instance.deleteTableData("Invitations", whereString: "1", whereFields: [])
        
        if isDeleted2 {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }*/
        
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
            var isExtraEventDataDeleted = ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId IN (SELECT objectId FROM Events WHERE eventCreatorObjectId != '\(self.currentUserId)' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)')) ", whereFields: [])
            if isExtraEventDataDeleted
            {
                isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Events", whereString: "eventCreatorObjectId != '\(self.currentUserId)' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)')", whereFields: [])
                
                if isExtraEventDataDeleted
                {
                    print("Extra data deleted successfully")
                    
                    isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Invitations", whereString: "userObjectId != '\(self.currentUserId)' AND eventObjectId NOT IN (SELECT objectId FROM Events WHERE eventCreatorObjectId = '\(self.currentUserId)')", whereFields: [])
                    
                    if isExtraEventDataDeleted
                    {
                        print("Extra data deleted successfully")
                        
                        isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Notifications", whereString: "receiverId != '\(self.currentUserId)'", whereFields: [])
                        
                    }
                    
                }
            }
        }
        
        
        // Do any additional setup after loading the view.
        invitedBoardingScreen.hidden = true
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        
        var attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(12.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        
        inviteBoardingTextView.attributedText = NSAttributedString(string:inviteBoardingTextView.text, attributes:attributes)
        inviteBoardingTextView.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        inviteBoardingTextView.textAlignment = .Center
        inviteBoardingTextView.textColor = UIColor(red: 150.0/255, green: 150.0/255, blue: 150.0/255, alpha: 1.0)
        
        tableView.separatorColor = UIColor.clearColor()
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
        }
        
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        
        
        
        if deepLinkEmail != ""
        {
            
            if currentUserId != eventCreatorObjectId
            {
                if (deepLinkEmail != "noemail")
                {
                    self.loaderView.hidden = false
                    let linkedQuery = PFUser.query()
                    linkedQuery!.whereKey("email", equalTo:self.deepLinkEmail)
                    
                    ParseOperations.instance.fetchData(linkedQuery!, target: self, successSelector: "fetchEmailSuccess:", successSelectorParameters: deepLinkObjectId, errorSelector:"fetchEmailError:", errorSelectorParameters: nil)
                }
                else
                {
                    let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND userObjectId = '\(currentUserId!)'")
                    
                    let query = PFQuery(className:"Invitations", predicate: predicate)
                    
                    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
                }
                
            }
            else
            {
                self.loaderView.hidden = true
                
                if #available(iOS 8.0, *) {
                    let Alert = UIAlertController(title: "Error", message: "The event cannot be added to your shared events as you are the owner of the event.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    Alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                        
                        
                    }))
                    
                    self.presentViewController(Alert, animated: true, completion: nil)
                }
                else
                {
                    
                }
                
                
                deleteData()
                downloadData()
                refreshList()
                updateData()
                
            }
            
        }
        else
        {
            deleteData()
            downloadData()
            refreshList()
            
            if (mySharedEvents.count > 0)
            {
                updateData()
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func fetchEmailSuccess(timer:NSTimer)
    {
        let fetchedEmail = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let eventObjectId = timer.userInfo?.valueForKey("external") as! String
        
        if fetchedEmail?.count == 0
        {
            let linkedQuery = PFQuery(className: "LinkedAccounts")
            linkedQuery.whereKey("emailId", equalTo: deepLinkEmail)
            ParseOperations.instance.fetchData(linkedQuery, target: self, successSelector: "fetchLinkedAccountSuccess:", successSelectorParameters: eventObjectId, errorSelector:"fetchLinkedAccountError:", errorSelectorParameters: nil)
        }
        else
        {
            
            if deepLinkEmail == NSUserDefaults.standardUserDefaults().valueForKey("email") as! String
            {
                let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)'")
                
                let query = PFQuery(className:"Invitations", predicate: predicate)
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
            }
            else
            {
                self.loaderView.hidden = true
                
                if #available(iOS 8.0, *) {
                    let Alert = UIAlertController(title: "Error", message: "This email address is already associated with a different Eventnode account. Log in from that Eventnode account to continue.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    Alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                        
                        
                    }))
                    self.presentViewController(Alert, animated: true, completion: nil)
                }
                else{
                    
                }
                
                
                deleteData()
                downloadData()
                refreshList()
                updateData()
                
            }
            
        }
        
    }
    
    func fetchEmailError(timer:NSTimer)
    {
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        self.loaderView.hidden = true
    }
    
    
    func fetchInvitationsSuccess(timer:NSTimer)
    {
        let invitationObjects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if let fetchedobjects = invitationObjects
        {
            if fetchedobjects.count == 0
            {
                let inviteObject: PFObject = PFObject(className: "Invitations")
                
                inviteObject["invitedName"] = NSUserDefaults.standardUserDefaults().valueForKey("fullUserName") as! String
                inviteObject["eventObjectId"] = deepLinkObjectId
                inviteObject["isApproved"] = false
                inviteObject["userObjectId"] = currentUserId
                inviteObject["emailId"] = NSUserDefaults.standardUserDefaults().valueForKey("email") as! String
                inviteObject["attendingStatus"] = ""
                inviteObject["invitationType"] = "email"
                
                inviteObject["isUpdated"] = false
                inviteObject["noOfChilds"] = 0
                inviteObject["noOfAdults"] = 0
                inviteObject["invitationNote"] = ""
                inviteObject["isEventUpdated"] = false
                
                ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "saveInvitationSuccess:", successSelectorParameters: nil, errorSelector: "saveInvitationError:", errorSelectorParameters: nil)
            }
            else
            {
                for invitation in fetchedobjects
                {
                    if deepLinkEmail != "" && deepLinkEmail != "noemail"
                    {
                        invitation["isApproved"] = true
                        invitation["isEventUpdated"] = true
                        invitation["isUpdated"] = true
                        invitation["userObjectId"] = currentUserId
                        
                        ParseOperations.instance.saveData(invitation, target: self, successSelector: "saveInvitationSuccess:", successSelectorParameters: nil, errorSelector: "saveInvitationError:", errorSelectorParameters: nil)
                    }
                    else
                    {
                        //deepLinkEmail = ""
                        //eventCreatorObjectId = ""
                        deleteData()
                        downloadData()
                        refreshList()
                        updateData()
                    }
                }
            }
        }
    }
    func fetchInvitationsError(timer:NSTimer)
    {
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        self.loaderView.hidden = true
    }
    
    func fetchLinkedAccountSuccess(timer:NSTimer)
    {
        let fetchedEmails = timer.userInfo?.valueForKey("internal") as? [PFObject]
        var eventObjectId = timer.userInfo?.valueForKey("external") as! String
        //var userObjectid = fetchedEmail[0][""]
        
        if let fetchedobjects = fetchedEmails
        {
            if fetchedobjects.count == 0
            {
                let linkEmail = PFObject(className: "LinkedAccounts")
                
                linkEmail["emailId"] = deepLinkEmail
                linkEmail["userObjectId"] = currentUserId!
                linkEmail["isEmailVerified"] = true
                linkEmail.saveInBackground()
                
                let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)'")
                
                let query = PFQuery(className:"Invitations", predicate: predicate)
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
                
                fetchUnRespondedInvitations()
                
            }
            else
            {
                for emailObject in fetchedobjects
                {
                    if currentUserId == emailObject["userObjectId"] as! String
                    {
                        
                        let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)'")
                        
                        let query = PFQuery(className:"Invitations", predicate: predicate)
                        
                        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
                        
                        
                    }
                    else
                    {
                        if emailObject["isEmailVerified"] as! Bool == true
                        {
                            
                            self.loaderView.hidden = true
                            
                            if #available(iOS 8.0, *) {
                                var Alert = UIAlertController(title: "Error", message: "This email address is already associated with a different Eventnode account. Log in from that Eventnode account to continue.", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                
                                Alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                                    
                                    
                                }))
                                
                                self.presentViewController(Alert, animated: true, completion: nil)
                            }
                            else
                            {
                            }
                            
                            deleteData()
                            downloadData()
                            refreshList()
                            updateData()
                        }
                        else
                        {
                            //deletetion code
                            //emailObject["userObjectId"] as! String
                            ParseOperations.instance.deleteData(emailObject, target: self, successSelector: "deleteEmailSuccess:", successSelectorParameters: [emailObject["userObjectId"] as! String, emailObject["emailId"] as! String], errorSelector: "deleteEmailError:", errorSelectorParameters: nil)
                        }
                    }
                    
                }
            }
            
        }
    }
    
    func fetchLinkedAccountError(timer:NSTimer)
    {
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
        self.loaderView.hidden = true
        
    }
    
    func deleteEmailSuccess(timer:NSTimer)
    {
        var emailData = timer.userInfo?.valueForKey("internal") as! PFObject
        var eventObjectId = timer.userInfo?.valueForKey("external") as! [String]
        
        let linkEmail = PFObject(className: "LinkedAccounts")
        
        linkEmail["emailId"] = deepLinkEmail
        linkEmail["userObjectId"] = currentUserId!
        linkEmail["isEmailVerified"] = true
        
        linkEmail.saveInBackground()
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)'")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
        
        fetchUnRespondedInvitations()
        
    }
    
    
    func deleteEmailError(timer:NSTimer)
    {
        var emailData = timer.userInfo?.valueForKey("internal") as! PFObject
        
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
        self.loaderView.hidden = true
        
    }
    
    
    func fetchUnRespondedInvitations()
    {
        
        let predicate = NSPredicate(format: "eventObjectId != '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)' AND userObjectId == ''")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchUnRespondedInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchUnRespondedInvitationsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchUnRespondedInvitationsSuccess(timer:NSTimer)
    {
        let invitationObjects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if var fetchedobjects = invitationObjects
        {
            if fetchedobjects.count > 0
            {
                for var i = 0; i < fetchedobjects.count; i++
                {
                    fetchedobjects[i]["isApproved"] = true
                    fetchedobjects[i]["isEventUpdated"] = true
                    fetchedobjects[i]["isUpdated"] = true
                    fetchedobjects[i]["userObjectId"] = currentUserId
                }
                
                PFObject.saveAllInBackground(fetchedobjects)
                
            }
        }
    }
    
    
    func fetchUnRespondedInvitationsError(timer:NSTimer)
    {
        
    }
    
    
    func saveInvitationSuccess(timer:NSTimer)
    {
        var invitationObject = timer.userInfo?.valueForKey("internal") as! PFObject
        
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        //isDownloadComplete = false
        //isUpdateComplete = false
        
        self.loaderView.hidden = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
    }
    
    func saveInvitationError(timer:NSTimer)
    {
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
        self.loaderView.hidden = true
        
        
    }
    
    
    func refreshList()
    {
        //isEventDataUpDated = false
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') GROUP BY objectId ORDER BY eventId DESC", whereFields: [])
        
        mySharedEvents = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                
                let userevent = PFObject(className: "Events")
                
                userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                
                userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                let isRSVP = resultSet.stringForColumn("isRSVP")
                
                if isRSVP == "0"
                {
                    userevent["isRSVP"] = false
                }
                else
                {
                    userevent["isRSVP"] = true
                    userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                    userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                    userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                    userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
                    userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                    userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                }
                
                userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                userevent["frameX"] = resultSet.doubleForColumn("frameX")
                userevent["frameY"] = resultSet.doubleForColumn("frameY")
                
                
                userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
                
                userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                
                userevent["senderName"] = resultSet.stringForColumn("senderName")
                userevent["socialSharingURL"] = resultSet.stringForColumn("socialSharingURL")
                
                
                if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil)
                {
                    userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                    userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                    userevent.objectId = resultSet.stringForColumn("objectId")
                }
                
                let isPosted = resultSet.stringForColumn("isPosted")
                
                if isPosted == "0"
                {
                    userevent["isPosted"] = false
                }
                else
                {
                    userevent["isPosted"] = true
                }
                
                userevent["isUploading"] = false
                
                userevent["isDownloading"] = true
                
                mySharedEvents.append(userevent)
                
                //println(noOfNewPosts)
            }
        }
        
        resultSet.close()
        
        var i = 0
        
        for userevent in mySharedEvents
        {
            let isApproved = getApprovedStatus(userevent)
            
            let invitationNote = getInvitationNote(userevent)
            
            let noOfNewPosts = getNoOfPosts(userevent)
            
            let attendingStatus = getAttendingStatus(userevent)
            
            let invitationId = getInvitationId(userevent)
            
            let noOfAdults = getNoOfAdults(userevent)
            
            let noOfChilds = getNoOfChilds(userevent)
            
            mySharedEvents[i]["isApproved"] = isApproved
            mySharedEvents[i]["noOfNewPosts"] = noOfNewPosts
            mySharedEvents[i]["attendingStatus"] = attendingStatus
            mySharedEvents[i]["invitationId"] = invitationId
            mySharedEvents[i]["noOfAdults"] = noOfAdults
            mySharedEvents[i]["noOfChilds"] = noOfChilds
            mySharedEvents[i]["invitationNote"] = invitationNote
            
            i++
        }
        self.tableView.reloadData()
        
        if mySharedEvents.count == 0
        {
            invitedBoardingScreen.hidden = false
        }
        else
        {
            invitedBoardingScreen.hidden = true
        }
        
        
        
        
        if isDownloadComplete == true && isUpdateComplete == true && deepLinkEmail != ""
        {
            deepLinkEmail = ""
            eventCreatorObjectId = ""
            
            isDownloadComplete = false
            isUpdateComplete = false
            
            self.loaderView.hidden = true
            
            for event in mySharedEvents
            {
                if deepLinkObjectId == event.objectId!
                {
                    currentSharedEvent = event
                    
                    mySharedEventData = []
                    
                    if((event["isApproved"] as! Bool) == true || (event["isRSVP"] as! Bool) == true)
                    {
                        let streamVC = self.storyboard!.instantiateViewControllerWithIdentifier("StreamViewController") as! StreamViewController
                        
                        self.navigationController?.pushViewController(streamVC, animated: true)
                    }
                    else
                    {
                        
                        self.loaderView.hidden = true
                        
                        if #available(iOS 8.0, *) {
                            var Alert = UIAlertController(title: "Error", message: "This is a private online event and the host needs to approve before you can get access to it.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            
                            Alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                                
                            }))
                            
                            self.presentViewController(Alert, animated: true, completion: nil)
                        }
                        else
                        {
                            
                        }
                        
                        //deleteData()
                        //downloadData()
                        //refreshList()
                        //updateData()
                        
                    }
                    
                }
            }
        }
        
        
    }
    
    
    
    
    func deleteData()
    {
        let updatePredicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
        
        let updateQuery = PFQuery(className:"Invitations", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchExistingEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchExistingEventsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchExistingEventsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects
        {
            var i = 0
            var existingEventObjectIds: Array<String>
            existingEventObjectIds = []
            
            for invitationObject in fetchedobjects
            {
                existingEventObjectIds.append(invitationObject["eventObjectId"] as! String)
            }
            
            let existingEventObjectIdsString = existingEventObjectIds.joinWithSeparator("','")
            
            var whereQuery = ""
            
            if existingEventObjectIdsString != ""
            {
                whereQuery = "eventCreatorObjectId != '\(currentUserId)' AND objectId NOT IN ('\(existingEventObjectIdsString)') AND objectId != ''"
            }
            else
            {
                whereQuery = "eventCreatorObjectId != '\(currentUserId)' AND objectId != ''"
            }
            
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["objectId"], whereString: whereQuery, whereFields: [])
            
            
            var nonExistingEventObjectIds: Array<String>
            nonExistingEventObjectIds = []
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    nonExistingEventObjectIds.append(resultSet.stringForColumn("objectId"))
                }
            }
            
            resultSet.close()
            
            let nonExistingEventObjectIdsString = nonExistingEventObjectIds.joinWithSeparator("','")
            
            if nonExistingEventObjectIdsString != ""
            {
                ModelManager.instance.deleteTableData("Events", whereString: "objectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("Invitations", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("PostLikes", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventComments", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                refreshList()
            }
            
        }
    }
    
    
    func fetchExistingEventsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    
    
    func updateData()
    {
        let updatePredicate = NSPredicate(format: "userObjectId = '\(currentUserId)' AND (isEventUpdated = true OR isEventStreamUpdated = true)")
        
        let updateQuery = PFQuery(className:"Invitations", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchEventUpdatesSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventUpdatesError:", errorSelectorParameters:nil)
    }
    
    
    
    
    func fetchEventUpdatesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            var updateIds = [String]()
            
            var updateStreamEventIds = [String]()
            
            
            var tblFields: Dictionary! = [String: String]()
            
            for invitation in fetchedobjects
            {
                if invitation["isEventUpdated"] as! Bool == true
                {
                    updateIds.append(invitation["eventObjectId"] as! String)
                }
                
                if invitation["isEventStreamUpdated"] as! Bool == true
                {
                    updateStreamEventIds.append(invitation["eventObjectId"] as! String)
                }
                
                fetchedobjects[i]["isEventUpdated"] = false
                fetchedobjects[i]["isEventStreamUpdated"] = false
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }
                
                var isUpdated = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitation.objectId!])
                
                i++
                //updateInvitationIds.append(invitation.objectId!)
            }
            
            if fetchedobjects.count > 0
            {
                PFObject.saveAllInBackground(fetchedobjects)
            }
            
            
            var updateIdsString = ""
            var updateStreamEventIdsString = ""
            
            if updateIds.count > 0
            {
                updateIdsString = updateIds.joinWithSeparator("','")
                
                //println(invitationIds)
            }
            
            if updateStreamEventIds.count > 0
            {
                updateStreamEventIdsString = updateStreamEventIds.joinWithSeparator("','")
                
                //println(invitationIds)
            }
            
            if updateIdsString != ""
            {
                let predicate = NSPredicate(format: "objectId IN {'\(updateIdsString)'}")
                
                let query = PFQuery(className:"Events", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchUpdatedEventsSuccess:", successSelectorParameters: fetchedobjects, errorSelector: "fetchUpdatedEventsError:", errorSelectorParameters:nil)
            }
            
            if updateStreamEventIdsString != ""
            {
                
                let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["objectId"], whereString: "eventObjectId IN ('\(updateStreamEventIdsString)')", whereFields: [])
                
                
                var newPostIds = [String]()
                
                if (resultSet != nil) {
                    while resultSet.next()
                    {
                        newPostIds.append(resultSet.stringForColumn("objectId"))
                    }
                }
                
                resultSet.close()
                
                if newPostIds.count > 0
                {
                    var newPostIdsString = ""
                    
                    newPostIdsString = newPostIds.joinWithSeparator("','")
                    
                    let predicate = NSPredicate(format: "NOT (objectId IN {'\(newPostIdsString)'}) AND eventObjectId IN {'\(updateStreamEventIdsString)'}")
                    
                    let query = PFQuery(className:"EventImages", predicate: predicate)
                    
                    query.orderByAscending("createdAt")
                    
                    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
                }
            }
            
            
        }
    }
    
    
    func fetchEventUpdatesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    
    func fetchUpdatedEventsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        let invitationObjects = timer.userInfo?.valueForKey("external") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            for eventObject in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                tblFields["eventImage"] = eventObject["eventImage"] as? String
                tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                
                let frameX = eventObject["frameX"] as! CGFloat
                let frameY = eventObject["frameY"] as! CGFloat
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                tblFields["senderName"] = eventObject["senderName"] as? String
                
                tblFields["eventFolder"] = eventObject["eventFolder"] as? String
                
                if(eventObject["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if eventObject["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                        print(date)
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if eventObject["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                        print(date)
                        tblFields["eventEndDateTime"] = date
                        print(tblFields["eventEndDateTime"])
                    }
                    
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    
                    let eventLatitude = eventObject["eventLatitude"] as! Double
                    let eventLongitude = eventObject["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                    
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                }
                
                
                tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                
                tblFields["isPosted"] = "1"
                
                var date = ""
                
                if eventObject.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if eventObject.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [eventObject.objectId!])
                
            }
            
            if deepLinkEmail != ""
            {
                isUpdateComplete = true
            }
            
            
            refreshList()
            
            PFObject.saveAllInBackground(invitationObjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    
                }
                else
                {
                    
                }
            }
        }
    }
    
    
    
    func fetchUpdatedEventsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    
    func getNoOfPosts(userevent: PFObject) -> Int
    {
        let newPostsResultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "eventObjectId = '\(userevent.objectId!)' AND isRead = 0", whereFields: [])
        
        newPostsResultSet.next()
        
        let noOfNewPosts = Int(newPostsResultSet.intForColumn("count"))
        
        newPostsResultSet.close()
        
        return noOfNewPosts
    }
    
    func getApprovedStatus(userevent: PFObject) -> Bool
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let isApproved = statusResultSet.stringForColumn("isApproved")
        
        statusResultSet.close()
        
        if isApproved == "0"
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    
    func getNoOfChilds(userevent: PFObject) -> Int
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let noOfChilds = statusResultSet.intForColumn("noOfChilds")
        
        statusResultSet.close()
        
        print("noOfChilds: \(noOfChilds)")
        
        return Int(noOfChilds)
    }
    
    func getNoOfAdults(userevent: PFObject) -> Int
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let noOfAdults = statusResultSet.intForColumn("noOfAdults")
        
        statusResultSet.close()
        
        print("noOfAdults: \(noOfAdults)")
        
        return Int(noOfAdults)
    }
    
    
    func getAttendingStatus(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let attendingStatus = statusResultSet.stringForColumn("attendingStatus")
        
        statusResultSet.close()
        
        return attendingStatus
    }
    
    
    func getInvitationId(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let invitationId = statusResultSet.stringForColumn("objectId")
        
        statusResultSet.close()
        
        return invitationId
    }
    
    
    func getInvitationNote(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let invitationNote = statusResultSet.stringForColumn("invitationNote")
        
        statusResultSet.close()
        
        return invitationNote
    }
    
    func downloadData()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "userObjectId = '\(self.currentUserId)' ORDER BY invitationId DESC", whereFields: [])
        
        var invitationObjectIds: Array<String>
        
        invitationObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                invitationObjectIds.append(resultSet.stringForColumn("objectId"))
            }
        }
        
        resultSet.close()
        
        let invitationObjectIdsString = invitationObjectIds.joinWithSeparator("','")
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(invitationObjectIdsString)'}) AND userObjectId = '\(self.currentUserId)'")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for invitation in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = invitation.objectId!
                tblFields["userObjectId"] = invitation["userObjectId"] as? String
                tblFields["attendingStatus"] = invitation["attendingStatus"] as? String
                tblFields["invitationType"] = invitation["invitationType"] as? String
                
                tblFields["invitationNote"] = invitation["invitationNote"] as? String
                let noOfAdults = invitation["noOfAdults"] as? Int
                let noOfChilds = invitation["noOfChilds"] as? Int
                
                tblFields["noOfAdults"] = "\(noOfAdults)"
                tblFields["noOfChilds"] = "\(noOfChilds)"
                
                
                if let needsContentApproval = invitation["needsContentApproval"] as? Bool
                {
                    if needsContentApproval as Bool == true
                    {
                        tblFields["needsContentApprovel"] = "1"
                    }
                        
                    else
                    {
                        tblFields["needsContentApprovel"] = "0"
                    }
                }
                else
                {
                    tblFields["needsContentApprovel"] = "0"
                }
                
                var date = ""
                
                if invitation.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if invitation.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                tblFields["emailId"] = invitation["emailId"] as? String
                tblFields["eventObjectId"] = invitation["eventObjectId"] as? String
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }
                
                var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
                
                fetchedEventObjectIds.append(invitation["eventObjectId"] as! String)
                
            }
            
            let eventObjectIdsString = fetchedEventObjectIds.joinWithSeparator("','")
            print(eventObjectIdsString)
            if eventObjectIdsString != ""
            {
                let predicate = NSPredicate(format: "objectId IN {'\(eventObjectIdsString)'}")
                
                let query = PFQuery(className:"Events", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllEventsSuccess:", successSelectorParameters: eventObjectIdsString, errorSelector: "fetchAllEventsError:", errorSelectorParameters:nil)
            }
            else
            {
                if deepLinkEmail != ""
                {
                    isDownloadComplete = true
                }
                
                self.refreshList()
            }
        }
    }
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    
    func fetchAllEventsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let eventObjectIdsString = timer.userInfo?.valueForKey("external") as! String
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            
            for eventObject in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                tblFields["eventImage"] = eventObject["eventImage"] as? String
                tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                
                let frameX = eventObject["frameX"] as! CGFloat
                let frameY = eventObject["frameY"] as! CGFloat
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                
                tblFields["eventFolder"] = eventObject["eventFolder"] as? String
                
                tblFields["senderName"] = eventObject["senderName"] as? String
                
                if(eventObject["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if eventObject["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                        print(date)
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if eventObject["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                        print(date)
                        tblFields["eventEndDateTime"] = date
                        print(tblFields["eventEndDateTime"])
                    }
                    
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    
                    let eventLatitude = eventObject["eventLatitude"] as! Double
                    let eventLongitude = eventObject["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                }
                
                
                tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                tblFields["objectId"] = eventObject.objectId
                tblFields["isPosted"] = "1"
                
                var date = ""
                
                if eventObject.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if eventObject.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                let insertedId = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                
                if insertedId>0
                {
                    print("Record inserted at \(insertedId).")
                }
                else
                {
                    print("Error in inserting record.")
                }
            }
            
            
            if deepLinkEmail != ""
            {
                isDownloadComplete = true
            }
            
            self.refreshList()
            
            print(eventObjectIdsString)
            
            let predicate = NSPredicate(format: "eventObjectId IN {'\(eventObjectIdsString)'}")
            
            let query = PFQuery(className:"EventImages", predicate: predicate)
            
            query.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
            
        }
    }
    
    func fetchAllEventsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId NOT IN ( SELECT eventId FROM Events ORDER BY eventId DESC", whereFields: [])
        ModelManager.instance.deleteTableData("Invitations", whereString: "eventObjectId NOT IN (SELECT objectId FROM Events)", whereFields: [])
    }
    
    
    func fetchAllPostsSuccess(timer:NSTimer)
    {
        
        //self.loaderView.hidden = true
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        isPostUpdated = false
        print("Successfully retrieved \(objects!.count) posts.")
        
        if let fetchedobjects = objects {
            //self.loaderView.hidden=true
            for post in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["postData"] = post["postData"] as? String
                
                tblFields["isApproved"] = "0"
                
                let postHeight = post["postHeight"] as! CGFloat
                let postWidth = post["postWidth"] as! CGFloat
                
                tblFields["postHeight"] = "\(postHeight)"
                tblFields["postWidth"] = "\(postWidth)"
                tblFields["eventObjectId"] = post["eventObjectId"] as? String
                
                tblFields["eventFolder"] = post["eventFolder"] as? String
                
                tblFields["postType"] = post["postType"] as? String
                
                tblFields["objectId"] = post.objectId!
                tblFields["isPosted"] = "1"
                tblFields["isRead"] = "0"
                
                var date = ""
                
                if post.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if post.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "objectId = '\(post.objectId!)'", whereFields: [])
                
                resultSet.next()
                
                let noOfPosts = Int(resultSet.intForColumn("objectId"))
                
                resultSet.close()
                
                if noOfPosts > 0
                {
                    var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "objectId=?", whereFields: [post.objectId!])
                }
                else
                {
                    var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
                }
            }
            
            self.refreshList()
        }
    }
    
    func fetchAllPostsError(timer:NSTimer)
    {
        //self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return mySharedEvents.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        
        return tableView.frame.width*(3.0/4)
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("SharedEventsTableViewCell", forIndexPath: indexPath) as! SharedEventsTableViewCell
        
        let row = indexPath.row
        
        for view in cell.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        var eventTitleText: String = (mySharedEvents[row]["eventTitle"] as? String)!
        
        eventTitleText = String(eventTitleText.characters.prefix(1)).capitalizedString + String(eventTitleText.characters.suffix(eventTitleText.characters.count - 1))
        
        let eventImageView = UIImageView()
        
        eventImageView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height+(1*self.view.frame.height/568))
        
        cell.contentView.addSubview(eventImageView)
        
        let eventImageOverlayView = UIView()
        
        eventImageOverlayView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height+(1*self.view.frame.height/568))
        
        eventImageOverlayView.backgroundColor = UIColor.blackColor()
        eventImageOverlayView.alpha = 0.5
        
        cell.contentView.addSubview(eventImageOverlayView)
        
        
        let eventTitleView = UITextView()
        
        eventTitleView.frame = CGRectMake(0.053125*cell.contentView.frame.width, 0.520833*cell.contentView.frame.height, 0.665625*cell.contentView.frame.width, 0.25*cell.contentView.frame.height)
        
        
        
        eventTitleView.text = eventTitleText
        eventTitleView.frame.size.height = eventTitleView.sizeThatFits(eventTitleView.bounds.size).height
        eventTitleView.textColor = UIColor.whiteColor()
        eventTitleView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        eventTitleView.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)
        eventTitleView.editable = false
        
        cell.contentView.addSubview(eventTitleView)
        
        if eventTitleView.contentSize.height > eventTitleView.frame.height
        {
            eventTitleView.font = UIFont(name: "AvenirNext-Demibold", size: 18.0)
        }
        
        
        let senderNameView = UITextView()
        
        senderNameView.frame = CGRectMake(0.053125*cell.contentView.frame.width, eventTitleView.frame.origin.y+eventTitleView.frame.height, 0.665625*cell.contentView.frame.width, 0.11*cell.contentView.frame.height)
        
        
        print(eventTitleView.frame.height)
        
        senderNameView.text = mySharedEvents[row]["senderName"] as! String
        senderNameView.textColor = UIColor.whiteColor()
        senderNameView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        senderNameView.font = UIFont(name: "AvenirNext-Medium", size: 14.0)
        senderNameView.editable = false
        
        cell.contentView.addSubview(senderNameView)
        
        
        var pdate: NSDate!
        
        let eventDateView = UITextView()
        
        if mySharedEvents[row]["isRSVP"] as! Bool == true
        {
            
            pdate = mySharedEvents[row]["eventStartDateTime"] as! NSDate
            
            
            
            print(pdate)
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: pdate!)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            
            let startDate = "\(monthsArray[smonth-1]) \(sday), \(shour):\(sminute) \(sam)"
            
            print("senderName height: \(senderNameView.frame.height)")
            
            eventDateView.frame = CGRectMake(0.053125*cell.contentView.frame.width, senderNameView.frame.origin.y+senderNameView.frame.height - 7 , 0.665625*cell.contentView.frame.width, 0.11*cell.contentView.frame.height)
            
            eventDateView.text = startDate
            eventDateView.textColor = UIColor.whiteColor()
            eventDateView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
            eventDateView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            eventDateView.editable = false
            
            cell.contentView.addSubview(eventDateView)
        }
        else
        {
            //var eventTypeView = UITextView()
            
            print("senderName height: \(senderNameView.frame.height)")
            
            eventDateView.frame = CGRectMake(0.053125*cell.contentView.frame.width, senderNameView.frame.origin.y+senderNameView.frame.height, 0.665625*cell.contentView.frame.width, 0.11*cell.contentView.frame.height)
            
            eventDateView.text = "Online Event"
            eventDateView.textColor = UIColor.whiteColor()
            eventDateView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
            eventDateView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            eventDateView.editable = false
            
            cell.contentView.addSubview(eventDateView)
            
        }
        
        let loaderCellView = UIView()
        
        let cellIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderCellView.addSubview(cellIndicator)
        
        loaderCellView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height+(1*self.view.frame.height/568))
        
        loaderCellView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        
        cellIndicator.frame = CGRectMake((cell.contentView.frame.width/2)-(cell.contentView.frame.width*(25/320)/2), (cell.contentView.frame.height/2)-(cell.contentView.frame.width*(25/320)/2), cell.contentView.frame.width*(25/320), cell.contentView.frame.width*(25/320))
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Downloading..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name:"AvenirNext-Regular", size: 9.0)
        loadingMessage.textAlignment = .Center
        loaderCellView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , cellIndicator.frame.origin.y+(cell.contentView.frame.width*(25/320)+10), cell.contentView.frame.width, 20)
        cellIndicator.startAnimating()
        
        cell.contentView.addSubview(loaderCellView)
        
        let attendingStatusImageView = UIImageView()
        
        attendingStatusImageView.frame = CGRectMake(0.70875*cell.contentView.frame.width+((0.20*cell.contentView.frame.width/2)-(0.0625*cell.contentView.frame.width/2)), (0.520833*cell.contentView.frame.height)+10, 0.0625*cell.contentView.frame.width, 0.0625*cell.contentView.frame.width)
        
        if  mySharedEvents[row]["attendingStatus"] as! String == "yes"
        {
            attendingStatusImageView.image = UIImage(named:"check-green.png")
        }
        
        if  mySharedEvents[row]["attendingStatus"] as! String == "no"
        {
            attendingStatusImageView.image = UIImage(named:"check-red.png")
        }
        
        if  mySharedEvents[row]["attendingStatus"] as! String == "maybe"
        {
            attendingStatusImageView.image = UIImage(named:"questionmark.png")
        }
        
        if  mySharedEvents[row]["attendingStatus"] as! String == "online"
        {
            attendingStatusImageView.image = UIImage(named:"web-globe.png")
        }
        
        if  mySharedEvents[row]["isRSVP"] as! Bool == false
        {
            attendingStatusImageView.image = UIImage(named:"web-globe.png")
        }
        
        cell.contentView.addSubview(attendingStatusImageView)
        
        
        let eventShareCount = UILabel()
        
        eventShareCount.frame = CGRectMake(0.70875*cell.contentView.frame.width, attendingStatusImageView.frame.origin.y+attendingStatusImageView.frame.height+20, 0.30*cell.contentView.frame.width, 0.087*cell.contentView.frame.height + 0.087*cell.contentView.frame.height)
        eventShareCount.lineBreakMode = NSLineBreakMode.ByWordWrapping
        eventShareCount.numberOfLines = 2
        
        print(0.10*cell.contentView.frame.width)
        
        
        if mySharedEvents[row]["isRSVP"] as! Bool == true
        {
            if  mySharedEvents[row]["isApproved"] as! Bool == true
            {
                
                let noOfNewPosts = mySharedEvents[row]["noOfNewPosts"] as! Int
                
                print("noOfNewPosts:\(noOfNewPosts)")
                
                if noOfNewPosts > 0
                {
                    eventShareCount.layer.masksToBounds = true
                    eventShareCount.layer.cornerRadius = 3.0
                    eventShareCount.text = "\(noOfNewPosts)"
                    eventShareCount.backgroundColor = UIColor.redColor()
                    eventShareCount.textColor = UIColor.whiteColor()
                    
                    if noOfNewPosts < 10
                    {
                        eventShareCount.frame.size.width = CGFloat(0.08333*cell.contentView.frame.height)
                    }
                    else if noOfNewPosts < 100
                    {
                        eventShareCount.frame.size.width = CGFloat(0.1*cell.contentView.frame.height)
                    }
                    else
                    {
                        eventShareCount.frame.size.width = CGFloat(0.13*cell.contentView.frame.height)
                    }
                    
                    eventShareCount.frame.origin.x = (0.70875*cell.contentView.frame.width)+((0.20*cell.contentView.frame.width/2)-(eventShareCount.frame.width/2))
                    
                    eventShareCount.frame.size.height = CGFloat(0.08333*cell.contentView.frame.height)
                }
                else
                {
                    eventShareCount.text = ""
                }
            }
            else
            {
                //eventShareCount.text = "Pending Approval"
                eventShareCount.text = ""
                eventShareCount.textColor = UIColor.lightGrayColor()
                eventShareCount.frame.size.height = 2*CGFloat(0.08333*cell.contentView.frame.height)
                eventShareCount.numberOfLines = 2
            }
            
        }
        else
        {
            if mySharedEvents[row]["isApproved"] as! Bool == false
            {
                eventShareCount.text = "Pending\nApproval"
                eventShareCount.textAlignment = .Center
                eventShareCount.textColor = UIColor.lightGrayColor()
                eventShareCount.frame.size.height = 2*CGFloat(0.08333*cell.contentView.frame.height)
                eventShareCount.numberOfLines = 2
            }
            else
            {
                let noOfNewPosts = mySharedEvents[row]["noOfNewPosts"] as! Int
                
                print("noOfNewPosts:\(noOfNewPosts)")
                
                if noOfNewPosts > 0
                {
                    eventShareCount.layer.masksToBounds = true
                    eventShareCount.layer.cornerRadius = 3.0
                    eventShareCount.text = "\(noOfNewPosts)"
                    eventShareCount.backgroundColor = UIColor.redColor()
                    eventShareCount.textColor = UIColor.whiteColor()
                    
                    if noOfNewPosts < 10
                    {
                        eventShareCount.frame.size.width = CGFloat(0.08333*cell.contentView.frame.height)
                    }
                    else if noOfNewPosts < 100
                    {
                        eventShareCount.frame.size.width = CGFloat(0.1*cell.contentView.frame.height)
                    }
                    else
                    {
                        eventShareCount.frame.size.width = CGFloat(0.13*cell.contentView.frame.height)
                    }
                    
                    eventShareCount.frame.origin.x = (0.70875*cell.contentView.frame.width)+((0.20*cell.contentView.frame.width/2)-(eventShareCount.frame.width/2))
                    
                    eventShareCount.frame.size.height = CGFloat(0.08333*cell.contentView.frame.height)
                }
                else
                {
                    eventShareCount.text = ""
                }
            }
        }
        
        eventShareCount.textAlignment = .Center
        
        eventShareCount.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        //attendingStatusImageView.frame.origin.x = eventShareCount.frame.origin.x + ((eventShareCount.frame.width/2)-(attendingStatusImageView.frame.width/2))
        
        eventShareCount.frame.origin.y = eventDateView.frame.origin.y + ((eventDateView.frame.height/2)-(eventShareCount.frame.height/2))
        
        eventShareCount.frame.origin.x = attendingStatusImageView.frame.origin.x + ((attendingStatusImageView.frame.width/2)-(eventShareCount.frame.width/2))
        
        
        cell.contentView.addSubview(eventShareCount)
        
        let eventCellOverlayView = UIView()
        
        eventCellOverlayView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height+(1*self.view.frame.height/568))
        
        eventCellOverlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        cell.contentView.addSubview(eventCellOverlayView)
        
        
        let eventImageFile = mySharedEvents[row]["eventImage"] as! String
        
        let eventFolder = mySharedEvents[row]["eventFolder"] as! String
        
        let eventImagePath = "\(documentDirectory)/\(eventImageFile)"
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath(eventImagePath)) {
            let image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
            eventImageView.image = image
            loaderCellView.hidden = true
        }
        else
        {
            
            let isDownloading = mySharedEvents[row]["isDownloading"] as! Bool
            print("isDownloading for \(row) is \(isDownloading)")
            
            if mySharedEvents[row]["isDownloading"] as! Bool == true
            {
                mySharedEvents[row]["isDownloading"] = false
                
                let s3BucketName = "eventnodepublicpics"
                let fileName = eventImageFile
                
                let downloadFilePath = "\(documentDirectory)/\(fileName)"
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
                
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                print("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            print("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        print("downloading successfull")
                        
                        let image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
                        eventImageView.image = image
                        loaderCellView.hidden = true
                        
                    }
                    
                    return nil
                    
                })
            }
        }
        
        
        /* var eventOriginalImageFile = mySharedEvents[row]["originalEventImage"] as! String
        
        var eventOriginalImagePath = "\(documentDirectory)/\(eventOriginalImageFile)"
        let managerOriginal = NSFileManager.defaultManager()
        
        if (managerOriginal.fileExistsAtPath(eventOriginalImagePath) != true)
        {
        let s3OriginalBucketName = "eventnodepublicpics"
        let fileOriginalName = eventOriginalImageFile
        
        let downloadOriginalFilePath = documentDirectory.stringByAppendingPathComponent(fileOriginalName)
        let downloadingOriginalFileURL = NSURL.fileURLWithPath(downloadOriginalFilePath)
        
        let downloadOriginalRequest = AWSS3TransferManagerDownloadRequest()
        downloadOriginalRequest.bucket = s3OriginalBucketName
        downloadOriginalRequest.key  = "\(eventFolder)\(fileOriginalName)"
        downloadOriginalRequest.downloadingFileURL = downloadingOriginalFileURL
        
        let transferOriginalManager = AWSS3TransferManager.defaultS3TransferManager()
        transferOriginalManager.download(downloadOriginalRequest).continueWithBlock {
        (task: AWSTask!) -> AnyObject! in
        
        if task.error != nil {
        println("Error downloading")
        println(task.error.description)
        }
        else{
        
        }
        return nil
        }
        
        }*/
        
        cell.selectionStyle = .None
        
        return cell
    }
    
    
    /*func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    
    }*/
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        currentSharedEvent = mySharedEvents[indexPath.row]
        
        mySharedEventData = []
        
        if((mySharedEvents[indexPath.row]["isApproved"] as! Bool) == true || (mySharedEvents[indexPath.row]["isRSVP"] as! Bool) == true)
        {
            let streamVC = self.storyboard!.instantiateViewControllerWithIdentifier("StreamViewController") as! StreamViewController
            
            self.navigationController?.pushViewController(streamVC, animated: true)
        }
        else
        {
            if mySharedEvents[indexPath.row]["isApproved"] as! Bool == false
            {
                Util.invokeAlertMethod("", strBody: "This is a private online event and the host needs to approve you before you can get access to it.", delegate: nil)
            }
        }
    }
    
    
    func createInvitation()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["isApproved"] = "1"
        tblFields["isUpdated"] = "1"
        tblFields["isEventUpdated"] = "1"
        
        tblFields["objectId"] = "ewrewrer"
        tblFields["invitedName"] = "rewrewrewrer"
        tblFields["userObjectId"] = "retyrgrhyr"
        tblFields["eventObjectId"] = "jkffgregrt"
        tblFields["emailId"] = "ewfergtee@fertre.tewr"
        tblFields["attendingStatus"] = "yes"
        
        tblFields["invitationType"] = "email"
        tblFields["noOfChilds"] = "3"
        tblFields["noOfAdults"] = "2"
        tblFields["invitationNote"] = "ergewgtgergrg"
        
        tblFields["needsContentApprovel"] = "0"
        tblFields["createdAt"] = "2015-12-08 22:23:54"
        
        tblFields["updatetAt"] = "2015-12-08 22:23:54"
        
        tblFields["isPosted"] = "1"
        
        
        var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
    }
    
    
    @IBAction func invitationCodeButtonClicked(sender: UIButton) {
        
        
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteCodeViewController") as! InviteCodeViewController
        eventVC.backOrPop = false
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func streamButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let streamVC = self.storyboard!.instantiateViewControllerWithIdentifier("StreamViewController") as! StreamViewController
        self.navigationController?.pushViewController(streamVC, animated: true)
    }
    
    
    @IBAction func eventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewControllerOne") as! settingViewControllerOne
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
}
