//
//  Util.swift
//  DemoProject
//
//  Created by Krupa-iMac on 24/07/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

class Util: NSObject
{
    
    class func getPath(fileName: String) -> String
    {
        print("\(NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0])/\(fileName)")
        
        return "\(NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0])/\(fileName)"
        
//        let dirPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask, true)
//        
//        let docsDir = "\(dirPaths[0] )/\(fileName)"
//        
//        print(docsDir)
//        
//        return docsDir
        
        
        
    }
    
    class func copyFile(fileName: NSString)
    {
        let dbPath: String = getPath(fileName as String)
        print(dbPath)
        
        let fileManager = NSFileManager.defaultManager()
        if !fileManager.fileExistsAtPath(dbPath)
        {
            
            print(fileName as String)
            
            let fromPath = "\(documentDirectory)/\(fileName as String)"
            
           // let fromPath = NSBundle.mainBundle().pathForResource(fileName as String, ofType:"html")
            
            var error : NSError?
            
            print(fromPath)
             print(dbPath)
            
            let docsPath = NSBundle.mainBundle().resourcePath!
            print(docsPath)
            let fileManager = NSFileManager.defaultManager()
            print(fileManager)
            let docsArray: [AnyObject]?
            do {
                docsArray = try fileManager.contentsOfDirectoryAtPath(docsPath)
            } catch let error1 as NSError {
                error = error1
                docsArray = nil
            }
            
            print(docsArray!)
            
            
            do {
                print(fromPath)
                print(dbPath)
                
                try fileManager.copyItemAtPath(fromPath, toPath: dbPath)
                
                
            } catch let error1 as NSError {
                error = error1
            }
            let alert: UIAlertView = UIAlertView()
            if (error != nil) {
                alert.title = "Error Occured"
                alert.message = error?.localizedDescription
                alert.delegate = nil
                alert.addButtonWithTitle("Ok")
                alert.show()
            }
            else
            {
                alert.title = "Successfully Copy"
                alert.message = "Your database copy successfully"
            }
        }
    }
    
    class func invokeAlertMethod(strTitle: NSString, strBody: NSString, delegate: AnyObject?)
    {
        let alert: UIAlertView = UIAlertView()
        alert.message = strBody as String
        alert.title = strTitle as String
        alert.delegate = delegate
        alert.addButtonWithTitle("Ok")
        alert.show()
    }
}