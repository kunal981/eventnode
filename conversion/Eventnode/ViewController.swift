//
//  ViewController.swift
//  TipCalculator
//
//  Created by mrinal khullar on 4/22/15.
//  Copyright (c) 2015 TBI LLC. All rights reserved.
//

import UIKit
import MobileCoreServices

var isFacebookLogin: Bool = false
var hasPassword: Bool = false

var facebookFriends: Array<NSDictionary!> = []

var showInviteCodePopup: Bool = false

class ViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var facebookLoginButton : UIButton!
    //@IBOutlet var facebookSignupButton : UIButton!
    
    @IBOutlet weak var registerWithemail: UIButton!
    //@IBOutlet var facebookSignupLabel : UILabel!
    @IBOutlet var tAndCText : UITextView!
    //@IBOutlet var loginView : UIView!
    
    @IBOutlet var loaderView : UIView!
    
    @IBOutlet var loaderSubView : UIView!
    
    @IBOutlet var wakeUpView : UIImageView!
    var resultdict = NSDictionary()
    var nsdefault = NSUserDefaults.standardUserDefaults()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //var user: PFUser = PFUser.currentUser()!
        
        //println(user["fullUserName"] as! String)
        
        self.view.addSubview(wakeUpImageView)
        
        facebookLoginButton.hidden=true
        //facebookSignupButton.hidden=true
        //facebookSignupLabel.hidden=true
        tAndCText.hidden=true
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        //indicator.frame = (loaderSubView.frame.width/2) - (indicator.frame.width/2)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        self.wakeUpView.hidden = false
        self.loaderView.hidden = false
        
        //registerWithemail.backgroundColor = UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0)
        
        
        // TODO(geetikak): What does isNormalLogin signify ?
        if let isNormalLogin = NSUserDefaults.standardUserDefaults().objectForKey("isNormalLogin") as? String
        {
            var isLoggedIn = "No"
            
            if let loggedInStatus = NSUserDefaults.standardUserDefaults().objectForKey("isLoggedIn") as? String
            {
                isLoggedIn = loggedInStatus
            }
            
            if isNormalLogin == "Yes" && isLoggedIn == "Yes"
            {
                var email = ""
                var password = ""
                
                if let savedEmail = NSUserDefaults.standardUserDefaults().objectForKey("email") as? String
                {
                    email = savedEmail
                }
                
                if let savedPassword = NSUserDefaults.standardUserDefaults().objectForKey("password") as? String
                {
                    password = savedPassword
                }
                
                doNormalLogin(email, password: password)
            }
            else
            {
                // TODO(geetikak): what is the point of calling loginAutomatically ? It takes care of only FB user.
                // but this code path is reachable when isNormalLogin is true and isLoggedIn is false.
                // Is that a scenario that we need to handle ?
                loginAutomaticaly()
            }
        }
        else
        {
            loginAutomaticaly()
        }
        
        
        
        
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            var imag = UIImagePickerController()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func viewTapped(sender : AnyObject) {
        //totalTextField.resignFirstResponder()
    }
    
    func doNormalLogin(email: String, password: String)
    {
        PFUser.logInWithUsernameInBackground(email, password: password)
            {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user
                {
                    
                    hasPassword = true
                    
                    print("user found")
                    
                    print(user["emailVerified"] as! Bool)
                    
                    NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                    
                    
                    NSUserDefaults.standardUserDefaults().setObject(email, forKey: "email")
                    NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                    NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isNormalLogin")
                    NSUserDefaults.standardUserDefaults().setObject(password, forKey: "password")
                    NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                    
                    
                    let s3BucketName = "eventnodepublicpics"
                    let fileName = "profilePic.png"
                    
                    let downloadFilePath = "\(documentDirectory)/\(fileName)"
                    let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                    
                    let downloadRequest = AWSS3TransferManagerDownloadRequest()
                    downloadRequest.bucket = s3BucketName
                    print("\(user.objectId!)/profilePic/profilePic.png")
                    downloadRequest.key  = "\(user.objectId!)/profilePic/profilePic.png"
                    downloadRequest.downloadingFileURL = downloadingFileURL
                    
                    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                    
                    
                    transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                        
                        if (task.error != nil){
                            if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                                switch (task.error.code) {
                                case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                    break;
                                case AWSS3TransferManagerErrorType.Paused.rawValue:
                                    break;
                                    
                                default:
                                    print("error downloading")
                                    break;
                                }
                            } else {
                                // Unknown error.
                                print("error downloading")
                            }
                        }
                        
                        if (task.result != nil) {
                            
                            print("downloading successfull")
                            
                        }
                        
                        return nil
                        
                    })
                    
                    
                    isFacebookLogin = user["isFacebookLogin"] as! Bool
                    
                    // TODO(geetikak): Should we log a user logged in metric here ?
                    
                    self.facebookLoginButton.hidden=false
                    self.tAndCText.hidden=false
                    
                    if isFacebookLogin
                    {
                        self.fetchFacebookFriends()
                    }
                    
                    
                    self.updateDeviceToken(user.objectId!)
                    
                    self.navigateToNextScreen(user["fullUserName"] as! String)
                    
                    print("\(PFUser.currentUser())")
                }
                else
                {
                    self.loaderView.hidden = true
                    self.wakeUpView.hidden = true
                    self.facebookLoginButton.hidden=false
                    self.tAndCText.hidden=false
                    print("Could not login");
                }
        }
    }
    
    func checkExistance(email: String, facebookId: String, firstName: String, lastName: String, userObject: PFUser, isSignedUp: Bool )
    {
        print(email)
        let query = PFUser.query()
        query!.whereKey("email", equalTo: email)
        query!.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            print(users!.count)
            if let users = users as? [PFObject]
            {
                print(users)
                if users.count == 0 || self.isValidEmail(userObject.username!)
                {
                    let query = PFQuery(className: "LinkedAccounts")
                    query.whereKey("email", equalTo: email)
                    query.findObjectsInBackgroundWithBlock {
                        (users: [AnyObject]?, error: NSError?) -> Void in
                        
                        if let users = users as? [PFObject]
                        {
                            print(users)
                            if users.count == 0
                            {
                                let fbId = facebookId
                                print("\(fbId)")
                                
                                
                                let emailData = email
                                
                                let usernameData = firstName
                                let lastnameData = lastName
                                
                                
                                let s3BucketName = "eventnodepublicpics"
                                let fileName = "profilePic.png"
                                
                                let downloadFilePath = "\(documentDirectory)/\(fileName)"
                                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                                
                                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                                downloadRequest.bucket = s3BucketName
                                print("\(userObject.objectId!)/profilePic/profilePic.png")
                                downloadRequest.key  = "\(userObject.objectId!)/profilePic/profilePic.png"
                                downloadRequest.downloadingFileURL = downloadingFileURL
                                
                                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                                
                                
                                transferManager.download(downloadRequest).continueWithBlock {
                                    (task: AWSTask!) -> AnyObject! in
                                    
                                    if task.error != nil
                                    {
                                        print("Error downloading")
                                        print(task.error.description)
                                        
                                        let url:NSURL = NSURL(string:"https://graph.facebook.com/\(fbId)/picture?type=square")!
                                        let data:NSData = NSData(contentsOfURL: url)!
                                        
                                        
                                        let fileManager = NSFileManager.defaultManager()
                                        
                                        let filePathToWrite = "\(documentDirectory)/profilePic.png"
                                        
                                        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
                                        
                                        self.uploadProfilePic()
                                        
                                    }
                                    else
                                    {
                                        print("successfull")
                                    }
                                    
                                    return nil
                                }
                                
                                let user: PFUser = userObject
                                
                                user["isFacebookLogin"] =  true
                                user["facebookId"] =  fbId
                                
                                if let hasSetPassword = user["hasPassword"] as? Bool
                                {
                                    user["hasPassword"] =  hasSetPassword
                                    hasPassword = hasSetPassword
                                    if !hasPassword
                                    {
                                        user.email = emailData
                                        user.username = emailData
                                    }
                                }
                                else
                                {
                                    user["hasPassword"] =  false
                                    hasPassword = false
                                    user.email = emailData
                                    user.username = emailData
                                }
                                
                                
                                // user["emailVerified"] = true
                                //println("\(isFacebookLogin)")
                                
                                print("\(user.email)")
                                
                                user["fullUserName"] = "\(usernameData) \(lastnameData)"
                                
                                user.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                                    if success
                                    {
                                        if isSignedUp
                                        {
                                            self.updateDeviceToken(user.objectId!)
                                            
                                            let localNotification = UILocalNotification()
                                            
                                            localNotification.fireDate = NSDate()
                                            
                                            localNotification.alertBody = "Welcome to eventnode, we love that you chose us. Don’t wait for a special occasion, everyday can be an event with eventnode."
                                            
                                            // localNotification.applicationIconBadgeNumber = 100
                                            
                                            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                                        }
                                        
                                        self.setFacebookUser(user, usernameData: usernameData, lastnameData: lastnameData, emailData: emailData)
                                        
                                    }
                                    else
                                    {
                                        self.loaderView.hidden = true
                                        self.wakeUpView.hidden = true
                                        
                                        print(error?.localizedDescription)
                                    }
                                })
                            }
                            else
                            {
                                self.deleteUser(userObject, userToLogin: users[0] as! PFUser, usernameData: firstName, lastnameData: lastName, emailData: email)
                            }
                        }
                    }
                    
                    
                }
                else
                {
                    self.deleteUser(userObject, userToLogin: users[0] as! PFUser, usernameData: firstName, lastnameData: lastName, emailData: email)
                }
            }
            else
            {
                self.loaderView.hidden = true
                self.wakeUpView.hidden = true
            }
            
        }
    }
    
    func deleteUser(user: PFUser, userToLogin: PFUser, usernameData: String, lastnameData: String, emailData: String)
    {
        print(user)
        
        PFFacebookUtils.unlinkUserInBackground(user, block: { (success:Bool, error:NSError?) -> Void in
            print("deleted")
            
            /*if(userToLogin["isFacebookLogin"] as? Bool == true)
            {
            if var hasSetPassword = userToLogin["hasPassword"] as? Bool
            {
            userToLogin["hasPassword"] =  hasSetPassword
            hasPassword = hasSetPassword
            }
            else
            {
            userToLogin["hasPassword"] =  false
            hasPassword = false
            }`
            self.setFacebookUser(userToLogin, usernameData: usernameData, lastnameData: lastnameData, emailData: emailData)
            }
            else
            {*/
            if #available(iOS 8.0, *) {
                let refreshAlert = UIAlertController(title: "Error", message: "There's an existing Eventnode account using your facebook email address. Either link your facebook account with that Eventnode account or change the email address in that Eventnode account.", preferredStyle: UIAlertControllerStyle.Alert)
                
                
                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                    self.loaderView.hidden=true
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
            else
            {
                
            }
            /*}*/
        });
    }
    
    
    func setFacebookUser(user: PFUser, usernameData: String, lastnameData: String, emailData: String)
    {
        isFacebookLogin = true
        //hasPassword = (user["hasPassword"] as? Bool)!
        
        print("data is updated")
        
        NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
        NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
        NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
        NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
        
        if hasPassword
        {
            NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
            NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setObject(emailData, forKey: "email")
            NSUserDefaults.standardUserDefaults().setObject("\(usernameData) \(lastnameData)", forKey: "fullUserName")
        }
        NSUserDefaults.standardUserDefaults().setObject(emailData, forKey: "facebookEmail")
        NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "facebookName")
        
        self.fetchFacebookFriends()
        
        self.updateDeviceToken(user.objectId!)
        
        self.navigateToNextScreen(user["fullUserName"] as! String)
    }
    
    func navigateToNextScreen(userName: String) {
        countMyEvents()
    }
    
    func countMyEvents() {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            
            let myEventsPredicate = NSPredicate(format: "eventCreatorObjectId = '\(currentUserId)'")
            
            let myEventsQuery = PFQuery(className:"Events", predicate: myEventsPredicate)
            
            myEventsQuery.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(myEventsQuery, target: self, successSelector: "fetchMyEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchMyEventsError:", errorSelectorParameters:nil)
        }
    }
    
    func fetchMyEventsSuccess(timer:NSTimer) {
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if (objects!.count > 0) {
            
            self.loaderView.hidden = true
            self.wakeUpView.hidden = true
            
            let myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            self.navigationController?.pushViewController(myEventsVC, animated: false)
        } else {
            countMySharedEvents()
        }
    }
    
    func fetchMyEventsError(timer:NSTimer) {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        print("Unable to fetch myEvents count. What to do here ?")
        checkOfflineData()
    }
    
    func countMySharedEvents() {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            let mySharedEventsPredicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
            
            let mySharedEventsQuery = PFQuery(className:"Invitations", predicate: mySharedEventsPredicate)
            
            mySharedEventsQuery.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(mySharedEventsQuery, target: self, successSelector: "fetchMySharedEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchMySharedEventsError:", errorSelectorParameters:nil)
        }
    }
    
    func fetchMySharedEventsSuccess(timer:NSTimer) {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        self.loaderView.hidden = true
        self.wakeUpView.hidden = true
        
        if (objects!.count > 0) {
            let mySharedEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
            self.navigationController?.pushViewController(mySharedEventsVC, animated: false)
        } else {
            print("Navigating to OnboardingVC", terminator: "")
            let onboardingVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
            //onboardingVC.firstName = userName
            self.navigationController?.pushViewController(onboardingVC, animated: false)
        }
    }
    
    func fetchMySharedEventsError(timer:NSTimer) {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        print("Unable to fetch mySharedEvents count. What to do here ?")
        checkOfflineData()
    }
    
    
    func checkOfflineData()
    {
        self.loaderView.hidden = true
        self.wakeUpView.hidden = true
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            let resultSetEventCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "eventCreatorObjectId = ? ", whereFields: [currentUserId])
            
            resultSetEventCount.next()
            
            let totalEventCount = Int(resultSetEventCount.intForColumn("count"))
            resultSetEventCount.close()
            
            if totalEventCount > 0
            {
                let myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                self.navigationController?.pushViewController(myEventsVC, animated: false)
            }
            else
            {
                let resultSetSharedEventCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "objectId IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = ?  )", whereFields: [currentUserId])
                
                resultSetSharedEventCount.next()
                
                let totalSharedEventCount = Int(resultSetSharedEventCount.intForColumn("count"))
                resultSetSharedEventCount.close()
                if totalSharedEventCount > 0 {
                    let mySharedEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
                    self.navigationController?.pushViewController(mySharedEventsVC, animated: false)
                } else {
                    print("Navigating to OnboardingVC", terminator: "")
                    let onboardingVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
                    //onboardingVC.firstName = userName
                    self.navigationController?.pushViewController(onboardingVC, animated: false)
                }
            }
            
        }
    }
    
    
    func loginAutomaticaly()
    {
        
        let accessToken = FBSDKAccessToken.currentAccessToken() // Use existing access token.
        
        if(accessToken != nil)
        {
            print(accessToken)
            PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken, block: {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user {
                    let user_id = user.objectId!
                    print("\(user_id)")
                    
                    if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
                    {
                        print(currentUserId)
                        if let isLoggedIn = NSUserDefaults.standardUserDefaults().objectForKey("isLoggedIn") as? String
                        {
                            print(isLoggedIn)
                            if isLoggedIn == "Yes"
                            {
                                isFacebookLogin = true
                                
                                // TODO(geetikak): Confirm this is always existing user and remove the isNew condition.
                                if (user.isNew) {
                                    AnalyticsModel.instance.identifyNewUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                                } else {
                                    AnalyticsModel.instance.identifyExistingUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                                }
                                
                                if let password = user["hasPassword"] as? Bool
                                {
                                    // Why would FB user have password ? It's a feature. You can turn it on in EventNode Settings.
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                                    NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
                                    NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
                                    NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                                    
                                    hasPassword = password
                                    
                                    self.fetchFacebookFriends()
                                    
                                    self.updateDeviceToken(user.objectId!)
                                    
                                    self.navigateToNextScreen(user["fullUserName"] as! String)
                                }
                                else
                                {
                                    
                                    let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                                    
                                    let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                                        if error != nil {
                                            print(error)
                                        } else
                                        {
                                            
                                            self.resultdict = result as! NSDictionary
                                            
                                            let fbId = self.resultdict.valueForKey("id") as! String
                                            print("\(fbId)")
                                            print(self.resultdict)
                                            
                                            
                                            let emailData = self.resultdict.valueForKey("email") as! String
                                            
                                            let usernameData = self.resultdict.valueForKey("first_name") as! String
                                            let lastnameData = self.resultdict.valueForKey("last_name") as! String
                                            
                                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user ,isSignedUp: false)
                                            
                                        }
                                    })
                                    
                                    connection.start()
                                    
                                }
                                
                            }
                        }
                    }
                    self.facebookLoginButton.hidden=false
                    //self.facebookSignupButton.hidden=false
                    //self.facebookSignupLabel.hidden=false
                    self.tAndCText.hidden=false
                    
                    /*let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(homeVC, animated: true)*/
                    
                } else {
                    print("Uh oh. There was an error logging in.")
                    self.loaderView.hidden = true
                    self.wakeUpView.hidden = true
                    self.facebookLoginButton.hidden=false
                    //self.facebookSignupButton.hidden=false
                    //self.facebookSignupLabel.hidden=false
                    self.tAndCText.hidden=false
                }
            })
        }
        else
        {
            self.loaderView.hidden = true
            self.wakeUpView.hidden = true
            self.facebookLoginButton.hidden=false
            //facebookSignupButton.hidden=false
            //facebookSignupLabel.hidden=false
            self.tAndCText.hidden=false
        }
        
    }
    
    
    func updateDeviceToken(userObjectId: String!)
    {
        let installation = PFInstallation.currentInstallation()
        
        let user: PFUser = PFUser.currentUser()!
        
        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = user["inviteNotification"] as! Bool
        installation["hostActivityNotification"] = user["inviteNotification"] as! Bool
        installation["guestActivityNotification"] = user["inviteNotification"] as! Bool
        
        installation.saveInBackground()
    }
    
    
    func updateNotificationSettingsWithDeviceToken(userObjectId: String!)
    {
        let installation = PFInstallation.currentInstallation()
        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = true
        installation["hostActivityNotification"] = true
        installation["guestActivityNotification"] = true
        
        let user: PFUser = PFUser.currentUser()!
        
        user["inviteEmail"] = true
        user["hostActivityEmail"] = true
        user["guestActivityEmail"] = true
        user["inviteNotification"] = true
        user["hostActivityNotification"] = true
        user["guestActivityNotification"] = true
        
        installation.saveInBackground()
        user.saveInBackground()
        
    }
    
    
    
    
    
    @IBAction func loginButtonClicked(sender : AnyObject){
        
        /*let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)*/
        //NSLog("sdd")
        self.wakeUpView.hidden = true
        self.loaderView.hidden = false
        
        
        let accessToken = FBSDKAccessToken.currentAccessToken() // Use existing access token.
        
        if(accessToken != nil)
        {
            // TODO(geetikak): Understand why there would be an access token at this point ?
            // If we did, we should have been automatically logged in.
            print(accessToken)
            PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken, block: {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user {
                    let user_id = user.objectId!
                    print("\(user_id)")
                    //self.loaderView.hidden = true
                    //self.wakeUpView.hidden = true
                    if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
                    {
                        print(currentUserId)
                        
                        isFacebookLogin = true
                        
                        // TODO(geetikak): Confirm that this is always an existing user and cannot be a new FB User Signup.
                        if (user.isNew) {
                            AnalyticsModel.instance.identifyNewUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                        } else {
                            AnalyticsModel.instance.identifyExistingUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                        }
                        
                        //SupportModel.instance.initializeWithUsernameAndEmail((user["fullUserName"] as? String)!, email: user.email!)
                        
                        //pX2x64NbCa
                        if let password = user["hasPassword"] as? Bool
                        {
                            
                            NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                            NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
                            
                            NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
                            NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                            
                            NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                            NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
                            
                            hasPassword = password
                            
                            
                            self.fetchFacebookFriends()
                            
                            self.updateDeviceToken(user.objectId!)
                            
                            self.navigateToNextScreen(user["fullUserName"] as! String)
                        }
                        else
                        {
                            
                            
                            let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                            
                            let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                            connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                                if error != nil {
                                    print(error)
                                } else
                                {
                                    
                                    self.resultdict = result as! NSDictionary
                                    
                                    let fbId = self.resultdict.valueForKey("id") as! String
                                    print("\(fbId)")
                                    print(self.resultdict)
                                    
                                    
                                    let emailData = self.resultdict.valueForKey("email") as! String
                                    
                                    let usernameData = self.resultdict.valueForKey("first_name") as! String
                                    let lastnameData = self.resultdict.valueForKey("last_name") as! String
                                    
                                    self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user , isSignedUp: false)
                                    
                                }
                            })
                            
                            connection.start()
                            
                        }
                        
                    }
                    else
                    {
                        print("Uh oh. There was an error logging in.")
                        self.openFacebookLogin()
                    }
                    
                } else {
                    print("Uh oh. There was an error logging in.")
                    self.openFacebookLogin()
                }
            })
        }
        else
        {
            self.openFacebookLogin()
        }
        
    }
    
    func openFacebookLogin(){
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile", "email", "user_friends"], block: {
            (user: PFUser?, error: NSError?) -> Void in
            
            if let user = user {
                if user.isNew {
                    
                    
                    print("User signed up and logged in through Facebook!")
                    
                    self.updateNotificationSettingsWithDeviceToken(user.objectId!)
                    
                    showInviteCodePopup = true
                    
                    // TODO(geetikak): The next piece of code until connection.start() seems to be same for the else() condition. Refactor and Simplify.
                    let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                    
                    let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                        if error != nil {
                            //handle error
                            print(error)
                        } else
                        {
                            
                            self.resultdict = result as! NSDictionary
                            
                            let fbId = self.resultdict.valueForKey("id") as! String
                            print("\(fbId)")
                            print(self.resultdict)
                            
                            
                            let emailData = self.resultdict.valueForKey("email") as! String
                            
                            let usernameData = self.resultdict.valueForKey("first_name") as! String
                            let lastnameData = self.resultdict.valueForKey("last_name") as! String
                            
                            
                            
                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user, isSignedUp: true )
                            
                            // New FB User SignUp.
                            AnalyticsModel.instance.identifyNewUserForAnalytics(emailData, isFbUser: true, name: usernameData + " " + lastnameData, user: user)
                            
                            
                        }
                    })
                    connection.start()
                }
                else
                {
                    let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                    
                    let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                        if error != nil {
                            print(error)
                        } else
                        {
                            self.resultdict = result as! NSDictionary
                            
                            let fbId = self.resultdict.valueForKey("id") as! String
                            print("\(fbId)")
                            print(self.resultdict)
                            
                            
                            let emailData = self.resultdict.valueForKey("email") as! String
                            
                            let usernameData = self.resultdict.valueForKey("first_name") as! String
                            let lastnameData = self.resultdict.valueForKey("last_name") as! String
                            
                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user, isSignedUp:false)
                            
                            AnalyticsModel.instance.identifyExistingUserForAnalytics(emailData, isFbUser: true, name: usernameData + " " + lastnameData, user: user)
                        }
                    })
                    
                    connection.start()
                }
            }
            else
            {
                self.loaderView.hidden = true
                self.wakeUpView.hidden = true
                print("Uh oh. The user cancelled the Facebook login.")
                NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isLoggedIn")
            }
        })
    }
    
    
    
    
    func isValidEmail(emailid: NSString)->Bool
    {
        
        var isValid = true
        
        if !emailid.containsString(" ")
        {
            var atRateSplitArray = emailid.componentsSeparatedByString("@")
            
            if(atRateSplitArray.count>=2)
            {
                for component in atRateSplitArray
                {
                    if component == ""
                    {
                        isValid = false
                    }
                }
                
                if(isValid)
                {
                    let dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                    
                    if(dotSplitArray.count>=2)
                    {
                        for component in dotSplitArray
                        {
                            if component == ""
                            {
                                isValid = false
                            }
                        }
                    }
                    else
                    {
                        isValid = false
                    }
                }
            }
            else
            {
                isValid = false
            }
            
        }
        else
        {
            isValid = false
        }
        
        return isValid
    }
    
    
    func fetchFacebookFriends()
    {
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me/friends?fields=name,email", parameters: nil)
        
        let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
        connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
            if error != nil {
                //handle error
                print(error)
            } else
            {
                //println(result)
                facebookFriends = result.valueForKey("data") as! Array<NSDictionary!>
                print(facebookFriends)
            }
        })
        
        connection.start()
    }
    
    func uploadProfilePic()
    {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        let profilePicUrl = NSURL(fileURLWithPath: "\(documentDirectory)/profilePic.png")
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        
        
        
        
        let pickedImage = UIImage(named: "\(documentDirectory)/profilePic.png")
        
        //profileImageView.image = pickedImage
        
        let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(pickedImage!), 0.5)
        
        var error:NSErrorPointer = NSErrorPointer()
        
        let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        //NSFileManager.defaultManager().removeItemAtPath(documentDirectory.stringByAppendingPathComponent("profilePic.png"), error: error)
        uploadRequest.bucket = "eventnodepublicpics"
        uploadRequest.key =  "\(currentUserId)/profilePic/profilePic.png"
        uploadRequest.body = profilePicUrl
        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
        
        transferManager.upload(uploadRequest)
        
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage
    {
        
        if image.imageOrientation == UIImageOrientation.Up
        {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    /*
    {"facebook":
    {
    "access_token":"CAAJASOaPZBL0BAIG69wspO3vPxoWb6XlF6aAuux5x8ZBP4Hg821F15aQsjVIjd5S7teY9ViFhnrmdeM5BnBrusZCc9z5DpZBejC2WtdxsejEiFniaz8meGMHycSpcAVM8rLbGtBZCguHQfPwnahXfyISKgzKuRlCCkq6RDnn3pCT10ED3fr2HOtkLNXNmkW7IJELjzTZBxZCAME8HqwLEUBVwBDQTubbR31Tbby0cd1x0YZAwj1w1m7j","expiration_date":"2015-09-25T08:27:26.453Z","id":"1648646342030855"
    }
    }
    */
    
    @IBAction func termsAndConditionsButtonClicked(sender : AnyObject)
    {
        let termsAndCondition = self.storyboard!.instantiateViewControllerWithIdentifier("TermsAndConditions") as! TermsAndConditionViewController
        self.navigationController?.pushViewController(termsAndCondition, animated: false)
    }
    
    
    @IBAction func registerWithEmail(sender: AnyObject)
    {
        let registerView = self.storyboard!.instantiateViewControllerWithIdentifier("RegistrationView") as! RegistrationViewController
        self.navigationController?.pushViewController(registerView, animated: true)
    }
    
    @IBAction func LoginView(sender: AnyObject)
    {
        let logInView = self.storyboard!.instantiateViewControllerWithIdentifier("logInWithEmail") as! LogInWithEmailViewController
        print("log in button pressed")
        self.navigationController?.pushViewController(logInView, animated: true)
    }
}

