//
//  SearchAndInviteContactsViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 7/15/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

import AddressBookUI
import AddressBook
import CoreFoundation

class SearchAndInviteContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate  {
    
    
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet var emailContactsHeading: UITextView!
    @IBOutlet weak var emailContactsTable: UITableView!
    
    @IBOutlet weak var searchContactField: UITextField!
    @IBOutlet weak var sendEmailField: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var headerView: UIView!
    
    var contactLists: Array<NSDictionary>!
    var contactEmails: Array<String>!
    var contactEmailDetails: Array<NSDictionary>!
    
    var currentUserId: String!
    
    
    var unRegisteredContacts: Array<NSDictionary>!
    var unRegisteredContactsOriginal: Array<NSDictionary>!
    
    var unRegisteredInviteStatus: Array<Bool>!
    var unRegisteredInviteStatusOriginal: Array<Bool>!
    
    var userEmails = [String]()
    var userPrimaryEmail = ""
    
    var noOfContacts = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        self.view.addSubview(wakeUpImageView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        var loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        loadingMessage.textAlignment = .Center
        loaderSubView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        self.loaderView.hidden = false
        
        
        unRegisteredContacts = []
        unRegisteredInviteStatus = []
        
        unRegisteredContactsOriginal = []
        unRegisteredInviteStatusOriginal = []
        
        
        //eventnodeContactsTable.separatorColor = UIColor.clearColor()
        emailContactsTable.separatorColor = UIColor.clearColor()
        //self.navigationController?.popViewControllerAnimated(true)
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        userPrimaryEmail = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        
        let predicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
        
        let query = PFQuery(className: "LinkedAccounts", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchLinkedEmailSuccess:", successSelectorParameters: nil, errorSelector: "fetchLinkedEmailError:", errorSelectorParameters: nil)
        
        //x IN {1, 2, 3}
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchLinkedEmailSuccess(timer: NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if let fetchedobjects = objects {
            
            userEmails = []
            
            for object in fetchedobjects
            {
                userEmails.append(object["emailId"] as! String)
            }
            
            userEmails.append(userPrimaryEmail)
            
            getAddressBookNames()
        }
    }
    
    func fetchLinkedEmailError(timer: NSTimer)
    {
        userEmails.append(userPrimaryEmail)
        getAddressBookNames()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return unRegisteredContacts.count
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        return 40
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        
        let row = indexPath.row
        
        var cellIdentifier: String! = ""
        
        
        cellIdentifier = "EmailContactsTableViewCell"
        let cell: EmailContactsTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? EmailContactsTableViewCell
        
        for view in cell!.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        let imageView = UIImageView()
        imageView.frame = CGRectMake(13, 13, 24, 24)
        imageView.image = unRegisteredContacts[row]["photo"] as? UIImage
        
        imageView.layer.masksToBounds = true;
        imageView.layer.cornerRadius = 12
        
        let nameLabel = UILabel()
        nameLabel.frame = CGRectMake(45, 10, self.view.frame.width-140, 20)
        //nameLabel.numberOfLines = 2
        nameLabel.text = unRegisteredContacts[row]["contactName"] as? String
        nameLabel.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        nameLabel.adjustsFontSizeToFitWidth = true
        
        let emailLabel = UILabel()
        emailLabel.frame = CGRectMake(45, 25, self.view.frame.width-140, 20)
        //emailLabel.numberOfLines = 2
        emailLabel.text = unRegisteredContacts[row]["email"] as? String
        emailLabel.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        emailLabel.textColor = UIColor(red: 155.0/255, green: 155.0/255, blue: 155.0/255, alpha: 1.0)
        emailLabel.adjustsFontSizeToFitWidth = true
        //adjustsFontSizeToFitWidth
        
        loaderView.hidden = true
        
        
        var isLoggedInEmail = false
        
        for email in userEmails
        {
            if unRegisteredContacts[row]["email"] as! String == email
            {
                isLoggedInEmail = true
            }
        }
        
        let sendButton = UIButton()
        if unRegisteredInviteStatus[row] || isLoggedInEmail
        {
            if isLoggedInEmail
            {
                sendButton.enabled = false
            }
            //
            sendButton.setTitle("Invited", forState: UIControlState.Normal)
            sendButton.setTitleColor(UIColor(red: 155.0/255, green: 155.0/255, blue: 155.0/255, alpha: 1.0), forState: UIControlState.Normal)
            sendButton.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
            
            sendButton.frame = CGRectMake(self.view.frame.width-100-sendButton.sizeThatFits(sendButton.bounds.size).width+80+10, 10, sendButton.sizeThatFits(sendButton.bounds.size).width, 20)
            
        }
        else
        {
            sendButton.setTitle("Invite", forState: UIControlState.Normal)
            sendButton.setTitleColor(UIColor(red: 68.0/255, green: 185.0/255, blue: 227.0/255, alpha: 1.0), forState: UIControlState.Normal)
            
            sendButton.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
            
            sendButton.frame = CGRectMake(self.view.frame.width-104-sendButton.sizeThatFits(sendButton.bounds.size).width+80+10, 10, sendButton.sizeThatFits(sendButton.bounds.size).width, 20)
        }
        
        
        if unRegisteredContacts[row]["type"] as! String == "user"
        {
            sendButton.addTarget(self, action:"sendInvitationToParseUser:",forControlEvents: UIControlEvents.TouchUpInside)
        }
        else
        {
            sendButton.addTarget(self, action:"sendInvitationToEmail:",forControlEvents: UIControlEvents.TouchUpInside)
        }
        
        
        sendButton.tag = row
        
        cell?.contentView.addSubview(imageView)
        cell?.contentView.addSubview(nameLabel)
        cell?.contentView.addSubview(emailLabel)
        cell?.contentView.addSubview(sendButton)
        
        //cell?.textLabel!.text = unRegisteredContacts[row]["contactName"] as? String
        
        cell?.selectionStyle = .None
        
        return cell!
        
    }
    
    
    func sendInvitationToParseUser(sender: UIButton)
    {
        print("parse")
        
        sender.enabled = false
        
        if !unRegisteredInviteStatus[sender.tag]
        {
            let inviteObject: PFObject = PFObject(className: "Invitations")
            
            let userObjectId = unRegisteredContacts[sender.tag]["userObjectId"] as! String
            let email = unRegisteredContacts[sender.tag]["email"] as! String
            
            inviteObject["invitedName"] = unRegisteredContacts[sender.tag]["contactName"] as? String
            inviteObject["eventObjectId"] = currentEvent.objectId!
            inviteObject["isApproved"] = true
            inviteObject["userObjectId"] = userObjectId
            inviteObject["emailId"] = email
            inviteObject["attendingStatus"] = ""
            inviteObject["invitationType"] = "email"
            
            inviteObject["isUpdated"] = false
            inviteObject["noOfChilds"] = 0
            inviteObject["noOfAdults"] = 0
            inviteObject["invitationNote"] = ""
            inviteObject["isEventUpdated"] = false
            
            inviteObject["isEventStreamUpdated"] = false
            inviteObject["isTextUpdated"] = false
            
            
            print(inviteObject["invitedName"])
            
            var suppliedParameters: Dictionary<String, String>! = Dictionary()
            
            suppliedParameters["invitationType"] = "parse"
            suppliedParameters["invitationNo"] = "\(sender.tag)"
            
            
            ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "createInvitationSuccess:", successSelectorParameters: suppliedParameters, errorSelector: "createInvitationError:", errorSelectorParameters:"parse")
        }
    }
    
    
    func sendInvitationToEmail(sender: UIButton)
    {
        print("email")
        
        sender.enabled = false
        
        if !unRegisteredInviteStatus[sender.tag]
        {
            let inviteObject: PFObject = PFObject(className: "Invitations")
            
            
            let email = unRegisteredContacts[sender.tag]["email"] as! String
            
            inviteObject["invitedName"] = unRegisteredContacts[sender.tag]["contactName"] as? String
            inviteObject["eventObjectId"] = currentEvent.objectId!
            inviteObject["isApproved"] = false
            inviteObject["userObjectId"] = ""
            inviteObject["emailId"] = email
            inviteObject["attendingStatus"] = ""
            inviteObject["invitationType"] = "email"
            
            inviteObject["isUpdated"] = false
            inviteObject["noOfChilds"] = 0
            inviteObject["noOfAdults"] = 0
            inviteObject["invitationNote"] = ""
            inviteObject["isEventUpdated"] = false
            
            inviteObject["isEventStreamUpdated"] = false
            inviteObject["isTextUpdated"] = false
            
            var suppliedParameters: Dictionary<String, String>! = Dictionary()
            
            suppliedParameters["invitationType"] = "email"
            suppliedParameters["invitationNo"] = "\(sender.tag)"
            
            ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "createInvitationSuccess:", successSelectorParameters: suppliedParameters, errorSelector: "createInvitationError:", errorSelectorParameters:"email")
            
        }
    }
    
    func createInvitationSuccess(timer: NSTimer)
    {
        
        
        
        
        let invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var recievedParameters: Dictionary<String,String> = (timer.userInfo?.valueForKey("external") as? Dictionary)!
        
        let type = recievedParameters["invitationType"] as String!
        
        let invitationNo = recievedParameters["invitationNo"] as String!
        
        print(type)
        
        let email = invitation["emailId"] as! String
        
        
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = invitation.objectId!
        tblFields["userObjectId"] = invitation["userObjectId"] as? String
        tblFields["attendingStatus"] = ""
        tblFields["invitationType"] = "email"
        tblFields["invitedName"] = invitation["invitedName"] as? String
        tblFields["needsContentApprovel"] = "0"
        
        var date = ""
        
        if invitation.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((invitation.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
        }
        
        if invitation.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((invitation.updatedAt)!)
            print(date)
            tblFields["updatedAt"] = date
        }
        
        tblFields["isPosted"] = "1"
        tblFields["emailId"] = "\(email)"
        tblFields["eventObjectId"] = "\(currentEvent.objectId!)"
        if type == "parse"
        {
            tblFields["isApproved"] = "1"
        }
        else
        {
            tblFields["isApproved"] = "0"
        }
        
        
        tblFields["isUpdated"] = "0"
        tblFields["noOfChilds"] = "0"
        tblFields["noOfAdults"] = "0"
        tblFields["invitationNote"] = ""
        tblFields["isEventUpdated"] = "0"
        
        var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
        
        //if insertedId>0
        
        var inviteCode = currentEvent.objectId!
        let eventTitle = currentEvent["eventTitle"] as! String
        print("\(eventTitle)")
        var dateString = ""
        var timeString = ""
        var locationString = ""
        
        var eventLatitude = 0.0
        var eventLongitude = 0.0
        
        let eventFolder = currentEvent["eventFolder"] as! String!
        let eventImage = currentEvent["eventImage"] as! String!
        print(eventFolder)
        let hostName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        var eventType = "online"
        
        
        let invitedName = invitation["invitedName"] as! String
        let isApproved = invitation["isApproved"] as! Bool
        //var userObjectId = invitation["userObjectId"] as! String
        let emailId = invitation["emailId"] as! String
        let attendingStatus = invitation["attendingStatus"] as! String
        let invitationType = invitation["invitationType"] as! String
        let isUpdated = invitation["isUpdated"] as! Bool
        let noOfChilds = invitation["noOfChilds"] as! Int
        let noOfAdults = invitation["noOfAdults"] as! Int
        let invitationNote = invitation["invitationNote"] as! String
        let isEventUpdated = invitation["isEventUpdated"] as! Bool
        let userObjectId = invitation["userObjectId"] as! String
        let eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let createdAt = dateFormatter.stringFromDate((invitation.createdAt)!)
        
        let updatedAt = dateFormatter.stringFromDate((invitation.updatedAt)!)
        
        
        if currentEvent["isRSVP"] as! Bool
        {
            
            let sdate = currentEvent["eventStartDateTime"] as! NSDate
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            let sweekday = scomponents.weekday
            
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            
            eventType = "rsvp"
            
            dateString = "\(weekDaysArray[sweekday-1]) \(monthsArray[smonth-1]) \(sday), \(syear)"
            timeString = "\(shour):\(sminute) \(sam)"
            
            print(sweekday)
            
            locationString = currentEvent["eventLocation"] as! String!
            
            eventLatitude = currentEvent["eventLatitude"] as! Double
            eventLongitude = currentEvent["eventLongitude"] as! Double
            
        }
        
        var message = ""
        
        if type == "parse"
        {
            
            
            if Int(invitationNo) >= 0
            {
                unRegisteredInviteStatus[Int(invitationNo)!] = true
                unRegisteredInviteStatusOriginal[unRegisteredContacts[Int(invitationNo)!]["contactIndex"] as! Int] = true
            }
            
            emailContactsTable.reloadData()
            
            
            
            var notifMessage = ""
            
            if eventType == "rsvp"
            {
                notifMessage = "\(hostName) invited you to the event, \(eventTitle). Please respond to the invitation."
            }
            else
            {
                notifMessage = "\(hostName) shared the event, \(eventTitle) with you. Check it out."
                
            }
            
            
            let notificationObject = PFObject(className: "Notifications")
            notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
            notificationObject["notificationImage"] = "profilePic.png"
            notificationObject["senderId"] = currentUserId
            notificationObject["receiverId"] = userObjectId
            notificationObject["notificationActivityMessage"] = notifMessage
            notificationObject["eventObjectId"] = currentEvent.objectId!
            notificationObject["notificationType"] = "invitation"
            
            notificationObject.saveInBackground()
            
            
            
            
            let data = [
                "alert" : "\(notifMessage)",
                "notifType" :  "invitation",
                "objectId" :  invitation.objectId!,
                "eventObjectId": currentEvent.objectId!,
                "invitedName" : "\(invitedName)",
                "isUpdated" : "\(isUpdated)",
                "isEventUpdated": "\(isEventUpdated)",
                "isApproved": "\(isApproved)",
                "userObjectId" : "\(userObjectId)",
                "emailId": "\(emailId)",
                "attendingStatus" : "\(attendingStatus)",
                "invitationType" : "\(invitationType)",
                "noOfChilds": "\(noOfChilds)",
                "noOfAdults": "\(noOfAdults)",
                "invitationNote": "\(invitationNote)",
                
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "eventCreatorId" : "\(eventCreatorId)"
            ]
            
            
            var urlString = String()
            
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    print(url!)
                    
                    urlString = url!
                    
                    
                    if eventType == "rsvp"
                    {
                        let inviteUserEmail = InPerson()
                        
                        message = inviteUserEmail.emailMessage(invitation.objectId!, eventTitle: eventTitle, dateString: dateString, timeString: timeString, locationString: locationString, hostName: hostName, type: "rsvp", latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                        
                        
                    }
                    else
                    {
                        let inviteUserEmail = OnlineOnlyInviteEmailGuest()
                        
                        print(eventTitle)
                        
                        message = inviteUserEmail.emailMessage(invitation.objectId!, eventTitle:eventTitle, hostName:hostName, type: "online", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                    }
                    
                    
                    let sendNonInvitationEmailsObject = SendNonInvitationEmails()
                    
                    
                    sendNonInvitationEmailsObject.sendEmail("\(hostName) invited you to the event, \(eventTitle)", message: message, emails: [email])
                    
                }
                
            })
            
            
            
            
            let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)' AND inviteNotification = true ")
            
            
            let query = PFInstallation.queryWithPredicate(predicate)
            
            let push = PFPush()
            push.setQuery(query)
            push.setData(data)
            push.sendPushInBackground()
            
        }
        else
        {
            
            var notifMessage = ""
            if eventType == "rsvp"
            {
                notifMessage = "\(hostName) invited you to the event, \(eventTitle). Please respond to the invitation."
            }
            else
            {
                notifMessage = "\(hostName) shared the event, \(eventTitle) with you. Check it out."
                
            }
            
            
            let data = [
                "alert" : "\(notifMessage)",
                "notifType" :  "invitation",
                "objectId" :  invitation.objectId!,
                "eventObjectId": currentEvent.objectId!,
                "invitedName" : "\(invitedName)",
                "isUpdated" : "\(isUpdated)",
                "isEventUpdated": "\(isEventUpdated)",
                "isApproved": "\(isApproved)",
                "userObjectId" : "\(userObjectId)",
                "emailId": "\(emailId)",
                "attendingStatus" : "\(attendingStatus)",
                "invitationType" : "\(invitationType)",
                "noOfChilds": "\(noOfChilds)",
                "noOfAdults": "\(noOfAdults)",
                "invitationNote": "\(invitationNote)",
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "eventCreatorId" : "\(eventCreatorId)"
            ]
            
            
            var urlString = String()
            
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    print(url!)
                    
                    urlString = url!
                    if eventType == "rsvp"
                    {
                        let inviteUserEmail = InPerson()
                        message = inviteUserEmail.emailMessage(invitation.objectId!, eventTitle: eventTitle, dateString: dateString, timeString: timeString, locationString: locationString, hostName: hostName, type: "rsvp", latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                    }
                    else
                    {
                        let inviteUserEmail = OnlineOnlyInviteEmailGuest()
                        
                        message = inviteUserEmail.emailMessage(invitation.objectId!, eventTitle:eventTitle, hostName:hostName, type:"online", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                    }
                    
                    let sendNonInvitationEmailsObject = SendNonInvitationEmails()
                    
                    sendNonInvitationEmailsObject.sendEmail("\(hostName) invited you to the event, \(eventTitle)", message: message, emails: [email])
                    
                }
                
            })
            
            
            
            if Int(invitationNo) >= 0
            {
                unRegisteredInviteStatus[Int(invitationNo)!] = true
                unRegisteredInviteStatusOriginal[unRegisteredContacts[Int(invitationNo)!]["contactIndex"] as! Int] = true
            }
            
            emailContactsTable.reloadData()
        }
        
        
        sendEmailField.text = ""
        
        
        //let message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!
        
        //println("text: \(text2)")
        
        
        
        
        /*var sns = AWSSES.defaultSES()
        
        var messageBody = AWSSESContent()
        var subject = AWSSESContent()
        var body = AWSSESBody()
        
        
        
        subject.data = "\(hostName) invited you to the event, \(eventTitle)"
        
        //messageBody.data = "\(senderName) invited you to the event, \(eventTitle). Your invitation code is \(invitation.objectId!)"
        
        messageBody.data = message
        
        //body.text = messageBody
        body.html = messageBody
        
        var theMessage = AWSSESMessage()
        theMessage.subject = subject
        
        theMessage.body = body
        
        //email = "dimpal1990@gmail.com"
        
        var destination = AWSSESDestination()
        destination.toAddresses = [email]
        
        var send = AWSSESSendEmailRequest()
        send.source = "noreply@eventnode.co"
        send.destination = destination
        send.message = theMessage
        send.returnPath = "noreply@eventnode.co"
        
        self.sendEmailField.text = ""
        
        sns.sendEmail(send).continueWithBlock {(task: AnyObject!) -> AWSTask! in
        
        if task.error != nil
        {
        println(task.error.debugDescription)
        }
        else
        {
        println("success")
        }
        
        return nil
        }*/
        
        
    }
    
    func createInvitationError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    func getAddressBookNames()
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            var error : Unmanaged<CFError>? = nil
            let addressBook : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
            if addressBook == nil
            {
                print(error)
                return
            }
            
            
            ABAddressBookRequestAccessWithCompletion(addressBook) {
                (granted: Bool, error: CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if !granted {
                        print("Just denied")
                        
                        if #available(iOS 8.0, *) {
                            let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide Eventnode with access to your contacts. Go to Settings > Privacy > Contacts and enable Eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                                
                            }))
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                        }
                        else
                        {
                            
                        }
                    } else {
                        //println("Just authorized")
                        self.processContactNames();
                    }
                }
            }
            
            /*ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
            if success {
            self.processContactNames();
            }
            else {
            NSLog("unable to request access")
            }
            })*/
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
            NSLog("access denied")
            if #available(iOS 8.0, *) {
                let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide Eventnode with access to your contacts. Go to Settings > Privacy > Contacts and enable Eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
            else{
                
            }
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
            NSLog("access granted")
            processContactNames()
        }
    }
    
    func processContactNames()
    {
        var errorRef: Unmanaged<CFError>?
        let addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        
        let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        print("records in the array \(contactList.count)")
        
        
        contactLists = []
        contactEmails = []
        
        contactEmailDetails = []
        //contactLists.append
        
        for record:ABRecordRef in contactList {
            
            print(record)
            
            let contactDetails: NSDictionary! = processAddressbookRecord(record)
            let emailAddresses: Array<String>! = contactDetails.valueForKey("emails") as! Array
            
            if emailAddresses.count > 0
            {
                for email in emailAddresses
                {
                    let emailDetails: NSDictionary! = ["email":email, "contactIndex":contactEmailDetails.count,  "contactName":contactDetails.valueForKey("contactName") as! String, "photo":contactDetails.valueForKey("photo") as! UIImage]
                    
                    contactEmailDetails.append(emailDetails)
                    contactEmails.append(email)
                }
                //contactLists.append(contactDetails)
            }
            
        }
        
        reloadContacts()
        
    }
    
    
    func reloadContacts()
    {
        
        //println(currentUserId)
        
        var existingEmails: Array<NSDictionary>!
        
        existingEmails = []
        
        unRegisteredContacts = []
        unRegisteredInviteStatus = []
        
        unRegisteredContactsOriginal = []
        unRegisteredInviteStatusOriginal = []
        
        var existingEmailIndexes: Array<Int>!
        existingEmailIndexes = []
        
        var query = PFUser.query()
        query?.whereKey("email", containedIn: contactEmails)
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                print("Successfully retrieved \(objects!.count) contacts.")
                
                if let objects = objects as? [PFUser] {
                    for object in objects {
                        if let emailVerified = object["emailVerified"] as? Bool
                        {
                            /*if(emailVerified)
                            {*/
                            var existingEmailId: NSDictionary! = ["userObjectId": object.objectId!, "email": object.email!]
                            existingEmails.append(existingEmailId)
                            //}
                            print(object.email!)
                            
                        }
                        
                    }
                    var emailsString = self.contactEmails.joinWithSeparator("','")
                    print(emailsString)
                    let predicate = NSPredicate(format: "emailId IN {'\(emailsString)'} AND isEmailVerified = true")
                    var query = PFQuery(className: "LinkedAccounts", predicate: predicate)
                    //query.whereKey("isEmailVerified", equalTo:true)
                    
                    query.findObjectsInBackgroundWithBlock {
                        (objects: [AnyObject]?, error: NSError?) -> Void in
                        
                        if error == nil {
                            
                            print("Successfully retrieved \(objects!.count) contacts.")
                            
                            if let objects = objects as? [PFObject] {
                                for object in objects {
                                    print(object["emailId"]!)
                                    var existingEmailId: NSDictionary! = ["userObjectId": object["userObjectId"] as! String, "email": object["emailId"] as! String]
                                    existingEmails.append(existingEmailId)
                                }
                                
                                for emailDetail in self.contactEmailDetails
                                {
                                    
                                    //println(email)
                                    var foundIndex: Int = -1
                                    
                                    var emailStatus = false
                                    var existingEmailIdDetails: NSDictionary!
                                    
                                    for email in existingEmails
                                    {
                                        if emailDetail["email"] as! String == email["email"] as! String
                                        {
                                            emailStatus = true
                                            
                                            existingEmailIdDetails = ["userObjectId": email["userObjectId"] as! String, "email": email["email"] as! String,  "contactName":emailDetail["contactName"] as! String, "photo":emailDetail["photo"] as! UIImage, "contactIndex":self.unRegisteredContacts.count, "type": "user"]
                                            
                                            break
                                        }
                                    }
                                    
                                    var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["count(*) as count"], whereString: "emailId = ? AND eventObjectId = ? ", whereFields: [emailDetail["email"] as! String, currentEvent.objectId as String!])
                                    
                                    resultSetCount.next()
                                    
                                    var emailCount = resultSetCount.intForColumn("count")
                                    
                                    resultSetCount.close()
                                    
                                    
                                    if emailStatus == true
                                    {
                                        //foundIndex = emailDetail["contactIndex"] as! Int
                                        
                                        self.unRegisteredContacts.append(existingEmailIdDetails)
                                        self.unRegisteredContactsOriginal.append(existingEmailIdDetails)
                                        
                                        //existingEmailIndexes.append(foundIndex)
                                        
                                    }
                                    else
                                    {
                                        var unregisteredEmailDetail: NSDictionary! = ["userObjectId": "", "email":emailDetail["email"] as! String, "contactIndex":self.unRegisteredContacts.count,  "contactName":emailDetail["contactName"] as! String, "photo":emailDetail["photo"] as! UIImage, "type": "email"]
                                        
                                        self.unRegisteredContacts.append(unregisteredEmailDetail)
                                        self.unRegisteredContactsOriginal.append(unregisteredEmailDetail)
                                    }
                                    
                                    if emailCount>0
                                    {
                                        self.unRegisteredInviteStatus.append(true)
                                        self.unRegisteredInviteStatusOriginal.append(true)
                                    }
                                    else
                                    {
                                        self.unRegisteredInviteStatus.append(false)
                                        self.unRegisteredInviteStatusOriginal.append(false)
                                    }
                                    
                                }
                                
                                self.emailContactsTable.reloadData()
                                
                            }
                        } else {
                            print("Error: \(error!) \(error!.userInfo)")
                        }
                    }
                    
                }
            } else {
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        
    }
    
    
    func processAddressbookRecord(addressBookRecord: ABRecordRef)->NSDictionary
    {
        
        noOfContacts++
        
        if let contactName: String = ABRecordCopyCompositeName(addressBookRecord)?.takeRetainedValue() as? String
        {
            NSLog("contactName: \(contactName)")
            
            
            let emailAddresses: Array<String>! = processEmail(addressBookRecord)
            let photo: UIImage! = processPhoto(addressBookRecord)
            
            let contactDetails: NSDictionary! = ["contactName":contactName,"emails":emailAddresses,"photo":photo]
            
            print(emailAddresses.count)
            
            
            return contactDetails
        }
        else
        {
            var emailAddresses: Array<String>!
            emailAddresses = []
            
            let contactDetails: NSDictionary! = ["contactName":"","emails":emailAddresses,"photo":""]
            
            
            return contactDetails
        }
        
    }
    
    func processEmail(addressBookRecord: ABRecordRef) -> Array<String> {
        var emailAddresses: Array<String>!
        emailAddresses = []
        let emailArray:ABMultiValueRef = extractABEmailRef(ABRecordCopyValue(addressBookRecord, kABPersonEmailProperty))!
        for (var j = 0; j < ABMultiValueGetCount(emailArray); ++j) {
            let emailAdd = ABMultiValueCopyValueAtIndex(emailArray, j)
            let myString = extractABEmailAddress(emailAdd)
            //NSLog("email: \(myString!)")
            if(isValidEmail(myString!))
            {
                emailAddresses.append(myString!)
            }
        }
        return emailAddresses
    }
    
    func processPhoto(addressBookRecord: ABRecordRef) -> UIImage
    {
        if deviceName == "iPhone 6s Plus" || deviceName == "iPhone 6s" || deviceName == "unknown"
        {
            return UIImage(named: "unknown.png")!
        }
        else
        {
            if(ABPersonHasImageData(addressBookRecord))
            {
                if var image =  ABPersonCopyImageDataWithFormat(addressBookRecord, kABPersonImageFormatThumbnail).takeRetainedValue() as CFDataRef as? NSData
                {
                    return UIImage(data:image)!
                }
                else
                {
                    return UIImage(named: "unknown.png")!
                }
            }
            else
            {
                return UIImage(named: "unknown.png")!
            }
        }
        
    }
    
    func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
        if let ab = abRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    func extractABEmailRef (abEmailRef: Unmanaged<ABMultiValueRef>!) -> ABMultiValueRef? {
        if let ab = abEmailRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    
    func extractABEmailAddress (abEmailAddress: Unmanaged<AnyObject>!) -> String? {
        if let ab = abEmailAddress {
            return Unmanaged.fromOpaque(abEmailAddress.toOpaque()).takeUnretainedValue() as CFStringRef as String
        }
        return nil
    }
    
    
    func isValidEmail(emailid: NSString)->Bool
    {
        
        var isValid = true
        
        if !emailid.containsString(" ")
        {
            var atRateSplitArray = emailid.componentsSeparatedByString("@")
            
            if(atRateSplitArray.count>=2)
            {
                for component in atRateSplitArray
                {
                    if component as! String == ""
                    {
                        isValid = false
                    }
                }
                
                if(isValid)
                {
                    let dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                    
                    if(dotSplitArray.count>=2)
                    {
                        for component in dotSplitArray
                        {
                            if component == ""
                            {
                                isValid = false
                            }
                        }
                    }
                    else
                    {
                        isValid = false
                    }
                }
            }
            else
            {
                isValid = false
            }
            
        }
        else
        {
            isValid = false
        }
        
        return isValid
    }
    
    @IBAction func closeButtonClicked(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func searchTextFieldReturn(sender: UIButton) {
        searchContactField.resignFirstResponder()
        searchContacts(searchContactField.text!)
    }
    
    
    func searchContacts(searchQuery: String)
    {
        if searchQuery != ""
        {
            
            unRegisteredContacts = []
            unRegisteredInviteStatus = []
            
            for contact in unRegisteredContactsOriginal
            {
                let contactEmail = contact["email"] as! String
                let contactName = contact["contactName"] as! String
                
                
                if (contactEmail.lowercaseString.rangeOfString(searchQuery.lowercaseString) != nil) ||  (contactName.lowercaseString.rangeOfString(searchQuery.lowercaseString) != nil)
                {
                    unRegisteredContacts.append(contact)
                    unRegisteredInviteStatus.append(unRegisteredInviteStatusOriginal[contact["contactIndex"] as! Int])
                }
            }
            
        }
        else
        {
            unRegisteredContacts = unRegisteredContactsOriginal
            unRegisteredInviteStatus = unRegisteredInviteStatusOriginal
        }
        
        emailContactsTable.reloadData()
    }
    
    @IBAction func sendTextFieldReturn(sender: UIButton) {
        sendEmailField.resignFirstResponder()
        
        let predicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
        
        let query = PFQuery(className: "LinkedAccounts", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchLinkedEmailForInvitationSuccess:", successSelectorParameters: nil, errorSelector: "fetchLinkedEmailForInvitationError:", errorSelectorParameters: nil)
        
        //sendInvitationMail()
    }
    
    func fetchLinkedEmailForInvitationSuccess(timer: NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if let fetchedobjects = objects {
            
            userEmails = []
            
            for object in fetchedobjects
            {
                userEmails.append(object["emailId"] as! String)
            }
            
            userEmails.append(userPrimaryEmail)
            
            sendInvitationMail()
        }
    }
    
    func fetchLinkedEmailForInvitationError(timer: NSTimer)
    {
        userEmails.append(userPrimaryEmail)
        sendInvitationMail()
    }
    
    func sendInvitationMail()
    {
        if isValidEmail(sendEmailField.text!)
        {
            var isLoggedInEmail = false
            
            for email in userEmails
            {
                if sendEmailField.text! == email
                {
                    isLoggedInEmail = true
                }
            }
            //
            if isLoggedInEmail
            {
                if #available(iOS 8.0, *) {
                    
                    var refreshAlert = UIAlertController(title: "Oops!", message: "You cannot invite yourself", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                        
                    }))
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
            else
            {
                var i = 0
                var j = 0
                var emailExist = false
                
                for contact in unRegisteredContacts
                {
                    if (unRegisteredContacts[i]["email"] as! String).lowercaseString == sendEmailField.text!.lowercaseString && (unRegisteredContacts[i]["type"] as! String) == "user"
                    {
                        emailExist = true
                        j = i
                        break
                    }
                    i++
                }
                
                if emailExist
                {
                    if !unRegisteredInviteStatus[j]
                    {
                        let inviteObject: PFObject = PFObject(className: "Invitations")
                        
                        let userObjectId = unRegisteredContacts[j]["userObjectId"] as! String
                        
                        //let base64EncodedString = SwiftyBase64.EncodeString(userObjectId)
                        
                        let email = unRegisteredContacts[j]["email"] as! String
                        
                        inviteObject["eventObjectId"] = currentEvent.objectId!
                        inviteObject["isApproved"] = true
                        inviteObject["userObjectId"] = userObjectId
                        inviteObject["emailId"] = email
                        inviteObject["attendingStatus"] = ""
                        inviteObject["invitationType"] = "email"
                        inviteObject["invitedName"] = unRegisteredContacts[j]["contactName"] as? String
                        print(inviteObject["invitedName"])
                        
                        inviteObject["isUpdated"] = false
                        inviteObject["noOfChilds"] = 0
                        inviteObject["noOfAdults"] = 0
                        inviteObject["invitationNote"] = ""
                        inviteObject["isEventUpdated"] = false
                        
                        inviteObject["isEventStreamUpdated"] = false
                        inviteObject["isTextUpdated"] = false
                        
                        var suppliedParameters: Dictionary<String, String>! = Dictionary()
                        
                        suppliedParameters["invitationType"] = "parse"
                        suppliedParameters["invitationNo"] = "\(j)"
                        
                        
                        ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "createInvitationSuccess:", successSelectorParameters: suppliedParameters, errorSelector: "createInvitationError:", errorSelectorParameters:"parse")
                    }
                    else
                    {
                        if #available(iOS 8.0, *) {
                            
                            var refreshAlert = UIAlertController(title: "Alert", message: "This contact is already invited", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                                
                            }))
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    i = 0
                    j = 0
                    emailExist = false
                    for contact in unRegisteredContacts
                    {
                        if (unRegisteredContacts[i]["email"] as! String).lowercaseString == sendEmailField.text!.lowercaseString
                        {
                            emailExist = true
                            j = i
                            break
                        }
                        i++
                    }
                    
                    if emailExist
                    {
                        if !unRegisteredInviteStatus[j]
                        {
                            let inviteObject: PFObject = PFObject(className: "Invitations")
                            
                            let email = unRegisteredContacts[j]["email"] as! String
                            
                            inviteObject["invitedName"] = unRegisteredContacts[j]["contactName"] as? String
                            print(inviteObject["invitedName"])
                            inviteObject["eventObjectId"] = currentEvent.objectId!
                            inviteObject["isApproved"] = false
                            inviteObject["userObjectId"] = ""
                            inviteObject["emailId"] = email
                            inviteObject["attendingStatus"] = ""
                            inviteObject["invitationType"] = "email"
                            
                            inviteObject["isUpdated"] = false
                            inviteObject["noOfChilds"] = 0
                            inviteObject["noOfAdults"] = 0
                            inviteObject["invitationNote"] = ""
                            inviteObject["isEventUpdated"] = false
                            
                            inviteObject["isEventStreamUpdated"] = false
                            inviteObject["isTextUpdated"] = false
                            
                            var suppliedParameters: Dictionary<String, String>! = Dictionary()
                            
                            suppliedParameters["invitationType"] = "email"
                            suppliedParameters["invitationNo"] = "\(j)"
                            
                            ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "createInvitationSuccess:", successSelectorParameters: suppliedParameters, errorSelector: "createInvitationError:", errorSelectorParameters:"email")
                        }
                        else
                        {
                            if #available(iOS 8.0, *) {
                                
                                var refreshAlert = UIAlertController(title: "Alert", message: "This person is already invited", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                
                                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                                    
                                }))
                                self.presentViewController(refreshAlert, animated: true, completion: nil)
                            }
                            else
                            {
                                
                            }
                        }
                    }
                    else
                    {
                        let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["count(*) as count"], whereString: "emailId = ? AND eventObjectId = ? ", whereFields: [sendEmailField.text!, currentEvent.objectId as String!])
                        
                        resultSetCount.next()
                        
                        let emailCount = resultSetCount.intForColumn("count")
                        
                        resultSetCount.close()
                        
                        if emailCount > 0
                        {
                            if #available(iOS 8.0, *) {
                                
                                var refreshAlert = UIAlertController(title: "Alert", message: "This person is already invited", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                
                                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                                    
                                }))
                                self.presentViewController(refreshAlert, animated: true, completion: nil)
                            }
                            else
                            {
                                
                            }
                        }
                        else
                        {
                            
                            let query = PFUser.query()
                            
                            query?.whereKey("email", equalTo: sendEmailField.text!)
                            
                            query!.findObjectsInBackgroundWithBlock {
                                (objects: [AnyObject]?, error: NSError?) -> Void in
                                
                                if error == nil {
                                    
                                    print("Successfully retrieved \(objects!.count) contacts.")
                                    
                                    if let objects = objects as? [PFUser] {
                                        
                                        if objects.count > 0
                                        {
                                            self.createInvitationForEmailNotInContacts(objects[0].objectId!, guestName: objects[0]["fullUserName"] as! String, isApproved: true)
                                        }
                                        else
                                        {
                                            let predicate = NSPredicate(format: "emailId = '\(self.sendEmailField.text!)' AND isEmailVerified = true")
                                            let query = PFQuery(className: "LinkedAccounts", predicate: predicate)
                                            //query.whereKey("isEmailVerified", equalTo:true)
                                            
                                            query.findObjectsInBackgroundWithBlock {
                                                (objects: [AnyObject]?, error: NSError?) -> Void in
                                                
                                                if error == nil {
                                                    
                                                    print("Successfully retrieved \(objects!.count) contacts.")
                                                    
                                                    if let objects = objects as? [PFObject] {
                                                        if objects.count > 0
                                                        {
                                                            
                                                            let userObjectId = objects[0]["userObjectId"] as! String
                                                            
                                                            let query = PFUser.query()
                                                            
                                                            query?.whereKey("objectId", equalTo: objects[0]["userObjectId"] as! String)
                                                            
                                                            query!.findObjectsInBackgroundWithBlock {
                                                                (objects: [AnyObject]?, error: NSError?) -> Void in
                                                                
                                                                if error == nil {
                                                                    
                                                                    print("Successfully retrieved \(objects!.count) contacts.")
                                                                    
                                                                    if let objects = objects as? [PFUser] {
                                                                        if objects.count > 0
                                                                        {
                                                                            self.createInvitationForEmailNotInContacts(userObjectId, guestName: objects[0]["fullUserName"] as! String, isApproved: true)
                                                                        }
                                                                        else
                                                                        {
                                                                            self.createInvitationForEmailNotInContacts("", guestName: self.sendEmailField.text!.lowercaseString, isApproved: false)
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            self.createInvitationForEmailNotInContacts("", guestName: self.sendEmailField.text!.lowercaseString, isApproved: false)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if #available(iOS 8.0, *) {
                
                var refreshAlert = UIAlertController(title: "Error", message: "Please enter a valid email address", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
            else
            {
                
            }
        }
    }
    
    
    func createInvitationForEmailNotInContacts(userObjectId: String, guestName: String, isApproved: Bool)
    {
        let inviteObject: PFObject = PFObject(className: "Invitations")
        
        let email = sendEmailField.text!.lowercaseString
        
        print(isApproved)
        
        inviteObject["eventObjectId"] = currentEvent.objectId!
        inviteObject["isApproved"] = isApproved
        inviteObject["userObjectId"] = userObjectId
        inviteObject["emailId"] = email
        inviteObject["attendingStatus"] = ""
        
        
        
        inviteObject["invitedName"] = guestName
        print(inviteObject["invitedName"])
        
        inviteObject["isUpdated"] = false
        inviteObject["noOfChilds"] = 0
        inviteObject["noOfAdults"] = 0
        inviteObject["invitationNote"] = ""
        inviteObject["isEventUpdated"] = false
        
        inviteObject["isEventStreamUpdated"] = false
        inviteObject["isTextUpdated"] = false
        
        inviteObject["invitationType"] = "parse"
        
        if userObjectId == ""
        {
            inviteObject["invitationType"] = "email"
        }
        
        var suppliedParameters: Dictionary<String, String>! = Dictionary()
        
        suppliedParameters["invitationType"] = "parse"
        
        if userObjectId == ""
        {
            suppliedParameters["invitationType"] = "email"
        }
        
        //suppliedParameters["invitationType"] = "email"
        suppliedParameters["invitationNo"] = "-1"
        
        ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "createInvitationSuccess:", successSelectorParameters: suppliedParameters, errorSelector: "createInvitationError:", errorSelectorParameters:"email")
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool{
        
        print(string)
        //println(textField.text)
        if textField == searchContactField
        {
            if string == ""
            {
                if searchContactField.text! != ""
                {
                    //searchContactField.text = searchContactField.text!.substringToIndex(searchContactField.text!.endIndex.predecessor())
                }
                
            }
            else
            {
                //searchContactField.text = "\(searchContactField.text!)\(string)"
            }
            
            
            var txtAfterUpdate = self.searchContactField.text as? NSString
            
            //            var txtAfterUpdate  = self.searchContactField.text as String
            
            
            
            txtAfterUpdate = txtAfterUpdate?.stringByReplacingCharactersInRange(range, withString: string)
            
            //txtAfterUpdate = txtAfterUpdate!.stringByReplacingCharactersInRange(range, withString: string)
            
            //searchContactField.text = txtAfterUpdate as String
            
            //searchContactField.text!.stringByReplacingCharactersInRange(range,withString:replacementString)
            
            print(txtAfterUpdate)
            
            searchContacts(txtAfterUpdate! as String)
        }
        
        return true
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        
        textField.resignFirstResponder()
        
        if textField.tag == 1
        {
            searchTextFieldReturn(UIButton())
        }
        else
        {
            
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool{
        
        if textField.tag == 1
        {
            
        }
        else
        {
            contentView.frame.origin.y = -180
        }
        
        return true
    }
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        if textField.tag == 1
        {
            searchTextFieldReturn(UIButton())
        }
        else
        {
            contentView.frame.origin.y = headerView.frame.size.height + 20
        }
        
        return true
    }
    
    @IBAction func viewTapped(sender : AnyObject)
    {
        searchContactField.resignFirstResponder()
        sendEmailField.resignFirstResponder()
    }
    
}
