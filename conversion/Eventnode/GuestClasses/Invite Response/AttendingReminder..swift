//
//  AttendingReminder.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class AttendingReminder
{
    func emailMessage(eventId: String, eventTitle: String, dateString: String, timeString: String, locationString: String, hostName: String, latitude: String, longitude: String, type: String )-> String
    {
        
        let file = "Attending_Reminder.html"

        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)

        

        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)

        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

        print(base64String)

        

        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)

        

        message = message.stringByReplacingOccurrencesOfString("<td style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:18px; line-height:20px; color:#4A4A4A; text-align:center; font-weight:500;\">Rosy's Baby Shower<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by Sharon Tucker</em></span></td>", withString: "<td style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:18px; line-height:20px; color:#4A4A4A; text-align:center; font-weight:500;\">\(eventTitle)<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by \(hostName)</em></span></td>", options: [], range: nil)

        

        if type == "rsvp"

        {

            message = message.stringByReplacingOccurrencesOfString("<td class=\"textag\" style=\"font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#4A4A4A; text-align:left;\"><span style=\"font-size:10px; color:#9B9B9B;\">Date & Time</span><br>April 30,2015 - 7:00PM</td>", withString: "<td class=\"textag\" style=\"font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#4A4A4A; text-align:left;\"><span style=\"font-size:10px; color:#9B9B9B;\">Date & Time</span><br>\(dateString) - \(timeString)</td>", options: [], range: nil)

            

            message = message.stringByReplacingOccurrencesOfString("<td class=\"textag\" style=\"font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#4A4A4A; text-align:left;\"><span style=\"font-size:10px; color:#9B9B9B;\">Location</span><br>3444 Spectrum, Irvine, CA 92618</td>", withString: "<td class=\"textag\" style=\"font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#4A4A4A; text-align:left;\"><span style=\"font-size:10px; color:#9B9B9B;\">Location</span><br>\(locationString)</td>", options: [], range: nil)

            

            message = message.stringByReplacingOccurrencesOfString("<td style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#9B9B9B; text-align:left; font-weight:600;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Maps</a></td>", withString: "<td style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#9B9B9B; text-align:left; font-weight:600;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"http://maps.google.com/maps?q=\(latitude),\(longitude)\" target=\"_blank\">Go to Maps</a></td>", options: [], range: nil)

            

        }

        

        message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Change your Response</a></td>", withString: "<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)\" target=\"_blank\">Change your Response</a></td>", options: [], range: nil)
        
        return message
    }
}