//
//  LocationChange.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class LocationChange
{
    func emailMessage(eventId: String, eventTitle: String,  locationString: String, hostName: String, type: String, url:String  )-> String
    {
        
        let file = "location_change.html"
        
        
        
        let path = "\(documentDirectory)/\(file)"
        
        
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        print(base64String)
        
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        
        
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRRRRRRRosy's Baby Shower", withString: "\(eventTitle)", options: [], range: nil)
        
        
        
        if type == "rsvp"
            
        {
            
            message = message.stringByReplacingOccurrencesOfString("SSSSSSSSSSSharon Tucker changed the location for the event: <br><b>RRRRRRRRRRRRRosy’s Baby Shower", withString:"\(hostName) changed the location for the event: <br><b>\(eventTitle)", options: [], range: nil)
            
            
            
            message = message.stringByReplacingOccurrencesOfString("<b>###################3444 Spectrum, Irvine, CA 92618", withString:"\(locationString)", options: [], range: nil)
            
            
            
            message = message.stringByReplacingOccurrencesOfString("<a style=\"color:#4A4A4A; text-align:center;text-decoration: none;\"href=\"#\"target=\"_blank\">Go to Event</a>", withString:"<a style=\"color:#4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\"target=\"_blank\">Go to Event</a>", options: [], range: nil)
            
        }
        
        
        return message
    }
}