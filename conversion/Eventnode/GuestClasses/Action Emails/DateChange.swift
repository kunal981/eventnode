//
//  DateChange.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class DateChange
{
    func emailMessage(eventId: String, eventTitle: String, dateString: String, timeString: String, hostName: String, type: String, url:String )-> String
    {
        
        let file = "date_change.html"
        
        print(eventId)
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        print(base64String)
        
        
        
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        
        
        
        let path = "\(documentDirectory)/\(file)"
        
        
        
        
        
        
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRRRRRRRRRRRRRRRosy's Baby Shower", withString:"\(eventTitle)", options: [], range: nil)
        
        
        
        if type == "rsvp"
            
        {
            
            message = message.stringByReplacingOccurrencesOfString("SSSSSSSSSSSSSSSSSSharon Tucker changed the date & time for the event: <br><b>RRRRRRRRRRRRRRRRRosy’s Baby Shower", withString: "\(hostName) changed the date & time for the event: <br><b>\(eventTitle)", options: [], range: nil)
            
            
            
            message = message.stringByReplacingOccurrencesOfString("New Date & Time</b></span><br><b>April 3000000000,2015 - 77777777:00PM", withString: "New Date & Time</b></span><br><b>\(dateString) - \(timeString)", options: [], range: nil)
            
            
            
            
            
            
            
            message = message.stringByReplacingOccurrencesOfString("<a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Event</a>", withString: "<a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Go to Event</a>", options: [], range: nil)
            
        }
        
        return message
    }
}