//
//  NewMessageGuest.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class NewMessageGuest
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, messageText: String )-> String
{

let file = "new_message_guest.html"



let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)



    

    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)

    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

    print(base64String)



var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)



   message = message.stringByReplacingOccurrencesOfString("<td style=\"font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif; font-size:20px; line-height:22px; color:#4A4A4A; text-align:left;\"><b>Rosy's Baby Shower</b></td>", withString: "<td style=\"font-family:'Montserrat', Tahoma,Verdana,Segoe,sans-serif; font-size:20px; line-height:22px; color:#4A4A4A; text-align:left;\"><b>\(eventTitle)</b></td>", options: [], range: nil)





message = message.stringByReplacingOccurrencesOfString("<td style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:18px; line-height:20px; color:#4A4A4A; text-align:center; font-weight:500;\">Sharon Tucker sent a group message for the event: <br><b>Rosy’s Baby Shower</b></td>", withString:"<td style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:18px; line-height:20px; color:#4A4A4A; text-align:center; font-weight:500;\">\(hostName) sent a group message for the event: <br><b>\(eventTitle)</b></td>", options: [], range: nil)

    

    

    message = message.stringByReplacingOccurrencesOfString("<td class=\"textag\"style=\"font-family:\'Montserrat\', Tahoma,Verdana,Segoe,sans-serif; font-size:13px; line-height:18px; color:#4A4A4A; text-align:center;\"><span style=\"font-size:13px; color:#9B9B9B;\"><b>New Message</b></span><br><b>Hey guys, how are you?</b></td>", withString:"<td class=\"textag\"style=\"font-family:\'Montserrat\', Tahoma,Verdana,Segoe,sans-serif; font-size:13px; line-height:18px; color:#4A4A4A; text-align:center;\"><span style=\"font-size:13px; color:#9B9B9B;\"><b>New Message</b></span><br><b>\(messageText)</b></td>", options: [], range: nil)

    

    message = message.stringByReplacingOccurrencesOfString("<a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Event</a>", withString:"<a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)\" target=\"_blank\">Go to Event</a>", options: [], range: nil)
    
   
    
   

return message
}
}