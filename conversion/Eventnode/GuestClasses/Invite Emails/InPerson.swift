//
//  InPerson.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class InPerson
{
    func emailMessage(eventId: String, eventTitle: String, dateString: String, timeString: String, locationString: String, hostName: String, type: String, latitude: String, longitude: String, imageUrl: String, url:String)-> String
    {
        
        var file = "In_Person.html"
        
        let path = "\(documentDirectory)/\(file)"
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
       
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        message = message.stringByReplacingOccurrencesOfString("<b>SSSSSSSSSSharon Tucker</b> invited you to <b>RRRRRRRosy’s Baby Shower", withString:"\(hostName)</b> invited you to <b>\(eventTitle)", options: [], range: nil)
        
        if type == "rsvp"
        {
            message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRRRRRRRRRosy's Baby Shower", withString: "\(eventTitle)", options: [], range: nil)
            
            message = message.stringByReplacingOccurrencesOfString("SSSSSSSSSSSSSSSSSSSharon Tucker", withString: "\(hostName)", options: [], range: nil)
            
            message = message.stringByReplacingOccurrencesOfString("April 3000000,2015 - 7:00PM", withString:"\(dateString) - \(timeString)", options: [], range: nil)
            
            message = message.stringByReplacingOccurrencesOfString("3333333444 Spectrum, Irvine, CA 92618", withString:"\(locationString)", options: [], range: nil)
            
            message = message.stringByReplacingOccurrencesOfString("hhhhhhhhref=\"#\"", withString:"href=\"http://maps.google.com/maps?q=\(latitude),\(longitude)\"", options: [], range: nil)
            
            message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Respond to Invite</a></td>", withString:"<td height=\"30\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Respond to Invite</a></td>", options: [], range: nil)
            
            
            message = message.stringByReplacingOccurrencesOfString("<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"http://dcy86hdr5o800.cloudfront.net/image1.jpg\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", withString:"<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"\(imageUrl)\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", options: [], range: nil)
            
            
            
        }
        
        
        return message
    }
}