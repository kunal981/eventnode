//
//  InPersonReminder.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class InPersonReminder
{
    func emailMessage(eventId: String, eventTitle: String, dateString: String, timeString: String, locationString: String, hostName: String, type: String, imageUrl: String, longitude: String , latitude: String )-> String
{

let file = "In_Person_Reminder.html"



let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)



    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)

    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

    print(base64String)



var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)



message = message.stringByReplacingOccurrencesOfString("<td style=\"font-family:\'Montserrat\', Tahoma,Verdana,Segoe,sans-serif; font-size:20px; line-height:22px; color:#4A4A4A; text-align:center;\"><b>Sharon Tucker</b> invited you to <b>Rosy’s Baby Shower</b></td>", withString:"<td style=\"font-family:\'Montserrat\', Tahoma,Verdana,Segoe,sans-serif; font-size:20px; line-height:22px; color:#4A4A4A; text-align:center;\"><b>\(hostName)</b> invited you to <b>\(eventTitle)</b></td>", options: [], range: nil)



if type == "rsvp"

{

    message = message.stringByReplacingOccurrencesOfString("<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"http://dcy86hdr5o800.cloudfront.net/image1.jpg\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", withString:"<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\(imageUrl)\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", options: [], range: nil)





    message = message.stringByReplacingOccurrencesOfString("<td class=\"text\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:18px; line-height:20px; color:#4A4A4A; text-align:left; font-weight:500;\">Rosy's Baby Shower<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by Sharon Tucker</em></span></td>", withString:"<td class=\"text\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:18px; line-height:20px; color:#4A4A4A; text-align:left; font-weight:500;\">\(eventTitle)<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by \(hostName)</em></span></td>", options: [], range: nil)

    

    message = message.stringByReplacingOccurrencesOfString("<td class=\"text\" style=\"font-family:\'Montserrat\', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#4A4A4A; text-align:left;\"><span style=\"font-size:10px; color:#9B9B9B;\">Date & Time</span><br>April 30,2015 - 7:00PM</td>", withString:"<td class=\"text\" style=\"font-family:\'Montserrat\', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#4A4A4A; text-align:left;\"><span style=\"font-size:10px; color:#9B9B9B;\">Date & Time</span><br>\(dateString) - \(timeString)</td>", options: [], range: nil)



    message = message.stringByReplacingOccurrencesOfString("<td class=\"text\" style=\"font-family:\'Montserrat\', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#4A4A4A; text-align:left;\"><span style=\"font-size:10px; color:#9B9B9B;\">Location</span><br>3444 Spectrum, Irvine, CA 92618</td>", withString:"<td class=\"text\" style=\"font-family:\'Montserrat\', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#4A4A4A; text-align:left;\"><span style=\"font-size:10px; color:#9B9B9B;\">Location</span><br>\(locationString)</td>", options: [], range: nil)

    

     message = message.stringByReplacingOccurrencesOfString("<td style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#9B9B9B; text-align:left; font-weight:600;\"><a target=\"_blank\" href=\"#\" style=\"text-decoration:none;font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#9B9B9B; text-align:left; font-weight:600;\">Go to Maps</a></td>", withString:"<td style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#9B9B9B; text-align:left; font-weight:600;\"><a target=\"_blank\" href=\"http://maps.google.com/maps?q=\(latitude),\(longitude)\" style=\"text-decoration:none;font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px; line-height:18px; color:#9B9B9B; text-align:left; font-weight:600;\">Go to Maps</a></td>", options: [], range: nil)

    

     message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Respond to Invite</a></td>", withString:"<td height=\"30\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)\" target=\"_blank\">Respond to Invite</a></td>", options: [], range: nil)
    
    
    
    

}

return message
}
}