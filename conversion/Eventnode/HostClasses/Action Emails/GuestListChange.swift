//
//  GuestListChange.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class GuestListChange
{
    func emailMessage(eventId: String, eventTitle: String, guestName: String, oldAdultCount: Int, oldChildsCount: Int, newAdultCount: Int, newChildCount: Int, type: String, url:String )-> String
    {
        
        let file = "guest_list_change.html"
        
        
        
        let path = "\(documentDirectory)/\(file)"
        
        
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        print(base64String)
        
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("<b>Rosy'shjmghjghjghjghjghjghj Babyhjghjughjghjghjghjhjguyty Showeriuyiuyiuyiuyiuyiuyiuyiuuiuiouiouioy8uyo8yiuyiuyiuyiuyiuyiuyiuyiuyiuyiuyiuyiuyiutuyiuyiuiutyiouuioyuytuyiuytydtyeerwy5tghswq45iuhgghdruyuy</b>", withString:"<b>\(eventTitle)</b>", options: [], range: nil)
        
        
        
        if type == "rsvp"
            
        {
            
            message = message.stringByReplacingOccurrencesOfString("CCCCCCCCCCChandra Kilaru changed the number of guests attending your event: <br><b>Rosy’s Baby Shower", withString:"\(guestName) changed the number of guests attending your event: <br><b>\(eventTitle)", options: [], range: nil)
            
            
            
            message = message.stringByReplacingOccurrencesOfString("<b>Previous Guest Count:</b> 2 Adults, 1 Children<br><b>New Guest Count:</b> 1 Adults, 1 Children", withString: "<b>Previous Guest Count:</b> \(oldAdultCount) Adults, \(oldChildsCount) Children<br><b>New Guest Count:</b> \(newAdultCount) Adults, \(newChildCount) Children", options: [], range: nil)
            
            
            
            
            
        }
        
        
        
        message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Event</a></td>", withString: "<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Go to Event</a></td>", options: [], range: nil)
        
        
        
        return message
    }
}