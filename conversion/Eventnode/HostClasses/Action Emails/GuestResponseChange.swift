//
//  GuestResponseChange.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class GuestResponseChange
{
    func emailMessage(eventId: String, eventTitle: String, prevoiusStatus: String, guestName:String, newStatus: String ,type: String, url:String)-> String
    {
        
        let file = "guest_response_change.html"
        
        
        
        let path = "\(documentDirectory)/\(file)"
        
        //let writePath = (NSTemporaryDirectory() as NSString).stringByAppendingPathComponent("instagram.igo")
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        print(base64String)
        
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("RRRRRRRosy's Baby Shower", withString:"\(eventTitle)", options: [], range: nil)
        
        
        
        if type == "rsvp"
            
        {
            
            message = message.stringByReplacingOccurrencesOfString("Chandra Kilaru changed status for your event: ", withString:"\(guestName) changed status for your event:", options: [], range: nil)
            
            
            
            message = message.stringByReplacingOccurrencesOfString("RRRRRRosy’s Baby Shower", withString:"\(eventTitle)", options: [], range: nil)
            
            
            
            message = message.stringByReplacingOccurrencesOfString("<b>Previous Status:</b> Maybe<br><b>New Status:</b> Attending", withString: "<b>Previous Status:</b> \(prevoiusStatus)<br><b>New Status:</b> \(newStatus)", options: [], range: nil)
            
            
            
        }
        
        
        
        message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Event</a></td>", withString: "<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Go to Event</a></td>", options: [], range: nil)
        
        
        
        return message
    }
}