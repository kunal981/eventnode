//
//  InviteCodeViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 7/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class InviteCodeViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var firstParagraph : UITextView!
    @IBOutlet var secondParagraph : UITextView!
    
    @IBOutlet var invitationTextField : UITextField!
    @IBOutlet var emailTextField : UITextField!
    
    var currentUserId = ""
    
    var backOrPop: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        let attributes = [NSParagraphStyleAttributeName : style]
        
        firstParagraph.attributedText = NSAttributedString(string:firstParagraph.text, attributes:attributes)
        firstParagraph.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
        firstParagraph.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        
        secondParagraph.attributedText = NSAttributedString(string:secondParagraph.text, attributes:attributes)
        secondParagraph.font = UIFont(name: "AvenirNext-Medium", size: 10.0)
        secondParagraph.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        
        currentUserId = (NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String)!
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool{
        print("ddsd")
        
        if textField.tag == 1
        {
            
        }
        else
        {
            self.view.frame.origin.y = -200
        }
        
        return true
    }
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool{
        
        self.view.frame.origin.y = 0
        
        return true
    }
    
    @IBAction func viewTapped(sender : AnyObject) {
        invitationTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
    }
    
    
    @IBAction func skipInvitationCode(sender: UIButton)
    {
        
        if (backOrPop == true)
        {
            let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            self.navigationController?.pushViewController(eventVC, animated: false)
        }
        else
        {
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    func isValidEmail(emailid: NSString)->Bool
    {
        
        var isValid = true
        
        if #available(iOS 8.0, *) {
            if !emailid.containsString(" ")
            {
                var atRateSplitArray = emailid.componentsSeparatedByString("@")
                
                if(atRateSplitArray.count>=2)
                {
                    for component in atRateSplitArray
                    {
                        if component == ""
                        {
                            isValid = false
                        }
                    }
                    
                    if(isValid)
                    {
                        let dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                        
                        if(dotSplitArray.count>=2)
                        {
                            for component in dotSplitArray
                            {
                                if component == ""
                                {
                                    isValid = false
                                }
                            }
                        }
                        else
                        {
                            isValid = false
                        }
                    }
                }
                else
                {
                    isValid = false
                }
                
            }
            else
            {
                isValid = false
            }
        } else {
            // Fallback on earlier versions
        }
        
        return isValid
    }
    
    @IBAction func processInvitationCode(sender: UIButton)
    {
        
        var validInfo = false
        
        if (invitationTextField.text!).characters.count >= 10
        {
            if isValidEmail(emailTextField.text!)
            {
                let predicate = NSPredicate(format: "emailId = '\(emailTextField.text!)' AND userObjectId = '' AND eventObjectId = '\(invitationTextField.text!)'")
                
                let userQuery = PFQuery(className: "Invitations", predicate: predicate)
                
                ParseOperations.instance.fetchData(userQuery, target: self, successSelector: "invitationFetchedSuccessful:", successSelectorParameters: nil, errorSelector:"invitationFetchedError:", errorSelectorParameters: nil)
            }
            else
            {
                if #available(iOS 8.0, *) {
                    let refreshAlert = UIAlertController(title: "Error", message: "Enter a valid invitation code.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                        
                    }))
                    
                }
                else{
                    
                }
            }
        }
        else
        {
            if #available(iOS 8.0, *) {
                let refreshAlert = UIAlertController(title: "Error", message: "Enter a valid invitation code.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        }
        
        
        func invitationFetchedSuccessful(timer:NSTimer)
        {
            isPostUpdated = true
            
            let invitationObjects = timer.userInfo?.valueForKey("internal") as! [PFObject]!
            
            if let objects = invitationObjects
            {
                var i = 0
                for object in objects
                {
                    objects[i]["userObjectId"] = currentUserId
                    i++
                }
                
                PFObject.saveAllInBackground(objects, block:{
                    (success: Bool, error: NSError?) -> Void in
                    if (success)
                    {
                        if (self.backOrPop == true)
                        {
                            let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                            self.navigationController?.pushViewController(eventVC, animated: false)
                        }
                        else
                        {
                            self.navigationController?.popViewControllerAnimated(false)
                        }
                    }
                    else
                    {
                        
                        if #available(iOS 8.0, *) {
                            var refreshAlert = UIAlertController(title: "Error", message: "Oops! Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                                
                            }))
                            
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                        }
                        else
                        {
                            
                        }
                        
                    }
                })
                
                
            }
            else
            {
                if #available(iOS 8.0, *) {
                    
                    var refreshAlert = UIAlertController(title: "Error", message: "Oops! Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                        
                    }))
                    
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
            
        }
        
        
        func invitationFetchedError(timer:NSTimer)
        {
            if #available(iOS 8.0, *) {
                
                let refreshAlert = UIAlertController(title: "Error", message: "Oops! Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
            }
            else
            {
                
            }
            
        }
        
    }
}
