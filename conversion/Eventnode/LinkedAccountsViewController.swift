//
//  LinkedAccountsViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class LinkedAccountsViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showLinkfacebookView = true
            
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        let attributes = [NSParagraphStyleAttributeName : style]
        
        textView.attributedText = NSAttributedString(string:textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func facebookLinkedButton(sender: AnyObject)
    {
        print(isFacebookLogin)
        
        if isFacebookLogin
        {
            
            let UnlinkFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("unlinkFacebook") as! facebookAccountUnlinkViewController
            self.navigationController?.pushViewController(UnlinkFacebook, animated: false)
        }
            
        else
        {
            showLinkfacebookView = true
            
            let ConnectFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("connectFacebookAccount") as! facebookLinkedViewController
            
            ConnectFacebook.isInviteView = false
            
            self.navigationController?.pushViewController(ConnectFacebook, animated: false)
            
        }
        
        
        
        
    }
    
    @IBAction func backbutton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func emailLinkdButton(sender: AnyObject)
    {
        let Email = self.storyboard!.instantiateViewControllerWithIdentifier("Email") as! EmailListViewController
        self.navigationController?.pushViewController(Email, animated: false)
        
    }
}
