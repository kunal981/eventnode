//
//  ManageFreindsViewController.swift
//  Eventnode
//
//  Created by brst on 8/24/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MessageUI
import AddressBookUI
import AddressBook
import Contacts

class ManageFreindsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var inviteTextView: UITextView!
    @IBOutlet weak var eventStreamLabel: UILabel!
    @IBOutlet weak var sharedPopUpView: UIView!
    @IBOutlet weak var pendingPopUpView: UIView!
    @IBOutlet weak var pendingView: UIView!
    @IBOutlet weak var sharedView: UIView!
    @IBOutlet weak var invitemoreView: UIView!
    @IBOutlet weak var sharedtableView: UITableView!
    @IBOutlet weak var sharedtableView2: UITableView!
    @IBOutlet weak var approveAllButton: UIButton!
    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var inviteMoreButton: UIButton!
    @IBOutlet weak var sharedbutton: UIButton!
    @IBOutlet weak var sharedTextView2: UITextView!
    @IBOutlet weak var sharedTextView: UITextView!
    @IBOutlet weak var pendingTableView: UITableView!
    @IBOutlet weak var pendingTextView: UITextView!
    @IBOutlet weak var inviteMoreTextView: UITextView!
    @IBOutlet weak var inviteMoreTextView2: UITextView!
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var eventStream: UILabel!
    @IBOutlet weak var approveButton: UIButton!
    
    
    var sharedTextViewHeight:CGFloat!
    var sharedTextView2Height:CGFloat!
    var sharedtableViewHeight:CGFloat!
    var sharedtableView2Height:CGFloat!
    var approveButtonHeight:CGFloat!
    var eventStreamLabelHeight: CGFloat!
    
    var sharedTextViewY:CGFloat!
    var sharedTextView2Y:CGFloat!
    var sharedtableViewY:CGFloat!
    var sharedtableView2Y:CGFloat!
    var approveButtonY:CGFloat!
    var eventStreamLabelY:CGFloat!
    
    var textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
    var withAccess = [PFObject]()
    var withoutAcess = [PFObject]()
    var pendingInvites = [PFObject]()
    //var invitation = PFObject(className: "Invitations")
    
    var invitationIds = [String]()
    
    
    var currentTab = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        currentTab = 0
        
        sharedTextViewHeight = sharedTextView.frame.height
        sharedTextView2Height = sharedTextView2.frame.height
        sharedtableViewHeight = sharedtableView.frame.height
        sharedtableView2Height = sharedtableView2.frame.height
        approveButtonHeight = approveButton.frame.height
        eventStreamLabelHeight = eventStreamLabel.frame.height
        
        
        sharedTextViewY = sharedTextView.frame.origin.y
        sharedTextView2Y = sharedTextView2.frame.origin.y
        sharedtableViewY = sharedtableView.frame.origin.y
        sharedtableView2Y = sharedtableView2.frame.origin.y
        approveButtonY = approveButton.frame.origin.y
        eventStreamLabelY = eventStreamLabel.frame.origin.y
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 4
        let attributes = [NSParagraphStyleAttributeName : style]
        
        let attributedString = NSMutableAttributedString(string:"")
        approveButton.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(12.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 226.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        
        
        
        let buttonTitleStr = NSMutableAttributedString(string:"Approve All", attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        approveButton.setAttributedTitle(attributedString, forState: .Normal)
        
        
        approveButton.frame.size.width = approveButton.sizeThatFits(approveButton.bounds.size).width
        
        approveButton.frame.origin.x = self.view.frame.width - approveButton.frame.size.width - 20
        
        eventStreamLabel.frame.size.width = eventStreamLabel.sizeThatFits(eventStreamLabel.bounds.size).width
        eventStreamLabel.frame.origin.x = approveButton.frame.origin.x + ((approveButton.frame.width/2)-(eventStreamLabel.frame.width/2))
        
        inviteTextView.attributedText = NSAttributedString(string: inviteTextView.text, attributes:attributes)
        inviteTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        inviteTextView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        
        sharedTextView.attributedText = NSAttributedString(string:sharedTextView.text, attributes:attributes)
        sharedTextView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        sharedTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        sharedTextView2.attributedText = NSAttributedString(string:sharedTextView2.text, attributes:attributes)
        sharedTextView2.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        sharedTextView2.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        sharedTextView.frame.size.height = sharedTextView.sizeThatFits(sharedTextView.bounds.size).height
        
        sharedTextView2.frame.size.height = sharedTextView2.sizeThatFits(sharedTextView2.bounds.size).height
        
        fetchInvitations()
        
        pendingTextView.attributedText = NSAttributedString(string:pendingTextView.text, attributes:attributes)
        pendingTextView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        pendingTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        inviteMoreTextView.attributedText = NSAttributedString(string:inviteMoreTextView.text, attributes:attributes)
        inviteMoreTextView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        inviteMoreTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        inviteMoreTextView2.attributedText = NSAttributedString(string:inviteMoreTextView2.text, attributes:attributes)
        inviteMoreTextView2.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        inviteMoreTextView2.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        
        //        pendingTableView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        //        sharedtableView2.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        //        sharedtableView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pendingTableView.separatorColor = UIColor.whiteColor()
        sharedtableView2.separatorColor = UIColor.whiteColor()
        sharedtableView.separatorColor = UIColor.whiteColor()
        
        //self.invitationCode.backgroundColor = UIColor(patternImage: UIImage(named:"border.png")!)
        
        
        sharedbutton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        sharedbutton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
        pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
        inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        inviteMoreButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        
        
        var invitationIdsString = ""
        
        if invitationIds.count > 0
        {
            invitationIdsString = invitationIds.joinWithSeparator("','")
            
            //println(invitationIds)
            
        }
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(invitationIdsString)'}) AND eventObjectId = '\(currentEvent.objectId!)'")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        
        let updatePredicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)' AND isUpdated = true")
        
        let updateQuery = PFQuery(className:"Invitations", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        //        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchInvitationUpdatesSuccess:", successSelectorParameters: nil, errorSelector: "fetchInvitationUpdatesError:", errorSelectorParameters:nil)
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchUpdatedInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchUpdatedInvitationsError:", errorSelectorParameters:nil)
        
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        fetchInvitations()
        if currentTab == 0
        {
            
            invitemoreView.hidden = true
            sharedbutton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            sharedbutton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
            pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
            pendingButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
            inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
            inviteMoreButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
            
            if withAccess.count > 0 || withoutAcess.count > 0
            {
                sharedView.hidden = false
                sharedPopUpView.hidden = true
            }
            else
            {
                if withAccess.count == 0 && withoutAcess.count == 0
                {
                    sharedView.hidden = false
                    sharedPopUpView.hidden = false
                }
            }
            
        }
        if currentTab == 1
        {
            pendingButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            pendingButton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
            sharedbutton.setTitleColor(textColor, forState: UIControlState.Normal)
            sharedbutton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
            inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
            inviteMoreButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        }
        if currentTab == 2
        {
            inviteMoreButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            inviteMoreButton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
            sharedbutton.setTitleColor(textColor, forState: UIControlState.Normal)
            sharedbutton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
            pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
            pendingButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        }
    }
    
    func fetchUpdatedInvitationsError(timer:NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            for invitation in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = invitation.objectId!
                tblFields["invitedName"] = invitation["invitedName"] as? String
                tblFields["userObjectId"] = invitation["userObjectId"] as? String
                tblFields["attendingStatus"] = invitation["attendingStatus"] as? String
                tblFields["invitationType"] = invitation["invitationType"] as? String
                
                let noOfAdults = invitation["noOfAdults"] as! Int
                let noOfChilds = invitation["noOfChilds"] as! Int
                
                tblFields["noOfAdults"] = "\(noOfAdults)"
                tblFields["noOfChilds"] = "\(noOfChilds)"
                
                if invitation["needsContentApprovel"] as? Bool == true
                {
                    tblFields["needsContentApprovel"] = "1"
                }
                else
                {
                    tblFields["needsContentApprovel"] = "0"
                }
                
                
                
                var date = ""
                
                if invitation.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if invitation.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                tblFields["emailId"] = invitation["emailId"] as? String
                tblFields["eventObjectId"] = invitation["eventObjectId"] as? String
                
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }
                
                
                var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
                
            }
            
            fetchInvitations()
            
        }
    }
    
    func fetchUpdatedInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            let i = 0
            
            for invitation in fetchedobjects
            {
                
                fetchedobjects[i]["isUpdated"] = false
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["userObjectId"] = invitation["userObjectId"] as? String
                tblFields["attendingStatus"] = invitation["attendingStatus"] as? String
                
                
                
                let noOfAdults = invitation["noOfAdults"] as! Int
                let noOfChilds = invitation["noOfChilds"] as! Int
                
                tblFields["noOfAdults"] = "\(noOfAdults)"
                tblFields["noOfChilds"] = "\(noOfChilds)"
                
                if invitation["needsContentApprovel"] as? Bool == true
                {
                    tblFields["needsContentApprovel"] = "1"
                }
                else
                {
                    tblFields["needsContentApprovel"] = "0"
                }
                
                
                var date = ""
                
                if invitation.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if invitation.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                tblFields["emailId"] = invitation["emailId"] as? String
                tblFields["eventObjectId"] = invitation["eventObjectId"] as? String
                
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }
                
                
                //var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
                
                //var insertedId = ModelManager.instance.updateTableData(
                
                var isUpdated = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitation.objectId!])
                
            }
            
            
            PFObject.saveAllInBackground(fetchedobjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    
                }
                else
                {
                    
                }
            }
            
            
            fetchInvitations()
            
        }
    }
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchInvitations()
    {
        
        
        withAccess = []
        withoutAcess = []
        pendingInvites = []
        invitationIds = []
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString:"eventObjectId='\(currentEvent.objectId!)'", whereFields: [])
        
        if (resultSet != nil) {
            
            while resultSet.next()
            {
                let invitation = PFObject(className: "Invitations")
                
                var attendingStatus = resultSet.stringForColumn("attendingStatus")
                let isApproved = resultSet.stringForColumn("isApproved")
                
                invitation.objectId = resultSet.stringForColumn("objectId")
                print(invitation.objectId)
                invitation["userObjectId"] = resultSet.stringForColumn("userObjectId")
                invitation["attendingStatus"] = resultSet.stringForColumn("attendingStatus")
                invitation["invitationType"] = resultSet.stringForColumn("invitationType")
                // invitation["needsContentApprovel"] = resultSet.stringForColumn("needsContentApprovel")
                //                invitation["createdAt"] = resultSet.stringForColumn("createdAt")
                //                invitation["updatedAt"] = resultSet.stringForColumn("updatedAt")
                invitation["dateCreated"] = resultSet.stringForColumn("dateCreated")
                invitation["dateUpdated"] = resultSet.stringForColumn("dateUpdated")
                invitation["isApproved"] = resultSet.stringForColumn("isApproved")
                invitation["emailId"] = resultSet.stringForColumn("emailId")
                invitation["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
                invitation["invitedName"] = resultSet.stringForColumn("invitedName")
                
                if  invitation["userObjectId"] as! String != ""
                {
                    if isApproved == "0"
                    {
                        withoutAcess.append(invitation)
                    }
                    else
                    {
                        withAccess.append(invitation)
                    }
                }
                else
                {
                    pendingInvites.append(invitation)
                }
                
                invitationIds.append(invitation.objectId!)
                
                print("sdedfew: " + (invitation["invitedName"] as! String))
                
            }
            
            
            resultSet.close()
            
        }
        
        if withAccess.count == 0 && withoutAcess.count == 0 && pendingInvites.count == 0
        {
            buttonView.hidden = true
            sharedView.hidden = true
            invitemoreView.hidden = false
            currentTab = 2
        }
        else
        {
            buttonView.hidden = false
            
            if  withAccess.count == 0 && withoutAcess.count == 0
            {
                sharedPopUpView.hidden = false
            }
            else if pendingInvites.count == 0
            {
                pendingPopUpView.hidden = false
            }
            else
            {
                sharedPopUpView.hidden = true
                pendingPopUpView.hidden = true
            }
            
        }
        
        print(withAccess)
        print(withoutAcess)
        
        if withoutAcess.count > 0 && withAccess.count>0
        {
            
            eventStreamLabel.hidden = false
            sharedTextView.hidden = false
            sharedtableView.hidden = false
            sharedTextView2.hidden = false
            sharedtableView2.hidden = false
            approveButton.hidden = false
            
            sharedTextView.frame.origin.y = eventStreamLabelY
            eventStreamLabel.frame.origin.y = sharedTextView.frame.origin.y + sharedTextView.frame.height + 5
            
            sharedtableView.frame.origin.y = eventStreamLabel.frame.origin.y + eventStreamLabel.frame.height + 5
            
            if sharedtableView.contentSize.height > sharedtableViewHeight
            {
                sharedtableView.frame.size.height = sharedtableViewHeight
            }
            else
            {
                sharedtableView.frame.size.height = sharedtableView.contentSize.height
            }
            
            sharedTextView2.frame.origin.y = sharedtableView.frame.size.height + sharedtableView.frame.origin.y + 10
            approveButton.frame.origin.y = sharedTextView2.frame.origin.y + sharedTextView2.frame.height + 5
            
            sharedtableView2.frame.origin.y = approveButton.frame.origin.y + approveButton.frame.height + 5
            
            sharedtableView2.frame.size.height = sharedPopUpView.frame.height - (sharedtableView2.frame.origin.y - sharedPopUpView.frame.origin.y)
        }
        else
        {
            if withAccess.count > 0
            {
                
                eventStreamLabel.hidden = false
                sharedTextView.hidden = false
                sharedtableView.hidden = false
                sharedTextView2.hidden = true
                sharedtableView2.hidden = true
                approveButton.hidden = true
                
                sharedTextView.frame.origin.y = eventStreamLabelY
                eventStreamLabel.frame.origin.y = sharedTextView.frame.origin.y + sharedTextView.frame.height + 5
                
                sharedtableView.frame.origin.y = eventStreamLabel.frame.origin.y + eventStreamLabel.frame.height + 5
                
                sharedtableView.frame.size.height = sharedPopUpView.frame.height - (sharedtableView.frame.origin.y - sharedPopUpView.frame.origin.y)
            }
            else
            {
                eventStreamLabel.hidden = true
                sharedTextView.hidden = true
                sharedtableView.hidden = true
                sharedTextView2.hidden = false
                sharedtableView2.hidden = false
                approveButton.hidden = false
                
                sharedTextView2.frame.origin.y = eventStreamLabelY
                approveButton.frame.origin.y = sharedTextView2.frame.origin.y + sharedTextView2.frame.height + 5
                
                sharedtableView2.frame.origin.y = approveButton.frame.origin.y + approveButton.frame.height + 5
                
                sharedtableView2.frame.size.height = sharedPopUpView.frame.height - (sharedtableView2.frame.origin.y - sharedPopUpView.frame.origin.y)
                
            }
        }
        
        sharedtableView.reloadData()
        sharedtableView2.reloadData()
        pendingTableView.reloadData()
        
    }
    
    
    @IBAction func sharedButton(sender: AnyObject)
    {
        currentTab = 0
        //sharedtableView2.backgroundColor = UIColor.blackColor()
        sharedbutton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        sharedbutton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
        pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
        inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        inviteMoreButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        pendingView.hidden = true
        invitemoreView.hidden = true
        sharedView.hidden = false
        
        fetchInvitations()
    }
    @IBAction func invitemoreButton(sender: AnyObject)
    {
        currentTab = 2
        sharedbutton.setTitleColor(textColor, forState: UIControlState.Normal)
        sharedbutton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
        inviteMoreButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        inviteMoreButton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
        pendingView.hidden = true
        invitemoreView.hidden = false
        sharedView.hidden = true
        
    }
    @IBAction func pendingButton(sender: AnyObject)
    {
        currentTab = 1
        sharedbutton.setTitleColor(textColor, forState: UIControlState.Normal)
        sharedbutton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        pendingButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
        inviteMoreButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        pendingView.hidden = false
        invitemoreView.hidden = true
        sharedView.hidden = true
        fetchInvitations()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if tableView == sharedtableView
        {
            return 1
        }
            
        else if tableView == sharedtableView2
        {
            return 1
            
        }
        else
        {
            return 1
            
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == sharedtableView
        {
            return withAccess.count
        }
            
        else if tableView == sharedtableView2
        {
            return withoutAcess.count
            
        }
        else
        {
            return pendingInvites.count
            
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let row = indexPath.row
        var cellIdentifier: String! = ""
        
        if tableView == sharedtableView
        {
            
            
            
            cellIdentifier = "SharedWithAccessTableViewCell"
            
            let cell: SharedWithAccessTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? SharedWithAccessTableViewCell
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            
            let nameLabel = UILabel()
            //nameLabel.frame = CGRectMake(self.view.frame.width*(16/320),self.view.frame.height*(2/568),self.view.frame.width*(143/320), nameLabel.frame.height)
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320),cell!.contentView.frame.height*(5/50), self.view.frame.width*(220.0/320), self.view.frame.height*(25.0/568))
            nameLabel.text = withAccess[row]["invitedName"] as? String
            print(nameLabel.text)
            nameLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 14.0)
            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            nameLabel.adjustsFontSizeToFitWidth = true
            
            let contactLabel = UILabel()
            contactLabel.frame = CGRectMake(self.view.frame.width*(20/320),cell!.contentView.frame.height*(30/50),self.view.frame.width*(143/320), self.view.frame.height*(12.0/568))
            
            
            
            contactLabel.text = withAccess[row]["emailId"] as? String
            
            contactLabel.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            
            contactLabel.textColor = UIColor(red: 165.0/255, green: 165.0/255, blue: 165.0/255, alpha: 1.0)
            contactLabel.adjustsFontSizeToFitWidth = true
            
            
            let image = UIImage(named: "check-box.png") as UIImage?
            let button = UIButton()
            button.frame = CGRectMake(approveButton.frame.origin.x + ((approveButton.frame.width/2)-(self.view.frame.width*(7/320))), self.view.frame.height*(15/568), self.view.frame.width*(14/320),self.view.frame.width*(14/320))
            button.setImage(image, forState: UIControlState.Normal)
            button.addTarget(self, action: "disapproval:", forControlEvents:.TouchUpInside)
            button.tag = indexPath.row
            
            cell?.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            cell?.contentView.addSubview(button)
            cell?.contentView.addSubview(nameLabel)
            cell?.contentView.addSubview(contactLabel)
            cell?.selectionStyle = .None
            return cell!
            
        }
            
        else if tableView == sharedtableView2
        {
            
            cellIdentifier = "SharedWithoutAccessTableViewCell"
            
            let cell: SharedWithoutAccessTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? SharedWithoutAccessTableViewCell
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            
            let nameLabel = UILabel()
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320),cell!.contentView.frame.height*(5/50), self.view.frame.width*(220.0/320), self.view.frame.height*(25.0/568))
            nameLabel.text = withoutAcess[row]["invitedName"] as? String
            print(nameLabel.text)
            nameLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 14.0)
            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            nameLabel.adjustsFontSizeToFitWidth = true
            
            let contactLabel = UILabel()
            contactLabel.frame = CGRectMake(self.view.frame.width*(20/320),cell!.contentView.frame.height*(30/50),self.view.frame.width*(143/320), self.view.frame.height*(12.0/568))
            
            contactLabel.text = withoutAcess[row]["emailId"] as? String
            contactLabel.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            contactLabel.textColor = UIColor(red: 165.0/255, green: 165.0/255, blue: 165.0/255, alpha: 1.0)
            contactLabel.adjustsFontSizeToFitWidth = true
            
            
            let image = UIImage(named: "checkbox.png") as UIImage?
            let button = UIButton()
            button.frame = CGRectMake(approveButton.frame.origin.x + ((approveButton.frame.width/2)-(self.view.frame.width*(7/320))), self.view.frame.height*(15/568), self.view.frame.width*(14/320), self.view.frame.width*(14/320))
            //button.frame = CGRectMake(286, 10, 14, 14)
            button.setImage(image, forState: UIControlState.Normal)
            button.addTarget(self, action: "accessButton:", forControlEvents:.TouchUpInside)
            button.tag = indexPath.row
            
            cell?.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            cell?.contentView.addSubview(button)
            cell?.contentView.addSubview(nameLabel)
            cell?.contentView.addSubview(contactLabel)
            
            cell?.selectionStyle = .None
            return cell!
            
        }
        else
        {
            cellIdentifier = "pendingViewCell"
            
            let cell: pendingViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? pendingViewCell
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            let nameLabel = UILabel()
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320), cell!.contentView.frame.height*(2.0/50), self.view.frame.width*(220.0/320), self.view.frame.height*(26.0/568))
            nameLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 14.0)
            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            nameLabel.adjustsFontSizeToFitWidth = true
            nameLabel.text =  pendingInvites[row]["invitedName"] as? String
            
            let emaillabel = UILabel()
            emaillabel.frame = CGRectMake(self.view.frame.width*(20.0/320), cell!.contentView.frame.height*(20.0/50), self.view.frame.width*(220.0/320), self.view.frame.height*(20.0/568))
            print(cell!.contentView.frame.height)
            emaillabel.font = UIFont(name:"AvenirNext-Medium", size: 12.0)
            emaillabel.textColor = UIColor(red: 165.0/255, green: 165.0/255, blue: 165.0/255, alpha: 1.0)
            emaillabel.adjustsFontSizeToFitWidth = true
            emaillabel.text =  pendingInvites[row]["emailId"] as? String
            print(nameLabel.text)
            
            let remindButton = UIButton()
            remindButton.frame = CGRectMake(self.view.frame.width*(230/320),cell!.contentView.frame.height*(13/50), self.view.frame.width*(90/320),cell!.contentView.frame.height*(30.0/50))
            
            remindButton.setTitle("Remind", forState: UIControlState.Normal)
            
            remindButton.setTitleColor(UIColor(red: 68.0/255, green: 185.0/255, blue: 227.0/255, alpha: 1.0), forState: UIControlState.Normal)
            
            remindButton.tag = indexPath.row
            remindButton.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            remindButton.addTarget(self, action: "remindButton:", forControlEvents:.TouchUpInside)
            cell?.contentView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            
            cell?.contentView.addSubview(nameLabel)
            cell?.contentView.addSubview(remindButton)
            cell?.contentView.addSubview(emaillabel)
            cell?.selectionStyle = .None
            return cell!
            
        }
        
        
    }
    
    func disapproval(sender:UIButton)
    {
        var inviteObject:PFObject!
        
        inviteObject = withAccess[sender.tag]
        
        inviteObject["isApproved"] = false
        inviteObject["isEventUpdated"] = true
        
        ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "disapprovalSuccess:", successSelectorParameters: nil, errorSelector: "disapprovalError:", errorSelectorParameters:"email")
        
    }
    
    func disapprovalSuccess(timer: NSTimer)
    {
        let invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        let invitationObjectId = invitation.objectId!
        
        var tblFields: Dictionary! = [String: String]()
        
        
        tblFields["isApproved"] = "0"
        
        
        var insertedId = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitationObjectId])
        
        fetchInvitations()
        
        sharedtableView.reloadData()
        sharedtableView2.reloadData()
        
    }
    
    func disapprovalError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    func remindButton(sender:UIButton)
    {
        
        sender.setTitle("Reminded", forState: UIControlState.Normal)
        sender.setTitleColor(UIColor(red: 155.0/255, green: 155.0/255, blue: 155.0/255, alpha: 1.0), forState: UIControlState.Normal)
        
        let eventType = pendingInvites[sender.tag]["invitationType"] as! String!
        let email = pendingInvites[sender.tag]["emailId"] as! String
        var inviteCode = currentEvent.objectId!
        let eventTitle = currentEvent["eventTitle"] as! String
        let hostName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        let eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        let eventFolder = currentEvent["eventFolder"] as! String!
        let eventImage = currentEvent["eventImage"] as! String!
        let timeString = ""
        let dateString = ""
        var message = ""
        let locationString = ""
        let createdAt = ""
        let updatedAt = ""
        
        let notifMessage = "Reminder: \(hostName) invited you to the event, \(eventTitle). Please respond to the invitation."
        
        let data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "invitationreminder",
            "eventType" : "\(eventType)",
            "emailId" : "\(email)",
            "eventObjectId": currentEvent.objectId!,
            "eventTitle": "\(eventTitle)",
            "dateString": "\(dateString)",
            "timeString": "\(timeString)",
            "locationString" : "\(locationString)",
            "hostName" : "\(hostName)",
            "createdAt": "\(createdAt)",
            "updatedAt" : "\(updatedAt)",
            "eventCreatorId": "\(eventCreatorId)"
            
        ]
        
        var urlString = String()
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                print(url!)
                
                urlString = url!
                
                let onlineOnlyReminder = OnlineOnlyReminder()
                
                let emailMessage = onlineOnlyReminder.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: hostName, type: "online",imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                
                let sendNonInvitationEmailsObject = SendNonInvitationEmails()
                
                sendNonInvitationEmailsObject.sendEmail("Reminder, please respond.", message: emailMessage, emails: [email])
                
            }
            
        })
        
        
        let userObjectId = pendingInvites[sender.tag]["userObjectId"] as! String
        
        let predicate = NSPredicate(format: "userObjectId IN {'\(userObjectId)'}")
        
        let query = PFInstallation.queryWithPredicate(predicate)
        
        let push = PFPush()
        push.setQuery(query)
        push.setData(data)
        push.sendPushInBackground()
        
    }
    
    
    
    func accessButton(sender:UIButton)
    {
        var inviteObject:PFObject!
        
        inviteObject = withoutAcess[sender.tag]
        
        inviteObject["isApproved"] = true
        inviteObject["isEventUpdated"] = true
        
        
        ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "approveSuccess:", successSelectorParameters: nil, errorSelector: "approveError:", errorSelectorParameters:"email")
        
        
    }
    
    
    func approveSuccess(timer: NSTimer)
    {
        let invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        let invitationObjectId = invitation.objectId!
        var tblFields: Dictionary! = [String: String]()
        
        
        tblFields["isApproved"] = "1"
        
        
        var insertedId = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitationObjectId])
        
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        let eventTitle = currentEvent["eventTitle"] as! String
        
        let notifMessage = "\(fullUserName) gave you access to photos/videos/notes for the event, \(eventTitle). Check them out."
        
        
        let isApproved = invitation["isApproved"] as! Bool
        let isEventUpdated = invitation["isEventUpdated"] as! Bool
        
        
        let createdAt = ""
        let updatedAt = ""
        
        let data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "streamaccesschanged",
            "eventObjectId": currentEvent.objectId!,
            "createdAt": "\(createdAt)",
            "updatedAt": "\(updatedAt)",
            "isApproved": "\(isApproved)",
            "isEventUpdated": "\(isEventUpdated)"
            
        ]
        
        
        let userObjectId = invitation["userObjectId"] as! String
        
        let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        let notificationObject = PFObject(className: "Notifications")
        notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
        notificationObject["notificationImage"] = "profilePic.png"
        notificationObject["senderId"] = currentUserId
        notificationObject["receiverId"] = userObjectId
        notificationObject["notificationActivityMessage"] = notifMessage
        notificationObject["eventObjectId"] = currentEvent.objectId!
        notificationObject["notificationType"] = "streamaccesschanged"
        
        
        
        let predicate = NSPredicate(format: "userObjectId IN {'\(userObjectId)'} AND hostActivityNotification = true ")
        
        notificationObject.saveInBackground()
        
        let query = PFInstallation.queryWithPredicate(predicate)
        
        let push = PFPush()
        push.setQuery(query)
        push.setData(data)
        push.sendPushInBackground()
        
        
        fetchInvitations()
        
        
    }
    
    func approveError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    
    
    @IBAction func approveAllButton(sender: AnyObject)
    {
        if withoutAcess.count > 0
        {
            var withouAccessInvites = [PFObject]()
            var userObjectIds = [String]()
            var notificationObjects = [PFObject]()
            
            
            let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            let eventTitle = currentEvent["eventTitle"] as! String
            
            let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
            
            let notifMessage = "\(fullUserName) gave you access to photos/videos/notes for the event, \(eventTitle). Check them out."
            
            
            
            for (var i = 0; i < withoutAcess.count; i++)
            {
                withouAccessInvites.append(withoutAcess[i])
                withouAccessInvites[i]["isApproved"] = true
                withouAccessInvites[i]["isEventUpdated"] = true
                
                userObjectIds.append(withouAccessInvites[i]["userObjectId"] as! String)
                
                let notificationObject = PFObject(className: "Notifications")
                notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                notificationObject["notificationImage"] = "profilePic.png"
                notificationObject["senderId"] = currentUserId
                notificationObject["receiverId"] = withouAccessInvites[i]["userObjectId"] as! String
                notificationObject["notificationActivityMessage"] = notifMessage
                notificationObject["eventObjectId"] = currentEvent.objectId!
                notificationObject["notificationType"] = "streamaccesschanged"
                
                notificationObjects.append(notificationObject)
                
                
            }
            
            PFObject.saveAllInBackground(notificationObjects)
            
            let createdAt = ""
            let updatedAt = "'"
            
            let data = [
                "alert" : "\(notifMessage)",
                "notifType" :  "streamaccesschanged",
                "eventObjectId" : currentEvent.objectId!,
                "isApproved": "true",
                "isEventUpdated": "true",
                "createdAt": "\(createdAt)",
                "updatedAt" : "\(updatedAt)"
            ]
            
            let userObjectIdsString = userObjectIds.joinWithSeparator("','")
            
            let predicate = NSPredicate(format: "userObjectId IN {'\(userObjectIdsString)'} AND hostActivityNotification = true  ")
            
            
            let query = PFInstallation.queryWithPredicate(predicate)
            
            let push = PFPush()
            push.setQuery(query)
            push.setData(data)
            push.sendPushInBackground()
            
            PFObject.saveAllInBackground( withouAccessInvites) { (success:Bool, error:NSError?) -> Void in
                if success
                {
                    var invitationIdsString = ""
                    
                    if self.withoutAcess.count > 0
                    {
                        for (var i = 0; i < self.withoutAcess.count; i++)
                        {
                            let invitationId = self.withoutAcess[i].objectId!
                            invitationIdsString = "\(invitationIdsString)\(invitationId)"
                            if i < self.withoutAcess.count - 1
                            {
                                invitationIdsString = "\(invitationIdsString)','"
                            }
                        }
                        
                    }
                    
                    if invitationIdsString != ""
                    {
                        var tblFields: Dictionary! = [String: String]()
                        tblFields["isApproved"] = "1"
                        ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId IN ('\(invitationIdsString)') ", whereFields: [])
                    }
                    
                    self.fetchInvitations()
                    
                }
                else
                {
                    
                }
            }
        }
        
    }
    
    @IBAction func fetchEmailContacts(sender: AnyObject)
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            var error : Unmanaged<CFError>? = nil
            let addressBook : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
            if addressBook == nil
            {
                print(error)
                return
            }
            
            ABAddressBookRequestAccessWithCompletion(addressBook) {
                (granted: Bool, error: CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if !granted {
                        print("Just denied")
                        
                        if #available(iOS 8.0, *) {
                            var refreshAlert = UIAlertController(title: "Provide Access", message: "Provide Eventnode with access to your contacts. Go to Settings > Privacy > Contacts and enable Eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                                
                            }))
                            
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                            
                        } else {
                            // Fallback on earlier versions
                        }
                        
                        
                        
                        
                    } else {
                        //println("Just authorized")
                        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
                        self.navigationController?.pushViewController(homeVC, animated: false)
                        
                    }
                }
            }
            
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
            NSLog("access denied")
            
            if #available(iOS 8.0, *) {
                var refreshAlert = UIAlertController(title: "Provide Access", message: "Provide Eventnode with access to your contacts. Go to Settings > Privacy > Contacts and enable Eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
                
            } else {
                // Fallback on earlier versions
            }
            
            
            
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
        
    }
    
    @IBAction func fetchFacebookFriends(sender: AnyObject)
    {
        if isFacebookLogin
        {
            let inviteFbVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFacebookFriendsViewController") as! InviteFacebookFriendsViewController
            self.navigationController?.pushViewController(inviteFbVC, animated: false)
        }
        else
        {
            showLinkfacebookView = true
            
            let ConnectFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("connectFacebookAccount") as! facebookLinkedViewController
            
            ConnectFacebook.isInviteView = true
            
            self.navigationController?.pushViewController(ConnectFacebook, animated: false)
            
        }
        
    }
    
    
    
    
    @IBAction func socialShareButton(sender: AnyObject)
    {
        let plainData = (currentEvent.objectId! as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        print(base64String)
        
        print(currentEvent)
        
        let branchUrl = currentEvent["socialSharingURL"] as! String
        
        let plainDataUrl = (branchUrl as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        print(base64UrlString)
        
        
        
        let urlString = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId=\(base64String)&url=\(base64UrlString)"
        
        let eventTitle = currentEvent["eventTitle"] as! String
        
        let objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(urlString)"
        
        let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
        self.navigationController!.presentViewController(activityVC,
            animated: true,
            completion: nil)
    }
    
    @IBAction func cancelButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
}
