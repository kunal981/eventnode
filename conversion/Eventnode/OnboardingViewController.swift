//
//  OnboardingViewController.swift
//  Eventnode
//
//  Created by Chandra Kilaru on 9/26/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation
class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var welcomeMsg: UITextView!
    
    override func viewDidLoad() {
        var fullUserName: String = NSUserDefaults.standardUserDefaults().valueForKey("fullUserName") as! String
        
        welcomeMsg.text = "Welcome \(fullUserName)"
        welcomeMsg.textAlignment = NSTextAlignment.Center
        welcomeMsg.font = UIFont(name: "Avenir Next Medium", size: 16.0)
        welcomeMsg.textColor = UIColor(red: 155.0/255, green: 155.0/255, blue: 155.0/255, alpha: 1.0)
        
    }
    
    @IBAction func createInPersonButtonClicked(sender: AnyObject) {
        print("create In Person Button Clicked")
        let eventDetailsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventDetailsVC.isInPersonEvent = true
        self.navigationController?.pushViewController(eventDetailsVC, animated: false)
    }
    
    @IBAction func createOnlineButtonClicked(sender: AnyObject) {
        print("create Online Person Button Clicked")
        let eventDetailsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        self.navigationController?.pushViewController(eventDetailsVC, animated: false)
    }
    
    @IBAction func joinEventButtonClicked(sender: AnyObject) {
        print("Join Event Button Clicked")
        let refreshAlert = UIAlertController(title: "Do you have an event invite?", message: "If you have an event invitation email or a Link, click on that to access the event.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func supportButtonClicked(sender: AnyObject) {
       // Mobihelp.sharedInstance().presentSupport(self)
    }
    
}