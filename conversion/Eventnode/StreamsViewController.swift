////
////  StreamsViewController.swift
////  Eventnode
////
////  Created by mrinal khullar on 8/21/15.
////  Copyright (c) 2015 eventnode LLC. All rights reserved.
////
//
//import UIKit
//import MobileCoreServices
//import AVKit
//import AVFoundation
//import MediaPlayer
//import EventKit
//
////var isSharedPostUpdated:Bool! = true
////
////
////
////
////var mySharedEventData = [PFObject]()
////var mySharedRowHeights = [CGFloat]()
////
////var SharedEventTextTitle = "Add Text"
//
//class StreamsViewController: UIViewController {
//    
//    @IBOutlet var tableView : UITableView!
//    
//    @IBOutlet weak var selectedImageWrapper: UIView!
//    @IBOutlet weak var selectedImage: UIImageView!
//    
//    @IBOutlet weak var invitationView: UIView!
//    @IBOutlet weak var streamView: UIView!
//    
//    @IBOutlet weak var adultsLabel: UILabel!
//    @IBOutlet weak var childLabel: UILabel!
//    
//    @IBOutlet weak var invitationBottom: UIView!
//    @IBOutlet weak var streamBottom: UIView!
//    
//    
//    var currentStatus:String!
//    var statusToBeChanged:String!
//    
//    var invitationNoteViewHeight: CGFloat = 0
//    
//    @IBOutlet var statusInfo: UILabel!
//    @IBOutlet var openMessageView: UIView!
//    @IBOutlet var messagePopupView: UIView!
//    @IBOutlet var statusButtonsView: UIView!
//    @IBOutlet var statusInfoView: UIView!
//    @IBOutlet var noOfChildText: UITextField!
//    @IBOutlet var noOfAdultsText: UITextField!
//    
//    @IBOutlet var messageTitle: UITextView!
//    @IBOutlet var noOfChildWrapper: UIView!
//    @IBOutlet var noOfAdultsWrapper: UIView!
//    
//    @IBOutlet var invitationNoteView: UITextView!
//    @IBOutlet var attendingStatusImageView: UIImageView!
//    
//    
//    @IBOutlet weak var secondSubView: UIView!
//    @IBOutlet weak var hostNameBottom: UILabel!
//    @IBOutlet weak var hostName: UILabel!
//    @IBOutlet weak var eventLocationText: UITextView!
//    
//    @IBOutlet weak var senderName: UILabel!
//    
//    @IBOutlet weak var eventHeaderTitle: UILabel!
//    
//    @IBOutlet weak var eventTitleText: UITextView!
//    @IBOutlet weak var eventStartText: UITextView!
//    @IBOutlet weak var eventEndText: UITextView!
//    
//    @IBOutlet weak var notefromhost: UILabel!
//    
//    @IBOutlet weak var eventDescriptionText: UITextView!
//    
//    @IBOutlet weak var subView: UIView!
//    
//    @IBOutlet weak var attendEvent: UIButton!
//    @IBOutlet weak var notAttendEvent: UIButton!
//    @IBOutlet weak var notSure: UIButton!
//    @IBOutlet weak var followOnline: UIButton!
//    
//    var eventTitle: String!
//    var eventObject: PFObject!
//    var eventStore : EKEventStore = EKEventStore()
//    
//    
//    var newMedia: Bool = true
//    
//    var moviePlayer : MPMoviePlayerController?
//    
//    @IBOutlet weak var invitationMessageView: UIView!
//    @IBOutlet weak var invitationStreamView: UIView!
//    @IBOutlet weak var postMessageView: UIView!
//    @IBOutlet weak var postStreamView: UIView!
//    
//    var moviePlayers = [Int: MPMoviePlayerController]()
//    var currentScrollTop = 0
//    var hideNow = 0
//    var currentUserId: String!
//    
//    var eventLogoFile: String!
//    
//    var eventLogoFileUrl: NSURL!
//    
//    
//    var likeObjectIds = [String]()
//    
//    @IBOutlet weak var eventTitleLabel: UILabel!
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        self.view.addSubview(wakeUpImageView)
//        
//        eventObject = currentSharedEvent
//        
//        statusInfoView.layer.borderWidth = 0.3
//        statusInfoView.layer.borderColor = UIColor(red: 145.0/255, green: 145.0/255, blue: 145.0/255, alpha: 0.7).CGColor
//        
//        
//        if((eventObject["isRSVP"] as! Bool) == true)
//        {
//            invitationView.hidden = false
//            
//            
//            
//            subView.layer.cornerRadius = 3
//            subView.layer.borderWidth = 0.3
//            subView.backgroundColor = UIColor(red: 242/255, green:  242/255, blue: 242/255, alpha: 1.0)
//            
//            subView.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
//            
//            
//            secondSubView.layer.cornerRadius = 3
//            secondSubView.layer.borderWidth = 0.3
//            secondSubView.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
//            
//            
//            /*var firstLine : UIView! = UIView(frame: CGRectMake(156, 3, 1, 27))
//            firstLine.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1.0)
//            self.secondSubView.addSubview(firstLine)*/
//            
//            
//            var latitude = eventObject["eventLatitude"] as! Double
//            var longitude = eventObject["eventLongitude"] as! Double
//            var eventLocation = eventObject["eventLocation"] as! String
//            print(eventObject["eventStartDateTime"] as! NSDate)
//            
//            //
//            
//            
//            let sdate = eventObject["eventStartDateTime"] as! NSDate
//            let edate = eventObject["eventEndDateTime"] as! NSDate
//            
//            let calendar = NSCalendar.currentCalendar()
//            
//            let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
//            
//            var sam = "AM"
//            var shour: Int!
//            if(scomponents.hour >= 12)
//            {
//                if(scomponents.hour > 12)
//                {
//                    shour = scomponents.hour-12
//                }
//                else
//                {
//                    shour = 12
//                }
//                
//                
//                sam = "PM"
//            }
//            else
//            {
//                shour = scomponents.hour
//                sam = "AM"
//                if(scomponents.hour==0){
//                    shour = 12
//                }
//            }
//            
//            var sminute = "\(scomponents.minute)"
//            let sday = scomponents.day
//            let smonth = scomponents.month
//            let syear = scomponents.year
//            
//            if(scomponents.minute<10)
//            {
//                sminute="0\(sminute)"
//            }
//            
//            
//            let startDate = "\(monthsArray[smonth-1]) \(sday), \(syear) - \(shour):\(sminute) \(sam)"
//            
//            
//            
//            let ecomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: edate)
//            
//            var eam = "AM"
//            var ehour: Int!
//            if(ecomponents.hour > 12)
//            {
//                if(ecomponents.hour > 12)
//                {
//                    ehour = ecomponents.hour-12
//                }
//                else
//                {
//                    ehour = 12
//                }
//                
//                
//                eam = "PM"
//            }
//            else
//            {
//                ehour = ecomponents.hour
//                eam = "AM"
//                if(ecomponents.hour==0){
//                    ehour = 12
//                }
//            }
//            
//            var eminute = "\(ecomponents.minute)"
//            let eday = ecomponents.day
//            let emonth = ecomponents.month
//            let eyear = ecomponents.year
//            
//            if(ecomponents.minute<10)
//            {
//                eminute = "0\(eminute)"
//            }
//            
//            let endDate = "\(monthsArray[emonth-1]) \(eday), \(eyear) - \(ehour):\(eminute) \(eam)"
//            
//            
//            print(startDate)
//            print(endDate)
//            print(eventTitle)
//            
//            invitationNoteViewHeight = invitationNoteView.frame.height
//            
//            eventTitleText.text = eventObject["eventTitle"] as! String
//            eventTitleText.textAlignment = NSTextAlignment.Center
//            eventTitleText.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
//            
//            
//            eventStartText.text = startDate
//            
//            eventStartText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
//            eventStartText.textAlignment = NSTextAlignment.Center
//            
//            eventLocationText.text = eventObject["eventLocation"] as! String!
//            eventLocationText.textAlignment = NSTextAlignment.Center
//            eventLocationText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
//            
//            
//            
//            eventDescriptionText.text = eventObject["eventDescription"] as! String!
//            eventDescriptionText.textColor = UIColor.blackColor()
//            
//            
//            let style = NSMutableParagraphStyle()
//            style.lineSpacing = 5
//            let attributes = [NSParagraphStyleAttributeName : style]
//            
//            eventDescriptionText.attributedText = NSAttributedString(string:eventDescriptionText.text, attributes:attributes)
//            eventDescriptionText.font = UIFont(name: "AvenirNext-Regular", size: 18.0)
//            eventDescriptionText.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
//            
//            let by = "by"
//            
//            let host =  eventObject["senderName"] as! String
//            
//            hostName.text = "\(by) \(host)"
//            hostNameBottom.text = eventObject["senderName"] as? String
//            
//        }
//        else
//        {
//            invitationView.hidden = true
//        }
//        
//        
//        if((eventObject["isApproved"] as! Bool) == true)
//        {
//            streamView.hidden = false
//            
//        }
//        else
//        {
//            streamView.hidden = true
//        }
//        
//        if (eventObject["isApproved"] as! Bool) == true && (eventObject["isRSVP"] as! Bool) == true
//        {
//            invitationMessageView.hidden = false
//            invitationStreamView.hidden = false
//            postMessageView.hidden = false
//            postStreamView.hidden = false
//        }
//        else if(eventObject["isApproved"] as! Bool) == true && (eventObject["isRSVP"] as! Bool) == false
//        {
//            invitationMessageView.hidden = true
//            invitationStreamView.hidden = true
//            postMessageView.hidden = true
//            postStreamView.hidden = true
//            
//            invitationBottom.hidden = true
//            streamBottom.hidden = true
//            
//            tableView.frame.size.height = self.view.frame.height - (tableView.frame.origin.y)
//            
//            postStreamView.frame.size.width = self.view.frame.width
//            
//            postStreamView.frame.origin.x = 0
//        }
//        else if(eventObject["isApproved"] as! Bool) == false && (eventObject["isRSVP"] as! Bool) == true
//        {
//            invitationMessageView.hidden = false
//            invitationStreamView.hidden = true
//            postMessageView.hidden = true
//            postStreamView.hidden = true
//            
//            invitationMessageView.frame.size.width = self.view.frame.width
//            
//            invitationMessageView.frame.origin.x = 0
//            
//        }
//        
//        
//        currentStatus = currentSharedEvent["attendingStatus"] as! String
//        statusToBeChanged = currentSharedEvent["attendingStatus"] as! String
//        
//        messagePopupView.hidden = true
//        
//        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
//        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
//        
//        
//        noOfAdultsText.text = "\(noOfAdults)"
//        noOfChildText.text = "\(noOfChilds)"
//        
//        if currentStatus != ""
//        {
//            
//            if currentStatus == "yes"
//            {
//                statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
//                attendingStatusImageView.image = UIImage(named:"check-green.png")
//            }
//            
//            if currentStatus == "online"
//            {
//                statusInfo.text = "I will follow online"
//                attendingStatusImageView.image = UIImage(named:"web-globe.png")
//            }
//            
//            if currentStatus == "maybe"
//            {
//                statusInfo.text = "I am not sure"
//                attendingStatusImageView.image = UIImage(named:"questionmark.png")
//            }
//            
//            if currentStatus == "no"
//            {
//                statusInfo.text = "I will not attend"
//                attendingStatusImageView.image = UIImage(named:"cross-red.png")
//            }
//            
//            openMessageView.hidden = false
//            statusButtonsView.hidden = true
//            statusInfoView.hidden = false
//        }
//        else
//        {
//            openMessageView.hidden = true
//            statusButtonsView.hidden = false
//            statusInfoView.hidden = true
//        }
//        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
//        
//        
//        
//        //senderName.text = eventObject["senderName"] as? String
//        
//        //styleLabelFont(senderName)
//        
//        
//        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
//        
//        if((currentSharedEvent["eventTitle"] as? String) == "")
//        {
//            eventTitleLabel.text = "No Title"
//        }
//        else
//        {
//            let eventTitleString = (currentSharedEvent["eventTitle"] as? String)!
//            
//            
//            currentSharedEvent["eventTitle"] = String(eventTitleString.characters.prefix(1)).capitalizedString + String(eventTitleString.characters.suffix(eventTitleString.characters.count - 1))
//            eventTitleLabel.text = currentSharedEvent["eventTitle"] as? String
//        }
//        
//        
//        moviePlayer = MPMoviePlayerController()
//        
//        moviePlayer!.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
//        
//        moviePlayer!.prepareToPlay()
//        
//        moviePlayer!.shouldAutoplay = false
//        moviePlayer!.view.hidden = true
//        self.view.addSubview(moviePlayer!.view)
//        
//        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "doneButtonClick:", name: MPMoviePlayerWillExitFullscreenNotification, object:nil)
//        
//        
//        tableView.separatorColor = UIColor.clearColor()
//        
//        
//        print(currentSharedEvent.objectId!)
//        
//        //var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("refreshContent"), userInfo: nil, repeats: true)
//        
//    }
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    
//    func styleTextFont(textView: UITextView)
//    {
//        textView.textColor = UIColor.blackColor()
//        textView.font = UIFont(name: "AveniNext-Medium", size: 13.0)
//    }
//    
//    
//    
//    func styleLabelFont(textLabel: UILabel)
//    {
//        textLabel.textColor = UIColor.whiteColor()
//        textLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
//    }
//    
//    
//    override func viewDidAppear(animated: Bool) {
//        
//        super.viewDidAppear(animated)
//        
//        if((currentSharedEvent["eventTitle"] as? String) == "")
//        {
//            eventTitleLabel.text = "No Title"
//        }
//        else
//        {
//            eventTitleLabel.text = currentSharedEvent["eventTitle"] as? String
//        }
//        
//        mySharedEventData.removeAll()
//        refreshList()
//        downloadData()
//        
//    }
//    
//    
//    /*
//    // MARK: - Navigation
//    
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//    // Get the new view controller using segue.destinationViewController.
//    // Pass the selected object to the new view controller.
//    }
//    */
//    
//    
//    @IBAction func addCommentButtonClicked(sender : AnyObject){
//        let addCommentVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddCommentsViewController") as! AddCommentsViewController
//        
//        addCommentVC.eventObject = currentSharedEvent
//        
//        self.navigationController?.pushViewController(addCommentVC, animated: true)
//        
//    }
//    
//    
//    @IBAction func invitationButtonClicked(sender : AnyObject){
//        
//        invitationView.hidden=false
//        streamView.hidden=true
//    }
//    
//    @IBAction func streamButtonClicked(sender : AnyObject){
//        
//        //NSLog("sdd")
//        invitationView.hidden=true
//        streamView.hidden=false
//    }
//    
//    
//    @IBAction func closeButton(sender: AnyObject)
//    {
//        self.navigationController?.popViewControllerAnimated(false)
//    }
//    
//    
//    @IBAction func backButtonClicked(sender : AnyObject){
//        
//        self.navigationController?.popViewControllerAnimated(true)
//    }
//    
//    
//    @IBAction func addToCalender(sender: AnyObject)
//    {
//        var eventStore : EKEventStore = EKEventStore()
//        // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
//        eventStore.requestAccessToEntityType(EKEntityType.Event, completion: {
//            granted, error in
//            if (granted) && (error == nil) {
//                print("granted \(granted)")
//                print("error  \(error)")
//                
//                var event:EKEvent = EKEvent(eventStore: eventStore)
//                event.title = self.eventTitle
//                event.startDate = self.eventObject["eventStartDateTime"] as! NSDate
//                event.endDate = self.eventObject["eventEndDateTime"] as! NSDate
//                event.notes = self.eventObject["eventDescription"] as! String!
//                event.calendar = eventStore.defaultCalendarForNewEvents
//                do {
//                    try eventStore.saveEvent(event, span: .ThisEvent)
//                } catch _ {
//                }
//                
//                if #available(iOS 8.0, *) {
//                    
//                    var refreshAlert = UIAlertController(title: "Event Saved", message: "Your event has been saved to calender.", preferredStyle: UIAlertControllerStyle.Alert)
//                    
//                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
//                        
//                        
//                    }))
//                    
//                    
//                    self.presentViewController(refreshAlert, animated: true, completion: nil)
//                }
//                else
//                {
//                    
//                }
//                
//                
//                
//                
//            }
//        })
//    }
//    
//    
//    
//    @IBAction func goToMapsButton(sender: AnyObject)
//    {
//        
//        let latitude = eventObject["eventLatitude"] as! Double
//        let longitude = eventObject["eventLongitude"] as! Double
//        
//        
//        let openLink = NSURL(string : "http://maps.google.com/maps?q=\(latitude),\(longitude)")
//        UIApplication.sharedApplication().openURL(openLink!)
//    }
//    
//    
//    func downloadData()
//    {
//        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId = '\(currentSharedEvent.objectId!)' GROUP BY objectId ORDER BY eventImageId DESC", whereFields: [])
//        
//        var postObjectIds: Array<String>
//        
//        postObjectIds = []
//        
//        if (resultSet != nil) {
//            while resultSet.next() {
//                postObjectIds.append(resultSet.stringForColumn("objectId"))
//            }
//        }
//        
//        resultSet.close()
//        
//        
//        let postObjectIdsString = postObjectIds.joinWithSeparator("','")
//        
//        print("Ids: \(postObjectIdsString)")
//        
//        let predicate = NSPredicate(format: "NOT (objectId IN {'\(postObjectIdsString)'}) AND eventObjectId = '\(currentSharedEvent.objectId!)'")
//        
//        let query = PFQuery(className:"EventImages", predicate: predicate)
//        
//        query.orderByAscending("createdAt")
//        
//        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
//        
//        let resultSetLikes: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["*"], whereString: "eventObjectId = '\(currentSharedEvent.objectId!)'", whereFields: [])
//        
//        var likeObjectIds: Array<String>
//        
//        likeObjectIds = []
//        
//        if (resultSetLikes != nil) {
//            while resultSetLikes.next() {
//                likeObjectIds.append(resultSetLikes.stringForColumn("objectId"))
//            }
//        }
//        
//        resultSetLikes.close()
//        
//        
//        let likeObjectIdsString = likeObjectIds.joinWithSeparator("','")
//        
//        print("Ids: \(likeObjectIdsString)")
//        
//        let likePredicate = NSPredicate(format: "NOT (objectId IN {'\(likeObjectIdsString)'}) AND eventObjectId = '\(currentSharedEvent.objectId!)'")
//        
//        let likeQuery = PFQuery(className:"PostLikes", predicate: likePredicate)
//        
//        ParseOperations.instance.fetchData(likeQuery, target: self, successSelector: "fetchAllLikesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllLikesError:", errorSelectorParameters:nil)
//        
//    }
//    
//    
//    
//    func fetchAllLikesSuccess(timer:NSTimer)
//    {
//        
//        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
//        
//        if let fetchedobjects = objects {
//            var i=0;
//            for object in fetchedobjects
//            {
//                var tblFields: Dictionary! = [String: String]()
//                
//                tblFields["eventObjectId"] = object["eventObjectId"] as? String
//                tblFields["postObjectId"] = object["postObjectId"] as? String
//                tblFields["userObjectId"] = currentUserId
//                tblFields["objectId"] = object.objectId!
//                tblFields["isUpdated"] = "1"
//                
//                
//                var insertedId = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
//            }
//            
//            self.refreshList()
//        }
//    }
//    
//    func fetchAllLikesError(timer:NSTimer)
//    {
//        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
//        print("Error: \(error) \(error.userInfo)")
//    }
//    
//    
//    func fetchAllPostsSuccess(timer:NSTimer)
//    {
//        
//        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
//        
//        isSharedPostUpdated = false
//        print("Successfully retrieved \(objects!.count) posts.")
//        
//        if let fetchedobjects = objects {
//            mySharedEventData = fetchedobjects
//            var i=0;
//            for post in mySharedEventData
//            {
//                var tblFields: Dictionary! = [String: String]()
//                
//                tblFields["postData"] = post["postData"] as? String
//                
//                tblFields["isApproved"] = "0"
//                tblFields["isRead"] = "1"
//                
//                let postHeight = post["postHeight"] as! CGFloat
//                let postWidth = post["postWidth"] as! CGFloat
//                
//                tblFields["postHeight"] = "\(postHeight)"
//                tblFields["postWidth"] = "\(postWidth)"
//                tblFields["eventObjectId"] = post["eventObjectId"] as? String
//                
//                tblFields["eventFolder"] = post["eventFolder"] as? String
//                
//                tblFields["postType"] = post["postType"] as? String
//                
//                tblFields["objectId"] = post.objectId
//                tblFields["isPosted"] = "1"
//                
//                var date = ""
//                
//                if post.createdAt != nil {
//                    let dateFormatter = NSDateFormatter()
//                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
//                    date = dateFormatter.stringFromDate((post.createdAt)!)
//                    print(date)
//                    tblFields["createdAt"] = date
//                }
//                
//                if post.updatedAt != nil {
//                    let dateFormatter = NSDateFormatter()
//                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
//                    date = dateFormatter.stringFromDate((post.updatedAt)!)
//                    print(date)
//                    tblFields["updatedAt"] = date
//                }
//                
//                var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
//                
//                
//            }
//            
//            self.refreshList()
//        }
//    }
//    
//    func fetchAllPostsError(timer:NSTimer)
//    {
//        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
//        print("Error: \(error) \(error.userInfo)")
//    }
//    
//    
//    
//    func doneButtonClick(sender: NSNotification){
//        print("jiouiop")
//        moviePlayer?.setFullscreen(false, animated: true)
//        moviePlayer?.stop()
//        moviePlayer?.view.hidden = true
//    }
//    
//    
//    func refreshList()
//    {
//        
//        //isPostDataUpDated = false
//        
//        mySharedEventData.removeAll()
//        mySharedRowHeights.removeAll()
//        print("fetching....")
//        
//        let currentEventId = currentSharedEvent.objectId as String!
//        
//        
//        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId=? ORDER BY eventImageId DESC", whereFields: [currentEventId])
//        
//        mySharedEventData = []
//        likeObjectIds = []
//        var i = 0
//        
//        if (resultSet != nil) {
//            while resultSet.next() {
//                
//                let userpost = PFObject(className: "EventImages")
//                
//                userpost["eventImageId"] = Int(resultSet.intForColumn("eventImageId"))
//                userpost["postData"] = resultSet.stringForColumn("postData")
//                userpost["eventFolder"] = resultSet.stringForColumn("eventFolder")
//                
//                
//                userpost["postType"] = resultSet.stringForColumn("postType")
//                userpost["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
//                userpost["postHeight"] = resultSet.doubleForColumn("postHeight")
//                userpost["postWidth"] = resultSet.doubleForColumn("postWidth")
//                
//                
//                
//                userpost["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
//                userpost["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
//                
//                print(resultSet.stringForColumn("createdAt"))
//                
//                if(resultSet.stringForColumn("createdAt") != "" && resultSet.stringForColumn("updatedAt") != "" && resultSet.stringForColumn("createdAt") != nil && resultSet.stringForColumn("updatedAt") != nil)
//                {
//                    userpost.objectId = resultSet.stringForColumn("objectId")
//                }
//                else
//                {
//                    
//                }
//                
//                
//                let isPosted = resultSet.stringForColumn("isPosted")
//                
//                
//                if isPosted == "0"
//                {
//                    userpost["isPosted"] = false
//                }
//                else
//                {
//                    userpost["isPosted"] = true
//                }
//                
//                
//                let isApproved = resultSet.stringForColumn("isApproved")
//                
//                if(isApproved != nil)
//                {
//                    if isApproved == "0"
//                    {
//                        userpost["isApproved"] = false
//                    }
//                    else
//                    {
//                        userpost["isApproved"] = true
//                    }
//                }
//                else
//                {
//                    userpost["isApproved"] = false
//                }
//                
//                
//                
//                
//                mySharedEventData.append(userpost)
//                
//                print(userpost["isPosted"]!)
//                
//                
//                var rowHeight:CGFloat = 380.0
//                
//                var extraHeight:CGFloat = 0
//                
//                if(userpost["postType"] as! String == "text")
//                {
//                    extraHeight = 36.0
//                    let postText = userpost["postData"] as! String
//                    
//                    let charCount: CGFloat = CGFloat(postText.characters.count)
//                    
//                    let trowCount: CGFloat = (charCount/18)+2
//                    
//                    if( trowCount < 11)
//                    {
//                        rowHeight = 380.0 - (320.0-(trowCount*29.0))
//                    }
//                }
//                else
//                {
//                    print(userpost["postHeight"])
//                    let rheight = userpost["postHeight"] as! CGFloat
//                    let rwidth = userpost["postWidth"] as! CGFloat
//                    
//                    print("height: \(rheight)")
//                    
//                    rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+60.0
//                }
//                
//                if i == 0
//                {
//                    //rowHeight = rowHeight
//                }
//                
//                mySharedRowHeights.append(rowHeight+extraHeight)
//                
//                i++
//            }
//        }
//        
//        resultSet.close()
//        print(mySharedRowHeights)
//        
//        for var j = 0; j < mySharedEventData.count; j++
//        {
//            
//            let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["count(*) as count"], whereString: "postObjectId=? AND userObjectId=?", whereFields: [mySharedEventData[j].objectId!, currentUserId])
//            
//            resultSetCount.next()
//            
//            let userLikeCount = resultSetCount.intForColumn("count")
//            resultSetCount.close()
//            
//            if userLikeCount > 0
//            {
//                
//                let resultSetObjectId: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["*"], whereString: "postObjectId=? AND userObjectId=?", whereFields: [mySharedEventData[j].objectId!, currentUserId])
//                
//                resultSetObjectId.next()
//                
//                let userLikeObjectId = resultSetObjectId.stringForColumn("objectId")
//                resultSetObjectId.close()
//                
//                likeObjectIds.append(userLikeObjectId)
//            }
//            else
//            {
//                likeObjectIds.append("")
//            }
//            
//            
//        }
//        
//        
//        self.tableView.reloadData()
//        
//        var tblFields: Dictionary! = [String: String]()
//        
//        tblFields["isRead"] = "1"
//        
//        let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventObjectId=?", whereFields: [currentSharedEvent.objectId!])
//        if isUpdated {
//            print("Record Updated Successfully")
//            print("eventImage")
//        } else {
//            print("Record not Updated Successfully")
//        }
//        
//        
//    }
//    
//    
//    func stringToDate(dateString: String)->NSDate
//    {
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
//        
//        let date = dateFormatter.dateFromString(dateString)
//        
//        return date!
//    }
//    
//    
//    func likeSelectedPost(sender : UIButton)
//    {
//        
//        
//        var likeObject: PFObject!
//        likeObject = PFObject(className: "PostLikes")
//        likeObject["eventObjectId"] = currentSharedEvent.objectId!
//        likeObject["postObjectId"] = mySharedEventData[sender.tag].objectId!
//        likeObject["userObjectId"] = currentUserId
//        likeObject["isUpdated"] = true
//        
//        if likeObjectIds[sender.tag] != ""
//        {
//            likeObject.objectId = likeObjectIds[sender.tag]
//            ParseOperations.instance.deleteData(likeObject, target: self, successSelector: "deletePostLikeSuccess:", successSelectorParameters: nil, errorSelector: "deletePostLikeError:", errorSelectorParameters: likeObject)
//        }
//        else
//        {
//            ParseOperations.instance.saveData(likeObject, target: self, successSelector: "likePostSuccess:", successSelectorParameters: nil, errorSelector: "likePostError:", errorSelectorParameters: nil)
//        }
//        
//    }
//    
//    
//    func likePostSuccess(timer:NSTimer)
//    {
//        
//        let object = timer.userInfo?.valueForKey("internal") as! PFObject
//        
//        var tblFields: Dictionary! = [String: String]()
//        
//        tblFields["eventObjectId"] = object["eventObjectId"] as? String
//        tblFields["postObjectId"] = object["postObjectId"] as? String
//        tblFields["userObjectId"] = currentUserId
//        tblFields["objectId"] = object.objectId!
//        tblFields["isUpdated"] = "1"
//        
//        let insertedId = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
//        if insertedId > 0 {
//            print("Record inserted Successfully")
//            print("eventImage")
//        } else {
//            print("Record not inserted Successfully")
//        }
//        
//        refreshList()
//        
//    }
//    
//    func likePostError(timer:NSTimer)
//    {
//        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
//        print("Error: \(error) \(error.userInfo)")
//    }
//    
//    
//    func deletePostLikeSuccess(timer:NSTimer)
//    {
//        
//        let object = timer.userInfo?.valueForKey("internal") as! PFObject
//        
//        let isDeleted = ModelManager.instance.deleteTableData("PostLikes", whereString: "objectId=?", whereFields: [object.objectId!])
//        if isDeleted {
//            print("Record deleted Successfully")
//            print("eventImage")
//        } else {
//            print("Record not deleted Successfully")
//        }
//        
//        refreshList()
//        
//        
//    }
//    
//    func deletePostLikeError(timer:NSTimer)
//    {
//        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
//        print("Error: \(error) \(error.userInfo)")
//        
//        var likeObject: PFObject = timer.userInfo?.valueForKey("external") as! PFObject
//        
//        let isDeleted = ModelManager.instance.deleteTableData("PostLikes", whereString: "objectId=?", whereFields: [likeObject.objectId!])
//        if isDeleted {
//            print("Record deleted Successfully")
//            print("eventImage")
//        } else {
//            print("Record not deleted Successfully")
//        }
//        
//        refreshList()
//        
//    }
//    
//    
//    func playSelectedVideo(sender : UIButton) {
//        let videoUrl: String = (mySharedEventData[sender.tag]["postData"] as? String)!
//        
//        let manager = NSFileManager.defaultManager()
//        if (manager.fileExistsAtPath("\(documentDirectory)/\(videoUrl)"))
//        {
//            moviePlayer?.contentURL = NSURL(fileURLWithPath: "\(documentDirectory)/\(videoUrl)")
//            moviePlayer?.view.hidden = false
//            moviePlayer!.setFullscreen(true, animated: true)
//            //moviePlayer!.scalingMode = .AspectFill
//            moviePlayer!.controlStyle = .Embedded
//            moviePlayer!.play()
//        }
//        else
//        {
//            if #available(iOS 8.0, *) {
//                let refreshAlert = UIAlertController(title: "Error", message: "Video doesn't exist.", preferredStyle: UIAlertControllerStyle.Alert)
//                
//                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
//                    
//                }))
//                
//                self.presentViewController(refreshAlert, animated: true, completion: nil)
//            }
//            else
//            {
//                
//            }
//        }
//    }
//    
//    
//    func getFirstFrame(postImageView: UIImageView, videoURL: NSURL, viewTop: CGFloat)
//    {
//        var asset : AVAsset = AVAsset(URL:videoURL)
//        
//        var assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
//        assetImgGenerate.appliesPreferredTrackTransform = true
//        var error       : NSError? = nil
//        var time        : CMTime = CMTimeMake(1, 30)
//        var img         : CGImageRef
//        do {
//            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
//        } catch var error1 as NSError
//        {
//            error = error1
//            //     img = nil
//        }
//        //        let frameImg    : UIImage = UIImage(CGImage: img)
//        //
//        //
//        //        postImageView.image = frameImg
//        
//    }
//    
//    
//    @IBAction func closeImagePreview(sender: AnyObject!)
//    {
//        selectedImageWrapper.hidden = true
//    }
//    
//    
//    @IBAction func attendingButtonClicked(sender: UIButton)
//    {
//        //updateStatus("yes")
//        statusToBeChanged = "yes"
//        
//        messageTitle.text = "Tell your host how many from your side will be attending the event."
//        
//        noOfAdultsWrapper.hidden = false
//        noOfChildWrapper.hidden = false
//        
//        adultsLabel.hidden = false
//        childLabel.hidden = false
//        
//        messagePopupView.hidden = false
//        
//        
//        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
//        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
//        
//        
//        noOfAdultsText.text = "\(noOfAdults)"
//        noOfChildText.text = "\(noOfChilds)"
//        
//        
//        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
//        
//        invitationNoteView.frame.origin.y = (self.view.frame.height*239)/568
//    }
//    
//    
//    @IBAction func maybeButtonClicked(sender: UIButton)
//    {
//        //updateStatus("maybe")
//        statusToBeChanged = "maybe"
//        
//        messageTitle.text = "Tell your host the reason."
//        
//        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
//        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
//        
//        
//        noOfAdultsText.text = "\(noOfAdults)"
//        noOfChildText.text = "\(noOfChilds)"
//        
//        
//        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
//        
//        invitationNoteView.frame.origin.y = (self.view.frame.height*139)/568
//        
//        noOfAdultsWrapper.hidden = true
//        noOfChildWrapper.hidden = true
//        
//        adultsLabel.hidden = true
//        childLabel.hidden = true
//        
//        messagePopupView.hidden = false
//    }
//    
//    
//    @IBAction func notAttendingButtonClicked(sender: UIButton)
//    {
//        //updateStatus("no")
//        statusToBeChanged = "no"
//        
//        messageTitle.text = "Tell your host the reason."
//        
//        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
//        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
//        
//        
//        noOfAdultsText.text = "\(noOfAdults)"
//        noOfChildText.text = "\(noOfChilds)"
//        
//        
//        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
//        
//        invitationNoteView.frame.origin.y = (self.view.frame.height*139)/568
//        
//        noOfAdultsWrapper.hidden = true
//        noOfChildWrapper.hidden = true
//        
//        adultsLabel.hidden = true
//        childLabel.hidden = true
//        
//        messagePopupView.hidden = false
//    }
//    
//    
//    @IBAction func onlineButtonClicked(sender: UIButton)
//    {
//        //updateStatus("online")
//        statusToBeChanged = "online"
//        
//        messageTitle.text = "Tell your host the reason."
//        
//        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
//        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
//        
//        
//        noOfAdultsText.text = "\(noOfAdults)"
//        noOfChildText.text = "\(noOfChilds)"
//        
//        
//        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
//        
//        invitationNoteView.frame.origin.y = (self.view.frame.height*139)/568
//        
//        noOfAdultsWrapper.hidden = true
//        noOfChildWrapper.hidden = true
//        
//        adultsLabel.hidden = true
//        childLabel.hidden = true
//        
//        messagePopupView.hidden = false
//        
//    }
//    
//    
//    @IBAction func goButtonClicked(sender: UIButton)
//    {
//        updateStatus(statusToBeChanged)
//    }
//    
//    
//    @IBAction func reselectStatusButtonClicked(sender: UIButton)
//    {
//        openMessageView.hidden = true
//        statusButtonsView.hidden = false
//        statusInfoView.hidden = true
//    }
//    
//    
//    @IBAction func editStatusInfoButtonClicked(sender: UIButton)
//    {
//        
//        if currentStatus == "yes"
//        {
//            
//            noOfAdultsWrapper.hidden = false
//            noOfChildWrapper.hidden = false
//            
//            adultsLabel.hidden = false
//            childLabel.hidden = false
//            
//            messagePopupView.hidden = false
//            
//            messageTitle.text = "Tell your host how many from your side will be attending the event."
//            invitationNoteView.frame.origin.y = (self.view.frame.height*239)/568
//        }
//        else
//        {
//            noOfAdultsWrapper.hidden = true
//            noOfChildWrapper.hidden = true
//            
//            adultsLabel.hidden = true
//            childLabel.hidden = true
//            
//            messagePopupView.hidden = false
//            
//            messageTitle.text = "Tell your host the reason."
//            invitationNoteView.frame.origin.y = (self.view.frame.height*139)/568
//        }
//        
//        
//        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
//        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
//        
//        
//        noOfAdultsText.text = "\(noOfAdults)"
//        noOfChildText.text = "\(noOfChilds)"
//        
//        
//        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
//    }
//    
//    
//    @IBAction func closePopupButtonClicked(sender: UIButton)
//    {
//        messagePopupView.hidden = true
//        
//        
//        if currentStatus != ""
//        {
//            
//            if currentStatus == "yes"
//            {
//                let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
//                let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
//                
//                statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
//                attendingStatusImageView.image = UIImage(named:"check-green.png")
//            }
//            
//            if currentStatus == "online"
//            {
//                statusInfo.text = "I will follow online"
//                attendingStatusImageView.image = UIImage(named:"web-globe.png")
//            }
//            
//            if currentStatus == "maybe"
//            {
//                statusInfo.text = "I am not sure"
//                attendingStatusImageView.image = UIImage(named:"questionmark.png")
//            }
//            
//            if currentStatus == "no"
//            {
//                statusInfo.text = "I will not attend"
//                attendingStatusImageView.image = UIImage(named:"cross-red.png")
//            }
//            
//            openMessageView.hidden = false
//            statusButtonsView.hidden = true
//            statusInfoView.hidden = false
//        }
//        else
//        {
//            openMessageView.hidden = true
//            statusButtonsView.hidden = false
//            statusInfoView.hidden = true
//        }
//        
//        hideKeyBoard()
//        
//    }
//    
//    
//    func updateStatus(status: String)
//    {
//        var invitationObject: PFObject!
//        invitationObject = PFObject(className: "Invitations")
//        invitationObject.objectId = currentSharedEvent["invitationId"] as? String
//        invitationObject["attendingStatus"] = status
//        invitationObject["isUpdated"] = true
//        
//        let noOfAdults = Int(noOfAdultsText.text!)
//        let noOfChilds = Int(noOfChildText.text!)
//        
//        invitationObject["noOfAdults"] = noOfAdults
//        invitationObject["noOfChilds"] = noOfChilds
//        
//        invitationObject["invitationNote"] = invitationNoteView.text!
//        
//        
//        ParseOperations.instance.saveData(invitationObject, target: self, successSelector: "updateStatusSuccess:", successSelectorParameters: status, errorSelector: "updateStatusError:", errorSelectorParameters: nil)
//    }
//    
//    
//    func updateStatusSuccess(timer:NSTimer)
//    {
//        
//        let object = timer.userInfo?.valueForKey("internal") as! PFObject
//        let status = timer.userInfo?.valueForKey("external") as! String
//        
//        //println("Successfully retrieved \(object!.count) posts.")
//        
//        
//        let noOfAdults = object["noOfAdults"] as! Int
//        let noOfChilds = object["noOfChilds"] as! Int
//        
//        
//        var tblFields: Dictionary! = [String: String]()
//        
//        tblFields["attendingStatus"] = status
//        tblFields["noOfAdults"] = "\(noOfAdults)"
//        tblFields["noOfChilds"] = "\(noOfChilds)"
//        
//        tblFields["invitationNote"] = object["invitationNote"] as? String
//        
//        let isUpdated = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [object.objectId!])
//        if isUpdated {
//            print("Record Updated Successfully")
//            print("eventImage")
//        } else {
//            print("Record not Updated Successfully")
//        }
//        
//        
//        currentSharedEvent["noOfAdults"] = noOfAdults
//        currentSharedEvent["noOfChilds"] = noOfChilds
//        
//        currentSharedEvent["invitationNote"] = object["invitationNote"] as! String
//        
//        
//        messagePopupView.hidden = true
//        
//        currentStatus = statusToBeChanged
//        
//        if currentStatus == "yes"
//        {
//            statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
//            attendingStatusImageView.image = UIImage(named:"check-green.png")
//        }
//        
//        if currentStatus == "online"
//        {
//            statusInfo.text = "I will follow online"
//            attendingStatusImageView.image = UIImage(named:"web-globe.png")
//        }
//        
//        if currentStatus == "maybe"
//        {
//            statusInfo.text = "I am not sure"
//            attendingStatusImageView.image = UIImage(named:"questionmark.png")
//        }
//        
//        if currentStatus == "no"
//        {
//            statusInfo.text = "I will not attend"
//            attendingStatusImageView.image = UIImage(named:"cross-red.png")
//        }
//        
//        openMessageView.hidden = false
//        statusButtonsView.hidden = true
//        statusInfoView.hidden = false
//        
//    }
//    
//    func updateStatusError(timer:NSTimer)
//    {
//        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
//        print("Error: \(error) \(error.userInfo)")
//    }
//    
//    
//    
//    func getAttendingStatus(userevent: PFObject) -> String
//    {
//        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
//        
//        statusResultSet.next()
//        
//        let attendingStatus = statusResultSet.stringForColumn("attendingStatus")
//        
//        statusResultSet.close()
//        
//        return attendingStatus
//    }
//    
//    
//    func correctlyOrientedImage(image: UIImage) -> UIImage {
//        if image.imageOrientation == UIImageOrientation.Up {
//            return image
//        }
//        
//        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
//        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
//        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        
//        return normalizedImage;
//    }
//    
//    
//    
//    private func playVideo(url: NSURL) {
//        if let
//            moviePlayer = MPMoviePlayerController(contentURL: url) {
//                self.moviePlayer = moviePlayer
//                moviePlayer.view.frame = CGRectMake(20, 20, 280, 400)
//                moviePlayer.prepareToPlay()
//                moviePlayer.shouldAutoplay = false
//                moviePlayer.scalingMode = .AspectFill
//                moviePlayer.controlStyle = .None
//                self.view.addSubview(moviePlayer.view)
//        } else {
//            debugPrint("Ops, something wrong when playing video.m4v")
//        }
//    }
//    
//    
//    func textFieldShouldReturn(textField: UITextField) -> Bool
//    {
//        textField.resignFirstResponder()
//        return true
//    }
//    
//    func textViewShouldReturn(textView: UITextView) -> Bool
//    {
//        textView.resignFirstResponder()
//        return true
//    }
//    
//    func textViewShouldBeginEditing(textView: UITextView) -> Bool
//    {
//        if statusToBeChanged == "yes"
//        {
//            invitationNoteView.frame.size.height = invitationNoteViewHeight/2
//        }
//        else
//        {
//            invitationNoteView.frame.size.height = invitationNoteViewHeight
//        }
//        
//        return true
//    }
//    
//    func textViewShouldEndEditing(textView: UITextView) -> Bool
//    {
//        
//        invitationNoteView.frame.size.height = invitationNoteViewHeight
//        
//        return true
//    }
//    
//    
//    @IBAction func hideKeyBoard()
//    {
//        noOfChildText.resignFirstResponder()
//        noOfAdultsText.resignFirstResponder()
//        invitationNoteView.resignFirstResponder()
//    }
//    
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return mySharedEventData.count
//    }
//    
//    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
//    {
//        
//        let row = section.row
//        
//        return mySharedRowHeights[row]
//    }
//    
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
//    {
//        
//        
//        let row = indexPath.row
//        
//        var postType = mySharedEventData[row]["postType"] as! String!
//        
//        var cellIdentifier: String! = "SharedEventPhotoCell1"
//        
//        
//        var cell: SharedPhotosTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? SharedPhotosTableViewCell
//        
//        
//        if (cell == nil)
//        {
//            cell = SharedPhotosTableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: cellIdentifier)
//        }
//        
//        
//        for view in cell!.contentView.subviews
//        {
//            view.removeFromSuperview()
//        }
//        
//        var viewTop: CGFloat = 0
//        
//        if row == 0
//        {
//            //viewTop = 45
//        }
//        
//        var heightDiff: CGFloat = 0
//        
//        if(postType == "text")
//        {
//            
//            var postTextView = UITextView()
//            var postTextClickableView = UIView()
//            
//            postTextView.text = mySharedEventData[row]["postData"] as! String
//            
//            postTextView.font = UIFont(name: "AvenirNext-Regular", size: 13.0)
//            
//            postTextView.editable = false
//            postTextView.selectable = false
//            
//            
//            
//            var postText = mySharedEventData[row]["postData"] as! String
//            
//            
//            var charCount: CGFloat = CGFloat(postText.characters.count)
//            
//            var postTextHeight: CGFloat = 320
//            
//            var trowCount: CGFloat = (charCount/18) + 2
//            
//            if( trowCount < 11)
//            {
//                postTextHeight = trowCount*29
//                heightDiff = 320-(trowCount*29)
//            }
//            
//            
//            
//            postTextView.frame = CGRectMake(10, viewTop, cell!.contentView.frame.width-20, postTextHeight)
//            
//            postTextClickableView.frame = CGRectMake(10, viewTop, cell!.contentView.frame.width-20, postTextHeight)
//            
//            cell!.contentView.addSubview(postTextView)
//            cell!.contentView.addSubview(postTextClickableView)
//        }
//        
//        if row == 0
//        {
//            //heightDiff = heightDiff-45
//        }
//        
//        var rheight = mySharedEventData[row]["postHeight"] as! CGFloat
//        var rwidth = mySharedEventData[row]["postWidth"] as! CGFloat
//        
//        var postImageView = UIImageView()
//        
//        if(rheight>0 && rwidth>0)
//        {
//            
//            postImageView.frame.size.width = self.tableView.frame.width
//            postImageView.frame.size.height = (rheight/rwidth)*self.tableView.frame.width
//            
//            postImageView.frame.origin.x = 0
//            postImageView.frame.origin.y = viewTop
//            
//            print(postImageView.frame.size.width)
//            print(postImageView.frame.size.height)
//            print(postImageView.frame.origin.x)
//            print(postImageView.frame.origin.y)
//            
//        }
//        if(postType == "image")
//        {
//            
//            var imageName = mySharedEventData[row]["postData"] as! String
//            
//            var eventFolder = mySharedEventData[row]["eventFolder"] as! String
//            
//            var eventImagePath = "\(documentDirectory)/\(imageName)"
//            
//            print(eventImagePath)
//            
//            let manager = NSFileManager.defaultManager()
//            if (manager.fileExistsAtPath(eventImagePath))
//            {
//                var image = UIImage(named: eventImagePath)
//                
//                postImageView.image = image
//                
//                cell!.contentView.addSubview(postImageView)
//            }
//            else
//            {
//                let s3BucketName = "eventnode1"
//                let fileName = imageName
//                
//                let downloadFilePath = "\(documentDirectory)/\(fileName)"
//                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
//                
//                let downloadRequest = AWSS3TransferManagerDownloadRequest()
//                downloadRequest.bucket = s3BucketName
//                downloadRequest.key  = "\(eventFolder)\(fileName)"
//                downloadRequest.downloadingFileURL = downloadingFileURL
//                
//                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//                
//                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
//                    
//                    if (task.error != nil){
//                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
//                            switch (task.error.code) {
//                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
//                                break;
//                            case AWSS3TransferManagerErrorType.Paused.rawValue:
//                                break;
//                                
//                            default:
//                                print("error downloading")
//                                break;
//                            }
//                        } else {
//                            // Unknown error.
//                            print("error downloading")
//                        }
//                    }
//                    
//                    if (task.result != nil) {
//                        print("downloading successfull")
//                        
//                        var image = UIImage(named: "\(documentDirectory)/\(imageName)")
//                        
//                        var postImageView = UIImageView()
//                        
//                        postImageView.image = image
//                        
//                        cell!.contentView.addSubview(postImageView)
//                        
//                    }
//                    
//                    return nil
//                    
//                })
//            }
//        }
//        
//        
//        if(postType == "video")
//        {
//            
//            var videoName = mySharedEventData[row]["postData"] as! String
//            
//            var eventFolder = mySharedEventData[row]["eventFolder"] as! String
//            
//            var eventVideoPath = "\(documentDirectory)/\(videoName)"
//            
//            
//            
//            let manager = NSFileManager.defaultManager()
//            if (manager.fileExistsAtPath(eventVideoPath)) {
//                
//                
//                var postPlayButton = UIButton()
//                
//                postPlayButton.frame = postImageView.frame
//                
//                postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
//                
//                postPlayButton.tag = indexPath.row
//                postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
//                
//                
//                var videoUrl = NSURL(fileURLWithPath: eventVideoPath)
//                self.getFirstFrame(postImageView, videoURL: videoUrl, viewTop: viewTop)
//                cell!.contentView.addSubview(postImageView)
//                cell!.contentView.addSubview(postPlayButton)
//            }
//            else
//            {
//                let s3BucketName = "eventnode1"
//                let fileName = videoName
//                
//                let downloadFilePath = "\(documentDirectory)/\(fileName)"
//                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
//                
//                let downloadRequest = AWSS3TransferManagerDownloadRequest()
//                downloadRequest.bucket = s3BucketName
//                downloadRequest.key  = "\(eventFolder)\(fileName)"
//                downloadRequest.downloadingFileURL = downloadingFileURL
//                
//                
//                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//                
//                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
//                    
//                    if (task.error != nil){
//                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
//                            switch (task.error.code) {
//                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
//                                break;
//                            case AWSS3TransferManagerErrorType.Paused.rawValue:
//                                break;
//                                
//                            default:
//                                print("error downloading")
//                                break;
//                            }
//                        } else {
//                            // Unknown error.
//                            print("error downloading")
//                        }
//                    }
//                    
//                    if (task.result != nil) {
//                        print("downloading successfull")
//                        
//                        
//                        var postPlayButton = UIButton()
//                        
//                        postPlayButton.frame = postImageView.frame
//                        
//                        postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
//                        postPlayButton.tag = indexPath.row
//                        postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
//                        
//                        var videoUrl = NSURL(fileURLWithPath: eventVideoPath)
//                        
//                        self.getFirstFrame(postImageView, videoURL: videoUrl, viewTop: viewTop)
//                        cell!.contentView.addSubview(postImageView)
//                        cell!.contentView.addSubview(postPlayButton)
//                    }
//                    
//                    return nil
//                    
//                })
//                
//            }
//            
//            
//        }
//        
//        
//        var pdate: NSDate!
//        
//        if(mySharedEventData[row].createdAt != nil)
//        {
//            pdate = mySharedEventData[row].createdAt
//        }
//        else
//        {
//            pdate = mySharedEventData[row]["dateCreated"] as! NSDate
//        }
//        
//        
//        print(pdate)
//        
//        let calendar = NSCalendar.currentCalendar()
//        
//        let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: pdate!)
//        
//        let sday = scomponents.day
//        let smonth = scomponents.month
//        let syear = scomponents.year
//        
//        var postDate = "\(monthsArray[smonth-1]) \(sday), \(syear)"
//        
//        
//        var resultSetTotalCount: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["count(*) as count"], whereString: "postObjectId=?", whereFields: [mySharedEventData[row].objectId!])
//        
//        resultSetTotalCount.next()
//        
//        var totalLikeCount = resultSetTotalCount.intForColumn("count")
//        resultSetTotalCount.close()
//        
//        
//        var infoView = UIView()
//        var likeButton = UIButton()
//        
//        var likeButtonWrapper = UIView()
//        likeButtonWrapper.frame = CGRectMake(0, 0, self.view.frame.width, postImageView.frame.height)
//        if likeObjectIds[row] != ""
//        {
//            likeButton.setImage(UIImage(named:"heart-circle.png"), forState: UIControlState.Normal)
//        }
//        else
//        {
//            likeButton.setImage(UIImage(named:"unlike.png"), forState: UIControlState.Normal)
//        }
//        
//        
//        likeButton.addTarget(self, action:"likeSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        likeButton.tag = indexPath.row
//        
//        
//        if rheight>0 && rwidth>0
//        {
//            print(postImageView.frame)
//            
//            //postImageView.frame.height-heightDiff+((10/568)*self.view.frame.height)
//            
//            likeButtonWrapper.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
//            likeButton.frame = CGRectMake((postImageView.frame.width)-((75/568)*self.view.frame.height), (postImageView.frame.height)-((75/568)*self.view.frame.height), (45/568)*self.view.frame.height, (45/568)*self.view.frame.height)
//            
//            infoView.frame = CGRectMake(0, postImageView.frame.height-heightDiff+((10/568)*self.view.frame.height), cell!.contentView.frame.width, (0.120625)*380)
//        }
//        else
//        {
//            likeButtonWrapper.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
//            likeButton.frame = CGRectMake(self.view.frame.width-((60/320)*self.view.frame.width), cell!.contentView.frame.width-heightDiff-((5/568)*self.view.frame.height), (45/568)*self.view.frame.height, (45/568)*self.view.frame.height)
//            
//            infoView.frame = CGRectMake(0, cell!.contentView.frame.width-heightDiff+((33/568)*self.view.frame.height), cell!.contentView.frame.width, (0.120625)*380)
//        }
//        
//        likeButtonWrapper.addSubview(likeButton)
//        
//        print(likeButton.frame)
//        print(infoView.frame)
//        
//        
//        /*var deleteImageView = UIImageView()
//        
//        deleteImageView.frame = CGRectMake(17, 18, 11,11)
//        
//        deleteImageView.image = UIImage(named:"cross-grey.png")
//        
//        
//        
//        var deleteButton = UIButton()
//        
//        deleteButton.setTitle ("", forState: UIControlState.Normal)
//        
//        deleteButton.frame = CGRectMake(0, 0, (0.120625)*380,(0.140625)*380)
//        
//        deleteButton.tag = indexPath.row
//        
//        deleteButton.addTarget(self, action:"deleteSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        */
//        
//        var postDateText = UITextView()
//        
//        postDateText.frame = CGRectMake((10/320)*self.view.frame.width, (8/568)*self.view.frame.height, (168/320)*self.view.frame.width, (31/568)*self.view.frame.height)
//        
//        postDateText.text = postDate
//        
//        postDateText.editable = false
//        postDateText.selectable = false
//        postDateText.scrollEnabled = false
//        
//        postDateText.textColor = UIColor.grayColor()
//        
//        postDateText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
//        
//        
//        var likeImageView = UIImageView()
//        
//        likeImageView.frame = CGRectMake((222/320)*self.view.frame.width, (15/568)*self.view.frame.height, (16/320)*self.view.frame.width, (15/568)*self.view.frame.height)
//        
//        likeImageView.image = UIImage(named:"heart.png")
//        
//        
//        
//        var likeButtonSmall = UIButton()
//        
//        likeButtonSmall.setTitle ("", forState: UIControlState.Normal)
//        
//        likeButtonSmall.frame = CGRectMake((208/320)*self.view.frame.width, 0, (0.120625)*380,(0.120625)*380)
//        
//        likeButtonSmall.tag = indexPath.row
//        
//        //likeButton.addTarget(self, action:"likeSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
//        
//        
//        var postLikeText = UITextView()
//        
//        postLikeText.frame = CGRectMake((244/320)*self.view.frame.width, (8/568)*self.view.frame.height, (68/320)*self.view.frame.width, (31/568)*self.view.frame.height)
//        
//        postLikeText.text = "\(totalLikeCount) Loved it"
//        
//        
//        postLikeText.editable = false
//        postLikeText.selectable = false
//        
//        postLikeText.textColor = UIColor.grayColor()
//        postLikeText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
//        
//        
//        
//        //infoView.addSubview(deleteImageView)
//        //infoView.addSubview(deleteButton)
//        infoView.addSubview(postDateText)
//        infoView.addSubview(likeImageView)
//        infoView.addSubview(likeButtonSmall)
//        infoView.addSubview(postLikeText)
//        
//        
//        cell!.contentView.addSubview(likeButtonWrapper)
//        cell!.contentView.addSubview(infoView)
//        
//        var sepImageView = UIImageView()
//        
//        sepImageView.frame = CGRectMake((11/320)*self.view.frame.width, (infoView.frame.origin.y+infoView.frame.height-((2/568)*self.view.frame.height)), (298/320)*self.view.frame.width,(1/568)*self.view.frame.height)
//        
//        sepImageView.image = UIImage(named:"sep-line.png")
//        
//        cell!.contentView.addSubview(sepImageView)
//        
//        cell?.selectionStyle = .None
//        return cell!
//    }
//    
//    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
//    {
//        let row = indexPath.row
//        
//        let postType = mySharedEventData[row]["postType"] as! String!
//        
//        if(postType == "image")
//        {
//            
//            let manager = NSFileManager.defaultManager()
//            
//            
//            
//            let eventStreamImageFile = mySharedEventData[row]["postData"] as! String
//            
//            let eventStreamImagePath = "\(documentDirectory)/\(eventStreamImageFile)"
//            
//            
//            if (manager.fileExistsAtPath(eventStreamImagePath))
//            {
//                let image = UIImage(named: eventStreamImagePath)
//                
//                print(image!.size.height)
//                print(image!.size.width)
//                
//                if(image!.size.height>image!.size.width)
//                {
//                    print("a")
//                    selectedImage.frame.size.height = selectedImageWrapper.frame.height
//                    selectedImage.frame.size.width = (image!.size.width/image!.size.height)*selectedImageWrapper.frame.height
//                }
//                
//                if(image!.size.width>image!.size.height)
//                {
//                    print("b")
//                    selectedImage.frame.size.width = selectedImageWrapper.frame.width
//                    selectedImage.frame.size.height = (image!.size.height/image!.size.width)*selectedImageWrapper.frame.width
//                }
//                
//                
//                if(image!.size.width==image!.size.height)
//                {
//                    print("c")
//                    selectedImage.frame = CGRectMake(0, 0, selectedImageWrapper.frame.width, selectedImageWrapper.frame.width)
//                }
//                
//                selectedImage.frame.origin.x = (selectedImageWrapper.frame.width/2)-(selectedImage.frame.size.width/2)
//                selectedImage.frame.origin.y = (selectedImageWrapper.frame.height/2)-(selectedImage.frame.size.height/2)
//                
//                selectedImage.image = image
//                
//                
//                selectedImageWrapper.hidden = false
//            }
//            else
//            {
//                if #available(iOS 8.0, *) {
//                    let refreshAlert = UIAlertController(title: "Error", message: "Please wait while the image is downloading.", preferredStyle: UIAlertControllerStyle.Alert)
//                    
//                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
//                        
//                    }))
//                    
//                    self.presentViewController(refreshAlert, animated: true, completion: nil)
//                }
//                else
//                {
//                    
//                }
//            }
//            
//        }
//        
//    }
//}
