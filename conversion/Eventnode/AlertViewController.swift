//
//  AlertViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    
    @IBOutlet weak var alertTableView: UITableView!
    
    var currentUserId = ""
    
    var alertListView:NSMutableArray = []
    
    var notifications = [Dictionary<String,String>]()
    
    var textViewData = ["Aaliyah Cramer liked a photo in your event 2014 Europe trip","Aaliyah Cramer loved your story 2014 Europe trip"]
    
    var statusLabel = ["Just Now","5 min ago"]
    
    var profileImage = ["girl.jpeg", "boy.jpeg"]
    
    //var eventObjectId = [String]()
    
    var messageIds = [String]()
    
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("ALERTS VIEW")
        
        self.view.addSubview(wakeUpImageView)
        
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
            print("current User Id = \(currentUserId)")
        }
        
        
        refreshList()
        
        print(messageIds.count)
        
        var messageIdsString = ""
        
        if messageIds.count > 0
        {
            messageIdsString = messageIds.joinWithSeparator("','")
            
            print("message ids string = \(messageIdsString)")
        }
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(messageIdsString)'}) AND receiverId = '\(self.currentUserId)'")
        
        let query = PFQuery(className:"Notifications", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagesError:", errorSelectorParameters:nil)
        
    }
    
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    //MARK: - refreshList()
    func refreshList()
    {
        
        notifications = []
        
        messageIds = []
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Notifications", selectColumns: ["*"], whereString: "1", whereFields: [])
        
        if resultSet != nil
        {
            
            while resultSet.next()
            {
                var notifData: Dictionary! = [String: String]()
                
                print(resultSet.stringForColumn("notificationActivityMessage"))
                
                notifData["objectId"] = resultSet.stringForColumn("objectId")
                
                print(resultSet.stringForColumn("objectId"))
                
                messageIds.append(resultSet.stringForColumn("objectId"))
                
                
                notifData["notificationActivityMessage"] = resultSet.stringForColumn("notificationActivityMessage")
                
                notifData["notificationFolder"] = resultSet.stringForColumn("notificationFolder")
                
                notifData["notificationImage"] = resultSet.stringForColumn("notificationImage")
                
                notifData["dateCreated"] = resultSet.stringForColumn("dateCreated")
                
                notifications.append(notifData)
            }
            
            alertTableView.reloadData()
        }
        resultSet.close()
    }
    
    
    
    
    func fetchAllMessagesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = message.objectId!
                tblFields["eventObjectId"] = message["eventObjectId"] as? String
                tblFields["eventPhotoObjectId"] = message["eventPhotoObjectId"] as? String
                tblFields["notificationActivityMessage"] = message["notificationActivityMessage"] as? String
                tblFields["notficationType"] = message["notficationType"] as? String
                
                
                var date = ""
                
                if message.createdAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if message.updatedAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                
                var insertedId = ModelManager.instance.addTableData("Notifications", primaryKey: "eventObjectId", tblFields: tblFields)
                
                fetchedEventObjectIds.append(message["eventObjectId"] as! String)
                
            }
            
            refreshList()
        }
    }
    
    
    
    func fetchAllMessagesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    
    
    
    //MARK: - UITableViewDataSource() methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return notifications.count
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! AlertsTableViewCell
        
        
        let wrapperView:UIView = UIView()
        
        let textView:UITextView = UITextView()
        
        let status_Label:UILabel = UILabel()
        
        let watchImage:UIImageView = UIImageView()
        
        let profileImage:UIImageView = UIImageView()
        
        cell.contentView.addSubview(wrapperView)
        
        wrapperView.addSubview(textView)
        
        wrapperView.addSubview(status_Label)
        
        wrapperView.addSubview(profileImage)
        
        wrapperView.addSubview(watchImage)
        
        
        
        wrapperView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height)
        wrapperView.backgroundColor = UIColor(red: 246.0/255, green: 246.0/255, blue: 246.0/255, alpha: 1.0)
        
        //wrapperView.backgroundColor = UIColor.blueColor()
        
        profileImage.image = UIImage(named: "girl.jpeg")
        
        print(UIImage(named: "girl.jpeg"))
        
        profileImage.frame = CGRectMake(self.view.frame.width*(21/320), self.view.frame.height*(18.0/568), self.view.frame.width*(42.0/320), self.view.frame.height*(42.0/568))
        
        
        print(notifications[indexPath.row])
        
        textView.text = notifications[indexPath.row]["notificationActivityMessage"]
        
        print(notifications[indexPath.row]["notificationActivityMessage"])
        
        print("Notification Array count = \(notifications.count)")
        
        
        //        let contentSize = textView.sizeThatFits(textView.bounds.size)
        //        var frame = textView.frame
        //        frame.size.height = contentSize.height
        //        textView.frame = frame
        
        
        
        let size: CGSize = textView.systemLayoutSizeFittingSize(textView.contentSize)
        var frame: CGRect = textView.frame
        frame.size.height = size.height
        textView.frame = frame
        
        print("HEIGHT OF textView = \(frame.size.height)")
        
        
        textView.frame = CGRectMake(self.view.frame.width*(82/320), self.view.frame.height*(20.0/568), self.view.frame.width*(215/320), self.view.frame.height*(35.0/568))
        textView.font = UIFont(name: "AvenirNext-Regular", size: 12)
        
        //textView.backgroundColor = UIColor.blackColor()
        
        watchImage.image = UIImage(named: "clock-grey.png")
        watchImage.frame = CGRectMake(self.view.frame.width*(87/320), self.view.frame.height*((frame.size.height + 33)/568), self.view.frame.width*(13.0/320), self.view.frame.height*(13.0/568))
        
        print("HEIGHT OF Watch Image(Y) = \(self.view.frame.height*((frame.size.height + 33)/568))")
        
        let date = NSDate()
        print("date is = \(date)")
        
        
        let currentTimeStamp = Int(Int64(date.timeIntervalSince1970*1000))
        print("current Time Stamp = \(currentTimeStamp)")
        
        
        let dateCreated = stringToDate(notifications[indexPath.row]["dateCreated"]!)
        print("Date Created = \(dateCreated)")
        
        
        let createdTimeStamp = Int(Int64(dateCreated.timeIntervalSince1970*1000))
        print("Created Time Stamp = \(createdTimeStamp)")
        
        let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
        let timeDiff = Int64(currentTimeStamp - createdTimeStamp) - timezoneOffset
        print(timeDiff)
        
        
        let nYears = timeDiff / (1000*60*60*24*365)
        
        print(nYears)
        
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        
        print(nMonths)
        
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        print(nDays)
        
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        print(nHours)
        
        
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        print(nMinutes)
        
        var timeMsg = ""
        
        if nYears > 0
        {
            timeMsg = "about \(nYears) years ago"
        }
        else if nMonths > 0
        {
            timeMsg = "about \(nMonths) months ago"
        }
        else if nDays > 0
        {
            timeMsg = "about \(nDays) days ago"
        }
        else if nHours > 0
        {
            timeMsg = "about \(nHours) hours ago"
        }
        else if nMinutes > 0
        {
            timeMsg = "about \(nMinutes) minutes ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        
        status_Label.text = timeMsg
        
        status_Label.frame = CGRectMake(self.view.frame.width*(106/320), self.view.frame.height*((frame.size.height + 27)/568), self.view.frame.width*(145.0/320), self.view.frame.height*(21.0/568))
        
        status_Label.font = UIFont(name: "AvenirNext-DemiBold", size: 11)
        
        return cell
    }
    
    
    
    //MARK: - stringToDate()
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        print("date formatter = \(dateFormatter)")
        
        let date = dateFormatter.dateFromString(dateString)
        
        print("date  = \(date)")
        
        return date!
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: - eventButtonClicked()
    @IBAction func eventButtonClicked(sender : AnyObject)
    {
        //NSLog("sdd")
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    
    //MARK: - settingsButtonClicked()
    @IBAction func settingsButtonClicked(sender : AnyObject)
    {
        //NSLog("sdd")
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewControllerOne") as! settingViewControllerOne
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }
    
    
    //MARK: - sharedEventButtonClicked()
    @IBAction func sharedEventButtonClicked(sender : AnyObject)
    {
        //NSLog("sdd")
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
}
