//
//  ManageGuestViewController.swift
//  Eventnode
//
//  Created by brst on 8/11/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MessageUI
import AddressBookUI
import AddressBook

class ManageGuestViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate  {
    
    @IBOutlet weak var inviteTextView: UITextView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var sView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var respondView: UIView!
    @IBOutlet weak var pendingView: UIView!
    
    
   
   
    @IBOutlet weak var wrapperViewScroll: UIView!
    
    @IBOutlet weak var respondedTabHiddenTextview: UITextView!
    @IBOutlet weak var respondedTabHiddenView: UIView!
    @IBOutlet weak var pendingPop: UIView!
    @IBOutlet weak var pendingPopText: UITextView!
    @IBOutlet weak var respondPopView: UIView!
    @IBOutlet weak var respondTextView: UITextView!

    @IBOutlet weak var buttonViews: UIView!
    @IBOutlet weak var pendingtextView: UITextView!
    @IBOutlet weak var inviteMoreView: UIView!
    @IBOutlet weak var inviteMoreButton: UIButton!
    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var respondButton: UIButton!
    @IBOutlet weak var pendigTableView: UITableView!
    @IBOutlet weak var respondtableView: UITableView!
    @IBOutlet weak var highlightedView: UIView!
    @IBOutlet weak var repondTableView2: UITableView!
    @IBOutlet weak var onlineHighlightedView: UIView!
    @IBOutlet weak var mayBeHighlightedView: UIView!
    @IBOutlet weak var notAttendingHighlightedView: UIView!
    @IBOutlet weak var respodedTextView: UITextView!
    @IBOutlet weak var respondedTextView2: UITextView!
    @IBOutlet weak var approvalButton: UIButton!
    @IBOutlet weak var onlineStatus: UILabel!
    @IBOutlet weak var attendingStatus: UILabel!
    @IBOutlet weak var maybeStatus: UILabel!
    @IBOutlet weak var notattendingStatus: UILabel!
    @IBOutlet weak var attendingBtnOutlet: UIButton!
    
    @IBOutlet weak var onlinePopUpTextView: UITextView!
    @IBOutlet weak var onlineEventAttender: UILabel!
    @IBOutlet weak var eventSender: UILabel!
    @IBOutlet weak var onlinePopView: UIView!
    @IBOutlet var textView : UITextView!
    
    @IBOutlet weak var eventStreamLabel: UILabel!
    
    @IBOutlet weak var eventTitle: UILabel!
   
    @IBOutlet weak var invitedPerson: UILabel!
    @IBOutlet weak var invitationNote: UITextView!
    
    @IBOutlet weak var childrens: UILabel!
    @IBOutlet weak var adults: UILabel!
    @IBOutlet var onlineTexView: UITextView!
    @IBOutlet weak var approvalAllbutton: UIButton!
    @IBOutlet var attendingCount: UILabel!
    @IBOutlet weak var popUpView: UIView!
   
    
    var myMutableString = NSMutableAttributedString()
    
    var textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
    var totalCount = Int()
    var attendingWithAccess = [PFObject]()
    var attendingWithoutAcess = [PFObject]()
    var onlineWithAccess = [PFObject]()
    var onlineWithoutAccess = [PFObject]()
    var maybewithAccess = [PFObject]()
    var maybeWithoutAccess = [PFObject]()
    var notAttendingWithAccess = [PFObject]()
    var notAttendingWithoutAccess = [PFObject]()
    var withAccess = [PFObject]()
    var withoutAcess = [PFObject]()
    var pendingInvites = [PFObject]()
    var isFromCreated: Bool!
    var noOfAdults = 0
    var noOfChilds = 0
    
    var currentViewType = "yes"
    var parentViewType = "respond"
    
    //var popUpButton = UIButton()
    //var onlinePopUpButton = UIButton()
    var invitationIds = [String]()
    var updateIds = [String]()
    
    var onlineTexViewHeight: CGFloat!
    var respondedTextView2Height: CGFloat!
    var respondtableViewHeight: CGFloat!
    var repondTableView2Height: CGFloat!
    var approvalAllbuttonHeight: CGFloat!
    
    
    var onlineTexViewY: CGFloat!
    var respondedTextView2Y: CGFloat!
    var respondtableViewY: CGFloat!
    var repondTableView2Y: CGFloat!
    var approvalAllbuttonY: CGFloat!
    var eventStreamLabelY: CGFloat!

    override func viewDidLoad() {

        super.viewDidLoad()
        
        
        // approvalButton.titleLabel?.font = UIFont(name: "Monsterrat-Regular", size: 12)
        
        
        onlineTexViewHeight = onlineTexView.frame.height
        respondedTextView2Height = respondedTextView2.frame.height
        respondtableViewHeight = respondtableView.frame.height
        repondTableView2Height = repondTableView2.frame.height
        approvalAllbuttonHeight = approvalAllbutton.frame.height
        
        
        onlineTexViewY = onlineTexView.frame.origin.y
        respondedTextView2Y = respondedTextView2.frame.origin.y
        respondtableViewY = respondtableView.frame.origin.y
        repondTableView2Y = repondTableView2.frame.origin.y
        approvalAllbuttonY = approvalAllbutton.frame.origin.y
        eventStreamLabelY = eventStreamLabel.frame.origin.y
        
        print("onlineTexViewHeight: \(onlineTexViewHeight)")
        print("respondedTextView2Height: \(respondedTextView2Height)")
        print("respondtableViewHeight: \(respondtableViewHeight)")
        print("repondTableView2Height: \(repondTableView2Height)")
        print("approvalAllbuttonHeight: \(approvalAllbuttonHeight)")
        
        
        print("onlineTexViewY: \(onlineTexViewY)")
        print("respondedTextView2Y: \(respondedTextView2Y)")
        print("respondtableViewY: \(respondtableViewY)")
        print("repondTableView2Y: \(repondTableView2Y)")
        print("approvalAllbuttonY: \(approvalAllbuttonY)")

        
        
        popUpView.hidden = true
        onlinePopView.hidden = true
        //popUpButton.hidden = false
        //respondedPopUpView.hidden = true
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        
        let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(12.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        let attributedString = NSMutableAttributedString(string:"")
        
        
        let buttonTitleStr = NSMutableAttributedString(string:"Approve All", attributes:attrs)

        attributedString.appendAttributedString(buttonTitleStr)
        
        approvalAllbutton.setAttributedTitle(attributedString, forState: .Normal)
        
        approvalAllbutton.frame.size.width = approvalAllbutton.sizeThatFits(approvalAllbutton.bounds.size).width
        
        eventStreamLabel.frame.size.width = eventStreamLabel.sizeThatFits(eventStreamLabel.bounds.size).width
        eventStreamLabel.frame.origin.x = approvalAllbutton.frame.origin.x + ((approvalAllbutton.frame.width/2)-(eventStreamLabel.frame.width/2))
        
        respodedTextView.attributedText = NSAttributedString(string:respodedTextView.text, attributes:attributes)
        respodedTextView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        respodedTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        respondedTextView2.attributedText = NSAttributedString(string:respondedTextView2.text, attributes:attributes)
        respondedTextView2.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        respondedTextView2.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        pendingtextView.attributedText = NSAttributedString(string:pendingtextView.text, attributes:attributes)
        pendingtextView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        pendingtextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        respodedTextView.attributedText = NSAttributedString(string:respodedTextView.text, attributes:attributes)
        respodedTextView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        respodedTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        invitationNote.attributedText = NSAttributedString(string:invitationNote.text, attributes:attributes)
        invitationNote.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        invitationNote.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        invitationNote.layer.borderWidth = 3
        invitationNote.layer.borderColor =  UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1.0).CGColor
        
        onlinePopUpTextView.attributedText = NSAttributedString(string:onlinePopUpTextView.text, attributes:attributes)
        onlinePopUpTextView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        onlinePopUpTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

        onlinePopUpTextView.layer.borderWidth = 3
        onlinePopUpTextView.layer.borderColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1.0).CGColor
        
        inviteMoreView.hidden = true
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = true
        
        respondtableView.dataSource = self
        respondtableView.delegate = self
        repondTableView2.dataSource = self
        repondTableView2.delegate = self
        pendigTableView.dataSource = self
        pendigTableView.delegate = self
        
        //sView.layer.borderWidth = 0.2
        //sView.backgroundColor = UIColor(red: 200/255, green:  150/255, blue: 150/255, alpha: 1.0)
        
        
        
        scrollView.contentSize = CGSizeMake(self.wrapperViewScroll.frame.size.width+60, self.scrollView.frame.size.height)
        
        print("wrapper view width = \(self.wrapperViewScroll.frame.size.width)")
        
        print("Scroll view width = \(self.scrollView.frame.size.width)")
        
        
        pendingView.hidden = true
        
        style.lineSpacing = 5
        textView.attributedText = NSAttributedString(string: textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
       
        
        respondView.hidden = false
        respondButton.backgroundColor = UIColor(red: 68.0/255, green:  185.0/255, blue: 227.0/255, alpha: 1.0)
        respondButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        onlineTexView.text = "Guests who will be following your event  online only through the event stream."
        onlineTexView.font = UIFont(name: "Avenir Next Medium", size: 11.0)
        
        onlineTexView.frame.size.height = onlineTexView.sizeThatFits(onlineTexView.bounds.size).height
        
        respondedTextView2.frame.size.height = respondedTextView2.sizeThatFits(respondedTextView2.bounds.size).height
        
        fetchInvitations()
        
        withAccess = attendingWithAccess
        withoutAcess = attendingWithoutAcess
        
        respondtableView.separatorColor = UIColor.whiteColor()
        repondTableView2.separatorColor = UIColor.whiteColor()
        pendigTableView.separatorColor = UIColor.whiteColor()
        respondtableView.reloadData()
        repondTableView2.reloadData()
        pendigTableView.reloadData()
       
        
        /*onlineTexView.hidden = true
        respondtableView.frame.origin.y = (self.view.frame.height*170)/568
        respondtableView.frame.size.height = (self.view.frame.height*105)/568
        
        
        attendingCount.text = "\(noOfAdults) Adults,\(noOfChilds) Children "
        attendingStatus.text = "Attending(\(noOfAdults+noOfChilds))"
        onlineStatus.text = "Online(\(onlineWithAccess.count))"
        maybeStatus.text = "Maybe(\(maybewithAccess.count))"
        notattendingStatus.text = "Not Attending(\(notAttendingWithAccess.count))"
        println(currentEvent.objectId)
        
        
        
        onlineTexView.hidden = true
        highlightedView.hidden = false
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = true
        
        if attendingWithAccess.count == 0
        {
            respondtableView.hidden = true
            respondedTextView2.hidden = true
            approvalAllbutton.frame.origin.y = (self.view.frame.height*182)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*190)/56
        }
        else
        {
            respondtableView.hidden = false
            respondedTextView2.hidden = false
            approvalAllbutton.frame.origin.y = (self.view.frame.height*323)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*348)/568
        }*/
        
        
        print(pendingInvites)
        
        
        print(invitationIds.count)
        
        var invitationIdsString = ""
        
        if invitationIds.count > 0
        {
            invitationIdsString = invitationIds.joinWithSeparator("','")
            
            //println(invitationIds)
            
        }

        let predicate = NSPredicate(format: "NOT (objectId IN {'\(invitationIdsString)'}) AND eventObjectId = '\(currentEvent.objectId!)'")

        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        
        let updatePredicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)' AND isUpdated = true")
        
        let updateQuery = PFQuery(className:"Invitations", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
//        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchInvitationUpdatesSuccess:", successSelectorParameters: nil, errorSelector: "fetchInvitationUpdatesError:", errorSelectorParameters:nil)
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchUpdatedInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchUpdatedInvitationsError:", errorSelectorParameters:nil)
        
        
//        if pendingInvites.count == 0
//        {
//          inviteMoreView.hidden = false
//          inviteMoreView.frame.origin.y = self.view.frame.origin.y*(70/568)
//          inviteMoreView.frame.size.height = self.view.frame.size.height*(498/568)
//         
//            
//            println(inviteMoreView.frame.origin.y)
//        }
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        fetchInvitations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func fetchInvitations()
    {
        
        invitationIds = []
        attendingWithAccess = []
        attendingWithoutAcess = []
        onlineWithAccess = []
        onlineWithoutAccess = []
        maybewithAccess = []
        maybeWithoutAccess = []
        notAttendingWithAccess = []
        notAttendingWithoutAccess = []
        withAccess = []
        withoutAcess = []
        pendingInvites = []
        noOfAdults = 0
        noOfChilds = 0
        
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString:"eventObjectId='\(currentEvent.objectId!)'", whereFields: [])
        
        if (resultSet != nil) {
            while resultSet.next()
            {
                //var invitation = PFObject(className: "Invitations")
                let attendingStatus = resultSet.stringForColumn("attendingStatus")
                let isApproved = resultSet.stringForColumn("isApproved")
                
                let invitation = PFObject(className: "Invitations")
                
                invitation.objectId = resultSet.stringForColumn("objectId")
                
                invitationIds.append(invitation.objectId!)
                
                print(invitation.objectId)
                invitation["userObjectId"] = resultSet.stringForColumn("userObjectId")
                invitation["attendingStatus"] = resultSet.stringForColumn("attendingStatus")
                invitation["invitationType"] = resultSet.stringForColumn("invitationType")
                // invitation["needsContentApprovel"] = resultSet.stringForColumn("needsContentApprovel")
                //invitation["createdAt"] = resultSet.stringForColumn("createdAt")
                //invitation["updatedAt"] = resultSet.stringForColumn("updatedAt")
                invitation["dateCreated"] = resultSet.stringForColumn("dateCreated")
                invitation["dateUpdated"] = resultSet.stringForColumn("dateUpdated")
                invitation["isApproved"] = resultSet.stringForColumn("isApproved")
                invitation["emailId"] = resultSet.stringForColumn("emailId")
                invitation["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
                invitation["invitedName"] = resultSet.stringForColumn("invitedName")
                invitation["noOfAdults"] = Int(resultSet.intForColumn("noOfAdults"))
                invitation["noOfChilds"] = Int(resultSet.intForColumn("noOfChilds"))
                invitation["invitationNote"] = resultSet.stringForColumn("invitationNote")
                
                //println(invitation["invitedName"])
                
                if attendingStatus == "yes"
                {
                    
                    noOfAdults += Int(resultSet.intForColumn("noOfAdults"))
                    
                    noOfChilds += Int(resultSet.intForColumn("noOfChilds"))
                    
                    if isApproved == "0"
                    {
                        attendingWithoutAcess.append(invitation)
                        print(attendingWithoutAcess)
                    }
                    else
                    {
                        attendingWithAccess.append(invitation)
                    }
                }
                
                if attendingStatus == "no"
                {
                    if isApproved == "0"
                    {
                        notAttendingWithoutAccess.append(invitation)
                        print(notAttendingWithoutAccess)
                    }
                    else
                    {
                        notAttendingWithAccess.append(invitation)
                    }
                }
                
                if attendingStatus == "maybe"
                {
                    if isApproved == "0"
                    {
                        maybeWithoutAccess.append(invitation)
                    }
                    else
                    {
                        maybewithAccess.append(invitation)
                    }
                }
                
                if attendingStatus  == "online"
                {
                    if isApproved == "0"
                    {
                        onlineWithoutAccess.append(invitation)
                    }
                    else
                    {
                        onlineWithAccess.append(invitation)
                    }
                }
                print(attendingStatus)
                if attendingStatus == "" || attendingStatus == "0"
                {
                    pendingInvites.append(invitation)
                    print(pendingInvites)
                    
                }
                
            }
            
            if currentViewType == "yes"
            {
                withAccess = attendingWithAccess
                withoutAcess = attendingWithoutAcess
            }
            
            if currentViewType == "no"
            {
                withAccess = notAttendingWithAccess
                withoutAcess = notAttendingWithoutAccess
            }
            
            if currentViewType == "online"
            {
                withAccess = onlineWithAccess
                withoutAcess = onlineWithoutAccess
            }
            
            if currentViewType == "maybe"
            {
                withAccess = maybewithAccess
                withoutAcess = maybeWithoutAccess
            }
            
            
            attendingCount.text = "\(noOfAdults) Adults, \(noOfChilds) Children "
            attendingStatus.text = "Attending (\(noOfAdults+noOfChilds))"
            onlineStatus.text = "Online (\(onlineWithAccess.count + onlineWithoutAccess.count))"
            maybeStatus.text = "Maybe (\(maybewithAccess.count+maybeWithoutAccess.count))"
            notattendingStatus.text = "Not Attending (\(notAttendingWithAccess.count+notAttendingWithoutAccess.count))"
            print(currentEvent.objectId)
            
            respondtableView.reloadData()
            repondTableView2.reloadData()
            pendigTableView.reloadData()
        }
        
        resultSet.close()
        
        totalCount = attendingWithAccess.count + attendingWithoutAcess.count + onlineWithAccess.count + onlineWithoutAccess.count +
            maybewithAccess.count + maybeWithoutAccess.count + notAttendingWithAccess.count + notAttendingWithoutAccess.count
        

        
        if totalCount == 0 && pendingInvites.count == 0
        {
            buttonViews.hidden = true
            respondView.hidden = true
            inviteMoreView.hidden = false
            pendingView.hidden = true
            invitemore(UIButton())
            
        }
        else
        {
            buttonViews.hidden = false
        }

        
        
        
        if totalCount == 0
        {
            respondPopView.hidden = false
            print(respondPopView.frame)
            print(respondTextView.frame)
            
        }
        else
        {
            respondPopView.hidden = true
            
        }
        
        if pendingInvites.count == 0
        {
            pendingPop.hidden = false
            
        }
        else
        {
            pendingPop.hidden = true
        }
        
        if (withoutAcess.count + withAccess.count) == 0
        {
            respondedTabHiddenView.hidden = false
            if currentViewType == "yes"
            {
                respondedTabHiddenTextview.text = "You don't have any guests attending the event as of now. Try inviting more."
            }
            
            if currentViewType == "no"
            {
                respondedTabHiddenTextview.text = "You are popular. There doesn't seem to be anyone not attending your event."
            }
            
            if currentViewType == "maybe"
            {
                respondedTabHiddenTextview.text = "Cheer up! It's a rare day when everyone is sure of what they want."
            }
            
            if currentViewType == "online"
            {
                respondedTabHiddenTextview.text = "Doesn't look like anyone wants to follow this event online. Maybe you have an overseas friend?"
            }
            
        }
        else
        {
            respondedTabHiddenView.hidden = true
        }
        
        if withoutAcess.count > 0 && withAccess.count>0
        {
            
            eventStreamLabel.hidden = false
            onlineTexView.hidden = false
            respondtableView.hidden = false
            respondedTextView2.hidden = false
            repondTableView2.hidden = false
            approvalAllbutton.hidden = false
            
            onlineTexView.frame.origin.y = eventStreamLabelY
            eventStreamLabel.frame.origin.y = onlineTexView.frame.origin.y + onlineTexView.frame.height + 5
            
            respondtableView.frame.origin.y = eventStreamLabel.frame.origin.y + eventStreamLabel.frame.height + 5
            
            if respondtableView.contentSize.height > respondtableViewHeight
            {
                respondtableView.frame.size.height = respondtableViewHeight
            }
            else
            {
                respondtableView.frame.size.height = respondtableView.contentSize.height
            }
            
            respondedTextView2.frame.origin.y = respondtableView.frame.size.height + respondtableView.frame.origin.y + 10
            approvalAllbutton.frame.origin.y = respondedTextView2.frame.origin.y + respondedTextView2.frame.height + 5
            
            repondTableView2.frame.origin.y = approvalAllbutton.frame.origin.y + approvalAllbutton.frame.height + 5
            
            repondTableView2.frame.size.height = respondedTabHiddenView.frame.height - (repondTableView2.frame.origin.y - respondedTabHiddenView.frame.origin.y)
        }
        else
        {
            if withAccess.count > 0
            {
                
                eventStreamLabel.hidden = false
                onlineTexView.hidden = false
                respondtableView.hidden = false
                respondedTextView2.hidden = true
                repondTableView2.hidden = true
                approvalAllbutton.hidden = true
                
                onlineTexView.frame.origin.y = eventStreamLabelY
                eventStreamLabel.frame.origin.y = onlineTexView.frame.origin.y + onlineTexView.frame.height + 5
                
                respondtableView.frame.origin.y = eventStreamLabel.frame.origin.y + eventStreamLabel.frame.height + 5
                
                respondtableView.frame.size.height = respondedTabHiddenView.frame.height - (respondtableView.frame.origin.y - respondedTabHiddenView.frame.origin.y)
            }
            else
            {
                eventStreamLabel.hidden = true
                onlineTexView.hidden = true
                respondtableView.hidden = true
                respondedTextView2.hidden = false
                repondTableView2.hidden = false
                approvalAllbutton.hidden = false
                
                respondedTextView2.frame.origin.y = eventStreamLabelY
                approvalAllbutton.frame.origin.y = respondedTextView2.frame.origin.y + respondedTextView2.frame.height + 5
                
                repondTableView2.frame.origin.y = approvalAllbutton.frame.origin.y + approvalAllbutton.frame.height + 5
                
                repondTableView2.frame.size.height = respondedTabHiddenView.frame.height - (repondTableView2.frame.origin.y - respondedTabHiddenView.frame.origin.y)
                
            }
        }
        
    }
    

    func fetchInvitationUpdatesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            updateIds = []
            
            for invitationTransaction in fetchedobjects
            {
                updateIds.append(invitationTransaction["invitationObjectId"] as! String)
            }
            
            
            var updateIdsString = ""
            
            if updateIds.count > 0
            {
                updateIdsString = updateIds.joinWithSeparator("','")
                
                //println(invitationIds)
            }
            
            let predicate = NSPredicate(format: "objectId IN {'\(updateIdsString)'}")
            
            let query = PFQuery(className:"Invitations", predicate: predicate)
            
            query.orderByAscending("createdAt")
            
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchUpdatedInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchUpdatedInvitationsError:", errorSelectorParameters:nil)
            
        }
    }
    
    
    
    func fetchInvitationUpdatesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }

    
    func fetchUpdatedInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            let i = 0
            
            for invitation in fetchedobjects
            {
                
                fetchedobjects[i]["isUpdated"] = false
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["userObjectId"] = invitation["userObjectId"] as? String
                tblFields["attendingStatus"] = invitation["attendingStatus"] as? String
                tblFields["invitationNote"] = invitation["invitationNote"] as? String
                
                
                let noOfAdults = invitation["noOfAdults"] as! Int
                let noOfChilds = invitation["noOfChilds"] as! Int
                
                tblFields["noOfAdults"] = "\(noOfAdults)"
                tblFields["noOfChilds"] = "\(noOfChilds)"
                
                if invitation["needsContentApprovel"] as? Bool == true
                {
                    tblFields["needsContentApprovel"] = "1"
                }
                else
                {
                    tblFields["needsContentApprovel"] = "0"
                }
                
                
                var date = ""
                
                if invitation.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if invitation.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                tblFields["emailId"] = invitation["emailId"] as? String
                tblFields["eventObjectId"] = invitation["eventObjectId"] as? String
                
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }
                
                
                //var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
                
                //var insertedId = ModelManager.instance.updateTableData(
                
                var isUpdated = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitation.objectId!])
                
            }
           
            
            PFObject.saveAllInBackground(fetchedobjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {

                }
                else
                {
                    
                }
            }
            
            
            fetchInvitations()
            
        }
    }
    
    
    
    func fetchUpdatedInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    
    
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            for invitation in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = invitation.objectId!
                tblFields["invitedName"] = invitation["invitedName"] as? String
                tblFields["userObjectId"] = invitation["userObjectId"] as? String
                tblFields["attendingStatus"] = invitation["attendingStatus"] as? String
                tblFields["invitationType"] = invitation["invitationType"] as? String
                tblFields["invitationNote"] = invitation["invitationNote"] as? String
                
                
                
                let noOfAdults = invitation["noOfAdults"] as! Int
                let noOfChilds = invitation["noOfChilds"] as! Int

                tblFields["noOfAdults"] = "\(noOfAdults)"
                tblFields["noOfChilds"] = "\(noOfChilds)"
                
                if invitation["needsContentApprovel"] as? Bool == true
                {
                    tblFields["needsContentApprovel"] = "1"
                }
                else
                {
                    tblFields["needsContentApprovel"] = "0"
                }
                

                
                var date = ""
                
                if invitation.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if invitation.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                tblFields["emailId"] = invitation["emailId"] as? String
                tblFields["eventObjectId"] = invitation["eventObjectId"] as? String
                
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }


                var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
                
            }

            fetchInvitations()

            
        }
    }
    
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
    }
    

    
    @IBAction func respondButton(sender: AnyObject)
    {
        parentViewType = "respond"
        fetchInvitations()
        
        respondButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        respondButton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
        pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
        inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        inviteMoreButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        
        pendingView.hidden = true
        respondView.hidden  =  false
        inviteMoreView.hidden = true
        respondtableView.reloadData()
        
        
        
    }
    
    @IBAction func pendingButton(sender: AnyObject)
    {
        parentViewType = "pending"
        fetchInvitations()
        
        //pendigTableView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pendingButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        respondView.hidden = true
        pendingView.hidden = false
        respondButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
        inviteMoreButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
        respondButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        inviteMoreView.hidden = true
        //self.pendigTableView.reloadData()
        
        
    }
    
    @IBAction func invitemore(sender: AnyObject)
    {
        parentViewType = "invite"
        
        inviteMoreView.hidden = false
        
        inviteMoreButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        inviteMoreButton.backgroundColor = UIColor(red: 68/255, green:  185/255, blue: 227/255, alpha: 1.0)
        respondButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        respondButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if tableView == respondtableView
        {
            return 1
        }
            
        else if tableView == repondTableView2
        {
            return 1
            
        }
        else
        {
            return 1
            
        }
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == respondtableView
        {
            return withAccess.count
        }
            
        else if tableView == repondTableView2
        {
            return withoutAcess.count
            
        }
        else
        {
            return pendingInvites.count
            
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let row = indexPath.row
        var cellIdentifier: String! = ""
        
        if tableView == respondtableView
        {
            //respondtableView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            
            cellIdentifier = "Cell"
            
            let cell: withAccessTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? withAccessTableViewCell
            
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            let popUpButton = UIButton()
            
            let nameLabel = UILabel()
            //nameLabel.frame = CGRectMake(self.view.frame.width*(16/320),self.view.frame.height*(2/568),self.view.frame.width*(143/320), nameLabel.frame.height)
            
            let name = withAccess[row]["invitedName"] as! String
            
//            adults.text = withoutAcess[sender.tag]["noOfAdults"] as! String
//            childrens.text = withoutAcess[sender.tag]["noOfChilds"] as! String
            let adults = withAccess[row]["noOfAdults"] as! Int
            let childs = withAccess[row]["noOfChilds"] as! Int
            
            let value : Int = adults+childs
            
            //let total = "\(adults+childs)"
            print(value)

            
            
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320),cell!.contentView.frame.height*(3.0/50), self.view.frame.width*(306.0/320), self.view.frame.height*(25.0/568))
            
//           var myString:NSString = "\(name) \((value.description))"
           // nameLabel.attributedText = myMutableString
            
            if attendingStatus == "yes"
            {
                nameLabel.text = "\(name) (\(value))"
            }
            else
            {
               nameLabel.text = "\(name)"
            }
            
            
            nameLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 14.0)
            
             print(nameLabel.font)
            
            
            
            nameLabel.backgroundColor = UIColor.clearColor()
            nameLabel.frame.size.width = nameLabel.sizeThatFits(nameLabel.bounds.size).width+2
            nameLabel.frame.size.height = nameLabel.sizeThatFits(nameLabel.bounds.size).height

            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            
            
            let image = UIImage(named: "check-box.png") as UIImage?
            
            let popImage = UIImage(named: "message_new.png") as UIImage?
            
            popUpButton.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width+10, nameLabel.frame.origin.y+((nameLabel.frame.height/2)-(self.view.frame.width*(6/320))), self.view.frame.width*(12/320),self.view.frame.width*(12/320))
            
            popUpButton.setImage(popImage, forState: UIControlState.Normal)
            
            popUpButton.addTarget(self, action: "popUpClickedWithAccess:", forControlEvents:.TouchUpInside)
            
            popUpButton.tag = indexPath.row
            
            popUpButton.frame.origin.x = nameLabel.frame.origin.x + nameLabel.frame.size.width + 5
            
            let contactLabel = UILabel()
            contactLabel.frame = CGRectMake(self.view.frame.width*(20/320),cell!.contentView.frame.height*(27.0/50),self.view.frame.width*(190/320), self.view.frame.height*(12.0/568))

            contactLabel.backgroundColor = UIColor.clearColor()
            
            contactLabel.text = withAccess[row]["emailId"] as? String
            
            contactLabel.font = UIFont(name:"AvenirNext-Medium", size: 12.0)
            
            print(contactLabel.font)
            
            contactLabel.frame.size.width = contactLabel.sizeThatFits(contactLabel.bounds.size).width+2
            
            contactLabel.textColor = UIColor(red: 165.0/255, green: 165.0/255, blue: 165.0/255, alpha: 1.0)
            
            let button = UIButton()
            button.frame = CGRectMake(approvalAllbutton.frame.origin.x + ((approvalAllbutton.frame.width/2)-(self.view.frame.width*(7/320))), self.view.frame.height*(15/568), self.view.frame.width*(14/320),self.view.frame.width*(14/320))
            button.setImage(image, forState: UIControlState.Normal)
            button.addTarget(self, action: "disapproveAccessButton:", forControlEvents:.TouchUpInside)
            button.tag = indexPath.row
            
            let contentSize = nameLabel.sizeThatFits(nameLabel.bounds.size)
            var frame = nameLabel.frame
            frame.size.width = contentSize.width
            nameLabel.frame = frame
            
            cell?.contentView.addSubview(popUpButton)
            cell?.contentView.addSubview(button)
            cell?.contentView.addSubview(nameLabel)
            cell?.contentView.addSubview(contactLabel)
            cell?.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            cell?.selectionStyle = .None
            
            return cell!
            
        }
            
        else if tableView == repondTableView2
        {
            //            repondTableView2.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            cellIdentifier = "ApprovalCell"
            
            let cell: withoutAccessTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? withoutAccessTableViewCell
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            let adults = withoutAcess[row]["noOfAdults"] as! Int
            
            let childs = withoutAcess[row]["noOfChilds"] as! Int
            
            let value : Int = adults+childs
            
            let name = withoutAcess[row]["invitedName"] as! String
            
            let onlinePopUpButton = UIButton()
            
            let nameLabel = UILabel()
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320), self.view.frame.height*(3.0/568), self.view.frame.width*(220.0/320), self.view.frame.height*(25.0/568))
            if attendingStatus == "yes"
            {
                nameLabel.text = "\(name) (\(value))"
            }
            else
            {
                nameLabel.text = "\(name)"
            }

            nameLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 14.0)
            
            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            
            nameLabel.frame.size.width = nameLabel.sizeThatFits(nameLabel.bounds.size).width+2
            nameLabel.frame.size.height = nameLabel.sizeThatFits(nameLabel.bounds.size).height
            //onlinePopUpButton.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width+10, self.view.frame.height*(10/568), self.view.frame.width*(14/320),self.view.frame.width*(14/320))
            onlinePopUpButton.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width+10, nameLabel.frame.origin.y+((nameLabel.frame.height/2)-(self.view.frame.width*(6/320))), self.view.frame.width*(12/320),self.view.frame.width*(12/320))
            
            let popImage = UIImage(named: "message_new.png") as UIImage?
            onlinePopUpButton.setImage(popImage, forState: UIControlState.Normal)
            onlinePopUpButton.addTarget(self, action: "popUpClickedWithoutAccess:", forControlEvents:.TouchUpInside)
            onlinePopUpButton.tag = indexPath.row
            
            onlinePopUpButton.frame.origin.x = nameLabel.frame.origin.x + nameLabel.frame.size.width + 5
            
            let contactLabel = UILabel()
            contactLabel.frame = CGRectMake(self.view.frame.width*(20/320),cell!.contentView.frame.height*(27.0/50),self.view.frame.width*(143/320), self.view.frame.height*(12.0/568))
            
            contactLabel.text = withoutAcess[row]["emailId"] as? String
            contactLabel.font = UIFont(name:"AvenirNext-Medium", size: 12.0)
            contactLabel.textColor = UIColor(red: 165.0/255, green: 165.0/255, blue: 165.0/255, alpha: 1.0)
           
            contactLabel.frame.size.width = contactLabel.sizeThatFits(contactLabel.bounds.size).width+2
            
            let image = UIImage(named: "checkbox.png") as UIImage?
            let button = UIButton()
            button.frame = CGRectMake(approvalAllbutton.frame.origin.x + ((approvalAllbutton.frame.width/2)-(self.view.frame.width*(7/320))), self.view.frame.height*(15/568), self.view.frame.width*(14/320), self.view.frame.width*(14/320))
            
            button.setImage(image, forState: UIControlState.Normal)
            button.addTarget(self, action: "accessButton:", forControlEvents:.TouchUpInside)
            button.tag = indexPath.row
            
            cell?.contentView.addSubview(button)
            cell?.contentView.addSubview(onlinePopUpButton)
            cell?.contentView.addSubview(nameLabel)
            cell?.contentView.addSubview(contactLabel)
            cell?.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            
            cell?.selectionStyle = .None
            return cell!
            
        }
        else
        {
            cellIdentifier = "pendingCell"
            
            let cell: pendingTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? pendingTableViewCell
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            let nameLabel = UILabel()
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320),  cell!.contentView.frame.height*(2.0/50), self.view.frame.width*(220.0/320), self.view.frame.height*(30.0/568))
            nameLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 12.0)
            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            nameLabel.adjustsFontSizeToFitWidth = true
            nameLabel.text =  pendingInvites[row]["invitedName"] as? String
            
            let emaillabel = UILabel()
            emaillabel.frame = CGRectMake(self.view.frame.width*(20.0/320),cell!.contentView.frame.height*(20.0/50), self.view.frame.width*(220.0/320), self.view.frame.height*(20.0/568))
            emaillabel.font = UIFont(name:"AvenirNext-DemiBold", size: 10.0)
            emaillabel.textColor = UIColor(red: 165.0/255, green: 165.0/255, blue: 165.0/255, alpha: 1.0)
            emaillabel.adjustsFontSizeToFitWidth = true
            emaillabel.text =  pendingInvites[row]["emailId"] as? String

            let cellView = UIView()
            cellView.frame = CGRectMake(0, 0, cell!.contentView.frame.width, cell!.contentView.frame.height)
            cellView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            
            let remindButton = UIButton()
            remindButton.frame = CGRectMake(self.view.frame.width*(230/320),cell!.contentView.frame.height*(11/50), self.view.frame.width*(90/320),cell!.contentView.frame.height*(30.0/50))
            remindButton.setTitle("Remind", forState: UIControlState.Normal)
            remindButton.setTitleColor(UIColor(red: 68.0/255, green: 185.0/255, blue: 227.0/255, alpha: 1.0), forState: UIControlState.Normal)
            
            remindButton.tag = indexPath.row
            remindButton.titleLabel?.font = UIFont(name:"AvenirNext-DemiBold", size: 12.0)
            remindButton.addTarget(self, action: "remindButton:", forControlEvents:.TouchUpInside)
            
            cellView.addSubview(emaillabel)
            cellView.addSubview(nameLabel)
            cellView.addSubview(remindButton)
            cell?.contentView.addSubview(cellView)
            
            cell?.selectionStyle = .None
            return cell!
            
        }
        
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if tableView == respondtableView
        {
            if  withAccess[indexPath.row]["attendingStatus"] as! String == "yes"
            {
                popUpView.hidden = false
                
                let noOfAdults = withAccess[indexPath.row]["noOfAdults"] as! Int
                adults.text = "\(noOfAdults)"
                print(withAccess)
                
                let noOfChild = withAccess[indexPath.row]["noOfChilds"] as! Int
                childrens.text = "\(noOfChild)"
                
                let eventName = currentEvent["eventTitle"] as! String
                print(eventName)
                 let hostName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
                
                let invitedName = withAccess[indexPath.row]["invitedName"] as! String
                
                eventTitle.text = "\(invitedName)'s Party includes"
                
                invitedPerson.text = "Note from \(invitedName) "
                invitationNote.text = withAccess[indexPath.row]["invitationNote"] as! String
                
            }
            else
            {
                onlinePopView.hidden = false
                let invitedName = withAccess[indexPath.row]["invitedName"] as! String
                onlineEventAttender.text = "Note from \(invitedName) "
                onlinePopUpTextView.text = withAccess[indexPath.row]["invitationNote"] as! String
                
                
            }
        }
        if tableView == repondTableView2
        {
            if  withoutAcess[indexPath.row]["attendingStatus"] as! String == "yes"
            {
                popUpView.hidden = false
                
                let noOfAdults = withoutAcess[indexPath.row]["noOfAdults"] as! Int
                adults.text = "\(noOfAdults)"
                print(withAccess)
                
                let noOfChild = withoutAcess[indexPath.row]["noOfChilds"] as! Int
                childrens.text = "\(noOfChild)"

                
                let invitedName = withoutAcess[indexPath.row]["invitedName"]  as! String
                print(withoutAcess)
                
                invitedPerson.text = "A note from \(invitedName) "
                invitationNote.text = withoutAcess[indexPath.row]["invitationNote"] as! String
            }
            else
            {
                print(withoutAcess)
                onlinePopView.hidden = false
                let invitedName = withoutAcess[indexPath.row]["invitedName"] as! String
                
                onlineEventAttender.text = "A note from \(invitedName) "
                
                onlinePopUpTextView.text = withoutAcess[indexPath.row]["invitationNote"] as! String
            }

        }
    }
    
    func popUpClickedWithAccess(sender:UIButton)
    {
        if  withAccess[sender.tag]["attendingStatus"] as! String == "yes"
        {
            popUpView.hidden = false
            
            let noOfAdults = withAccess[sender.tag]["noOfAdults"] as! Int
            adults.text = "\(noOfAdults)"
            print(withAccess)
            
            let noOfChild = withAccess[sender.tag]["noOfChilds"] as! Int
            childrens.text = "\(noOfChild)"
            
            //headingLabel.text = "Tell the host more"
            
            
            let eventName = currentEvent["eventTitle"] as! String
            print(eventName)
            eventTitle.text = eventName
            
            let invitedName = withAccess[sender.tag]["invitedName"]  as! String
            invitedPerson.text = "A note from \(invitedName) "
            invitationNote.text = withAccess[sender.tag]["invitationNote"] as! String
            print(invitationNote.text)
            
        }
        else
        {
            onlinePopView.hidden = false
            let invitedName = withAccess[sender.tag]["invitedName"] as! String
            print(invitedName)
            invitedPerson.text = "A note from \(invitedName) "
            onlinePopUpTextView.text = withAccess[sender.tag]["invitationNote"] as! String
        }
    }
    
    func popUpClickedWithoutAccess(sender:UIButton)
    {
        if  withoutAcess[sender.tag]["attendingStatus"] as! String == "yes"
        {
            popUpView.hidden = false
            adults.text = withoutAcess[sender.tag]["noOfAdults"] as? String
            childrens.text = withoutAcess[sender.tag]["noOfChilds"] as? String
            let invitedName = withoutAcess[sender.tag]["invitedName"] as! String
            print(withoutAcess)
            
            headingLabel.text = "Tell the host more"

            
            invitedPerson.text = "A note from \(invitedName) "
            invitationNote.text = withoutAcess[sender.tag]["invitationNote"] as! String
        }
        else
        {
             print(withoutAcess)
            onlinePopView.hidden = false
            let invitedName = withoutAcess[sender.tag]["invitedName"] as! String
            onlineEventAttender.text = "A note from \(invitedName) "
            headingLabel.text = "Tell the host more"

            onlinePopUpTextView.text = withoutAcess[sender.tag]["invitationNote"] as! String
        }
        
    }

    
    func remindButton(sender:UIButton)
    {
        var eventType = pendingInvites[sender.tag]["invitationType"] as! String!
        let email = pendingInvites[sender.tag]["emailId"] as! String
        var inviteCode = currentEvent.objectId!
        let eventTitle = currentEvent["eventTitle"] as! String
        var dateString = ""
        var timeString = ""
        var locationString = ""
        var hostName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        if currentEvent["isRSVP"] as! Bool
        {
            let sdate = currentEvent["eventStartDateTime"] as! NSDate
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            let sweekday = scomponents.weekday
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            dateString = "\(weekDaysArray[sweekday-1]) \(monthsArray[smonth-1]) \(sday), \(syear)"
            timeString = "\(shour):\(sminute) \(sam)"
            
            locationString = currentEvent["eventLocation"] as! String!
            
        }
        var message = ""
        
        let eventFolder = currentEvent["eventFolder"] as! String!
        let eventImage = currentEvent["eventImage"] as! String!
        
        let eventLatitude = currentEvent["eventLatitude"] as! Double
        let eventLongitude = currentEvent["eventLongitude"] as! Double
        
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        let inPersonReminder = InPersonReminder()
        
        let emailMessage = inPersonReminder.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, dateString:  dateString, timeString: timeString, locationString:  currentEvent["eventLocation"] as! String, hostName: fullUserName, type: "rsvp",  imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", longitude: "\(eventLongitude)", latitude: "\(eventLatitude)")
        
        let sendEmailObject = SendEmail()
        
        sendEmailObject.sendEmail("Reminder, please respond.", message: emailMessage, emails: [email])
        
        let notifMessage = "Reminder: \(fullUserName) invited you to the event, \(eventTitle). Please respond to the invitation."
        
        let data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "invitationreminder"
        ]
        
        
        let userObjectId = pendingInvites[sender.tag]["userObjectId"] as! String
        
        let predicate = NSPredicate(format: "userObjectId IN {'\(userObjectId)'}")
        
        let query = PFInstallation.queryWithPredicate(predicate)
        
        let push = PFPush()
        push.setQuery(query)
        push.setData(data)
        push.sendPushInBackground()
        
    }
    
    func accessButton(sender:UIButton)
    {
        //var InvitationObjetId = currentEvent.objectId!
        
        var inviteObject:PFObject!
        
        inviteObject = withoutAcess[sender.tag]
        
        inviteObject["isApproved"] = true
        inviteObject["isEventUpdated"] = true
        
        /*if attendingStatus == "yes"
        {
            attendingWithoutAcess[sender.tag]["isApproved"] = true
            inviteObject = attendingWithoutAcess[sender.tag]
        }
        
        if attendingStatus == "no"
        {
            notAttendingWithoutAccess[sender.tag]["isApproved"] = true
            inviteObject = notAttendingWithoutAccess[sender.tag]
        }
        
        if attendingStatus == "maybe"
        {
            maybeWithoutAccess[sender.tag]["isApproved"] = true
            inviteObject = maybeWithoutAccess[sender.tag]
        }
        
        if attendingStatus  == "online"
        {
            onlineWithoutAccess[sender.tag]["isApproved"] = true
            inviteObject = onlineWithoutAccess[sender.tag]
        }*/
        
        ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "approveSuccess:", successSelectorParameters: nil, errorSelector: "approveError:", errorSelectorParameters:nil)
        
        
    }
    
    
    func approveSuccess(timer: NSTimer)
    {
        let invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        let invitationObjectId = invitation.objectId!
        var tblFields: Dictionary! = [String: String]()
        
        
        tblFields["isApproved"] = "1"
        
        
        var insertedId = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitationObjectId])
        
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
         let email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        
        let eventTitle = currentEvent["eventTitle"] as! String
        
        let notifMessage = " \(fullUserName) gave you access to photos/videos/notes for the event, \(eventTitle). Check them out."
        
        
        let eventFolder = currentEvent["eventFolder"] as! String!
        let eventImage = currentEvent["eventImage"] as! String!
        
        let streamAccess = StreamAccess()
        
        let emailMessage = streamAccess.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)")
        
        let sendEmail = SendEmail()
        
        sendEmail.sendEmail("You Got Access – \(eventTitle)", message: emailMessage, emails: [email])
        
        let data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "streamaccesschanged"
            
        ]
        
        
        let userObjectId = invitation["userObjectId"] as! String
        
        let notificationObject = PFObject(className: "Notifications")
        notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
        notificationObject["notificationImage"] = "profilePic.png"
        notificationObject["senderId"] = currentUserId
        notificationObject["receiverId"] = userObjectId
        notificationObject["notificationActivityMessage"] = notifMessage
        notificationObject["eventObjectId"] = currentEvent.objectId!
        notificationObject["notificationType"] = "streamaccesschanged"
        
        notificationObject.saveInBackground()
        
        
        
        
        let predicate = NSPredicate(format: "userObjectId IN {'\(userObjectId)'} AND hostActivityNotification = true")
        
        
        let query = PFInstallation.queryWithPredicate(predicate)
        
        let push = PFPush()
        push.setQuery(query)
        push.setData(data)
        push.sendPushInBackground()

        
        
        
        
        fetchInvitations()
        
        //respondtableView.reloadData()
        //repondTableView2.reloadData()
        
        
    }
    
    func approveError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    
    func disapproveAccessButton(sender:UIButton)
    {
        
        //var InvitationObjetId = currentEvent.objectId!
        
        var inviteObject:PFObject!
        
        inviteObject = withAccess[sender.tag]
        
        inviteObject["isApproved"] = false
        inviteObject["isEventUpdated"] = true
        /*if attendingStatus == "yes"
        {
            attendingWithAccess[sender.tag]["isApproved"] = true
            inviteObject = attendingWithAccess[sender.tag]
        }
        
        if attendingStatus == "no"
        {
            notAttendingWithAccess[sender.tag]["isApproved"] = true
            inviteObject = notAttendingWithAccess[sender.tag]
        }
        
        if attendingStatus == "maybe"
        {
            maybewithAccess[sender.tag]["isApproved"] = true
            inviteObject = maybewithAccess[sender.tag]
            
        }
        
        if attendingStatus  == "online"
        {
            onlineWithAccess[sender.tag]["isApproved"] = true
            inviteObject = onlineWithAccess[sender.tag]
        }*/
        
        
        ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "disapproveAccessSuccess:", successSelectorParameters: nil, errorSelector: "disapproveAccessError:", errorSelectorParameters:nil)
        
    }
    
    func disapproveAccessSuccess(timer: NSTimer)
    {
        let invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        let invitationObjectId = invitation.objectId!
        
        var tblFields: Dictionary! = [String: String]()
        
        
        tblFields["isApproved"] = "0"
        
        
        var insertedId = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitationObjectId])
        
        fetchInvitations()

    }
    
    func disapproveAccessError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    
    
    @IBAction func attendingButton(sender: AnyObject)
        
    {
        
        //popUpButton.hidden = false
        
        onlineTexView.hidden = true
        highlightedView.hidden = false
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = true
        
        currentViewType = "yes"
        
        onlineTexView.text = "These are the guests that are attending your event."
        onlineTexView.font = UIFont(name: "Avenir Next Medium", size: 11.0)
        
        onlineTexView.frame.size.height = onlineTexView.sizeThatFits(onlineTexView.bounds.size).height
        
        respondedTextView2.frame.size.height = respondedTextView2.sizeThatFits(respondedTextView2.bounds.size).height
        
        fetchInvitations()

    }
    
    @IBAction func onlineButton(sender: AnyObject)
    {
        
        //onlinePopUpButton.hidden = false
        
        onlineTexView.hidden = false
        highlightedView.hidden = true
        onlineHighlightedView.hidden = false
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = true
        
        currentViewType = "online"
        
        onlineTexView.text = "Guests who will be following your event  online only through the event stream."
        onlineTexView.font = UIFont(name: "Avenir Next Medium", size: 11.0)
        
        onlineTexView.frame.size.height = onlineTexView.sizeThatFits(onlineTexView.bounds.size).height
        
        respondedTextView2.frame.size.height = respondedTextView2.sizeThatFits(respondedTextView2.bounds.size).height
        
        fetchInvitations()

    }
    
    
    @IBAction func mayBeButton(sender: AnyObject)
    {
        //onlinePopUpButton.hidden = true
        //popUpButton.hidden = true
        onlineTexView.hidden = true
        highlightedView.hidden = true
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = false
        notAttendingHighlightedView.hidden = true
        
        currentViewType = "maybe"
        
        onlineTexView.text = "It happens. These guests can’t make up their mind yet."
        onlineTexView.font = UIFont(name: "Avenir Next Medium", size: 11.0)
        
        onlineTexView.frame.size.height = onlineTexView.sizeThatFits(onlineTexView.bounds.size).height
        
        respondedTextView2.frame.size.height = respondedTextView2.sizeThatFits(respondedTextView2.bounds.size).height
        
        fetchInvitations()
 
    }
    
    
    @IBAction func notAttendingButton(sender: AnyObject)
    {
        //onlinePopUpButton.hidden = true
        //popUpButton.hidden = true
        onlineTexView.hidden = true
        highlightedView.hidden = true
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = false
        
        currentViewType = "no"
        
        onlineTexView.text = "Bummer! These are the guests that can’t make it to your event."
        onlineTexView.font = UIFont(name: "Avenir Next Medium", size: 11.0)
        
        onlineTexView.frame.size.height = onlineTexView.sizeThatFits(onlineTexView.bounds.size).height
        
        respondedTextView2.frame.size.height = respondedTextView2.sizeThatFits(respondedTextView2.bounds.size).height
        
        fetchInvitations()
        
    }
    
    @IBAction func cancelButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    
    @IBAction func approveAllButton(sender: AnyObject)
    {
        
        if withoutAcess.count > 0
        {
            var withouAccessInvites = [PFObject]()
            var notificationObjects = [PFObject]()
            
            var userObjectIds = [String]()
            
            let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            let eventTitle = currentEvent["eventTitle"] as! String
            
            let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
            
            let notifMessage = " \(fullUserName) gave you access to photos/videos/notes for the event, \(eventTitle). Check them out."
            
            
            
            
            for (var i = 0; i < withoutAcess.count; i++)
            {
                withouAccessInvites.append(withoutAcess[i])
                withouAccessInvites[i]["isApproved"] = true
                withouAccessInvites[i]["isEventUpdated"] = true
                
                userObjectIds.append(withouAccessInvites[i]["userObjectId"] as! String)
                
                let notificationObject = PFObject(className: "Notifications")
                notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                notificationObject["notificationImage"] = "profilePic.png"
                notificationObject["senderId"] = currentUserId
                notificationObject["receiverId"] = withouAccessInvites[i]["userObjectId"] as! String
                notificationObject["notificationActivityMessage"] = notifMessage
                notificationObject["eventObjectId"] = currentEvent.objectId!
                notificationObject["notificationType"] = "streamaccesschanged"
                
                notificationObjects.append(notificationObject)
            }

            
            PFObject.saveAllInBackground(notificationObjects)
            
            
            let data = [
                "alert" : "\(notifMessage)",
                "notifType" :  "streamaccesschanged"
                
            ]
            
            let userObjectIdsString = userObjectIds.joinWithSeparator("','")
            
            let predicate = NSPredicate(format: "userObjectId IN {'\(userObjectIdsString)'} AND hostActivityNotification = true ")
            
            
            let query = PFInstallation.queryWithPredicate(predicate)
            
            let push = PFPush()
            push.setQuery(query)
            push.setData(data)
            push.sendPushInBackground()
            
            
            PFObject.saveAllInBackground(withouAccessInvites)
                {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    var invitationIdsString = ""
                    
                    if self.withoutAcess.count > 0
                    {
                        for (var i = 0; i < self.withoutAcess.count; i++)
                        {
                            let invitationId = self.withoutAcess[i].objectId!
                            invitationIdsString = "\(invitationIdsString)\(invitationId)"
                            if i < self.withoutAcess.count - 1
                            {
                                invitationIdsString = "\(invitationIdsString)','"
                            }
                        }
                        
                    }
                    
                    if invitationIdsString != ""
                    {
                        var tblFields: Dictionary! = [String: String]()
                        tblFields["isApproved"] = "1"
                        ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId IN ('\(invitationIdsString)') ", whereFields: [])
                    }
                    self.fetchInvitations()
                }
                else
                {
                    
                }
            }
        }
    }
    
    
    @IBAction func fetchFacebookFriends(sender: UIButton)
    {
        if isFacebookLogin
        {
            let inviteFbVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFacebookFriendsViewController") as! InviteFacebookFriendsViewController
            self.navigationController?.pushViewController(inviteFbVC, animated: false)
        }
        else
        {
            showLinkfacebookView = true
            
            let ConnectFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("connectFacebookAccount") as! facebookLinkedViewController
            
            ConnectFacebook.isInviteView = true
            
            self.navigationController?.pushViewController(ConnectFacebook, animated: false)
            
        }
        
    }
    
    
    
    @IBAction func fetchEmailContacts(sender: UIButton)
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            var error : Unmanaged<CFError>? = nil
            let addressBook : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
            
            if addressBook == nil {
                print(error)
                return
            }
            /*ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
            if success {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
            }
            else {
            NSLog("unable to request access")
            }
            })*/
            
            ABAddressBookRequestAccessWithCompletion(addressBook) {
                (granted: Bool, error: CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if !granted {
                        print("Just denied")
                        
                        let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                            
                        }))
                        
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                        
                    } else {
                        //println("Just authorized")
                        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
                        self.navigationController?.pushViewController(homeVC, animated: false)
                        
                    }
                }
            }
            
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
            NSLog("access denied")
            
            let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    
    @IBAction func closePopUpView(sender: AnyObject)
    {
        popUpView.hidden = true
    }
    
    @IBAction func closeOnlinePopUpView(sender: AnyObject)
    {
        onlinePopView.hidden = true
        
    }
    
    @IBAction func socialSharingButton(sender: AnyObject)
    {
        let plainData = (currentEvent.objectId! as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        print(base64String)
        
        var branchUrl = currentEvent["socialSharingURL"] as! String
        
        let plainDataUrl = (branchUrl as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        print(base64UrlString)
        
        
        
        var urlString = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId=\(base64String)&url=\(base64UrlString)"
        
        var eventTitle = currentEvent["eventTitle"] as! String
        
        var objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(urlString)"
        
        let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
        self.navigationController!.presentViewController(activityVC,
            animated: true,
            completion: nil)
    }
    
}




