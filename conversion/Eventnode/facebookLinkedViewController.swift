//
//  facebookLinkedViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class facebookLinkedViewController: UIViewController {
    
    @IBOutlet weak var facebookLinkedButtonView: UIView!
    
    @IBOutlet weak var textView: UITextView!
    
    var isInviteView: Bool!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        textView.attributedText = NSAttributedString(string:textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)           }
    
    override func viewDidAppear(animated: Bool)
    {
        if !showLinkfacebookView
        {
            showLinkfacebookView = true
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    
    
    @IBAction func linkedFacebookButton(sender: AnyObject)
    {
        
        let user = PFUser.currentUser()
        
        //if !PFFacebookUtils.isLinkedWithUser(user!)
        if !isFacebookLogin
        {
            PFFacebookUtils.linkUserInBackground( user!, withReadPermissions: nil, block: { (success:Bool, error:NSError?) -> Void in
                if error == nil
                {
                    if let user = user
                    {
                        
                        isFacebookLogin = true
                        user["isFacebookLogin"] = true
                        
                        
                        
                        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                        
                        let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                        connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                            if error != nil {
                                print(error)
                            } else
                            {
                                let resultdict = result as! NSDictionary
                                
                                let fbId = resultdict.valueForKey("id") as! String
                                print("\(fbId)")
                                print(resultdict)
                                user["facebookId"] =  fbId
                                
                                user.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                                    if success
                                    {
                                        isFacebookLogin = true
                                        
                                        
                                        
                                        
                                        let emailData = resultdict.valueForKey("email") as! String
                                        
                                        let usernameData = resultdict.valueForKey("first_name") as! String
                                        let lastnameData = resultdict.valueForKey("last_name") as! String
                                        
                                        NSUserDefaults.standardUserDefaults().setObject(emailData, forKey: "facebookEmail")
                                        NSUserDefaults.standardUserDefaults().setObject("\(usernameData) \(lastnameData)", forKey: "facebookName")
                                        
                                        if self.isInviteView == true
                                        {
                                            self.fetchFacebookFriends()
                                        }
                                        else
                                        {
                                            let UnlinkFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("unlinkFacebook") as! facebookAccountUnlinkViewController
                                            self.navigationController?.pushViewController(UnlinkFacebook, animated: false)
                                        }
                                        
                                        print("linked")
                                        
                                        print("data is updated")
                                        
                                    }
                                    else
                                    {
                                        print(error?.localizedDescription)
                                    }
                                })
                                
                            }
                        })
                        
                        connection.start()
                        
                    }
                }
                else
                {
                    print(error!.code)
                    
                    
                    let alert1 = UIAlertView()
                    alert1.title = "Error"
                    alert1.delegate = self
                    
                    if error!.code == 208
                    {
                        alert1.message = "Your Facebook account is already linked to a different eventnode account. You need to unlink it before you can connect it to this account."
                    }
                    else
                    {
                        alert1.message = "Something went wrong. Please try again later."
                    }
                    
                    //alert1.message = error!.localizedDescription
                    alert1.addButtonWithTitle("Ok")
                    alert1.show()
                }
            })
        }
        else
        {
            if #available(iOS 8.0, *) {
                let refreshAlert = UIAlertController(title: "Error", message: "You are already connected.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
            else{
                
            }
        }
        
        
    }
    
    @IBAction func backbutton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func fetchFacebookFriends()
    {
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me/friends?fields=name,email", parameters: nil)
        
        let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
        connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
            if error != nil {
                //handle error
                print(error)
            } else
            {
                //println(result)
                facebookFriends = result.valueForKey("data") as! Array<NSDictionary!>
                print(facebookFriends)
                
                let inviteFbVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFacebookFriendsViewController") as! InviteFacebookFriendsViewController
                self.navigationController?.pushViewController(inviteFbVC, animated: false)
                
            }
        })
        
        connection.start()
    }
    
}
