//
//  InviteFriendsFirstViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import AddressBookUI
import AddressBook
import ContactsUI
import Contacts

@available(iOS 9.0, *)
class InviteFriendsFirstViewController: UIViewController {


    @IBOutlet var textView : UITextView!
    
    
    var isFromCreated: Bool!
    var contactStore = CNContactStore()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        textView.attributedText = NSAttributedString(string: textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

    // MARK: - Navigation

    @IBAction func viewTapped(sender : AnyObject) {

    }
    
    @IBAction func fetchFacebookFriends(sender: UIButton) {
        
        if isFacebookLogin
        {
            let inviteFbVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFacebookFriendsViewController") as! InviteFacebookFriendsViewController
            self.navigationController?.pushViewController(inviteFbVC, animated: false)
        }   
        else
        {
            showLinkfacebookView = true
            
            let ConnectFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("connectFacebookAccount") as! facebookLinkedViewController
            
            ConnectFacebook.isInviteView = true
            
            self.navigationController?.pushViewController(ConnectFacebook, animated: false)
            
        }
    }

    
    func fetchEmail ()
    {
        let authorizationStatus = CNContactStore.authorizationStatusForEntityType(CNEntityType.Contacts)
        
        switch authorizationStatus {
        case .Authorized:
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
            
            
        case .Denied, .NotDetermined:
            self.contactStore.requestAccessForEntityType(CNEntityType.Contacts, completionHandler: { (access, accessError) -> Void in
                if access
                {
                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
                    self.navigationController?.pushViewController(homeVC, animated: false)

                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.Denied
                    {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            
                            let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS9 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                                
                            }))
                            
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                            
                        })
                    }
                }
            })
            
        default:
            
            break
            
        }
   
    
    }


    @IBAction func fetchEmailContacts(sender: UIButton) {
        
        fetchEmail()
        //getAddressBookNames()
        
//        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
//        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
//        {
//            NSLog("requesting access...")
//            var error : Unmanaged<CFError>? = nil
//            let addressBook : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
//            
//            if addressBook == nil {
//                print(error)
//                return
//            }
//            /*ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
//                if success {
//                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
//                    self.navigationController?.pushViewController(homeVC, animated: false)
//                }
//                else {
//                    NSLog("unable to request access")
//                }
//            })*/
//            
//            ABAddressBookRequestAccessWithCompletion(addressBook) {
//                (granted: Bool, error: CFError!) in
//                dispatch_async(dispatch_get_main_queue()) {
//                    if !granted {
//                        print("Just denied")
//                        
//                        let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
//                        
//                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
//                            
//                        }))
//                        
//                        self.presentViewController(refreshAlert, animated: true, completion: nil)
//                        
//                    } else {
//                        //println("Just authorized")
//                        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
//                        self.navigationController?.pushViewController(homeVC, animated: false)
//
//                    }
//                }
//            }
//
//            
//        }
//        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
//            NSLog("access denied")
//            
//            let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
//            
//            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
//                
//            }))
//            
//            self.presentViewController(refreshAlert, animated: true, completion: nil)
//            
//        }
//        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
//            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
//            self.navigationController?.pushViewController(homeVC, animated: false)
//        }
    }
    
    
    @IBAction func closeAdjustPhotoButtonClicked(sender : AnyObject){
        
        if isFromCreated == true
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            homeVC.redirect = true
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
        else
        {
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    @IBAction func socailShareButton(sender: AnyObject) {
        
        let plainData = (currentEvent.objectId! as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        print(base64String)
        
        let branchUrl = currentEvent["socialSharingURL"] as! String
        
        let plainDataUrl = (branchUrl as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        print(base64UrlString)
        
        
        
        let urlString = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId=\(base64String)&url=\(base64UrlString)"
        
        let eventTitle = currentEvent["eventTitle"] as! String
        
        let objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(urlString)"
        
        let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
        self.navigationController!.presentViewController(activityVC,
            animated: true,
            completion: nil)

    }
    
    
}
