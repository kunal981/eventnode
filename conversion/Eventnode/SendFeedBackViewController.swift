//
//  SendFeedBackViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MessageUI

class SendFeedBackViewController: UIViewController,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var texView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        let attributes = [NSParagraphStyleAttributeName : style]
        
        texView.attributedText = NSAttributedString(string:texView.text, attributes:attributes)
        texView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        texView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func emailfeedbackButton(sender: AnyObject)
    {
        //Mobihelp.sharedInstance().presentFeedback(self)
        
    }
    
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        if result.rawValue == MFMailComposeResultSent.rawValue
        {
            let alertView = UIAlertView()
            alertView.message = "Mail Sent!"
            alertView.addButtonWithTitle("OK")
            
            alertView.show()
        }
        
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func rateOnAppStorebutton(sender: AnyObject)
    {
        let openLink = NSURL(string : "https://itunes.apple.com/us/app/eventnode/id1049409726")
        UIApplication.sharedApplication().openURL(openLink!)
        
    }
    
    @IBAction func likeOnfacebookbutton(sender: AnyObject)
    {
        let openLink = NSURL(string : "https://www.facebook.com/eventnode")
        UIApplication.sharedApplication().openURL(openLink!)
        
        
    }
    
    @IBAction func followUsOnTwiiter(sender: AnyObject)
    {
        let openLink = NSURL(string : "https://twitter.com/eventnode")
        UIApplication.sharedApplication().openURL(openLink!)
    }
    
    @IBAction func backbutton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
