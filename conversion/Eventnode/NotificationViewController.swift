//
//  NotificationViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import AudioToolbox

class NotificationViewController: UIViewController {
    
    
    @IBOutlet weak var inviteOutlet: UIView!
    @IBOutlet weak var activityAttendingOutlet: UIView!
    @IBOutlet weak var guestActivityOutlet: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textView2: UITextView!
    @IBOutlet weak var someoneInvitedMeEmailcheckBox: UIButton!
    @IBOutlet weak var someoneInvitedMeAlertCheckBox: UIButton!
    
    @IBOutlet weak var newactivityImattendingAlertcheckBox: UIButton!
    
    @IBOutlet weak var newactivityImattendingemailcheckBox: UIButton!
    
    @IBOutlet weak var allActivityimhostingAlertCheckBox: UIButton!
    
    @IBOutlet weak var allActivityImhostingEmailCheckBox: UIButton!
    
    var currentUserId: String!
    
    
    //var  isChecked:Bool = false
    var  isVibrate:Bool!
    var isSoundAlert:Bool!
    
    var notificationStatuses: Array<Bool>!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*@IBOutlet weak var guestActivityOutlet: UITextView!
        @IBOutlet weak var activityAttendingOutlet: UITextView!
        @IBOutlet weak var invitedOutlet: UITextView!
        
        
        @IBOutlet weak var someoneInvitedMeEmailcheckBox: UIButton!
        @IBOutlet weak var someoneInvitedMeAlertCheckBox: UIButton!
        
        @IBOutlet weak var newactivityImattendingAlertcheckBox: UIButton!
        
        @IBOutlet weak var newactivityImattendingemailcheckBox: UIButton!
        
        @IBOutlet weak var allActivityimhostingAlertCheckBox: UIButton!
        
        @IBOutlet weak var allActivityImhostingEmailCheckBox: UIButton!*/
        
        inviteOutlet.frame.size.height = inviteOutlet.sizeThatFits(inviteOutlet.bounds.size).height
        activityAttendingOutlet.frame.size.height = activityAttendingOutlet.sizeThatFits(activityAttendingOutlet.bounds.size).height
        guestActivityOutlet.frame.size.height = guestActivityOutlet.sizeThatFits(guestActivityOutlet.bounds.size).height
        
        
        inviteOutlet.frame.origin.y = someoneInvitedMeAlertCheckBox.frame.origin.y + ((someoneInvitedMeAlertCheckBox.frame.height/2)-(inviteOutlet.frame.height/2))
        
        activityAttendingOutlet.frame.origin.y = newactivityImattendingAlertcheckBox.frame.origin.y + ((newactivityImattendingAlertcheckBox.frame.height/2)-(activityAttendingOutlet.frame.height/2))
        
        guestActivityOutlet.frame.origin.y = allActivityimhostingAlertCheckBox.frame.origin.y + ((allActivityimhostingAlertCheckBox.frame.height/2)-(guestActivityOutlet.frame.height/2))
        
        
        someoneInvitedMeAlertCheckBox.selected = false;
        someoneInvitedMeEmailcheckBox.selected = false;
        newactivityImattendingAlertcheckBox.selected = false
        newactivityImattendingemailcheckBox.selected = false
        allActivityimhostingAlertCheckBox.selected = false
        allActivityImhostingEmailCheckBox.selected = false
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        let attributes = [NSParagraphStyleAttributeName : style]
        
        textView.attributedText = NSAttributedString(string: textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        
        var style2 = NSMutableParagraphStyle()
        style.lineSpacing = 3
        let attributes2 = [NSParagraphStyleAttributeName : style2]
        
        
        
        //        textView2.attributedText = NSAttributedString(string:textView2.text, attributes:attributes2)
        //        textView2.font = UIFont(name: "AvenirNext-Medium", size: 10.0)
        //        textView2.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        
        var user: PFUser = PFUser.currentUser()!
        
        notificationStatuses = []
        
        if user["inviteNotification"] as! Bool == true
        {
            (self.view.viewWithTag(51) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(51) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["inviteEmail"] as! Bool == true
        {
            (self.view.viewWithTag(53) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(53) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["hostActivityNotification"] as! Bool == true
        {
            (self.view.viewWithTag(55) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(55) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["hostActivityEmail"] as! Bool == true
        {
            (self.view.viewWithTag(57) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(57) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["guestActivityNotification"] as! Bool == true
        {
            (self.view.viewWithTag(59) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(59) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["guestActivityEmail"] as! Bool == true
        {
            (self.view.viewWithTag(61) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(61) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func notificationToggle(sender : UIButton)
    {
        /* var index = ((sender.tag-50)/2)-1
        
        
        var installation = PFInstallation.currentInstallation()
        
        installation["userObjectId"] = currentUserId
        
        var user: PFUser = PFUser.currentUser()!
        
        var status = true
        
        if notificationStatuses[index] == true
        {
        status = false
        (self.view.viewWithTag(sender.tag-1) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
        }
        else
        {
        status = true
        (self.view.viewWithTag(sender.tag-1) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
        }
        
        notificationStatuses[index] = status
        
        if sender.tag == 52
        {
        user["inviteNotification"] = status
        installation["inviteNotification"] = status
        }
        
        if sender.tag == 54
        {
        user["inviteEmail"] = status
        }
        
        if sender.tag == 56
        {
        user["hostActivityNotification"] = status
        installation["hostActivityNotification"] = status
        }
        
        if sender.tag == 58
        {
        user["hostActivityEmail"] = status
        }
        
        if sender.tag == 60
        {
        user["guestActivityNotification"] = status
        installation["guestActivityNotification"] = status
        }
        
        if sender.tag == 62
        {
        user["guestActivityEmail"] = status
        }
        
        installation.saveInBackground()
        user.saveInBackground()
        
        if sender.tag == 64
        {
        
        (self.view.viewWithTag(63) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isSoundAlert")
        }
        else
        {
        (self.view.viewWithTag(63) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
        
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isSoundAlert")
        
        }
        
        if sender.tag == 66
        {
        
        (self.view.viewWithTag(65) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isVibrateAlert")
        
        }
        else
        {
        (self.view.viewWithTag(65) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
        
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isVibratesAlert")
        
        }*/
        
        
        var notificationAlert = UIAlertController(title: "Alert", message:"This feature is coming soon", preferredStyle: UIAlertControllerStyle.Alert)
        
        notificationAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        self.presentViewController(notificationAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addReplybutton(sender: AnyObject)
    {
        
    }
    
    
}
