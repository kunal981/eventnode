//
//  EventPhotosTableViewCell.swift
//  eventnode
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class EventPhotosTableViewCell: UITableViewCell {


    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postText: UITextView!
    @IBOutlet weak var playButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
