//
//  UpdateEventDateTimeViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 6/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class UpdateEventDateTimeViewController: UIViewController {
    
    var imageData: UIImage!
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var startDateTime: UIDatePicker!
    
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var endView: UIView!
    @IBOutlet weak var endDateTime: UIDatePicker!
    
    var endViewTop: CGFloat!
    
    var slideUp: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
        
        endViewTop = endView.frame.origin.y
        
        print(currentEvent["eventStartDateTime"])
        startDateTime.date = (currentEvent["eventStartDateTime"] as? NSDate)!
        endDateTime.date = (currentEvent["eventEndDateTime"] as? NSDate)!
        
        startDateTime.minimumDate = NSDate()
        
        endDateTime.minimumDate = startDateTime.date
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startButtonClicked(sender: UIButton) {
        if(slideUp == true)
        {
            slideUp = false
            startDateTime.hidden = false
            endDateTime.hidden = true
            endView.frame.origin.y = endViewTop
        }
    }
    
    @IBAction func endButtonClicked(sender: AnyObject) {
        if(slideUp == false)
        {
            slideUp = true
            startDateTime.hidden = true
            endDateTime.hidden = false
            endView.frame.origin.y = (endDateTime.frame.origin.y-endView.frame.height)
        }
    }
    
    @IBAction func startDateTimePickerValueChanged(sender: UIDatePicker) {
        endDateTime.minimumDate = startDateTime.date
    }
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        
        //currentEvent["eventStartDateTime"] = startDateTime.date
        //currentEvent["eventEndDateTime"] = endDateTime.date
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func saveButtonClicked(sender : AnyObject){
        
        isUpdated = true
        currentEvent["eventStartDateTime"] = startDateTime.date
        currentEvent["eventEndDateTime"] = endDateTime.date
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
