//
//  EventDetailsViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/24/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

var newEvent:PFObject!
var eventTitle:String! = ""


class EventDetailsViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet var evenNameTextBg : UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dateAndTime_label: UILabel!
    @IBOutlet var basicDetailsView: UIView!
    @IBOutlet var eventPreviewButton: UIButton!
    @IBOutlet var rsvpToggleButton : UIButton!
    @IBOutlet var descriptionMenu: UIView!
    @IBOutlet var locationMenu: UIView!
    @IBOutlet weak var datePickerMenu: UIView!
    @IBOutlet weak var inPersonDetailsView: UIView!
    @IBOutlet var rsvpButton: UIButton!
    @IBOutlet var loaderView : UIView!
    @IBOutlet var loaderSubView : UIView!
    @IBOutlet var eventImage: UIImageView!
    
    var imageData: UIImage!
    var originalImageData: UIImage!
    var show: Bool! = true
    var showalert: Bool! = true
    var currentUserId: String!
    var fullUserName: String!
    var eventLogoFile: String!
    var originalEventLogoFile: String!
    var startDate = NSString()
    var eventLogoFileUrl: NSURL!
    var originalEventLogoFileUrl: NSURL!
    var isAfterImage = false
    var isInPersonEvent = false
    var frameX: CGFloat!
    var frameY: CGFloat!
    var showPicker = false
    var datePickerView : UIDatePicker! = UIDatePicker()
    var inPersonDetailsViewOriginalHeight:CGFloat = 0
    var eventPreviewButtonOriginalHeight:CGFloat = 0
    var eventPreviewButtonOriginalY:CGFloat = 0
    var descriptionMenuY: CGFloat!
    var locationMenuY: CGFloat!
    var datePickerMenuY: CGFloat!
    var descriptionMenuHeight: CGFloat!
    var locationMenuHeight: CGFloat!
    var datePickerMenuHeight: CGFloat!
    var imageHeightDiff:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.addSubview(wakeUpImageView)
        
        inPersonDetailsViewOriginalHeight = inPersonDetailsView.frame.height
        eventPreviewButtonOriginalY = eventPreviewButton.frame.origin.y
        eventPreviewButtonOriginalHeight = eventPreviewButton.frame.height
        
        descriptionMenuY = descriptionMenu.frame.origin.y
        locationMenuY = locationMenu.frame.origin.y
        datePickerMenuY = datePickerMenu.frame.origin.y
        
        descriptionMenuHeight = descriptionMenu.frame.height
        locationMenuHeight = locationMenu.frame.height
        datePickerMenuHeight = datePickerMenu.frame.height
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("imageTapped"))
        eventImage.addGestureRecognizer(tapGestureRecognizer)
        
        isUpdated = false;
        
        if isAfterImage == false
        {
            eventTitle = ""
            newEvent = PFObject(className:"Events")
            newEvent["isRSVP"] = false
            newEvent["eventDescription"] = ""
            newEvent["eventLocation"] = ""
            newEvent["eventLatitude"] = 0
            newEvent["eventLongitude"] = 0
            newEvent["eventStartDateTime"] = NSDate()
            newEvent["eventEndDateTime"] = NSDate()
        }
        
        if isInPersonEvent == true {
            rsvpChecked(self.rsvpButton)
            
        } else {
            self.inPersonDetailsView.hidden = false
        }
        
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        self.evenNameTextBg.delegate = self;
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        if imageData != nil
        {
            let originalHeight = eventImage.frame.height
            eventImage.frame.size.height = 3*(self.view.frame.width)/4
            imageHeightDiff = eventImage.frame.height - originalHeight
            eventImage.image = imageData
            if newEvent["isRSVP"] as? Bool == true
            {
                scrollView.contentSize.height = scrollView.frame.height + imageHeightDiff
            }
        }
        
        print(eventTitle)
        
        evenNameTextBg.text = eventTitle
        
        addDatePicker()
        
        basicDetailsView.frame.origin.y = basicDetailsView.frame.origin.y + imageHeightDiff
        inPersonDetailsView.frame.origin.y = inPersonDetailsView.frame.origin.y + imageHeightDiff
        
        
        if newEvent["isRSVP"] as? Bool == true
        {
            show = false
            
            let image = UIImage(named: "check-box.png")
            rsvpButton.setImage(image, forState: .Normal)
            
            inPersonDetailsView.hidden = false
            
            scrollView.contentSize.height = inPersonDetailsView.frame.origin.y + inPersonDetailsView.frame.height
        }
        else
        {
            inPersonDetailsView.hidden = true
        }
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        showLoader("Creating Event...");
        
        indicator.startAnimating()
        
    }
    
    
    func addDatePicker()
    {
        
        //  println(currentEvent["eventStartDateTime"])
        datePickerView  = UIDatePicker(frame: CGRectMake(0,datePickerMenu.frame.origin.y + datePickerMenu.frame.height, inPersonDetailsView.frame.width , 230*(self.view.frame.height/568)))
        
        let sDate = newEvent["eventStartDateTime"] as? NSDate
        
        //showDate.hidden = true
        
        datePickerView.datePickerMode = UIDatePickerMode.DateAndTime
        datePickerView.backgroundColor = UIColor(red: 225/255, green: 241/255, blue: 249/255, alpha: 1.0)
        datePickerView.setDate(sDate!, animated: true)
        
        datePickerView.minuteInterval = 15
        //          datePickerView.set
        
        datePickerView.addTarget(self, action: Selector("dateChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        datePickerView.hidden = true
        
        datePickerView.minimumDate = NSDate()
        
        dateAndTime_label.text = getFormatedStringFromDate(sDate!)
        dateAndTime_label.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
        dateAndTime_label.textAlignment = .Right
        
        self.inPersonDetailsView.addSubview(datePickerView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dateChanged(sender: UIDatePicker)
    {
        let timeFormatter = NSDateFormatter()
        timeFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        startDate = timeFormatter.stringFromDate(datePickerView.date)
        newEvent["eventStartDateTime"] = datePickerView.date
        
        dateAndTime_label.text = getFormatedStringFromDate(datePickerView.date)
        dateAndTime_label.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
        dateAndTime_label.textAlignment = .Right
    }
    
    func getFormatedStringFromDate(sdate: NSDate) -> String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        
        let startDate = "\(monthsArray[smonth-1]) \(sday), \(syear), \(shour):\(sminute) \(sam)"
        return startDate
    }
    
    
    func imageTapped()
    {
        eventTitle = evenNameTextBg.text!
        
        print(eventTitle)
        
        print("Tapped on Event Image")
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            print("Button capture")
            
            
            //showLoader("Loading")
            loaderView.hidden = false
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as! String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
            loaderView.hidden=true
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {
        let data = UIImagePNGRepresentation(image)
        print(data!.length)
        
        self.loaderView.hidden = true
        
        let adjustPhotoVC = self.storyboard!.instantiateViewControllerWithIdentifier("AdjustPhotoViewController") as! AdjustPhotoViewController
        adjustPhotoVC.imageData = image
        
        self.navigationController?.pushViewController(adjustPhotoVC, animated: false)
        
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        NSLog("picker cancel.")
        self.loaderView.hidden = true
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - loader
    func showLoader(message: String)
    {
        let loadingMessage = UILabel()
        loadingMessage.text = "\(message)"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
    }
    // MARK: - Navigation
    
    //
    //    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool{
    //
    //        var _char = string.cStringUsingEncoding(NSUTF8StringEncoding)
    //
    //        if(string=="")
    //        {
    //            print("sdsdfs__\(_char?.count)")
    //        }
    //
    //        if(string != "")
    //        {
    //            if evenNameTextBg.tex!t.characters.count < 30
    //            {
    //                return true
    //            }
    //            else
    //            {
    //                return false
    //            }
    //        }
    //        else
    //        {
    //            return true
    //        }
    //    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        evenNameTextBg.resignFirstResponder()
        return true
    }
    
    
    func keyboardWillShow(sender: NSNotification) {
        //TODO(geetikak): Do we need to do anything here ?
        let userInfo = sender.userInfo!
        
        let keyboardScreenBeginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        if (self.view.frame.height - keyboardScreenBeginFrame.height) < (self.evenNameTextBg.frame.height + basicDetailsView.frame.origin.y + (60*self.view.frame.height/568) )
        {
            scrollView.contentOffset.y = (self.evenNameTextBg.frame.height + basicDetailsView.frame.origin.y + (80*self.view.frame.height/568) ) - (self.view.frame.height - keyboardScreenBeginFrame.height)
        }
        
        
    }
    
    func keyboardWillHide(sender: NSNotification) {
        // TODO(geetikak): Do we need to do anything here ?
        scrollView.contentOffset.y = 0
        
    }
    
    @IBAction func descriptionButtonClicked(sender: AnyObject) {
        let eventDescriptionVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDescriptionViewController") as! EventDescriptionViewController
        
        self.navigationController?.pushViewController(eventDescriptionVC, animated: true)
    }
    
    @IBAction func locationButtonClicked(sender : AnyObject){
        let addLocationVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddLocationViewController") as! AddLocationViewController
        addLocationVC.imageData=imageData
        addLocationVC.eventTitle = evenNameTextBg.text
        self.navigationController?.pushViewController(addLocationVC, animated: true)
    }
    
    @IBAction func dateTimeButtonClicked(sender : AnyObject){
        /*let dateTimeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDateTimeViewController") as! EventDateTimeViewController
        dateTimeVC.imageData=imageData
        self.navigationController?.pushViewController(dateTimeVC, animated: true)*/
        
        if showPicker
        {
            self.scrollView.contentSize.height =  scrollView.frame.height + imageHeightDiff
            self.datePickerView.hidden = true
            
            inPersonDetailsView.frame.size.height = inPersonDetailsViewOriginalHeight
            eventPreviewButton.frame.origin.y = eventPreviewButtonOriginalY
            
            showPicker = false
        }
        else
        {
            //showDate.hidden = false
            //showDate.text = self.sDate as String
            showPicker = true
            self.scrollView.contentSize.height = scrollView.frame.height + ((230*self.view.frame.height)/568) + imageHeightDiff
            self.datePickerView.hidden = false
            
            inPersonDetailsView.frame.size.height = inPersonDetailsViewOriginalHeight + ((230*self.view.frame.height)/568)
            eventPreviewButton.frame.origin.y = eventPreviewButtonOriginalY + ((230*self.view.frame.height)/568)
            
            showPicker = true
            
            dateAndTime_label.text = getFormatedStringFromDate(datePickerView.date)
            dateAndTime_label.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
            dateAndTime_label.textAlignment = .Right
            
            dateChanged(datePickerView)
        }
        
        
        descriptionMenu.frame.origin.y = descriptionMenuY
        locationMenu.frame.origin.y = locationMenuY
        datePickerMenu.frame.origin.y = datePickerMenuY
        
        descriptionMenu.frame.size.height = descriptionMenuHeight
        locationMenu.frame.size.height = locationMenuHeight
        datePickerMenu.frame.size.height = datePickerMenuHeight
        eventPreviewButton.frame.size.height = eventPreviewButtonOriginalHeight
        
    }
    
    @IBAction func previewButtonClicked(sender : AnyObject){
        let eventPreviewVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPreviewViewController") as! EventPreviewViewController
        //eventPreviewVC.imageData=imageData
        eventPreviewVC.eventTitle = evenNameTextBg.text
        eventPreviewVC.eventObject = newEvent
        self.navigationController?.pushViewController(eventPreviewVC, animated: true)
    }
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        // TODO(geetikak): User abandoned event creation. Do we need a metric to track why ?
        
        if #available(iOS 8.0, *) {
            let refreshAlert = UIAlertController(title: "Discard changes", message:" Are you sure you want to discard these changes?", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
                
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Discard", style: .Default, handler: { (action: UIAlertAction) in
                //            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                //            self.navigationController?.pushViewController(homeVC, animated: false)
                
                let myEventsResultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "eventCreatorObjectId='\(self.currentUserId)'", whereFields: [])
                
                myEventsResultSet.next()
                
                let myEventsCount = Int(myEventsResultSet.intForColumn("count"))
                
                myEventsResultSet.close()
                
                let mySharedEventsResultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "objectId IN (SELECT eventObjectId FROM Invitations WHERE userObjectId='\(self.currentUserId)')", whereFields: [])
                
                mySharedEventsResultSet.next()
                
                let mySharedEventsCount = Int(mySharedEventsResultSet.intForColumn("count"))
                
                mySharedEventsResultSet.close()
                
                if (myEventsCount>0)
                {
                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
                else if(mySharedEventsCount>0)
                {
                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
                else
                {
                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
                
                //self.navigationController?.popViewControllerAnimated(false)
                
            }))
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
            
        }
        else
        {
            
        }
    }
    
    @IBAction func viewTapped(sender : AnyObject) {
        evenNameTextBg.resignFirstResponder()
    }
    
    @IBAction func createButtonClicked(sender : AnyObject)
    {
        myEventData = [PFObject]()
        
        let replaced = evenNameTextBg.text!.stringByReplacingOccurrencesOfString(" ", withString: "", options: [], range: nil)
        
        
        var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
        
        
        if(evenNameTextBg.text == "" || replaced == "")
        {
            if #available(iOS 8.0, *) {
                var refreshAlert = UIAlertController(title: "Error", message: "Please enter the name of the event.", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
                
                
            } else {
                // Fallback on earlier versions
            }
            
            
        }
        else
        {
            var haveError: Bool = false
            
            if show == false
            {
                
                newEvent["isRSVP"] = true
                var eventDesc = newEvent["eventDescription"] as! String
                var eventLat = newEvent["eventLatitude"] as! Double
                var eventLong = newEvent["eventLongitude"] as! Double
                
                var startDate: NSDate = newEvent["eventStartDateTime"] as! NSDate
                var endDate: NSDate = newEvent["eventEndDateTime"] as! NSDate
                
                var startDateString = String(Int64(startDate.timeIntervalSince1970*1000))
                var endDateString = String(Int64(endDate.timeIntervalSince1970*1000))
                
                print(startDateString)
                print(endDateString)
                
                
                var errorElements: Array<String>!
                
                errorElements = []
                
                if(eventDesc == "")
                {
                    errorElements.append("description")
                    haveError = true
                }
                
                /*if(startDateString>=endDateString)
                {
                errorElements.append("start date, end date")
                haveError = true
                }*/
                
                if(eventLat == 0 || eventLong == 0)
                {
                    errorElements.append("location")
                    haveError = true
                }
                
                if(haveError){
                    
                    var errorString = errorElements.joinWithSeparator(", ")
                    
                    errorString = "Please enter \(errorString)"
                    
                    print(errorString)
                    
                    
                    
                    if #available(iOS 8.0, *) {
                        
                        let refreshAlert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                            
                        }))
                        
                         self.presentViewController(refreshAlert, animated: true, completion: nil)
                        
                    } else {
                        // Fallback on earlier versions
                    }
                   
                }
                
            }
            else
            {
                newEvent["isRSVP"] = false
                newEvent["eventDescription"] = ""
                newEvent["eventLocation"] = ""
                newEvent["eventLatitude"] = 0
                newEvent["eventLongitude"] = 0
                newEvent["eventStartDateTime"] = NSDate()
                newEvent["eventEndDateTime"] = NSDate()
            }
            
            newEvent["eventTimezoneOffset"] = timezoneOffset
            
            
            if !haveError
            {
                var date = NSDate()
                let currentTimeStamp = String(Int64(date.timeIntervalSince1970*1000))
                
                loaderView.hidden=false
                
                originalEventLogoFile = "\(currentTimeStamp)_\(currentUserId)_originaleventlogo.png"
                
                eventLogoFile = "\(currentTimeStamp)_\(currentUserId)_eventlogo.png"
                
                self.originalEventLogoFileUrl = NSURL(fileURLWithPath: "\(documentDirectory)/\(self.originalEventLogoFile)")
                
                if imageData != nil
                {
                    let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(self.originalImageData!), 0.5)
                    
                    originaldata!.writeToURL(self.originalEventLogoFileUrl!, atomically: true)
                    
                    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                    eventLogoFileUrl = NSURL(fileURLWithPath: "\(documentDirectory)/\(eventLogoFile)")
                    
                    var cropdata = UIImageJPEGRepresentation(self.correctlyOrientedImage(imageData!), 0.5)
                    var result = cropdata!.writeToURL(eventLogoFileUrl!, atomically: true)
                    
                    var tblFields: Dictionary! = [String: String]()
                    
                    tblFields["objectId"] = ""
                    tblFields["eventTitle"] = self.evenNameTextBg.text
                    tblFields["eventImage"] = self.eventLogoFile
                    tblFields["originalEventImage"] = self.originalEventLogoFile
                    
                    var frameX = self.frameX
                    var frameY = self.frameY
                    
                    tblFields["frameX"] = "\(frameX)"
                    tblFields["frameY"] = "\(frameY)"
                    tblFields["eventCreatorObjectId"] = self.currentUserId
                    tblFields["senderName"] = self.fullUserName
                    
                    tblFields["eventFolder"] = "\(self.currentUserId)/eventProfileImages/"
                    
                    if(self.show == false)
                    {
                        tblFields["isRSVP"] = "1"
                        
                        var date = ""
                        if newEvent["eventStartDateTime"] != nil {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                            date = dateFormatter.stringFromDate((newEvent["eventStartDateTime"] as? NSDate)!)
                            print(date)
                            tblFields["eventStartDateTime"] = date
                        }
                        
                        if newEvent["eventEndDateTime"] != nil {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                            date = dateFormatter.stringFromDate((newEvent["eventEndDateTime"] as? NSDate)!)
                            print(date)
                            tblFields["eventEndDateTime"] = date
                            print(tblFields["eventEndDateTime"])
                        }
                        
                        tblFields["eventDescription"] = newEvent["eventDescription"] as? String
                        
                        var eventLatitude = newEvent["eventLatitude"] as! Double
                        var eventLongitude = newEvent["eventLongitude"] as! Double
                        
                        tblFields["eventLatitude"] = "\(eventLatitude)"
                        tblFields["eventLongitude"] = "\(eventLongitude)"
                        
                        tblFields["eventLocation"] = newEvent["eventLocation"] as? String
                        
                        tblFields["eventTimezoneOffset"] = "\(timezoneOffset)"
                        
                    }
                    else
                    {
                        tblFields["isRSVP"] = "0"
                    }
                    
                    var insertedId = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                    print(insertedId)
                    if insertedId>0
                    {
                        newEvent["eventId"] = insertedId
                        var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("uploadEvent:"), userInfo: insertedId, repeats: false)
                    }
                    else
                    {
                        self.loaderView.hidden = true
                        Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
                    }
                }
                else
                {
                    Util.invokeAlertMethod("", strBody: "Please select event profile image first.", delegate: nil)
                    loaderView.hidden = true
                }
                
            }
        }
    }
    
    
    func uploadEvent(timer: NSTimer)
    {
        let insertedId = timer.userInfo as! Int
        
        if MyReachability.isConnectedToNetwork()
        {
            let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            uploadRequest.bucket = "eventnodepublicpics"
            uploadRequest.key =  "\(currentUserId)/eventProfileImages/\(eventLogoFile)"
            uploadRequest.body = eventLogoFileUrl
            
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
            
            upload(uploadRequest, isOriginal: false, insertedId: insertedId)
        }
        else
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        
        if image.imageOrientation == UIImageOrientation.Up
        {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    func createFirstPost(eventObject:PFObject, insertedId: Int)
    {
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createFirstPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createFirstPostError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    func createEvent(eventObject:PFObject, insertedId: Int)
    {
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createEventSuccess:", successSelectorParameters: insertedId, errorSelector: "createEventError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    
    @IBAction func rsvpToggle(sender : AnyObject){
        if(showalert == true)
        {
            if(show == true)
            {
                self.rsvpChecked(rsvpButton)
                
            }
            else
            {
                let image = UIImage(named: "checkbox.png") as UIImage?
                rsvpButton.setImage(image, forState: .Normal)
                
                self.inPersonDetailsView.hidden = true
                show = true
                newEvent["isRSVP"] = false
                scrollView.contentSize.height = scrollView.frame.height
            }
        }
    }
    
    func rsvpChecked(sender : AnyObject) {
        let image = UIImage(named: "check-box.png") as UIImage?
        sender.setImage(image, forState: .Normal)
        self.inPersonDetailsView.hidden = false
        show = false
        newEvent["isRSVP"] = true
        scrollView.contentSize.height = inPersonDetailsView.frame.origin.y + inPersonDetailsView.frame.height
    }
    
    @IBAction func alertToggle(sender : AnyObject){
       
            let refreshAlert = UIAlertController(title: "In-Person Event", message: "This applies to events you expect your friends to attend in person.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Got it", style: .Default, handler: { (action: UIAlertAction) in}))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
    func createEventSuccess(timer:NSTimer)
    {
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        // Log New Event metric.
        let creatorId = eventObject["eventCreatorObjectId"] as! String
        let isRsvp = eventObject["isRSVP"] as! Bool
        AnalyticsModel.instance.logNewEventEvent(creatorId, isRsvp: isRsvp)
        
        let eventId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId!
        tblFields["isPosted"] = "1"
        
        var date = ""
        var createdAt = ""
        var updatedAt = ""
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
            createdAt = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            print(date)
            tblFields["updatedAt"] = date
            updatedAt = date
        }
        print(eventId)
        
        currentEvent = eventObject
        
        let data = [
            "eventObjectId": eventObject.objectId!,
            "emailId": "noemail",
            "eventCreatorId": "\(currentUserId!)"
        ]
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                print(url!)
                
                let social = url!
                
                print(social)
                
                currentEvent["socialSharingURL"] = social as! String
                print(currentEvent["socialSharingURL"])
                tblFields["socialSharingURL"] = url! as String
                let isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
                
                //currentEvent["socialSharingURL"] = url
                //print(currentEvent["socialSharingURL"])
                
                
                eventObject["socialSharingURL"] = url! as String
                
                eventObject.saveInBackground()
                
            }
            
        })
        
        
        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
        if isUpdated {
            
            let localNotification = UILocalNotification()
            
            localNotification.fireDate = NSDate()
            
            localNotification.alertBody = "Congrats you just created an event. Don’t forget to invite your friends & guests."
            
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            
            let email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
            
            let eventCreatorId = eventObject["eventCreatorObjectId"] as! String
            
            
            let data = [
                "alert" : "Congrats you just created an event. Don’t forget to invite your friends & guests.",
                "notifType" :  "eventCreate",
                "eventObjectId": eventObject.objectId!,
                "objectId": "\(eventId)",
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "isUpdated": "\(isUpdated)",
                "emailId": "\(email)",
                "eventCreatorId": "\(eventCreatorId)"
            ]
            
            var urlString = ""
            
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    print("url: "+url!)
                    
                    urlString = url!
                    self.sendEventCreationEmail(eventObject, urlString: urlString, email: email)
                }
                else
                {
                    self.sendEventCreationEmail(eventObject, urlString: "", email: email)
                }
                
            })
            
            
            print("Record Updated Successfully")
            print("event")
            
        } else {
            print("Record not Updated Successfully")
        }
        
        isUpdated = true
        print(eventObject.objectId)
        
        var tblFieldsPost: Dictionary! = [String: String]()
        tblFieldsPost["objectId"] = ""
        tblFieldsPost["postData"] = "\(self.originalEventLogoFile)"
        tblFieldsPost["isApproved"] = "0"
        tblFieldsPost["postHeight"] = "\(self.originalImageData!.size.height)"
        tblFieldsPost["postWidth"] = "\(self.originalImageData!.size.width)"
        tblFieldsPost["postType"] = "image"
        tblFieldsPost["eventObjectId"] = "\(currentEvent.objectId!)"
        tblFieldsPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
        
        let insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFieldsPost)
        if insertedId>0
        {
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            self.originalEventLogoFileUrl = NSURL(fileURLWithPath: "\(documentDirectory)/\(self.originalEventLogoFile)")
            let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            
            let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(self.originalImageData!), 0.5)
            originaldata!.writeToURL(self.originalEventLogoFileUrl!, atomically: true)
            uploadRequest.bucket = "eventnode1"
            uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(self.originalEventLogoFile)"
            uploadRequest.body = self.originalEventLogoFileUrl
            
            self.uploadFirstPost(uploadRequest, insertedId: insertedId)
        }
        else
        {
            self.loaderView.hidden = true
            Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
        }
    }
    
    func createEventError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    
    func sendEventCreationEmail(eventObject: PFObject, urlString: String, email: String)
    {
        
        let eventTitleText = eventObject["eventTitle"] as! String
        
        let eventFolder = eventObject["eventFolder"] as! String!
        let eventImage = eventObject["eventImage"] as! String!
        let isRsvp = eventObject["isRSVP"] as! Bool
        
        let eventCreate = EventCreationSuccess()
        
        var emailMessage = ""
        
        if isRsvp == true
        {
            let eventLatitude = eventObject["eventLatitude"] as! Double
            print(eventLatitude)
            
            let eventLongitude = eventObject["eventLongitude"] as! Double
            print(eventLongitude)
            
            emailMessage = eventCreate.emailMessage(eventObject.objectId!, eventTitle: eventTitleText, dateString: dateStringFromNSDate(eventObject["eventStartDateTime"] as! NSDate), timeString: timeStringFromNSDate(eventObject["eventStartDateTime"] as! NSDate), locationString: eventObject["eventLocation"]as! String, hostName: fullUserName, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: "rsvp",url:urlString)
        }
        else
        {
            
            emailMessage = eventCreate.emailMessage(eventObject.objectId!, eventTitle: eventTitleText, dateString:"", timeString:"", locationString:"", hostName: fullUserName, latitude: "0", longitude: "0", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: "online",url:urlString)
        }
        
        
        let sendEmail = SendEmail()
        
        sendEmail.sendEmail(" Success! You just created an event.", message: emailMessage, emails:[email])
        
    }
    
    
    func dateStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        let sweekday = scomponents.weekday
        
        let dateString = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        
        return dateString
    }
    
    
    func timeStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        let timeString = "\(shour):\(sminute) \(sam)"
        
        return timeString
    }
    
    func createFirstPostSuccess(timer:NSTimer)
    {
        self.loaderView.hidden=true
        isPostUpdated = true
        
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        let postId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            print(date)
            tblFields["updatedAt"] = date
        }
        
        print("postId: \(postId)")
        
        let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            print("Record Updated Successfully")
        } else {
            print("Record not Updated Successfully")
        }
        
        if #available(iOS 9.0, *) {
            let inviteFriendsVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFriendsFirstViewController") as! InviteFriendsFirstViewController
            inviteFriendsVC.isFromCreated = true
            
            self.navigationController?.pushViewController(inviteFriendsVC, animated: true)
            
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    func createFirstPostError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        isUpdated = false
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int){
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    func internetErrorForFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int){
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    func uploadFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            print("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                        print("upload() failed: [\(error)]")
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                    print("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                print("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    myNewPost = PFObject(className:"EventImages")
                    myNewPost["postData"] = self.originalEventLogoFile
                    myNewPost["postHeight"] = self.originalImageData!.size.height
                    myNewPost["postWidth"] = self.originalImageData!.size.width
                    myNewPost["postType"] = "image"
                    myNewPost["eventObjectId"] = currentEvent.objectId!
                    myNewPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
                    
                    self.createFirstPost(myNewPost, insertedId: insertedId)
                    
                })
            }
            return nil
        }
    }
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int)
    {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                            })
                            break;
                        default:
                            self.loaderView.hidden=true
                            self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                            print("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                        print("upload() failed: [\(error)]")
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                    print("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                print("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    
                    if(isOriginal == true){
                        
                        print("original image uploaded. creating event now....")
                        let eventDetails = PFObject(className:"Events")
                        eventDetails["eventTitle"] = self.evenNameTextBg.text
                        NSUserDefaults.standardUserDefaults().setValue(self.evenNameTextBg.text, forKey: "EventTitle")
                        eventDetails["eventImage"] = self.eventLogoFile
                        eventDetails["originalEventImage"] = self.originalEventLogoFile
                        eventDetails["frameX"] = self.frameX
                        eventDetails["frameY"] = self.frameY
                        eventDetails["eventCreatorObjectId"] = self.currentUserId
                        eventDetails["senderName"] = self.fullUserName!
                        
                        eventDetails["eventFolder"] = "\(self.currentUserId)/eventProfileImages/"
                        
                        if self.show == true
                        {
                            eventDetails["isRSVP"] = false
                            eventDetails["eventDescription"] = ""
                            eventDetails["eventLatitude"] = 0
                            eventDetails["eventLongitude"] = 0
                        }
                        else
                        {
                            eventDetails["isRSVP"] = true
                            eventDetails["eventDescription"] = newEvent["eventDescription"]
                            eventDetails["eventLatitude"] = newEvent["eventLatitude"]
                            eventDetails["eventLongitude"] = newEvent["eventLongitude"]
                            eventDetails["eventStartDateTime"] = newEvent["eventStartDateTime"]
                            eventDetails["eventEndDateTime"] = newEvent["eventEndDateTime"]
                            eventDetails["eventLocation"] = newEvent["eventLocation"]
                        }
                        
                        eventDetails["eventId"] = newEvent["eventId"]
                        
                        eventDetails["isUpdated"] = false
                        
                        self.createEvent(eventDetails, insertedId: insertedId)
                    }
                    else
                    {
                        print("cropped image uploaded. uploading original image now....")
                        
                        
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        
                        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        
                        uploadRequest.bucket = "eventnodepublicpics"
                        uploadRequest.key =  "\(self.currentUserId)/eventProfileImages/\(self.originalEventLogoFile)"
                        uploadRequest.body = self.originalEventLogoFileUrl
                        
                        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                        
                        print(self.eventLogoFileUrl.relativePath)
                        print(self.eventLogoFileUrl.relativeString)
                        print(self.eventLogoFileUrl.path)
                        
                        self.upload(uploadRequest, isOriginal: true, insertedId: insertedId)
                    }
                    
                })
            }
            return nil
        }
    }
}