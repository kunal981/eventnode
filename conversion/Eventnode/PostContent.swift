//
//  Reachability.swift
//  eventnode
//
//  Created by mrinal khullar on 7/7/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation
import SystemConfiguration

public class PostContent: NSObject {
    
    var unPostedEvents = [PFObject]()
    var unPostedPosts = [PFObject]()
    
    var currentUserId: String!
    
    func postUnpublishedContent() {
        uploadUnpublishedEvents()
        uploadUnpublishedPosts()
    }
    
    
    func uploadUnpublishedEvents()
    {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
        }
        
        
        let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: " isPosted=0", whereFields: [])
        
        resultSetCount.next()
        
        let eventCount = resultSetCount.intForColumn("count")
        
        resultSetCount.close()
        
        
        if(eventCount>0)
        {
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: " isPosted=0 ORDER BY eventId DESC", whereFields: [])
            
            unPostedEvents = []
            
            if (resultSet != nil) {
                while resultSet.next() {
                    
                    let userevent = PFObject(className: "Events")
                    
                    userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                    userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                    userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                    let isRSVP = resultSet.stringForColumn("isRSVP")
                    
                    if isRSVP == "0"
                    {
                        userevent["isRSVP"] = false
                    }
                    else
                    {
                        userevent["isRSVP"] = true
                        
                        userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                        userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                        userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                        userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
                        userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                        
                        userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                    }
                    
                    userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                    userevent["senderName"] = resultSet.stringForColumn("senderName")
                    
                    userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                    userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                    userevent["frameX"] = resultSet.doubleForColumn("frameX")
                    userevent["frameY"] = resultSet.doubleForColumn("frameY")
                    
                    
                    
                    userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                    userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                    
                    if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil)
                    {
                        userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                        userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                        userevent.objectId = resultSet.stringForColumn("objectId")
                    }
                    
                    let isPosted = resultSet.stringForColumn("isPosted")
                    
                    if isPosted == "0"
                    {
                        userevent["isPosted"] = false
                    }
                    else
                    {
                        userevent["isPosted"] = true
                    }
                    
                    userevent["isUploading"] = false
                    
                    unPostedEvents.append(userevent)
                    
                    uploadEvent(userevent)
                    
                    print(userevent["isPosted"]!)
                }
            }
            
            resultSet.close()

        }


        
    }
    
    
    func uploadUnpublishedPosts()
    {

        let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "isPosted=0", whereFields: [])
        
        resultSetCount.next()
        
        let postCount = resultSetCount.intForColumn("count")
        
        resultSetCount.close()
        
        if(postCount>0)
        {
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "isPosted=0 ORDER BY eventImageId DESC", whereFields: [])
            
            unPostedPosts = []
            
            print("Successfully retrieved \(postCount) posts.")
            
            var i = 0
            
            if (resultSet != nil) {
                while resultSet.next() {
                    
                    let userpost = PFObject(className: "EventImages")
                    
                    userpost["eventImageId"] = Int(resultSet.intForColumn("eventImageId"))
                    userpost["postData"] = resultSet.stringForColumn("postData")
                    userpost["eventFolder"] = resultSet.stringForColumn("eventFolder")
                    
                    
                    userpost["postType"] = resultSet.stringForColumn("postType")
                    userpost["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
                    userpost["postHeight"] = resultSet.doubleForColumn("postHeight")
                    userpost["postWidth"] = resultSet.doubleForColumn("postWidth")
                    
                    
                    
                    userpost["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                    userpost["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                    
                    
                    if(resultSet.stringForColumn("createdAt") != "" && resultSet.stringForColumn("updatedAt") != "" && resultSet.stringForColumn("createdAt") != nil && resultSet.stringForColumn("updatedAt") != nil)
                    {
                        userpost.objectId = resultSet.stringForColumn("objectId")
                    }
                    
                    let isPosted = resultSet.stringForColumn("isPosted")
                    
                    
                    if isPosted == "0"
                    {
                        userpost["isPosted"] = false
                    }
                    else
                    {
                        userpost["isPosted"] = true
                    }
                    
                    
                    let isApproved = resultSet.stringForColumn("isApproved")
                    
                    if(isApproved != nil)
                    {
                        if isApproved == "0"
                        {
                            userpost["isApproved"] = false
                        }
                        else
                        {
                            userpost["isApproved"] = true
                        }
                    }
                    else
                    {
                        userpost["isApproved"] = false
                    }
                    
                    unPostedPosts.append(userpost)
                    
                    uploadPost(userpost)
                    
                    i++
                }
            }
            
            resultSet.close()
        }
    }
    
    
    func uploadEvent(event: PFObject)
    {
        
        if event.objectId != nil
        {
            event["isNew"] = false
        }
        else
        {
            event["isNew"] = true
        }
        
        let eventLogoFile = event["eventImage"] as! String
        
        let eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory).URLByAppendingPathComponent(eventLogoFile)
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        uploadRequest.bucket = "eventnodepublicpics"
        uploadRequest.key =  "\(currentUserId)/eventProfileImages/\(eventLogoFile)"
        uploadRequest.body = eventLogoFileUrl
        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
        upload(uploadRequest, isOriginal: false, insertedId: event["eventId"] as! Int, eventToBeUploaded: event)
        
    }
    
    func uploadPost(post: PFObject)
    {
        if post["postType"] as! String == "text"
        {
            ParseOperations.instance.saveData(post, target: self, successSelector: "createPostSuccess:", successSelectorParameters: post["eventImageId"] as! Int, errorSelector: "createPostError:", errorSelectorParameters:post)
        }
        else
        {
            let imageName = post["postData"] as! String
            
            let eventObjectId = post["eventObjectId"] as! String
            
            let eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory).URLByAppendingPathComponent(imageName)
            
            let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            
            uploadRequest.bucket = "eventnode1"
            uploadRequest.key =  "\(self.currentUserId)/\(eventObjectId)/\(imageName)"
            uploadRequest.body = eventLogoFileUrl
            uploadPostFile(uploadRequest, insertedId: post["eventImageId"] as! Int, postToBeUploaded: post)
        }
    }
    
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    
    
    
    func uploadFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int, postToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            })
                            break;
                            
                        default:
                            print("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        print("upload() failed: [\(error)]")
                    }
                } else {
                    print("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {

                print("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.createFirstPost(postToBeUploaded, insertedId: insertedId)
                    
                })
            }
            return nil
        }
    }
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int, eventToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            })
                            break;
                            
                        default:
                            print("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        print("upload() failed: [\(error)]")
                    }
                } else {


                    print("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    
                    if(isOriginal == true){
                        
                        self.createEvent(eventToBeUploaded, insertedId: insertedId)
                        
                    }
                    else
                    {
                        print("cropped image uploaded. uploading original image now....")
                        
                        let originalEventFile = eventToBeUploaded["originalEventImage"] as! String
                        
                        let originalEventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory).URLByAppendingPathComponent(originalEventFile)
                        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        
                        
                        uploadRequest.bucket = "eventnodepublicpics"
                        uploadRequest.key =  "\(self.currentUserId)/eventProfileImages/\(originalEventFile)"
                        uploadRequest.body = originalEventLogoFileUrl
                        
                        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                        
                        self.upload(uploadRequest, isOriginal: true, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                    }
                    
                })
            }
            return nil
        }
    }
    
    func uploadPostFile(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int, postToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in

                            })
                            break;
                            
                        default:
                            print("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        print("upload() failed: [\(error)]")
                    }
                } else {
                    print("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                print("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    print("file uploaded. creating post now....")
                    
                    ParseOperations.instance.saveData(postToBeUploaded, target: self, successSelector: "createPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createPostError:", errorSelectorParameters:postToBeUploaded)
                    
                })
            }
            return nil
        }
    }

    
    
    func createFirstPost(eventObject:PFObject, insertedId: Int){
        
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createFirstPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createFirstPostError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    
    
    func createEvent(eventObject:PFObject, insertedId: Int)
    {
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createEventSuccess:", successSelectorParameters: insertedId, errorSelector: "createEventError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    func createFirstPostSuccess(timer:NSTimer)
    {
        isPostUpdated = true
        
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        let postId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            print(date)
            tblFields["updatedAt"] = date
        }
        
        print("postId: \(postId)")
        
            let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            
            isPostDataUpDated = true
            
            print("Record Updated Successfully")
            print("eventImage")
        } else {
            print("Record not Updated Successfully")
        }
        
    }
    
    
    func createFirstPostError(timer:NSTimer)
    {
        
    }
    
    
    func createEventSuccess(timer:NSTimer)
    {
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var eventId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            print(date)
            tblFields["updatedAt"] = date
        }
        print(eventId)
        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
        if isUpdated {
            print("Record Updated Successfully")
            print("event")
            
            isEventDataUpDated = true
            
            if eventObject["isNew"] as! Bool == true
            {
                
                var tblFieldsPost: Dictionary! = [String: String]()
                
                var originalFileName = eventObject["originalEventImage"] as? String
                
                var originalImageData = UIImage(named: (documentDirectory as NSString).stringByAppendingPathComponent(originalFileName!))
                print(originalImageData?.size.height)
                tblFieldsPost["postData"] = originalFileName
                tblFieldsPost["isApproved"] = "0"
                tblFieldsPost["postHeight"] = "\(originalImageData!.size.height)"
                tblFieldsPost["postWidth"] = "\(originalImageData!.size.width)"
                tblFieldsPost["postType"] = "image"
                tblFieldsPost["eventObjectId"] = "\(eventObject.objectId!)"
                tblFieldsPost["eventFolder"] = "\(self.currentUserId)/\(eventObject.objectId!)/"
                
                var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFieldsPost)
                if insertedId>0
                {
                    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                    var originalEventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory).URLByAppendingPathComponent(originalFileName!)
                    let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                    
                    let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(originalImageData!), 0.5)

                    originaldata!.writeToURL(originalEventLogoFileUrl, atomically: true)
                    uploadRequest.bucket = "eventnode1"
                    uploadRequest.key =  "\(self.currentUserId)/\(eventObject.objectId)/\(originalFileName)"
                    uploadRequest.body = originalEventLogoFileUrl
                    
                    var myFirstPost = PFObject(className:"EventImages")
                    myFirstPost["postData"] = originalFileName
                    myFirstPost["postHeight"] = originalImageData!.size.height
                    myFirstPost["postWidth"] = originalImageData!.size.height
                    myFirstPost["postType"] = "image"
                    myFirstPost["eventObjectId"] = eventObject.objectId!
                    myFirstPost["eventFolder"] = "\(self.currentUserId)/\(eventObject.objectId!)/"
                    
                    
                    self.uploadFirstPost(uploadRequest, insertedId: insertedId, postToBeUploaded: myFirstPost)
                }
                else
                {
                    print("Post not created Successfully.")
                }
            }
            
        } else {
            print("Record not Updated Successfully")
        }
        
        isUpdated = true

        print(eventObject.objectId)
        
        currentEvent = eventObject;
    }
    
    func createEventError(timer:NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        let errorObject: NSArray = timer.userInfo?.valueForKey("external") as! NSArray
        var eventObject: PFObject = errorObject[0] as! PFObject
        
        var insertedId: Int = errorObject[1] as! Int
        
        print("error occured \(error.description)")
        
    }
    
    
    func createPostSuccess(timer:NSTimer)
    {
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        let postId = timer.userInfo?.valueForKey("external") as! Int!
        
        isPostUpdated = true
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            print(date)
            tblFields["updatedAt"] = date
        }
        
        
        let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            
            isPostDataUpDated = true
            
            print("Record Updated Successfully")
            print("eventImage")
        } else {
            print("Record not Updated Successfully")
        }
        
    }
    
    func createPostError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        var eventObject: PFObject = timer.userInfo?.valueForKey("external") as! PFObject
        
        print("error")
    }

    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
}
