//
//  AddCommentViewController.swift
//  Eventnode
//
//  Created by brst on 9/1/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class AddCommentViewController: UIViewController {
    
    var currentUserId = ""
    
    var messageIds = [String]()
    
    var chatsArray = [PFObject]();
    
    var senderName = ["fhn","aman","dsd","zfgdcfd","zfddzx","abhi"]
    
    //var senderId = ["vbnbj","cWKoulXs4G","h5RUuNIIJi","cWKoulXs4G","jhjmn","cWKoulXs4G"]
    
    var eventObject: PFObject!
    
    var rowHeights = [CGFloat]()
    
    var senderMessage = ["Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt."]
    
    var profileImage = ["girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg"]
    
    var objectId = ["1","1","2","3","4","5"]
    
    
    @IBOutlet weak var wrapperViewChat: UIView!
    @IBOutlet weak var addCommentTableView: UITableView!
    
    @IBOutlet weak var chatHeadBottom: UIView!
    @IBOutlet weak var enterTextMessage: UITextView!
    
    
    var textMessage:String = String()
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("add comments view")
        
        self.view.addSubview(wakeUpImageView)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        
        addCommentTableView.separatorColor = UIColor.clearColor()
        
        
        //com.eventnode.iospush
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
            print("current User Id = \(currentUserId)")
        }
        
        
        //        for (var i = 0; i < senderName.count; i++)
        //        {
        //            var chat = PFObject(className: "EventComments")
        //
        //            chat["senderName"] = senderName[i]
        //
        //            chat["senderId"] = senderId[i]
        //
        //            chat["senderMessage"] = senderMessage[i]
        //
        //            chat["profileImage"] = profileImage[i]
        //
        //            chatsArray.append(chat)
        //        }
        
        //addCommentTableView.reloadData()
        
        
        refreshList()
        
        print(messageIds.count)
        
        var messageIdsString = ""
        if messageIds.count > 0
        {
            messageIdsString = messageIds.joinWithSeparator("','")
            
            print("message ids string = \(messageIdsString)")
        }
        else
        {
            enterTextMessage.becomeFirstResponder()
        }
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(messageIdsString)'}) AND eventObjectId = '\(eventObject.objectId!)'")
        
        let query = PFQuery(className:"EventComments", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagesError:", errorSelectorParameters:nil)
        
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("refreshList"), userInfo: nil, repeats: true)
        
    }
    
    
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func fetchAllMessagesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = message.objectId!
                tblFields["messageText"] = message["messageText"] as? String
                tblFields["senderObjectId"] = message["senderObjectId"] as? String
                tblFields["eventObjectId"] = message["eventObjectId"] as? String
                tblFields["senderName"] = message["senderName"] as? String
                //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                
                var date = ""
                
                if message.createdAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if message.updatedAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.updatedAt)!)
                    print(date)
                    tblFields["updatetAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                
                var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                
                fetchedEventObjectIds.append(message["eventObjectId"] as! String)
                
            }
            
            refreshList()
        }
    }
    
    
    
    func fetchAllMessagesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    //MARK: - sendMessage()
    @IBAction func sendMessage(sender: AnyObject)
    {
        let messageEntered:NSString = self.enterTextMessage.text
        
        if (messageEntered.length > 0)
        {
            textMessage = self.enterTextMessage.text!
            
            print(textMessage)
            
            self.enterTextMessage.text = ""
            
            let chat = PFObject(className: "EventComments")
            
            
            chat["senderObjectId"] = currentUserId
            
            chat["eventObjectId"] = eventObject.objectId!
            
            chat["messageText"] = "\(textMessage)"
            
            chat["profileImage"] = "girl.jpeg"
            
            let senderName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            
            chat["senderName"] = senderName
            
            chat["timeString"] = "just now"
            
            let senderMessageTemp = UITextView()
            
            senderMessageTemp.frame.size.width = self.view.frame.width*(215.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(45.0/568)
            
            senderMessageTemp.text = chat["messageText"] as! String
            
            senderMessageTemp.font = UIFont(name: "AvenirNext-Medium", size: 12)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            
            var rowHeight: CGFloat = 0
            
            if contentSize.height > self.view.frame.height*(25.0/568)
            {
                rowHeight = contentSize.height - (self.view.frame.height*(25.0/568))
            }
            
            print(rowHeight)
            
            rowHeights.append(rowHeight)
            
            chatsArray.append(chat)
            
            addCommentTableView.reloadData()
            adjustTableHeight()
            adjustTableY()
            
            ParseOperations.instance.saveData(chat, target: self, successSelector: "sendMessageSuccess:", successSelectorParameters: nil, errorSelector: "sendMessageError:", errorSelectorParameters:nil)
            
        }
        
    }
    
    
    func sendMessageSuccess(timer: NSTimer)
    {
        let message = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = message.objectId!
        tblFields["messageText"] = message["messageText"] as? String
        tblFields["senderObjectId"] = message["senderObjectId"] as? String
        tblFields["eventObjectId"] = message["eventObjectId"] as? String
        tblFields["senderName"] = message["senderName"] as? String
        //tblFields["eventCommentId"] = message["eventCommentId"] as? String
        
        var date = ""
        
        if message.createdAt != nil
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((message.createdAt)!)
            print(date)
            tblFields["createdAt"] = date
        }
        
        if message.updatedAt != nil
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((message.updatedAt)!)
            print(date)
            tblFields["updatetAt"] = date
        }
        
        tblFields["isPosted"] = "1"
        
        var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
        
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(eventObject.objectId!)'")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: message, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        
        refreshList()
        
        
        //respondtableView.reloadData()
        //repondTableView2.reloadData()
    }
    
    func sendMessageError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    
    
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let payloadData = timer.userInfo?.valueForKey("external") as! PFObject
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            ///Users/brst981/Desktop/my projects/Eventnode Git/Eventnode/AddCommentsViewController.swift:317:34: Cannot invoke 'fetchData' with an argument list of type '(PFQuery, target: AddCommentsViewController, successSelector: String, successSelectorParameters: [String : String?], errorSelector: String, errorSelectorParameters: nil)'
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            
            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                }
            }
            
            let eventCreatorObjectId = eventObject["eventCreatorObjectId"] as! String
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //eventCreatorObjectId
            
            var createdAt = ""
            
            if payloadData.createdAt != nil
            {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                createdAt = dateFormatter.stringFromDate((payloadData.createdAt)!)
            }
            
            var updatedAt = ""
            if payloadData.updatedAt != nil
            {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                updatedAt = dateFormatter.stringFromDate((payloadData.updatedAt)!)
            }
            
            
            let objectId = payloadData.objectId!
            let messageText = payloadData["messageText"] as! String
            let senderObjectId = payloadData["senderObjectId"] as! String
            let eventObjectId = payloadData["eventObjectId"] as! String
            let senderName = payloadData["senderName"] as! String
            
            let eventTitle = eventObject["eventTitle"] as! String
            
            
            /*,
            "objectId" : objectId,
            "messageText" : messageText,
            "senderObjectId" : senderObjectId,
            "eventObjectId" : eventObjectId,
            "senderName" : senderName,
            "createdAt" : createdAt,
            "updatedAt" :  updatedAt,
            "notifType" :  "groupchat"*/
            
            let data = [
                "alert" : "\(senderName) sent you a new message in \(eventTitle)",
                "objectId" : "\(objectId)",
                "messageText" : "\(messageText)",
                "senderObjectId" : "\(senderObjectId)",
                "eventObjectId" : "\(eventObjectId)",
                "senderName" : "\(senderName)",
                "createdAt" : "\(createdAt)",
                "updatedAt" :  "\(updatedAt)",
                "notifType" :  "groupchat"
                
            ]
            
            
            let fetchedUserObjectIdsString = fetchedUserObjectIds.joinWithSeparator("','")
            
            let predicate = NSPredicate(format: "userObjectId IN {'\(fetchedUserObjectIdsString)'} AND userObjectId != '\(currentUserId)'")
            
            
            let query = PFInstallation.queryWithPredicate(predicate)
            
            let push = PFPush()
            push.setQuery(query)
            push.setData(data)
            push.sendPushInBackground()
            
            
        }
    }
    
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    
    func adjustTableHeight()
    {
        var totalHeight:CGFloat = 0
        for var i = 0; i < rowHeights.count; i++
        {
            totalHeight += (rowHeights[i] + (70*self.view.frame.height/568))
        }
        
        
        print("totalHeight: \(totalHeight)")
        
        //if totalHeight < ((404/568)*self.view.frame.height)
        if totalHeight < (wrapperViewChat.frame.origin.y - (chatHeadBottom.frame.origin.y + chatHeadBottom.frame.height))
        {
            addCommentTableView.frame.size.height = totalHeight
        }
        else
        {
            addCommentTableView.frame.size.height = (wrapperViewChat.frame.origin.y - (chatHeadBottom.frame.origin.y + chatHeadBottom.frame.height))
        }
        
        if totalHeight > addCommentTableView.frame.height
        {
            addCommentTableView.contentOffset.y = addCommentTableView.contentSize.height - addCommentTableView.frame.height
        }
        
    }
    
    func adjustTableY()
    {
        
        addCommentTableView.frame.origin.y = wrapperViewChat.frame.origin.y - addCommentTableView.frame.height
        
        //addCommentTableView.frame.origin.y = ((429/568)*self.view.frame.height) - totalHeight + ((74/568)*self.view.frame.height)
        //addCommentTableView.frame.origin.y = ((74/568)*self.view.frame.height)
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: - UITableViewDelegates and DataSource() Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return chatsArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return (100*self.view.frame.height/568) + rowHeights[indexPath.row]
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! AddCommentTableViewCell
        
        addCommentTableView.separatorColor = UIColor.clearColor()
        
        
        for view in cell.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        
        let wrapperView:UIView = UIView()
        
        let senderName_lbl:UILabel = UILabel()
        
        let senderImage:UIImageView = UIImageView()
        
        let senderMessage: UITextView = UITextView()
        
        let wrapperMessage:UIView = UIView()
        
        let messageTime:UILabel = UILabel()
        
        
        cell.contentView.addSubview(wrapperView)
        
        wrapperView.addSubview(wrapperMessage)
        
        wrapperView.addSubview(senderImage)
        
        wrapperMessage.addSubview(senderMessage)
        
        wrapperMessage.addSubview(senderName_lbl)
        
        wrapperMessage.addSubview(messageTime)
        
        
        print("current User Id1 = \(currentUserId)")
        
        if currentUserId == chatsArray[indexPath.row]["senderObjectId"] as! String
        {
            senderMessage.editable = false
            //cell.senderMessage.selectable = false
            senderMessage.scrollEnabled = false
            
            print("current User Id = \(currentUserId)")
            
            wrapperView.frame = CGRectMake(0, 0, cell.contentView.frame.width, (70*self.view.frame.height/568) + rowHeights[indexPath.row])
            
            wrapperMessage.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            senderMessage.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            senderName_lbl.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            
            //            wrapperMessage.backgroundColor = UIColor.blackColor()
            //
            //            senderMessage.backgroundColor = UIColor.blueColor()
            //
            //            senderName_lbl.backgroundColor = UIColor.redColor()
            
            
            wrapperMessage.frame = CGRectMake(self.view.frame.width*(33/320), self.view.frame.height*(0.0/568), self.view.frame.width*(235.0/320), self.view.frame.height*(senderMessage.frame.height/568))
            
            
            senderName_lbl.frame = CGRectMake(self.view.frame.height*(10.0/568), self.view.frame.height*(10.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
            
            senderName_lbl.textAlignment = NSTextAlignment.Right
            
            
            senderName_lbl.font = UIFont(name: "AvenirNext-DemiBold", size: 12)
            
            senderName_lbl.text = chatsArray[indexPath.row]["senderName"] as? String
            
            
            senderMessage.frame = CGRectMake( self.view.frame.height*(10.0/568), self.view.frame.height*(27.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(45.0/568))
            
            senderMessage.textAlignment = NSTextAlignment.Right
            senderMessage.font = UIFont(name: "AvenirNext-Regular", size: 12)
            
            senderMessage.text = chatsArray[indexPath.row]["messageText"] as! String
            
            messageTime.text = chatsArray[indexPath.row]["timeString"] as? String
            
            messageTime.font = UIFont(name: "AvenirNext-DemiBold", size: 11)
            
            
            messageTime.textAlignment = NSTextAlignment.Right
            
            senderImage.frame = CGRectMake(self.view.frame.width*(268.0/320), self.view.frame.height*(0.0/568), self.view.frame.width*(38.0/320), self.view.frame.height*(38.0/568))
            
            senderImage.image = UIImage(named: "boy.jpeg")
            
        }
        else
        {
            
            wrapperView.frame = CGRectMake(0, 0, cell.contentView.frame.width, (70*self.view.frame.height/568) + rowHeights[indexPath.row])
            
            senderImage.frame = CGRectMake(self.view.frame.width*(8/320), self.view.frame.height*(5.0/568), self.view.frame.width*(38.0/320), self.view.frame.height*(38.0/568))
            
            senderImage.image = UIImage(named: "girl.jpeg")
            
            wrapperMessage.frame = CGRectMake(self.view.frame.width*(57/320), self.view.frame.height*(0.0/568), self.view.frame.width*(235.0/320), self.view.frame.height*(senderMessage.frame.height/568))
            
            wrapperMessage.backgroundColor = UIColor(red: 206.0/255, green: 206.0/255, blue: 206.0/255, alpha: 1.0)
            
            
            
            senderName_lbl.backgroundColor = UIColor(red: 206.0/255, green: 206.0/255, blue: 206.0/255, alpha: 1.0)
            
            senderName_lbl.frame = CGRectMake( self.view.frame.height*(10.0/568), self.view.frame.height*(10.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
            
            senderMessage.textAlignment = NSTextAlignment.Left
            
            senderName_lbl.font = UIFont(name: "AvenirNext-DemiBold", size: 12)
            
            senderName_lbl.text = chatsArray[indexPath.row]["senderName"] as? String
            
            senderMessage.frame = CGRectMake( self.view.frame.height*(10.0/568), self.view.frame.height*(27.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(45.0/568))
            
            senderMessage.font = UIFont(name: "AvenirNext-Regular", size: 12)
            
            senderMessage.text = chatsArray[indexPath.row]["messageText"] as! String
            
            print(chatsArray[indexPath.row]["messageText"] as! String)
            
            
            messageTime.text = chatsArray[indexPath.row]["timeString"] as? String
            
            messageTime.font = UIFont(name: "AvenirNext-Medium", size: 10)
            messageTime.textAlignment = NSTextAlignment.Left
            
            senderMessage.backgroundColor = UIColor(red: 206.0/255, green: 206.0/255, blue: 206.0/255, alpha: 1.0)
            
            
            senderMessage.editable = false
            //cell.senderMessage.selectable = false
            senderMessage.scrollEnabled = false
            
        }
        
        
        let contentSize = senderMessage.sizeThatFits(senderMessage.bounds.size)
        var frame = senderMessage.frame
        frame.size.height = contentSize.height
        senderMessage.frame = frame
        
        wrapperMessage.frame.size.height = frame.height + self.view.frame.height*(61.0/568)
        wrapperMessage.layer.cornerRadius = 5
        wrapperMessage.layer.masksToBounds = true
        
        messageTime.frame = CGRectMake(self.view.frame.height*(10.0/568), self.view.frame.height*(30.0/568) + contentSize.height, self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
        
        print("SenderMessage Height = \(senderMessage.frame)")
        
        return cell
        
    }
    
    
    func refreshList()
    {
        
        chatsArray = []
        
        //messageIds = []
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["*"], whereString: "eventObjectId = '\(eventObject.objectId!)' ORDER BY eventCommentId ASC", whereFields: [])
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                let chat = PFObject(className: "EventComments")
                
                print(resultSet.stringForColumn("messageText"))
                
                chat["senderObjectId"] = resultSet.stringForColumn("senderObjectId")
                
                chat.objectId = resultSet.stringForColumn("objectId")
                
                print(resultSet.stringForColumn("objectId"))
                
                messageIds.append(resultSet.stringForColumn("objectId"))
                
                let textMessage = resultSet.stringForColumn("messageText")
                
                chat["messageText"] = "\(textMessage)"
                
                chat["profileImage"] = "girl.jpeg"
                
                // TODO(Dimpal): Fix this. App is crashing here. senderName is nil.
                print(resultSet.stringForColumn("senderName"))
                
                chat["senderName"] = resultSet.stringForColumn("senderName")
                
                let date = NSDate()
                print("date is = \(date)")
                
                
                let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
                print("current Time Stamp = \(currentTimeStamp)")
                
                
                let dateCreated = stringToDate(resultSet.stringForColumn("dateCreated"))
                print("Date Created = \(dateCreated)")
                
                
                let createdTimeStamp = Int64(dateCreated.timeIntervalSince1970*1000)
                print("Created Time Stamp = \(createdTimeStamp)")
                
                let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
                
                let timeDiff = Int64(currentTimeStamp - createdTimeStamp) - timezoneOffset
                print(timeDiff)
                
                
                let nYears = timeDiff / (1000*60*60*24*365)
                
                print(nYears)
                
                let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
                
                print(nMonths)
                
                let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
                print(nDays)
                
                let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
                print(nHours)
                
                
                let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
                print(nMinutes)
                
                var timeMsg = ""
                
                if nYears > 0
                {
                    timeMsg = "about \(nYears) years ago"
                }
                else if nMonths > 0
                {
                    timeMsg = "about \(nMonths) months ago"
                }
                else if nDays > 0
                {
                    timeMsg = "about \(nDays) days ago"
                }
                else if nHours > 0
                {
                    timeMsg = "about \(nHours) hours ago"
                }
                else if nMinutes > 0
                {
                    timeMsg = "about \(nMinutes) minutes ago"
                }
                else
                {
                    timeMsg = "just now"
                }
                
                
                chat["timeString"] = timeMsg
                
                
                let senderMessageTemp = UITextView()
                
                senderMessageTemp.frame.size.width = self.view.frame.width*(215.0/320)
                senderMessageTemp.frame.size.height = self.view.frame.height*(45.0/568)
                
                senderMessageTemp.text = chat["messageText"] as! String
                
                senderMessageTemp.font = UIFont(name: "AvenirNext-Medium", size: 12)
                
                let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
                var frame = senderMessageTemp.frame
                frame.size.height = contentSize.height
                senderMessageTemp.frame = frame
                
                var rowHeight: CGFloat = 0
                
                if contentSize.height > self.view.frame.height*(25.0/568)
                {
                    rowHeight = contentSize.height - (self.view.frame.height*(25.0/568))
                }
                
                print(rowHeight)
                
                rowHeights.append(rowHeight)
                
                chatsArray.append(chat)
            }
            
            addCommentTableView.reloadData()
            adjustTableHeight()
            adjustTableY()
        }
        resultSet.close()
        
    }
    
    
    
    //MARK: - stringToDate()
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        print("date formatter = \(dateFormatter)")
        
        let date = dateFormatter.dateFromString(dateString)
        
        print("date  = \(date)")
        
        return date!
    }
    
    
    
    //MARK: -  viewTapped()
    @IBAction func viewTapped(sender: AnyObject)
    {
        enterTextMessage.resignFirstResponder()
    }
    
    
    
    //MARK: -  keyboardWillShow()
    func keyboardWillShow(sender: NSNotification)
    {
        //        wrapperViewChat.frame.origin.y -= (253.0/568)*self.view.frame.height
        //        addCommentTableView.frame.origin.y -= (253.0/568)*self.view.frame.height
        
        wrapperViewChat.frame.origin.y -= 253.0
        addCommentTableView.frame.origin.y -= 253.0
        adjustTableHeight()
        adjustTableY()
    }
    
    
    
    //MARK: -  keyboardWillHide()
    func keyboardWillHide(sender: NSNotification)
    {
        
        //        wrapperViewChat.frame.origin.y += (253.0/568)*self.view.frame.height
        //        addCommentTableView.frame.origin.y += (253.0/568)*self.view.frame.height
        
        wrapperViewChat.frame.origin.y += 253.0
        addCommentTableView.frame.origin.y += 253.0
        
        adjustTableHeight()
        adjustTableY()
    }
    
    
    
    //MARK: -  closeEventDetailsButtonClicked()
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventPhotosVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
        self.navigationController?.pushViewController(eventPhotosVC, animated: false)*/
        self.navigationController?.popViewControllerAnimated(false)
    }
}
