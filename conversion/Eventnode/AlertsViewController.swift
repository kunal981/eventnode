//
//  AlertViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit


var alertRowHeights = [CGFloat]()

class AlertsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var alertsOnboardingScreen: UIView!
    @IBOutlet weak var alertTableView: UITableView!
    
    var currentUserId = ""
    
    var alertListView:NSMutableArray = []
    
    var notifications = [PFObject]()
    
    var textViewData = ["Aaliyah Cramer liked a photo in your event 2014 Europe trip","Aaliyah Cramer loved your story 2014 Europe trip"]
    
    var statusLabel = ["Just Now","5 min ago"]
    
    var profileImage = ["girl.jpeg", "boy.jpeg"]
    
    //var eventObjectId = [String]()
    
    var messageIds = [String]()
    
    var haveData: Bool! = false
    var alertEventObjectId: String = ""
    var alertNotificationType: String = ""
    var senderObjectId = ""
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("ALERTS VIEW")
        
        self.view.addSubview(wakeUpImageView)
        
        
        alertTableView.separatorColor = UIColor.clearColor()
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
            print("current User Id = \(currentUserId)")
        }
        
        if haveData == true
        {
            openDetails(alertEventObjectId, notificationType: alertNotificationType, transitionAnimated: false)
        }
        
        refreshList()
        
        print(messageIds.count)
        
        var messageIdsString = ""
        
        if messageIds.count > 0
        {
            messageIdsString = messageIds.joinWithSeparator("','")
            
            print("message ids string = \(messageIdsString)")
        }
        
        var predicateString = ""
        
        if messageIdsString == ""
        {
            predicateString = "receiverId = '\(self.currentUserId)'"
        }
        else
        {
            predicateString = "NOT (objectId IN {'\(messageIdsString)'}) AND receiverId = '\(self.currentUserId)'"
        }
        
        let predicate = NSPredicate(format: predicateString)
        
        let query = PFQuery(className:"Notifications", predicate: predicate)
        
        //query.orderByA("createdAt")
        query.orderByAscending("createdAt")
        
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagesError:", errorSelectorParameters:nil)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        refreshList()
    }
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    //MARK: - refreshList()
    func refreshList()
    {
        
        notifications = []
        
        messageIds = []
        
        
        alertRowHeights = []
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Notifications", selectColumns: ["*"], whereString: "receiverId = '\(self.currentUserId)' ORDER BY notificationId DESC", whereFields: [])
        //guest or guests? are you here?
        if resultSet != nil
        {
            while resultSet.next()
            {
                let notifData = PFObject(className: "Notifications")
                
                print(resultSet.stringForColumn("notificationActivityMessage"))
                
                notifData.objectId = resultSet.stringForColumn("objectId")
                
                print(resultSet.stringForColumn("objectId"))
                
                messageIds.append(resultSet.stringForColumn("objectId"))
                
                
                notifData["notificationActivityMessage"] = resultSet.stringForColumn("notificationActivityMessage")
                
                notifData["senderId"] = resultSet.stringForColumn("senderId")
                
                notifData["notificationFolder"] = resultSet.stringForColumn("notificationFolder")
                
                notifData["notificationImage"] = resultSet.stringForColumn("notificationImage")
                notifData["notificationType"] = resultSet.stringForColumn("notificationType")
                notifData["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
                
                notifData["createdAt"] = resultSet.stringForColumn("createdAt")
                
                notifications.append(notifData)
                
                let textView = UITextView()
                
                textView.frame = CGRectMake(self.view.frame.width*(82.0/320), self.view.frame.height*(5.0/568), self.view.frame.width*(215.0/320), 1)
                
                
                textView.text = resultSet.stringForColumn("notificationActivityMessage")
                textView.scrollEnabled = false
                textView.editable = false
                textView.selectable = false
                print(resultSet.stringForColumn("notificationActivityMessage"))
                
                print("Notification Array count = \(notifications.count)")
                
                textView.font = UIFont(name: "AvenirNext-Regular", size: 12)
                
                //        let contentSize = textView.sizeThatFits(textView.bounds.size)
                //        var frame = textView.frame
                //        frame.size.height = contentSize.height
                //        textView.frame = frame
                
                
                let contentSize: CGSize = textView.sizeThatFits(textView.bounds.size)
                var frame: CGRect = textView.frame
                frame.size.height = contentSize.height
                textView.frame = frame
                
                print("HEIGHT OF textView = \(frame.size.height)")
                
                
                textView.frame = CGRectMake(self.view.frame.width*(82.0/320), self.view.frame.height*(5.0/568), self.view.frame.width*(215.0/320), contentSize.height)
                
                var rowHeight: CGFloat = 0
                
                rowHeight = self.view.frame.height*(45.0/568) + contentSize.height
                
                if rowHeight < 100
                {
                    rowHeight = 100
                }
                
                alertRowHeights.append(rowHeight)
                
            }
            
            alertTableView.reloadData()
        }
        
        
        if notifications.count == 0
        {
            alertsOnboardingScreen.hidden = false
            
        }
        else
        {
            alertsOnboardingScreen.hidden = true
            
        }
        
        
        resultSet.close()
    }
    
    
    
    
    func fetchAllMessagesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                print(message)
                
                tblFields["objectId"] = message.objectId!
                tblFields["eventObjectId"] = message["eventObjectId"] as? String
                tblFields["senderId"] = message["senderId"] as? String
                tblFields["receiverId"] = message["receiverId"] as? String
                tblFields["notificationFolder"] = message["notificationFolder"] as? String
                tblFields["notificationImage"] = message["notificationImage"] as? String
                tblFields["notificationActivityMessage"] = message["notificationActivityMessage"] as? String
                tblFields["notificationType"] = message["notificationType"] as? String
                
                
                var date = ""
                
                if message.createdAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.createdAt)!)
                    print(date)
                    tblFields["createdAt"] = date
                }
                
                if message.updatedAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.updatedAt)!)
                    print(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                
                var insertedId = ModelManager.instance.addTableData("Notifications", primaryKey: "notificationId", tblFields: tblFields)
                
                fetchedEventObjectIds.append(message["eventObjectId"] as! String)
                
            }
            
            refreshList()
        }
    }
    
    
    
    func fetchAllMessagesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("Error: \(error) \(error.userInfo)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    
    
    
    //MARK: - UITableViewDataSource() methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return notifications.count
    }
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        
        let row = section.row
        
        return alertRowHeights[row]
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! AlertsTableViewCell
        
        
        for view in cell.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        let wrapperView:UIView = UIView()
        
        let textView:UITextView = UITextView()
        
        let status_Label:UILabel = UILabel()
        
        let watchImage:UIImageView = UIImageView()
        
        let profileImage:UIImageView = UIImageView()
        
        let seperatorView:UIView = UIView()
        
        cell.contentView.addSubview(wrapperView)
        
        wrapperView.addSubview(textView)
        
        wrapperView.addSubview(status_Label)
        
        wrapperView.addSubview(profileImage)
        
        wrapperView.addSubview(watchImage)
        
        cell.contentView.addSubview(seperatorView)
        
        alertTableView.separatorColor = UIColor.clearColor()
        
        wrapperView.frame = CGRectMake(0, 0, cell.contentView.frame.width, alertRowHeights[indexPath.row]-2)
        seperatorView.frame = CGRectMake(0, alertRowHeights[indexPath.row]-2, cell.contentView.frame.width, 2)
        
        
        wrapperView.backgroundColor = UIColor(red: 246.0/255, green: 246.0/255, blue: 246.0/255, alpha: 1.0)
        
        seperatorView.backgroundColor = UIColor.whiteColor()
        
        
        let senderObjectId = notifications[indexPath.row]["senderId"] as! String
        
        let imagePath = "\(documentDirectory)/\(senderObjectId).png"
        
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
            print("FILE AVAILABLE");
            profileImage.image = UIImage(named: imagePath)
        }
        else
        {
            print("FILE NOT AVAILABLE");
            
            profileImage.image = UIImage(named: "default.png")
            
            let notificationFolder = notifications[indexPath.row]["notificationFolder"] as! String
            let notificationImage = notifications[indexPath.row]["notificationImage"] as! String
            
            
            
            let s3BucketName = "eventnodepublicpics"
            let fileName = "profilePic.png"
            
            let downloadFilePath = "\(documentDirectory)/\(senderObjectId).png"
            let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest.bucket = s3BucketName
            //println("\(senderObjectId)/profilePic/profilePic.png")
            downloadRequest.key  = "\(notificationFolder)\(notificationImage)"
            downloadRequest.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            
            transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                
                if (task.error != nil){
                    if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                        switch (task.error.code) {
                        case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                            profileImage.image = UIImage(named: "default.png")
                            break;
                        case AWSS3TransferManagerErrorType.Paused.rawValue:
                            profileImage.image = UIImage(named: "default.png")
                            break;
                            
                        default:
                            print("error downloading")
                            profileImage.image = UIImage(named: "default.png")
                            break;
                        }
                    } else {
                        // Unknown error.
                        print("error downloading")
                        profileImage.image = UIImage(named: "default.png")
                    }
                }
                
                if (task.result != nil) {
                    print("downloading successfull")
                    
                    profileImage.image = UIImage(named: downloadFilePath)
                }
                
                return nil
                
            })
            
        }
        
        
        //profileImage.image = UIImage(named: "girl.jpeg")
        
        //println(UIImage(named: "girl.jpeg"))
        
        profileImage.frame = CGRectMake(self.view.frame.width*(21/320), self.view.frame.height*(18.0/568), self.view.frame.width*(42.0/320), self.view.frame.height*(42.0/568))
        
        
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = 21
        
        
        print(notifications[indexPath.row])
        
        
        textView.frame = CGRectMake(self.view.frame.width*(82.0/320), self.view.frame.height*(5.0/568), self.view.frame.width*(215.0/320), 1)
        
        
        textView.text = notifications[indexPath.row]["notificationActivityMessage"] as! String
        textView.scrollEnabled = false
        textView.editable = false
        //textView.selectable = false
        print(notifications[indexPath.row]["notificationActivityMessage"])
        
        print("Notification Array count = \(notifications.count)")
        
        textView.font = UIFont(name: "AvenirNext-Regular", size: 12)
        
        //        let contentSize = textView.sizeThatFits(textView.bounds.size)
        //        var frame = textView.frame
        //        frame.size.height = contentSize.height
        //        textView.frame = frame
        
        
        let contentSize: CGSize = textView.sizeThatFits(textView.bounds.size)
        var frame: CGRect = textView.frame
        frame.size.height = contentSize.height
        textView.frame = frame
        
        print("HEIGHT OF textView = \(frame.size.height)")
        
        
        textView.frame = CGRectMake(self.view.frame.width*(82.0/320), self.view.frame.height*(5.0/568), self.view.frame.width*(215.0/320), contentSize.height)
        
        
        //textView.backgroundColor = UIColor.blackColor()
        
        textView.backgroundColor = UIColor.clearColor()
        
        textView.userInteractionEnabled = false
        
        watchImage.image = UIImage(named: "clock-grey.png")
        watchImage.frame = CGRectMake(self.view.frame.width*(87/320), textView.frame.origin.y + textView.frame.height+10.0, self.view.frame.width*(13.0/320), self.view.frame.height*(13.0/568))
        
        print("HEIGHT OF Watch Image(Y) = \(self.view.frame.height*((frame.size.height + 33)/568))")
        
        let date = NSDate()
        print("date is = \(date)")
        
        let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
        print("current Time Stamp = \(currentTimeStamp)")
        
        let dateCreated = stringToDate(notifications[indexPath.row]["createdAt"] as! String)
        print("Date Created = \(dateCreated)")
        
        let createdTimeStamp = Int64(dateCreated.timeIntervalSince1970*1000)
        print("Created Time Stamp = \(createdTimeStamp)")
        
        var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
        
        //var timeDiff = Int64(currentTimeStamp - createdTimeStamp)-timezoneOffset
        let timeDiff = Int64(currentTimeStamp - createdTimeStamp)
        
        print(timeDiff)
        
        let nYears = timeDiff / (1000*60*60*24*365)
        
        print(nYears)
        
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        
        print(nMonths)
        
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        print(nDays)
        
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        print(nHours)
        
        
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        print(nMinutes)
        
        var timeMsg = ""
        
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = "about \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = "about \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = "about \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = "about \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = "about \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        
        status_Label.text = timeMsg
        
        status_Label.frame = CGRectMake(self.view.frame.width*(106/320), textView.frame.height+textView.frame.origin.y+6.0, self.view.frame.width*(145.0/320), self.view.frame.height*(21.0/568))
        
        status_Label.font = UIFont(name: "AvenirNext-DemiBold", size: 11)
        
        let overlayView = UIButton()
        
        overlayView.alpha = 0
        
        print("overlayHeight: \(wrapperView.frame.height)")
        
        overlayView.frame = CGRectMake(0, 0, wrapperView.frame.width, wrapperView.frame.height)
        
        cell.contentView.addSubview(overlayView)
        
        cell.selectionStyle = .None
        
        return cell
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let notifType = notifications[indexPath.row]["notificationType"] as! String
        if notifType == "likedpost" || notifType == "invitationresponse"
        {
            senderObjectId = notifications[indexPath.row]["senderId"] as! String
        }
        else
        {
            senderObjectId = currentUserId
        }
        
        print("overlay tapped")
        
        
        openDetails(notifications[indexPath.row]["eventObjectId"] as! String, notificationType: notifications[indexPath.row]["notificationType"] as! String, transitionAnimated: true)
    }
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if (editingStyle == UITableViewCellEditingStyle.Delete)
        {
            
            ParseOperations.instance.deleteData(notifications[indexPath.row], target: self, successSelector: "deleteNotificationSuccess:", successSelectorParameters: nil, errorSelector: "deleteNotificationError:", errorSelectorParameters: nil)
            
        }
    }
    
    func deleteNotificationSuccess(timer:NSTimer)
    {
        let object = timer.userInfo?.valueForKey("internal") as! PFObject
        
        var isDeleted = ModelManager.instance.deleteTableData("Notifications", whereString: "objectId=?", whereFields: [object.objectId!])
        refreshList()
    }
    
    func deleteNotificationError(timer:NSTimer)
    {
        
    }
    
    
    func openDetails(eventObjectId: String, notificationType: String, transitionAnimated: Bool)
    {
        if notificationType != "eventdeleted"
        {
            //var eventObjectId = eventObjectId
            
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "objectId = '\(eventObjectId)'", whereFields: [])
            
            
            //mySharedEvents = []
            
            if (resultSet != nil) {
                
                resultSet.next()
                if resultSet.intForColumn("count") > 0
                {
                    resultSet.close();
                    
                    let predicateString = "eventObjectId = '\(eventObjectId)' AND userObjectId = '\(senderObjectId)'"
                    
                    
                    let predicate = NSPredicate(format: predicateString)
                    
                    let query = PFQuery(className:"Invitations", predicate: predicate)
                    
                    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationDetailsSuccess:", successSelectorParameters: notificationType, errorSelector: "fetchInvitationDetailsError:", errorSelectorParameters:nil)
                }
                else
                {
                    resultSet.close();
                    let predicateString = "objectId = '\(eventObjectId)'"
                    
                    
                    let predicate = NSPredicate(format: predicateString)
                    
                    let query = PFQuery(className:"Events", predicate: predicate)
                    
                    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventDetailsSuccess:", successSelectorParameters: notificationType, errorSelector: "fetchEventDetailsError:", errorSelectorParameters:nil)
                }
                
            }
        }
    }
    
    
    func fetchEventDetailsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let notificationType = timer.userInfo?.valueForKey("external") as! String
        
        print("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            if fetchedobjects.count > 0
            {
                for eventObject in fetchedobjects
                {
                    var tblFields: Dictionary! = [String: String]()
                    
                    tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                    tblFields["eventImage"] = eventObject["eventImage"] as? String
                    tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                    
                    let frameX = eventObject["frameX"] as! CGFloat
                    let frameY = eventObject["frameY"] as! CGFloat
                    
                    tblFields["frameX"] = "\(frameX)"
                    tblFields["frameY"] = "\(frameY)"
                    tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                    
                    tblFields["eventFolder"] = eventObject["eventFolder"] as? String
                    
                    tblFields["senderName"] = eventObject["senderName"] as? String
                    
                    if(eventObject["isRSVP"] as? Bool == true)
                    {
                        tblFields["isRSVP"] = "1"
                        
                        var date = ""
                        if eventObject["eventStartDateTime"] != nil {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                            date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                            print(date)
                            tblFields["eventStartDateTime"] = date
                        }
                        
                        if eventObject["eventEndDateTime"] != nil {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                            date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                            print(date)
                            tblFields["eventEndDateTime"] = date
                            print(tblFields["eventEndDateTime"])
                        }
                        
                        tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                        
                        let eventLatitude = eventObject["eventLatitude"] as! Double
                        let eventLongitude = eventObject["eventLongitude"] as! Double
                        
                        tblFields["eventLatitude"] = "\(eventLatitude)"
                        tblFields["eventLongitude"] = "\(eventLongitude)"
                        
                        tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                        
                    }
                    else
                    {
                        tblFields["isRSVP"] = "0"
                    }
                    
                    var eventTimezoneOffset = eventObject["eventTimezoneOffset"] as! Int
                    
                    tblFields["eventTimezoneOffset"] = "\(eventTimezoneOffset)"
                    
                    
                    
                    tblFields["objectId"] = eventObject.objectId
                    tblFields["isPosted"] = "1"
                    
                    var date = ""
                    
                    if eventObject.createdAt != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                        print(date)
                        tblFields["createdAt"] = date
                    }
                    
                    if eventObject.updatedAt != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                        print(date)
                        tblFields["updatedAt"] = date
                    }
                    
                    tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                    
                    let insertedId = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                    
                    if insertedId>0
                    {
                        print("Record inserted at \(insertedId).")
                    }
                    else
                    {
                        print("Error in inserting record.")
                    }
                    
                    let predicateString = "eventObjectId = '\(eventObject.objectId!)' AND userObjectId = '\(senderObjectId)'"
                    
                    
                    let predicate = NSPredicate(format: predicateString)
                    
                    let query = PFQuery(className:"Invitations", predicate: predicate)
                    
                    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationDetailsSuccess:", successSelectorParameters: notificationType, errorSelector: "fetchInvitationDetailsError:", errorSelectorParameters:nil)
                }
            }
            else
            {
                Util.invokeAlertMethod("", strBody: "Event does not exist.", delegate: nil)
            }
        }
        
    }
    
    func fetchEventDetailsError(timer:NSTimer)
    {
        
    }
    
    func fetchInvitationDetailsSuccess(timer:NSTimer)
    {
        let invitationObjects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let notificationType = timer.userInfo?.valueForKey("external") as! String
        
        if let fetchedobjects = invitationObjects {
            
            var i = 0
            
            if fetchedobjects.count > 0
            {
                for invitationObject in fetchedobjects
                {
                    var tblFields: Dictionary! = [String: String]()
                    
                    if invitationObject["isApproved"] as! Bool == true
                    {
                        tblFields["isApproved"] = "1"
                    }
                    else
                    {
                        tblFields["isApproved"] = "0"
                    }
                    
                    if invitationObject["isUpdated"] as! Bool == true
                    {
                        tblFields["isUpdated"] = "1"
                    }
                    else
                    {
                        tblFields["isUpdated"] = "0"
                    }
                    
                    if invitationObject["isEventUpdated"] as! Bool == true
                    {
                        tblFields["isEventUpdated"] = "1"
                    }
                    else
                    {
                        tblFields["isEventUpdated"] = "0"
                    }
                    
                    tblFields["objectId"] = invitationObject.objectId!
                    tblFields["invitedName"] = invitationObject["invitedName"] as? String
                    tblFields["userObjectId"] = invitationObject["userObjectId"] as? String
                    tblFields["eventObjectId"] = invitationObject["eventObjectId"] as? String
                    tblFields["emailId"] = invitationObject["emailId"] as? String
                    tblFields["attendingStatus"] = invitationObject["attendingStatus"] as? String
                    
                    tblFields["invitationType"] = invitationObject["invitationType"] as? String
                    tblFields["noOfChilds"] = invitationObject["noOfChilds"] as? String
                    tblFields["noOfAdults"] = invitationObject["noOfAdults"] as? String
                    tblFields["invitationNote"] = invitationObject["invitationNote"] as? String
                    
                    tblFields["needsContentApprovel"] = "0"
                    tblFields["createdAt"] = invitationObject["createdAt"] as? String
                    
                    tblFields["updatedAt"] = invitationObject["updatedAt"] as? String
                    
                    tblFields["isPosted"] = "1"
                    
                    let objectId = invitationObject.objectId!
                    
                    let resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["count(*) as count"], whereString: "objectId = '\(objectId)'", whereFields: [])
                    
                    resultSet.next()
                    
                    let noOfRows = Int(resultSet.intForColumn("count"))
                    
                    resultSet.close()
                    
                    if noOfRows > 0
                    {
                        var insertedId = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId = '\(objectId)']", whereFields: [])
                    }
                    else
                    {
                        let insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
                        
                        if insertedId>0
                        {
                            print("Record inserted at \(insertedId).")
                        }
                        else
                        {
                            print("Error in inserting record.")
                        }
                    }
                    
                    fetchEventDetails(invitationObject["eventObjectId"] as? String, notificationType: notificationType, transitionAnimated: true)
                    
                }
            }
        }
    }
    
    func fetchInvitationDetailsError(timer:NSTimer)
    {
        
    }
    
    func fetchEventDetails(eventObjectId: String!, notificationType: String!, transitionAnimated: Bool)
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId = '\(eventObjectId)'", whereFields: [])
        
        resultSet.next()
        
        let userevent = PFObject(className: "Events")
        
        userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
        
        userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
        userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
        let isRSVP = resultSet.stringForColumn("isRSVP")
        
        if isRSVP == "0"
        {
            userevent["isRSVP"] = false
        }//2 minu
        else
        {
            userevent["isRSVP"] = true
            userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
            userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
            userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
            userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
            userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
            userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
        }
        
        userevent["eventImage"] = resultSet.stringForColumn("eventImage")
        userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
        userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
        userevent["frameX"] = resultSet.doubleForColumn("frameX")
        userevent["frameY"] = resultSet.doubleForColumn("frameY")
        userevent["socialSharingURL"] = resultSet.stringForColumn("socialSharingURL")
        userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
        
        
        userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
        userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
        
        userevent["senderName"] = resultSet.stringForColumn("senderName")
        
        
        if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil)
        {
            userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
            userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
            userevent.objectId = resultSet.stringForColumn("objectId")
        }
        
        let isPosted = resultSet.stringForColumn("isPosted")
        
        if isPosted == "0"
        {
            userevent["isPosted"] = false
        }
        else
        {
            userevent["isPosted"] = true
        }
        
        userevent["isUploading"] = false
        
        userevent["isDownloading"] = true
        
        resultSet.close()
        
        downloadEventImages(userevent)
        //println(noOfNewPosts)
        
        if userevent["eventCreatorObjectId"] as! String == currentUserId
        {
            currentEvent = userevent
            
            if( currentEvent["isPosted"] as! Bool == true && currentEvent.objectId != nil)
            {
                if notificationType == "likedpost"
                {
                    let manager = NSFileManager.defaultManager()
                    
                    let eventImageFile = currentEvent["eventImage"] as! String
                    
                    let eventImagePath = "\(documentDirectory)/\(eventImageFile)"
                    
                    let eventOriginalImageFile = currentEvent["originalEventImage"] as! String
                    
                    let eventOriginalImagePath = "\(documentDirectory)/\(eventOriginalImageFile)"
                    
                    if (manager.fileExistsAtPath(eventOriginalImagePath) && manager.fileExistsAtPath(eventImagePath))
                    {
                        
                        isPostUpdated = true
                        
                        myEventData.removeAll()
                        
                        let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
                        self.navigationController?.pushViewController(eventPhototsVC, animated: transitionAnimated)
                    }
                    else
                    {
                        if #available(iOS 8.0, *) {
                            var refreshAlert = UIAlertController(title: "Error", message: "Please wait while images download.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                                
                            }))
                            
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                            
                        } else {
                            // Fallback on earlier versions
                        }
                        
                        
                        
                    }
                }
                else
                {
                    if currentEvent["isRSVP"] as! Bool == true
                    {
                        let addTextVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageGuestViewController") as! ManageGuestViewController
                        
                        self.navigationController?.pushViewController(addTextVC, animated: false)
                    }
                    else
                    {
                        let manageFreinds = self.storyboard!.instantiateViewControllerWithIdentifier("ManageFreindsViewController") as! ManageFreindsViewController
                        
                        self.navigationController?.pushViewController(manageFreinds, animated: false)
                    }
                }
                
            }
            else
            {
                
            }
            
        }
        else
        {
            
            let isApproved = getApprovedStatus(userevent)
            
            let invitationNote = getInvitationNote(userevent)
            
            let noOfNewPosts = getNoOfPosts(userevent)
            
            let attendingStatus = getAttendingStatus(userevent)
            
            let invitationId = getInvitationId(userevent)
            
            let noOfAdults = getNoOfAdults(userevent)
            
            let noOfChilds = getNoOfChilds(userevent)
            
            userevent["isApproved"] = isApproved
            userevent["noOfNewPosts"] = noOfNewPosts
            userevent["attendingStatus"] = attendingStatus
            userevent["invitationId"] = invitationId
            userevent["noOfAdults"] = noOfAdults
            userevent["noOfChilds"] = noOfChilds
            userevent["invitationNote"] = invitationNote
            
            currentSharedEvent = userevent
            
            mySharedEventData = []
            
            if((currentSharedEvent["isApproved"] as! Bool) == true || (currentSharedEvent["isRSVP"] as! Bool) == true)
            {
                let streamVC = self.storyboard!.instantiateViewControllerWithIdentifier("StreamViewController") as! StreamViewController
                
                self.navigationController?.pushViewController(streamVC, animated: true)
            }
            else
            {
                Util.invokeAlertMethod("", strBody: "You don't have sufficient permissions to see this event. Please contact your host.", delegate: nil)
            }
        }
        
    }
    
    func downloadEventImages(eventObject: PFObject!)
    {
        let eventImageFile = eventObject["eventImage"] as! String
        
        let eventFolder = eventObject["eventFolder"] as! String
        
        let eventImagePath = "\(documentDirectory)/\(eventImageFile)"
        let manager = NSFileManager.defaultManager()
        if (!manager.fileExistsAtPath(eventImagePath))
        {
            let s3BucketName = "eventnodepublicpics"
            let fileName = eventImageFile
            
            let downloadFilePath = "\(documentDirectory)/\(fileName)"
            let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest.bucket = s3BucketName
            downloadRequest.key  = "\(eventFolder)\(fileName)"
            downloadRequest.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            
            transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                
                if (task.error != nil){
                    if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                        switch (task.error.code) {
                        case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                            break;
                        case AWSS3TransferManagerErrorType.Paused.rawValue:
                            break;
                            
                        default:
                            print("error downloading")
                            break;
                        }
                    } else {
                        // Unknown error.
                        print("error downloading")
                    }
                }
                
                if (task.result != nil) {
                    print("downloading successfull")
                }
                
                return nil
                
            })
            
        }
        
        let eventOriginalImageFile = eventObject["originalEventImage"] as! String
        
        let eventOriginalImagePath = "\(documentDirectory)/\(eventOriginalImageFile)"
        let managerOriginal = NSFileManager.defaultManager()
        
        if (managerOriginal.fileExistsAtPath(eventOriginalImagePath) != true)
        {
            let s3OriginalBucketName = "eventnodepublicpics"
            let fileOriginalName = eventOriginalImageFile
            
            let downloadOriginalFilePath = "\(documentDirectory)/\(fileOriginalName)"
            let downloadingOriginalFileURL = NSURL.fileURLWithPath(downloadOriginalFilePath)
            
            let downloadOriginalRequest = AWSS3TransferManagerDownloadRequest()
            downloadOriginalRequest.bucket = s3OriginalBucketName
            downloadOriginalRequest.key  = "\(eventFolder)\(fileOriginalName)"
            downloadOriginalRequest.downloadingFileURL = downloadingOriginalFileURL
            
            let transferOriginalManager = AWSS3TransferManager.defaultS3TransferManager()
            
            
            transferOriginalManager.download(downloadOriginalRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                
                if (task.error != nil){
                    if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                        switch (task.error.code) {
                        case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                            break;
                        case AWSS3TransferManagerErrorType.Paused.rawValue:
                            break;
                            
                        default:
                            print("error downloading")
                            break;
                        }
                    } else {
                        // Unknown error.
                        print("error downloading")
                    }
                }
                
                if (task.result != nil) {
                    print("downloading successfull")
                    
                }
                
                return nil
                
            })
            
            
        }
    }
    
    func getNoOfPosts(userevent: PFObject) -> Int
    {
        let newPostsResultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "eventObjectId = '\(userevent.objectId!)' AND isRead = 0", whereFields: [])
        
        newPostsResultSet.next()
        
        let noOfNewPosts = Int(newPostsResultSet.intForColumn("count"))
        
        newPostsResultSet.close()
        
        return noOfNewPosts
    }
    
    func getApprovedStatus(userevent: PFObject) -> Bool
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        var isApproved = "0"
        
        while(statusResultSet.next())
        {
            isApproved = statusResultSet.stringForColumn("isApproved")
        }
        
        statusResultSet.close()
        
        if isApproved == "0"
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    
    func getNoOfChilds(userevent: PFObject) -> Int
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        var noOfChilds = 0
        
        while(statusResultSet.next())
        {
            noOfChilds = Int(statusResultSet.intForColumn("noOfChilds"))
        }
        
        statusResultSet.close()
        
        print("noOfChilds: \(noOfChilds)")
        
        return noOfChilds
    }
    
    func getNoOfAdults(userevent: PFObject) -> Int
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        var noOfAdults = 0
        
        while(statusResultSet.next())
        {
            noOfAdults = Int(statusResultSet.intForColumn("noOfAdults"))
        }
        
        
        
        statusResultSet.close()
        
        print("noOfAdults: \(noOfAdults)")
        
        return noOfAdults
    }
    
    
    func getAttendingStatus(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        var attendingStatus = ""
        
        while(statusResultSet.next())
        {
            attendingStatus = statusResultSet.stringForColumn("attendingStatus")
        }
        
        statusResultSet.close()
        
        return attendingStatus
    }
    
    
    func getInvitationId(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        var invitationId = ""
        
        while(statusResultSet.next())
        {
            invitationId = statusResultSet.stringForColumn("objectId")
        }
        
        
        
        statusResultSet.close()
        
        return invitationId
    }
    
    func getInvitationNote(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        var invitationNote = ""
        
        while(statusResultSet.next())
        {
            invitationNote = statusResultSet.stringForColumn("invitationNote")
        }
        
        
        
        statusResultSet.close()
        
        return invitationNote
    }
    
    
    
    //MARK: - stringToDate()
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        print("date formatter = \(dateFormatter)")
        
        let date = dateFormatter.dateFromString(dateString)
        
        print("date  = \(date)")
        
        return date!
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: - eventButtonClicked()
    @IBAction func eventButtonClicked(sender : AnyObject)
    {
        //NSLog("sdd")
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    
    //MARK: - settingsButtonClicked()
    @IBAction func settingsButtonClicked(sender : AnyObject)
    {
        //NSLog("sdd")
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewControllerOne") as! settingViewControllerOne
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }
    
    
    //MARK: - sharedEventButtonClicked()
    @IBAction func sharedEventButtonClicked(sender : AnyObject)
    {
        //NSLog("sdd")
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
}
