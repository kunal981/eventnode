//
//  EmailtableViewCell.swift
//  eventnode
//
//  Created by brst on 7/7/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class EmailtableViewCell: UITableViewCell {

    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var emailstatus: UIButton!
    @IBOutlet weak var emaildelete: UIButton!
    @IBOutlet weak var emailDeleteWrapper: UIButton!
    
    var wrapperView: UIView = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       self.contentView.addSubview(wrapperView)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
