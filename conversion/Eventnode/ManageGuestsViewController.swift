//
//  ManageGuestsViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 8/20/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//


import UIKit
import MessageUI
import AddressBookUI
import AddressBook

// TODO(Dimpal): Is this a dummy file ? Where is ManageGuestsVC being used ? How is it different from ManageGuestVC ?
class ManageGuestsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate  {

    
    @IBOutlet weak var sView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var respondView: UIView!
    @IBOutlet weak var pendingView: UIView!
    
    @IBOutlet weak var inviteMoreView: UIView!
    @IBOutlet weak var inviteMoreButton: UIButton!
    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var respondButton: UIButton!
    @IBOutlet weak var pendigTableView: UITableView!
    @IBOutlet weak var respondtableView: UITableView!
    @IBOutlet weak var highlightedView: UIView!
    @IBOutlet weak var repondTableView2: UITableView!
    @IBOutlet weak var onlineHighlightedView: UIView!
    @IBOutlet weak var mayBeHighlightedView: UIView!
    @IBOutlet weak var notAttendingHighlightedView: UIView!
    @IBOutlet weak var respodedTextView: UITextView!
    @IBOutlet weak var respondedTextView2: UITextView!
    @IBOutlet weak var approvalButton: UIButton!
    @IBOutlet weak var onlineStatus: UILabel!
    @IBOutlet weak var attendingStatus: UILabel!
    @IBOutlet weak var maybeStatus: UILabel!
    @IBOutlet weak var notattendingStatus: UILabel!
    @IBOutlet weak var attendingBtnOutlet: UIButton!
    
    @IBOutlet var textView : UITextView!
    @IBOutlet weak var invitationCode: UILabel!
    
    @IBOutlet var onlineTexView: UITextView!
    @IBOutlet weak var approvalAllbutton: UIButton!
    @IBOutlet var attendingCount: UILabel!
    var textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
    
    var attendingWithAccess = [PFObject]()
    var attendingWithoutAcess = [PFObject]()
    var onlineWithAccess = [PFObject]()
    var onlineWithoutAccess = [PFObject]()
    var maybewithAccess = [PFObject]()
    var maybeWithoutAccess = [PFObject]()
    var notAttendingWithAccess = [PFObject]()
    var notAttendingWithoutAccess = [PFObject]()
    var withAccess = [PFObject]()
    var withoutAcess = [PFObject]()
    var pendingInvites = [PFObject]()
    var isFromCreated: Bool!
    var noOfAdults = 0
    var noOfChilds = 0
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        // approvalButton.titleLabel?.font = UIFont(name: "Monsterrat-Regular", size: 12)
        
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        respodedTextView.attributedText = NSAttributedString(string:respodedTextView.text, attributes:attributes)
        respodedTextView.font = UIFont(name: "Monsterrat - Light", size: 12.0)
        respodedTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        respondedTextView2.attributedText = NSAttributedString(string:respondedTextView2.text, attributes:attributes)
        respondedTextView2.font = UIFont(name: "Monsterrat - Light", size: 12.0)
        respondedTextView2.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        
        inviteMoreView.hidden = true
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = true
        
        
        respondtableView.dataSource = self
        respondtableView.delegate = self
        repondTableView2.dataSource = self
        repondTableView2.delegate = self
        pendigTableView.dataSource = self
        pendigTableView.delegate = self
        
        
        sView.layer.borderWidth = 0.2
        sView.backgroundColor = UIColor(red: 200/255, green:  150/255, blue: 150/255, alpha: 1.0)
        scrollView.contentSize = CGSizeMake(610, self.scrollView.frame.size.height)
        
        pendingView.hidden = true
        
        
        
        style.lineSpacing = 5
        textView.attributedText = NSAttributedString(string: textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        invitationCode.text = currentEvent.objectId!
        
        
        
        respondView.hidden = false
        respondButton.backgroundColor = UIColor(red: 75/255, green:  75/255, blue: 75/255, alpha: 1.0)
        respondButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        
        
        fetchInvitations()
        
        
        
        withAccess = attendingWithAccess
        withoutAcess = attendingWithoutAcess
        
        
        
        
        respondtableView.separatorColor = UIColor.whiteColor()
        repondTableView2.separatorColor = UIColor.whiteColor()
        pendigTableView.separatorColor = UIColor.whiteColor()
        respondtableView.reloadData()
        repondTableView2.reloadData()
        pendigTableView.reloadData()
        
        onlineTexView.hidden = true
        respondtableView.frame.origin.y = (self.view.frame.height*170)/568
        respondtableView.frame.size.height = (self.view.frame.height*105)/568
        
        
        attendingCount.text = "\(noOfAdults) Adults,\(noOfChilds) Children "
        attendingStatus.text = "Attending(\(noOfAdults+noOfChilds))"
        onlineStatus.text = "Online(\(onlineWithAccess.count))"
        maybeStatus.text = "Maybe(\(maybewithAccess.count))"
        notattendingStatus.text = "Not Attending(\(notAttendingWithAccess.count))"
        print(currentEvent.objectId)
        
        
        
        onlineTexView.hidden = true
        highlightedView.hidden = false
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = true
        
        if attendingWithAccess.count == 0
        {
            respondtableView.hidden = true
            respondedTextView2.hidden = true
            approvalAllbutton.frame.origin.y = (self.view.frame.height*182)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*190)/56
        }
        else
        {
            respondtableView.hidden = false
            respondedTextView2.hidden = false
            approvalAllbutton.frame.origin.y = (self.view.frame.height*323)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*348)/568
        }
        
        
        print(pendingInvites)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchInvitations()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString:"eventObjectId='\(currentEvent.objectId!)'", whereFields: [])
        
        if (resultSet != nil) {
            while resultSet.next()
            {
                //var invitation = PFObject(className: "Invitations")
                let attendingStatus = resultSet.stringForColumn("attendingStatus")
                let isApproved = resultSet.stringForColumn("isApproved")
                
                let invitation = PFObject(className: "Invitations")
                
                invitation.objectId = resultSet.stringForColumn("objectId")
                print(invitation.objectId)
                invitation["userObjectId"] = resultSet.stringForColumn("userObjectId")
                invitation["attendingStatus"] = resultSet.stringForColumn("attendingStatus")
                invitation["invitationType"] = resultSet.stringForColumn("invitationType")
                // invitation["needsContentApprovel"] = resultSet.stringForColumn("needsContentApprovel")
                invitation["createdAt"] = resultSet.stringForColumn("createdAt")
                invitation["updatedAt"] = resultSet.stringForColumn("updatedAt")
                invitation["dateCreated"] = resultSet.stringForColumn("dateCreated")
                invitation["dateUpdated"] = resultSet.stringForColumn("dateUpdated")
                invitation["isApproved"] = resultSet.stringForColumn("isApproved")
                invitation["emailId"] = resultSet.stringForColumn("emailId")
                invitation["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
                invitation["invitedName"] = resultSet.stringForColumn("invitedName")
                
                //println(invitation["invitedName"])
                
                noOfAdults += Int(resultSet.intForColumn("noOfAdults"))
                
                noOfChilds += Int(resultSet.intForColumn("noOfChilds"))
                
                if attendingStatus == "yes"
                {
                    if isApproved == "0"
                    {
                        attendingWithoutAcess.append(invitation)
                        print(attendingWithoutAcess)
                    }
                    else
                    {
                        attendingWithAccess.append(invitation)
                    }
                }
                
                if attendingStatus == "no"
                {
                    if isApproved == "0"
                    {
                        notAttendingWithoutAccess.append(invitation)
                        print(notAttendingWithoutAccess)
                    }
                    else
                    {
                        notAttendingWithAccess.append(invitation)
                    }
                }
                
                if attendingStatus == "maybe"
                {
                    if isApproved == "0"
                    {
                        maybeWithoutAccess.append(invitation)
                    }
                    else
                    {
                        maybewithAccess.append(invitation)
                    }
                }
                
                if attendingStatus  == "online"
                {
                    if isApproved == "0"
                    {
                        onlineWithoutAccess.append(invitation)
                    }
                    else
                    {
                        onlineWithAccess.append(invitation)
                    }
                }
                print(attendingStatus)
                if attendingStatus == "" || attendingStatus == "0"
                {
                    pendingInvites.append(invitation)
                    print(pendingInvites)
                    
                }
            }
            
            pendigTableView.reloadData()
        }
        
        resultSet.close()
        
    }
    
    @IBAction func respondButton(sender: AnyObject)
    {
        
        
        respondButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        respondButton.backgroundColor = UIColor(red: 75/255, green:  75/255, blue: 75/255, alpha: 1.0)
        pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
        inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0)
        inviteMoreButton.backgroundColor = UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0)
        
        pendingView.hidden = true
        respondView.hidden  =  false
        inviteMoreView.hidden = true
        respondtableView.reloadData()
        
    }
    
    @IBAction func pendingButton(sender: AnyObject)
    {
        pendigTableView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
        pendingButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        respondView.hidden = true
        pendingView.hidden = false
        respondButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 75/255, green:  75/255, blue: 75/255, alpha: 1.0)
        inviteMoreButton.backgroundColor = UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0)
        inviteMoreButton.setTitleColor(textColor, forState: UIControlState.Normal)
        respondButton.backgroundColor = UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0)
        inviteMoreView.hidden = true
        self.pendigTableView.reloadData()
    }
    
    @IBAction func invitemore(sender: AnyObject)
    {
        
        inviteMoreView.hidden = false
        
        inviteMoreButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        inviteMoreButton.backgroundColor = UIColor(red: 75/255, green:  75/255, blue: 75/255, alpha: 1.0)
        respondButton.backgroundColor = UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0)
        respondButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.setTitleColor(textColor, forState: UIControlState.Normal)
        pendingButton.backgroundColor = UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0)
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if tableView == respondtableView
        {
            return 1
        }
            
        else if tableView == repondTableView2
        {
            return 1
            
        }
        else
        {
            return 1
            
        }
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == respondtableView
        {
            return withAccess.count
        }
            
        else if tableView == repondTableView2
        {
            return withoutAcess.count
            
        }
        else
        {
            return pendingInvites.count
            
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let row = indexPath.row
        var cellIdentifier: String! = ""
        
        if tableView == respondtableView
        {
            //respondtableView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            
            cellIdentifier = "Cell"
            
            let cell: withAccessTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? withAccessTableViewCell
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            
            let nameLabel = UILabel()
            //nameLabel.frame = CGRectMake(self.view.frame.width*(16/320),self.view.frame.height*(2/568),self.view.frame.width*(143/320), nameLabel.frame.height)
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320), self.view.frame.height*(5.0/568), self.view.frame.width*(220.0/320), self.view.frame.height*(25.0/568))
            nameLabel.text = withAccess[row]["invitedName"] as? String
            nameLabel.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            nameLabel.adjustsFontSizeToFitWidth = true
            
            let contactLabel = UILabel()
            contactLabel.frame = CGRectMake(self.view.frame.width*(20/320),self.view.frame.height*(32/568),self.view.frame.width*(143/320), self.view.frame.height*(12.0/568))
            
            
            
            contactLabel.text = withAccess[row]["emailId"] as? String
            
            contactLabel.font = UIFont(name: "AvenirNext-Medium", size: 10.0)
            
            contactLabel.textColor = UIColor(red: 165.0/255, green: 165.0/255, blue: 165.0/255, alpha: 1.0)
            contactLabel.adjustsFontSizeToFitWidth = true
            
            
            let image = UIImage(named: "check-box.png") as UIImage?
            let button = UIButton()
            button.frame = CGRectMake(self.view.frame.width*(270/320), self.view.frame.height*(15/568), self.view.frame.width*(14/320),self.view.frame.width*(14/320))
            button.setImage(image, forState: UIControlState.Normal)
            button.addTarget(self, action: "btnTouched:", forControlEvents:.TouchUpInside)
            button.tag = indexPath.row
            
            cell?.contentView.addSubview(button)
            cell?.contentView.addSubview(nameLabel)
            cell?.contentView.addSubview(contactLabel)
            
            return cell!
            
        }
            
        else if tableView == repondTableView2
        {
            //            repondTableView2.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
            cellIdentifier = "ApprovalCell"
            
            let cell: withoutAccessTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? withoutAccessTableViewCell
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            
            let nameLabel = UILabel()
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320), self.view.frame.height*(5.0/568), self.view.frame.width*(220.0/320), self.view.frame.height*(25.0/568))
            nameLabel.text = withoutAcess[row]["invitedName"] as? String
            nameLabel.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            nameLabel.adjustsFontSizeToFitWidth = true
            
            let contactLabel = UILabel()
            contactLabel.frame = CGRectMake(self.view.frame.width*(20/320),self.view.frame.height*(32/568),self.view.frame.width*(143/320), self.view.frame.height*(12.0/568))
            
            contactLabel.text = withoutAcess[row]["emailId"] as? String
            contactLabel.font = UIFont(name: "AvenirNext-Medium", size: 10.0)
            contactLabel.textColor = UIColor(red: 165.0/255, green: 165.0/255, blue: 165.0/255, alpha: 1.0)
            contactLabel.adjustsFontSizeToFitWidth = true
            
            
            let image = UIImage(named: "checkbox.png") as UIImage?
            let button = UIButton()
            button.frame = CGRectMake(self.view.frame.width*(270/320), self.view.frame.height*(15/568), self.view.frame.width*(14/320), self.view.frame.width*(14/320))
            //button.frame = CGRectMake(286, 10, 14, 14)
            button.setImage(image, forState: UIControlState.Normal)
            button.addTarget(self, action: "accessButton:", forControlEvents:.TouchUpInside)
            button.tag = indexPath.row
            
            cell?.contentView.addSubview(button)
            cell?.contentView.addSubview(nameLabel)
            cell?.contentView.addSubview(contactLabel)
            
            return cell!
            
        }
        else
        {
            cellIdentifier = "pendingCell"
            
            let cell: pendingTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? pendingTableViewCell
            for view in cell!.contentView.subviews
            {
                view.removeFromSuperview()
            }
            
            let nameLabel = UILabel()
            nameLabel.frame = CGRectMake(self.view.frame.width*(20.0/320), self.view.frame.height*(10.0/568), self.view.frame.width*(220.0/320), self.view.frame.height*(30.0/568))
            nameLabel.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            nameLabel.adjustsFontSizeToFitWidth = true
            nameLabel.text =  pendingInvites[row]["invitedName"] as? String
            
            
            print(nameLabel.text)
            
            let remindButton = UIButton()
            remindButton.frame = CGRectMake(self.view.frame.width*(230/320), self.view.frame.height*(10/568), self.view.frame.width*(90/320), self.view.frame.height*(30.0/568))
            
            remindButton.setTitle("Remind", forState: UIControlState.Normal)
            
            remindButton.setTitleColor(UIColor(red: 68.0/255, green: 185.0/255, blue: 227.0/255, alpha: 1.0), forState: UIControlState.Normal)
            
            remindButton.tag = indexPath.row
            remindButton.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 12.0)
            remindButton.addTarget(self, action: "remindButton:", forControlEvents:.TouchUpInside)
            
            
            
            cell?.contentView.addSubview(nameLabel)
            cell?.contentView.addSubview(remindButton)
            
            
            return cell!
            
        }
        
        
    }
    
    func btnTouched(sender:UIButton)
    {
        var InvitationObjetId = currentEvent.objectId
        
        attendingWithAccess[sender.tag]["isApproved"] = false
        
        let inviteObject = attendingWithAccess[sender.tag]
        
        ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "btnSuccess:", successSelectorParameters: nil, errorSelector: "btnError:", errorSelectorParameters:"email")
        
    }
    
    func btnSuccess(timer: NSTimer)
    {
        var invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        let invitationObjectId = currentEvent.objectId!
        
        var tblFields: Dictionary! = [String: String]()
        
        
        tblFields["isApproved"] = "0"
        
        
        var insertedId = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitationObjectId])
        
        fetchInvitations()
        
        respondtableView.reloadData()
        repondTableView2.reloadData()
    }
    
    func btnError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    func remindButton(sender:UIButton)
    {
        let eventType = pendingInvites[sender.tag]["invitationType"] as! String!
        let email = pendingInvites[sender.tag]["emailId"] as! String
        let inviteCode = currentEvent.objectId!
        let eventTitle = currentEvent["eventTitle"] as! String
        var dateString = ""
        var timeString = ""
        var locationString = ""
        let hostName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        
        
        if currentEvent["isRSVP"] as! Bool
        {
            let sdate = currentEvent["eventStartDateTime"] as! NSDate
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            let sweekday = scomponents.weekday
            
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            
            
            
            dateString = "\(weekDaysArray[sweekday-1]) \(monthsArray[smonth-1]) \(sday), \(syear)"
            timeString = "\(shour):\(sminute) \(sam)"
            
            print(sweekday)
            
            locationString = currentEvent["eventLocation"] as! String!
            
        }
        var message = ""
        
        if eventType == "parse"
        {
            
            let inviteUserEmail = InviteEmailToUser()
            
            message = inviteUserEmail.emailMessage(inviteCode, eventTitle: eventTitle, dateString: dateString, timeString: timeString, locationString: locationString, hostName: hostName, type: eventType)
        }
        else
        {
            let inviteNonUserEmail = InviteEmailToNonUser()
            
            message = inviteNonUserEmail.emailMessage(inviteCode, eventTitle: eventTitle, dateString: dateString, timeString: timeString, locationString: locationString, hostName: hostName, type: eventType)
            
            
        }

        
        let sns = AWSSES.defaultSES()
        
        let messageBody = AWSSESContent()
        let subject = AWSSESContent()
        let body = AWSSESBody()
        
        subject.data = "\(hostName) invited you to the event, \(eventTitle)"
        
        messageBody.data = message
        
        body.html = messageBody
        
        let theMessage = AWSSESMessage()
        theMessage.subject = subject
        
        theMessage.body = body
        
        let destination = AWSSESDestination()
        
        //var emailId = "tejbal231@gmail.com"
        
        destination.toAddresses = [email]
        
        //destination.toAddresses = [email]
        
        let send = AWSSESSendEmailRequest()
        send.source = "noreply@eventnode.co"
        send.destination = destination
        send.message = theMessage
        send.returnPath = "noreply@eventnode.co"
        
        
        
        sns.sendEmail(send).continueWithBlock {(task: AnyObject!) -> AWSTask! in
            
            if task.error != nil
            {
                print(task.error.debugDescription)
            }
            else
            {
                print("success")
            }
            
            return nil
        }
        
    }
    
    
    
    
    func accessButton(sender:UIButton)
    {
        var InvitationObjectId = currentEvent.objectId
        
        attendingWithAccess[sender.tag]["isApproved"] = true
        
        let inviteObject = attendingWithAccess[sender.tag]
        
        ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "approveSuccess:", successSelectorParameters: nil, errorSelector: "approveError:", errorSelectorParameters:"email")
    }
    
    
    func approveSuccess(timer: NSTimer)
    {
        var invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        let invitationObjectId = currentEvent.objectId!
        var tblFields: Dictionary! = [String: String]()
        
        
        tblFields["isApproved"] = "1"
        
        
        var insertedId = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitationObjectId])
        
        fetchInvitations()
        
        respondtableView.reloadData()
        repondTableView2.reloadData()
        
        
    }
    
    func approveError(timer: NSTimer)
    {
        let error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print("error occured \(error.description)")
    }
    
    
    
    @IBAction func attendingButton(sender: AnyObject)
        
    {
        
        onlineTexView.hidden = true
        highlightedView.hidden = false
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = true
        
        if attendingWithAccess.count == 0
        {
            respondtableView.hidden = true
            respondedTextView2.hidden = true
            approvalAllbutton.frame.origin.y = (self.view.frame.height*210)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*230)/568
            
            
        }
        else
        {
            respondtableView.hidden = false
            respondedTextView2.hidden = false
            approvalAllbutton.frame.origin.y = (self.view.frame.height*323)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*348)/568
            
            
        }
        
        
        
    }
    
    
    
    
    @IBAction func onlineButton(sender: AnyObject)
    {
        
        onlineTexView.hidden = false
        highlightedView.hidden = true
        onlineHighlightedView.hidden = false
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = true
        
        if onlineWithAccess.count == 0
        {
            respondtableView.hidden = true
            respondedTextView2.hidden = true
            approvalAllbutton.frame.origin.y = (self.view.frame.height*210)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*232)/568
            repondTableView2.frame.size.height = (self.view.frame.height*105)/568
        }
        
    }
    
    
    @IBAction func mayBeButton(sender: AnyObject)
    {
        onlineTexView.hidden = true
        highlightedView.hidden = true
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = false
        notAttendingHighlightedView.hidden = true
        
        if maybewithAccess.count == 0
        {
            respondtableView.hidden = true
            respondedTextView2.hidden = true
            approvalAllbutton.frame.origin.y = (self.view.frame.height*172)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*199)/568
        }
        else
        {
            respondtableView.hidden = false
            respondedTextView2.hidden = false
        }
        
    }
    
    
    @IBAction func notAttendingButton(sender: AnyObject)
    {
        
        onlineTexView.hidden = true
        highlightedView.hidden = true
        onlineHighlightedView.hidden = true
        mayBeHighlightedView.hidden = true
        notAttendingHighlightedView.hidden = false
        
        if notAttendingWithAccess.count == 0
        {
            respondtableView.hidden = true
            respondedTextView2.hidden = true
            approvalAllbutton.frame.origin.y = (self.view.frame.height*172)/568
            repondTableView2.frame.origin.y = (self.view.frame.height*199)/568
            
        }
        
    }
    
    @IBAction func cancelButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    
    @IBAction func approveAllButton(sender: AnyObject)
    {
        
        if withoutAcess.count > 0
        {
            for (var i = 0; i < withoutAcess.count; i++)
            {
                withoutAcess[i]["isApproved"] = true
            }
            
            PFObject.saveAllInBackground( self.withoutAcess) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    var invitationIdsString = ""
                    
                    if self.withoutAcess.count > 0
                    {
                        for (var i = 0; i < self.withoutAcess.count; i++)
                        {
                            let invitationId = self.withoutAcess[i]["objectId"] as! String
                            invitationIdsString = "\(invitationIdsString)\(invitationId)"
                            if i < self.withoutAcess.count - 1
                            {
                                invitationIdsString = "\(invitationIdsString)','"
                            }
                        }
                        
                    }
                    
                    if invitationIdsString != ""
                    {
                        var tblFields: Dictionary<String, String>!
                        tblFields["isApproved"] = "1"
                        ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId IN {'invitationIdsString'} ", whereFields: [])
                    }
                    self.fetchInvitations()
                }
                else
                {
                    
                }
            }
        }
    }
    
    
    @IBAction func fetchFacebookFriends(sender: UIButton)
    {
        if isFacebookLogin
        {
            let inviteFbVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFacebookFriendsViewController") as! InviteFacebookFriendsViewController
            self.navigationController?.pushViewController(inviteFbVC, animated: false)
        }
        else
        {
            showLinkfacebookView = true
            
            let ConnectFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("connectFacebookAccount") as! facebookLinkedViewController
            
            ConnectFacebook.isInviteView = true
            
            self.navigationController?.pushViewController(ConnectFacebook, animated: false)
            
        }
        
    }
    
    
    
    @IBAction func fetchEmailContacts(sender: UIButton)
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            //let emptyDictionary: CFDictionaryRef?
            
            //let addressBook = !(ABAddressBookCreateWithOptions(emptyDictionary, nil) != nil)
            
            var error : Unmanaged<CFError>? = nil
            let addressBook : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
            
            if addressBook == nil {
                print(error)
                return
            }
            /*ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
            if success {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
            }
            else {
            NSLog("unable to request access")
            }
            })*/
            
            ABAddressBookRequestAccessWithCompletion(addressBook) {
                (granted: Bool, error: CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if !granted {
                        print("Just denied")
                        
                        let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                            
                        }))
                        
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                        
                    } else {
                        //println("Just authorized")
                        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
                        self.navigationController?.pushViewController(homeVC, animated: false)
                        
                    }
                }
            }
            
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
            NSLog("access denied")
            
            let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
}
