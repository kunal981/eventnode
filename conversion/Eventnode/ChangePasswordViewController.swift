//
//  ChangePasswordViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController,UIAlertViewDelegate {
    
    @IBOutlet weak var newpasswordtextfield: UITextField!
    @IBOutlet weak var oldpasswordTextfield: UITextField!
    
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    @IBOutlet var loaderView : UIView!
    
    @IBOutlet var loaderSubView : UIView!
    var alert1 = UIAlertView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        let loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        loadingMessage.textAlignment = .Center
        loaderSubView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        self.loaderView.hidden = true
        
        
        newpasswordtextfield.secureTextEntry = true
        oldpasswordTextfield.secureTextEntry = true
        confirmPasswordTextfield.secureTextEntry = true
        alert1.delegate = self
        textField()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        newpasswordtextfield.resignFirstResponder()
        oldpasswordTextfield.resignFirstResponder()
        confirmPasswordTextfield.resignFirstResponder()
    }
    @IBAction func changePassWordbutton(sender: AnyObject)
    {
        
        if newpasswordtextfield.text!.characters.count >= 7
        {
            if newpasswordtextfield.text != "" && confirmPasswordTextfield.text != ""
            {
                if newpasswordtextfield.text == confirmPasswordTextfield.text
                {
                    loaderView.hidden = false
                    
                    var username = NSUserDefaults.standardUserDefaults().valueForKey("currentUserName") as! NSString
                    
                    PFUser.logInWithUsernameInBackground(username as String, password: oldpasswordTextfield.text!)
                        { (user:PFUser?, error:NSError?) -> Void in
                            
                            if user != nil
                            {
                                PFUser.currentUser()?.password = self.newpasswordtextfield.text!
                                PFUser.currentUser()?.saveInBackgroundWithBlock(
                                    { (success:Bool, error:NSError?) -> Void in
                                        
                                        if success
                                        {
                                            
                                            
                                            
                                            NSUserDefaults.standardUserDefaults().setObject(self.newpasswordtextfield.text, forKey: "password")
                                            
                                            
                                            if #available(iOS 8.0, *) {
                                                
                                                var refreshAlert = UIAlertController(title: "Success", message: "Password changed successfully", preferredStyle: UIAlertControllerStyle.Alert)
                                                
                                                self.loginInBackground()
                                                
                                                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                                                    self.navigationController?.popViewControllerAnimated(true)
                                                }))
                                                self.presentViewController(refreshAlert, animated: true, completion: nil)
                                                
                                                
                                            }
                                            else
                                            {
                                                
                                            }
                                        }
                                        else
                                        {
                                            self.alert1.title = "Alert"
                                            self.alert1.message = error?.localizedDescription
                                            self.alert1.addButtonWithTitle("Ok")
                                            self.alert1.show()
                                        }
                                        
                                })
                                self.loaderView.hidden = true
                            }
                            else
                            {
                                self.loaderView.hidden = true
                                var alert  = UIAlertView()
                                alert.title = "Alert"
                                alert.message = "Old password does not match"
                                alert.addButtonWithTitle("OK")
                                alert.show()
                            }
                            
                    }
                }
                else
                {
                    var alert = UIAlertView()
                    alert.title = "Alert"
                    alert.message = "New password and confirm password must be same."
                    alert.addButtonWithTitle("Ok")
                    alert.show()
                }
            }
            else
            {
                var alert = UIAlertView()
                alert.title = "Alert"
                alert.message = "Please fill all the required fields."
                alert.addButtonWithTitle("Ok")
                alert.show()
            }
        }
        else
        {
            var alert = UIAlertView()
            alert.title = "Alert"
            alert.message = "Password must contain atleast 7 characters."
            alert.addButtonWithTitle("Ok")
            alert.show()
        }
    }
    
    
    func loginInBackground()
    {
        let email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        print(email)
        let newPasword = NSUserDefaults.standardUserDefaults().objectForKey("password") as! String
        print(newPasword)
        
        PFUser.logInWithUsernameInBackground(email, password:newPasword)
            {
                (user: PFUser?, error: NSError?) -> Void in
                if user != nil {
                    print("logInsuccessfull")
                    
                } else
                {
                    print("user not exist")
                }
        }
        
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if buttonIndex == 0
        {
            //self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func textField()
    {
        
        let paddingViewForNewPassword = UIView(frame: CGRectMake(0, 0, 15, self.newpasswordtextfield.frame.height))
        newpasswordtextfield.leftView = paddingViewForNewPassword
        newpasswordtextfield.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewForOldPassword = UIView(frame: CGRectMake(0, 0, 15, self.oldpasswordTextfield.frame.height))
        oldpasswordTextfield.leftView = paddingViewForOldPassword
        oldpasswordTextfield.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewForConfirmPassword = UIView(frame: CGRectMake(0, 0, 15, self.confirmPasswordTextfield.frame.height))
        confirmPasswordTextfield.leftView = paddingViewForConfirmPassword
        confirmPasswordTextfield.leftViewMode = UITextFieldViewMode.Always
        
        newpasswordtextfield.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        confirmPasswordTextfield.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        oldpasswordTextfield.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        
        
        newpasswordtextfield.attributedPlaceholder = NSAttributedString(string: "New Password",attributes:[NSForegroundColorAttributeName: UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)])
        oldpasswordTextfield.attributedPlaceholder = NSAttributedString(string: "Old Password",attributes:[NSForegroundColorAttributeName: UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)])
        confirmPasswordTextfield.attributedPlaceholder = NSAttributedString(string: "Confirm Password",attributes:[NSForegroundColorAttributeName: UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)])
        
    }
    
}
