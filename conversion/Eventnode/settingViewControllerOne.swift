//
//  settingViewControllerOne.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class settingViewControllerOne: UIViewController
{
    
    @IBOutlet weak var yourProfileView: UIView!
    @IBOutlet weak var changepasswordView: UIView!
    @IBOutlet weak var LinkedAccount: UIView!
    @IBOutlet weak var Notificationsview: UIView!
    @IBOutlet weak var AboutUsView: UIView!
    @IBOutlet weak var sendfeedBackView: UIView!
    
    @IBOutlet weak var CreateOrChangePassword: UILabel!
    
    
    override func viewDidLoad()
    {
        
        
        
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        
        
        //let authData: AnyObject? = PFUser.currentUser()?.valueForKey("authData")
        
        print(isFacebookLogin)
        
        print(PFUser.currentUser()?.password)
        
        if hasPassword
        {
            CreateOrChangePassword.text = "Change Password"
            CreateOrChangePassword.textColor = UIColor.blackColor()
            CreateOrChangePassword.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            
        }
        else
        {
            CreateOrChangePassword.text = "Create Password"
            CreateOrChangePassword.textColor = UIColor.blackColor()
            CreateOrChangePassword.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        }
        
    }
    
    override func viewDidAppear(animated: Bool)
    {
        
        if hasPassword
        {
            CreateOrChangePassword.text = "Change Password"
            CreateOrChangePassword.textColor = UIColor.blackColor()
            CreateOrChangePassword.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        }
        else
        {
            CreateOrChangePassword.text = "Create Password"
            CreateOrChangePassword.textColor = UIColor.blackColor()
            CreateOrChangePassword.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        }
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func yourProfileButton(sender: AnyObject)
    {
        
        let yourProfileView = self.storyboard!.instantiateViewControllerWithIdentifier("YourProfileView") as! YourProfileViewController
        self.navigationController?.pushViewController(yourProfileView, animated: false)
        
        
    }
    
    @IBAction func changePasswordbutton(sender: AnyObject)
    {
        //let authData: AnyObject? = PFUser.currentUser()?.valueForKey("authData")
        
        
        if hasPassword
        {
            let changedPassword = self.storyboard!.instantiateViewControllerWithIdentifier("ChangePassword") as! ChangePasswordViewController
            self.navigationController?.pushViewController(changedPassword, animated: false)
        }
        else
        {
            let createPassword = self.storyboard!.instantiateViewControllerWithIdentifier("CreatePassword") as! CreatePasswordViewController
            self.navigationController?.pushViewController(createPassword, animated: false)
        }
    }
    
    
    @IBAction func LinkedAccountButton(sender: AnyObject)
    {
        let linkedAccounts = self.storyboard!.instantiateViewControllerWithIdentifier("LinkedAccounts") as! LinkedAccountsViewController
        self.navigationController?.pushViewController(linkedAccounts, animated: false)
    }
    
    @IBAction func NotificationButton(sender: AnyObject)
    {
        
        let notification = self.storyboard!.instantiateViewControllerWithIdentifier("Notification") as! NotificationViewController
        self.navigationController?.pushViewController(notification, animated: false)
    }
    
    @IBAction func AboutUsButton(sender: AnyObject)
    {
        
        let aboutUs = self.storyboard!.instantiateViewControllerWithIdentifier("AboutUs") as! AboutUsViewController
        self.navigationController?.pushViewController(aboutUs, animated: false)
    }
    
    @IBAction func sendfeedbackButton(sender: AnyObject)
    {
        let feedback = self.storyboard!.instantiateViewControllerWithIdentifier("Feedback") as! SendFeedBackViewController
        self.navigationController?.pushViewController(feedback, animated: false)
        
    }
    
    
    
    @IBAction func logOutButton(sender: AnyObject)
    {
        let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: " isPosted=0", whereFields: [])
        
        resultSetCount.next()
        
        let eventCount = resultSetCount.intForColumn("count")
        
        resultSetCount.close()
        
        let resultSetPostCount: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: " isPosted=0", whereFields: [])
        
        resultSetPostCount.next()
        
        let postCount = resultSetPostCount.intForColumn("count")
        
        resultSetPostCount.close()
        
        
        if(eventCount>0 || postCount>0)
        {
            if #available(iOS 8.0, *) {
                let refreshAlert = UIAlertController(title: "Pending Changes", message: "You have pending changes that need to be uploaded. If you logout now you will lose those changes.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Logout", style: .Default, handler: { (action: UIAlertAction) in
                    self.logout()
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
            else{
                
            }
        }
        else
        {
            logout()
        }
    }
    
    @IBAction func supportButtonClicked(sender: AnyObject) {
       // Mobihelp.sharedInstance().presentSupport(self)
    }
    
    
    func logout()
    {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 1
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isLoggedIn")
        
        self.updateDeviceToken()
        
        PFUser.logOut()
        
        
        
        let isDeleted = ModelManager.instance.deleteTableData("Events", whereString: " isPosted=0", whereFields: [])
        if isDeleted {
            print("Record deleted successfully.")
        } else {
            print("Error in deleting record.")
        }
        
        
        let isDeleted1 = ModelManager.instance.deleteTableData("EventImages", whereString: " isPosted=0", whereFields: [])
        
        if isDeleted1 {
            print("Record deleted successfully.")
        } else {
            print("Error in deleting record.")
        }
        
        
        
        let VC = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        self.navigationController?.pushViewController(VC, animated: false)
        
    }
    
    func updateDeviceToken()
    {
        let installation = PFInstallation.currentInstallation()
        installation["userObjectId"] = ""
        installation.saveInBackground()
    }
    
    
    @IBAction func eventItemButtonClicked(sender: AnyObject)
    {
        let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventPhototsVC, animated: false)
        
    }
    
    @IBAction func alertButtonClicked(sender: AnyObject)
    {
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
    
    @IBAction func sharedEventButtonclicked(sender: AnyObject)
    {
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
    
    
}
