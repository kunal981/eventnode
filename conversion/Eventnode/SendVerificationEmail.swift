//
//  SendVerificationEmail.swift
//  Eventnode
//
//  Created by brst on 9/17/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class SendVerificationEmail
{
    func sendEmail(emailSubject: String, message: String, emails: [String])
    {
        let sns = AWSSES.defaultSES()
        
        let messageBody = AWSSESContent()
        let subject = AWSSESContent()
        let body = AWSSESBody()
        
        
        
        subject.data = emailSubject
        
        //messageBody.data = "\(senderName) invited you to the event, \(eventTitle). Your invitation code is \(invitation.objectId!)"
        
        messageBody.data = message
        
        //body.text = messageBody
        body.html = messageBody
        
        let theMessage = AWSSESMessage()
        theMessage.subject = subject
        
        theMessage.body = body
        
        //email = "dimpal1990@gmail.com"
        
        let destination = AWSSESDestination()
        destination.toAddresses = emails
        
        let username = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        print(username)
        let send = AWSSESSendEmailRequest()
        send.source = "Eventnode<noreply@eventnode.co>"
        send.destination = destination
        send.message = theMessage
        send.returnPath = "noreply@eventnode.co"
        
        
        
        sns.sendEmail(send).continueWithBlock {(task: AnyObject!) -> AWSTask! in
            
            if task.error != nil
            {
                print(task.error.debugDescription)
            }
            else
            {
                print("success")
            }
            
            return nil
        }
    }
}