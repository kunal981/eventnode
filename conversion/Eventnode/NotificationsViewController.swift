//
//  NotificationsViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 5/4/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func eventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func sharedEventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertViewController") as! AlertViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }
    
    
    @IBAction func notificationToggle(sender : UIButton)
    {
        
        if(sender.tag==1)
        {
            sender.tag=0;
            let image = UIImage(named: "checkbox.png") as UIImage?
            sender.setImage(image, forState: .Normal)
        }
        else
        {
            sender.tag=1;
            let image = UIImage(named: "check-box-white.png") as UIImage?
            sender.setImage(image, forState: .Normal)
        }
    }
    
    
}
