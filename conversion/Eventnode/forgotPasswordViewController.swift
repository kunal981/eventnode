//
//  forgotPasswordViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class forgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var wakeUpView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.layer.cornerRadius = 3
        self.view.addSubview(wakeUpImageView)
        
        let paddingViewForEmail = UIView(frame: CGRectMake(0, 0, 15, self.emailTextField.frame.height))
        emailTextField.leftView = paddingViewForEmail
        emailTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        
        emailTextField.keyboardType = UIKeyboardType.EmailAddress
        
        self.wakeUpView.hidden = true
        self.loaderView.hidden = true
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        
        emailTextField.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        emailTextField.resignFirstResponder()
    }
    
    @IBAction func BackButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func ResetPasswordButton(sender: AnyObject)
    {
        
        
        if emailTextField.text == ""
        {
            var alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Please Enter Your EmailID"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
            
        }
            
        else
        {
            self.wakeUpView.hidden = true
            self.loaderView.hidden = false
            PFUser.requestPasswordResetForEmailInBackground(emailTextField.text!) { (success, error) -> Void in
                if (error == nil) {
                    
                    self.loaderView.hidden = true
                    self.wakeUpView.hidden = true
                    
                    if #available(iOS 8.0, *) {
                        var refreshAlert = UIAlertController(title: "Success", message: "Check your email", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        
                        
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                            
                            self.navigationController?.popViewControllerAnimated(true)
                            
                        }))
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                    }
                        
                    else
                    {
                        
                    }
                    
                    
                }else {
                    self.loaderView.hidden = true
                    self.wakeUpView.hidden = true
                    
                    let errormessage = error!.userInfo["error"] as! NSString
                    
                    if #available(iOS 8.0, *) {
                        
                        let error = UIAlertController(title: "Cannot complete request", message: errormessage as String, preferredStyle: .Alert)
                        let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
                        error.addAction(okButton)
                        self.presentViewController(error, animated: false, completion: nil)
                    }
                    else{
                        
                    }
                }
            }
        }
    }
    
}