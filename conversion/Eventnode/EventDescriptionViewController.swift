//
//  EventDescriptionViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/24/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class EventDescriptionViewController: UIViewController {
    
    @IBOutlet var evenNameText : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
        evenNameText.text = newEvent["eventDescription"] as! String
        
        evenNameText.becomeFirstResponder()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func keyboardWillShow(sender: NSNotification) {
        evenNameText.frame = CGRectMake(15 , 90, evenNameText.frame.width, evenNameText.frame.height - 175)
    }
    func keyboardWillHide(sender: NSNotification) {
        evenNameText.frame = CGRectMake(15 , 90, evenNameText.frame.width, evenNameText.frame.height + 175)
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventVC.imageData=imageData
        self.navigationController?.pushViewController(eventVC, animated: true)*/
        newEvent["eventDescription"] = evenNameText.text
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func viewTapped(sender : AnyObject) {
        evenNameText.resignFirstResponder()
    }
    
    
    @IBAction func saveButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /* let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventVC.imageData=imageData
        self.navigationController?.pushViewController(eventVC, animated: true)*/
        newEvent["eventDescription"] = evenNameText.text
        self.navigationController?.popViewControllerAnimated(true)
    }
}
