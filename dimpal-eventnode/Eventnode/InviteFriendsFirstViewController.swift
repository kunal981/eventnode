//
//  InviteFriendsFirstViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import AddressBookUI
import AddressBook

class InviteFriendsFirstViewController: UIViewController {


    @IBOutlet weak var inviteURLWrapper: UIView!
    @IBOutlet weak var inviteTextView: UITextView!
    @IBOutlet var textView : UITextView!

    
    
    var currentUserId: String!
    
    var isFromCreated: Bool!
    
    var isApproved: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        isApproved = true
        
        // Do any additional setup after loading the view.
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        textView.attributedText = NSAttributedString(string: textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        textView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        
        
        
       
        
        inviteTextView.attributedText = NSAttributedString(string: inviteTextView.text, attributes:attributes)
        inviteTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        inviteTextView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        
        //invitationCode.text = currentEvent["socialSharingURL"] as? String
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

    // MARK: - Navigation

    @IBAction func viewTapped(sender : AnyObject) {

    }
    
    @IBAction func fetchFacebookFriends(sender: UIButton) {
        
        if isFacebookLogin
        {
            let inviteFbVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFacebookFriendsViewController") as! InviteFacebookFriendsViewController
            self.navigationController?.pushViewController(inviteFbVC, animated: false)
        }   
        else
        {
            showLinkfacebookView = true
            
            let ConnectFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("connectFacebookAccount") as! facebookLinkedViewController
            
            ConnectFacebook.isInviteView = true
            
            self.navigationController?.pushViewController(ConnectFacebook, animated: false)
            
        }
    }

    @IBAction func fetchEmailContacts(sender: UIButton) {
        //getAddressBookNames()
        
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            var emptyDictionary: CFDictionaryRef?
            
            var addressBook = !(ABAddressBookCreateWithOptions(emptyDictionary, nil) != nil)
            /*ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
                if success {
                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
                else {
                    NSLog("unable to request access")
                }
            })*/
            
            ABAddressBookRequestAccessWithCompletion(addressBook) {
                (granted: Bool, error: CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if !granted {
                        println("Just denied")
                        
                        var refreshAlert = UIAlertController(title: "Provide Access", message: "Provide Eventnode with access to your contacts. Go to Settings > Privacy > Contacts and enable Eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                            
                        }))
                        
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                        
                    } else {
                        //println("Just authorized")
                        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
                        self.navigationController?.pushViewController(homeVC, animated: false)

                    }
                }
            }

            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
            NSLog("access denied")
            
            var refreshAlert = UIAlertController(title: "Provide Access", message: "Provide Eventnode with access to your contacts. Go to Settings > Privacy > Contacts and enable Eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    
    @IBAction func closeAdjustPhotoButtonClicked(sender : AnyObject){
        
        if isFromCreated == true
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            homeVC.redirect = true
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
        else
        {
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    @IBAction func toggleAccess(sender: UIButton) {
        if isApproved == false
        {
            isApproved = true
            sender.setImage(UIImage(named: "check-box.png"), forState: .Normal)
        }
        else
        {
            isApproved = false
            sender.setImage(UIImage(named: "checkbox.png"), forState: .Normal)
        }
    }
    
    @IBAction func socialShareButton(sender: AnyObject)
    {
        var data = [
            "eventObjectId": currentEvent.objectId!,
            "emailId": "noemail",
            "eventCreatorId": "\(currentUserId!)",
            "isApproved": "true"
        ]
        
        
        if isApproved == false
        {
            data["isApproved"] = "false"
        }
        
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                println(url!)

                let plainData = (currentEvent.objectId! as NSString).dataUsingEncoding(NSUTF8StringEncoding)
                let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                println(base64String)

                var branchUrl = url! as String

                let plainDataUrl = (branchUrl as NSString).dataUsingEncoding(NSUTF8StringEncoding)

                let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                println(base64UrlString)

                var urlString = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId=\(base64String)&url=\(base64UrlString)"

                var eventTitle = currentEvent["eventTitle"] as! String

                var objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(urlString)"

                let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
                self.navigationController!.presentViewController(activityVC,
                    animated: true,
                    completion: nil)

            }
            
        })
    }
  
    
}
