//
//  StreamViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import AVFoundation
import MediaPlayer
import EventKit

var isSharedPostUpdated:Bool! = true

var isOnEventStream:Bool! = false
var isEventStreamUpdated:Bool! = false
var noOfStreamUpdates = 0

var mySharedEventData = [PFObject]()
var mySharedRowHeights = [CGFloat]()

var SharedEventTextTitle = "Add Text"


class StreamViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate {

    @IBOutlet var tableView : UITableView!
    
    @IBOutlet weak var selectedImageWrapper: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    
    @IBOutlet weak var invitationView: UIView!
    @IBOutlet weak var streamView: UIView!
    
    @IBOutlet var unreadCount: UILabel!
    @IBOutlet weak var adultsLabel: UILabel!
    @IBOutlet weak var childLabel: UILabel!
    
    @IBOutlet weak var invitationBottom: UIView!
    @IBOutlet weak var streamBottom: UIView!
    
    @IBOutlet var newUpdatesButton: UIButton!
    
    var currentStatus:String!
    var statusToBeChanged:String!
    
    var invitationNoteViewHeight: CGFloat = 0
    
    @IBOutlet var statusInfo: UILabel!
    @IBOutlet weak var openMeassageButtonView: UIButton!
    
    @IBOutlet var messagePopupView: UIView!
    @IBOutlet var statusButtonsView: UIView!
    @IBOutlet var statusInfoView: UIView!
    @IBOutlet var noOfChildText: UITextField!
    @IBOutlet var noOfAdultsText: UITextField!
    
    @IBOutlet var messageTitle: UITextView!
    @IBOutlet var noOfChildWrapper: UIView!
    @IBOutlet var noOfAdultsWrapper: UIView!
    
    @IBOutlet var invitationNoteView: UITextView!
    @IBOutlet var attendingStatusImageView: UIImageView!
    
    
    @IBOutlet weak var secondSubView: UIView!
    @IBOutlet weak var hostNameBottom: UILabel!
    @IBOutlet weak var hostName: UILabel!
    @IBOutlet weak var eventLocationText: UITextView!
    
    @IBOutlet weak var senderName: UILabel!
    
    @IBOutlet weak var eventHeaderTitle: UILabel!
    
    @IBOutlet weak var eventTitleText: UITextView!
    @IBOutlet weak var eventStartText: UITextView!
    @IBOutlet weak var eventEndText: UITextView!
    @IBOutlet weak var eventLocalStartText: UITextView!
    
    @IBOutlet weak var notefromhost: UILabel!
    
    @IBOutlet weak var eventDescriptionText: UITextView!
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var attendEvent: UIButton!
    @IBOutlet weak var notAttendEvent: UIButton!
    @IBOutlet weak var notSure: UIButton!
    @IBOutlet weak var followOnline: UIButton!
    @IBOutlet weak var invitationStreamButtonInnerView: UIView!
    @IBOutlet weak var postMessageButtonInnerView: UIView!
    
    //var eventTitle: String!
    var eventObject: PFObject!
    var eventStore : EKEventStore = EKEventStore()
    
    
    var newMedia: Bool = true
    
    var moviePlayer : MPMoviePlayerController?
    
    @IBOutlet weak var invitationMessageView: UIView!
    @IBOutlet weak var invitationStreamView: UIView!
    @IBOutlet weak var postMessageView: UIView!
    @IBOutlet weak var postStreamView: UIView!
    
    var moviePlayers = [Int: MPMoviePlayerController]()
    var currentScrollTop = 0
    var hideNow = 0
    var currentUserId: String!
    
    var eventLogoFile: String!
    
    var eventLogoFileUrl: NSURL!
    
    
    var likeObjectIds = [String]()
    
    var fullUserName: String!
    var eventTitle: String!
    
    @IBOutlet weak var eventTitleLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        unreadCount.hidden = true
        unreadCount.layer.masksToBounds = true
        unreadCount.layer.cornerRadius = 10
        
        
        
        var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentSharedEvent.objectId!])
        
        resultSetCount.next()
        
        var unreadMsgCount = resultSetCount.intForColumn("count")
        
        if unreadMsgCount == 0
        {
            unreadCount.hidden = true
        }
        else
        {
            unreadCount.hidden = false
            unreadCount.text = "\(unreadMsgCount)"
            unreadCount.textAlignment = .Center
            unreadCount.font = UIFont(name: "System-Bold", size: 11.0)
            
            var labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
            
            if labelWidth > 20
            {
                unreadCount.frame.size.width = labelWidth+4.0
            }
            
        }
        
        resultSetCount.close()
        
        
        fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        eventTitle = currentSharedEvent["eventTitle"] as! String

        
        if let isLikesReloaded = NSUserDefaults.standardUserDefaults().objectForKey("isLikesReloaded") as? String
        {
            if isLikesReloaded == "false"
            {
                ModelManager.instance.deleteTableData("PostLikes", whereString: "1", whereFields: [])
                NSUserDefaults.standardUserDefaults().setObject("true", forKey: "isLikesReloaded")
            }
        }
        else
        {
            ModelManager.instance.deleteTableData("PostLikes", whereString: "1", whereFields: [])
        }

        
        
        isOnEventStream = true
        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        var attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(15.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        invitationNoteView.attributedText = NSAttributedString(string:invitationNoteView.text, attributes:attributes)
        invitationNoteView.font = UIFont(name:"AvenirNext-Medium", size: 15.0)
        invitationNoteView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        newUpdatesButton.layer.cornerRadius = 3
        newUpdatesButton.hidden = true
        newUpdatesButton.backgroundColor = UIColor(red: 112.0/255, green: 102.0/255, blue: 119.0/255, alpha: 1.0)
        newUpdatesButton.layer.borderWidth = 2
        newUpdatesButton.layer.borderColor = UIColor(red: 81/255, green: 51.0/255, blue: 103.0/255, alpha: 1.0).CGColor
        newUpdatesButton.alpha = 1
        
        eventDescriptionText.attributedText = NSAttributedString(string:eventDescriptionText.text, attributes:attributes)
        eventDescriptionText.font = UIFont(name:"AvenirNext-Regular", size: 13.0)
        eventDescriptionText.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        eventObject = currentSharedEvent
        
        statusInfoView.layer.borderWidth = 0.3
        statusInfoView.layer.borderColor = UIColor(red: 145.0/255, green: 145.0/255, blue: 145.0/255, alpha: 0.7).CGColor
        
        
        if((eventObject["isRSVP"] as! Bool) == true)
        {
            invitationView.hidden = false
            
            
            
            subView.layer.cornerRadius = 3
            subView.layer.borderWidth = 0.3
            
            subView.backgroundColor = UIColor(red: 242/255, green:  242/255, blue: 242/255, alpha: 1.0)
            subView.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
            
            secondSubView.layer.cornerRadius = 3
            secondSubView.layer.borderWidth = 0.3
            
            secondSubView.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
            
            
            /*var firstLine : UIView! = UIView(frame: CGRectMake(156, 3, 1, 27))
            firstLine.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1.0)
            self.secondSubView.addSubview(firstLine)*/
            
            
            var latitude = eventObject["eventLatitude"] as! Double
            var longitude = eventObject["eventLongitude"] as! Double
            var eventLocation = eventObject["eventLocation"] as! String
            println(eventObject["eventStartDateTime"] as! NSDate)
            
            //
            
            
            //let sdate = eventObject["eventStartDateTime"] as! NSDate
            //let edate = eventObject["eventEndDateTime"] as! NSDate
            
            var eventStartDate = eventObject["eventStartDateTime"] as! NSDate
            
            var startTimeStamp = Int64(eventStartDate.timeIntervalSince1970)
            var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
            var eventTimezoneOffset = eventObject["eventTimezoneOffset"] as! Int
            
            var timeStampToBeShown = Int64(startTimeStamp-timezoneOffset+eventTimezoneOffset)
            
            var sdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: sdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour >= 12)
            {
                if(scomponents.hour > 12)
                {
                    shour = scomponents.hour-12
                }
                else
                {
                    shour = 12
                }
                
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0)
                {
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            
            var startDate = "\(monthsArray[smonth-1]) \(sday), \(syear) @ \(shour):\(sminute) \(sam)"
            
            
            
            let ecomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: eventStartDate)
            
            var eam = "AM"
            var ehour: Int!
            if(ecomponents.hour > 12)
            {
                if(ecomponents.hour > 12)
                {
                    ehour = ecomponents.hour-12
                }
                else
                {
                    ehour = 12
                }
                
                eam = "PM"
            }
            else
            {
                ehour = ecomponents.hour
                eam = "AM"
                if(ecomponents.hour==0){
                    ehour = 12
                }
            }
            
            var eminute = "\(ecomponents.minute)"
            let eday = ecomponents.day
            let emonth = ecomponents.month
            let eyear = ecomponents.year
            
            if(ecomponents.minute<10)
            {
                eminute = "0\(eminute)"
            }
            
            var startLocalDate = "\(monthsArray[emonth-1]) \(eday) @ \(ehour):\(eminute) \(eam) (Your Time)"
            
            
            println(startDate)
            //println(endDate)
            println(eventTitle)
            
            invitationNoteViewHeight = invitationNoteView.frame.height
            
            eventTitleText.text = eventObject["eventTitle"] as! String
            eventTitleText.textAlignment = NSTextAlignment.Center
            eventTitleText.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
            
            
            eventStartText.text = startDate
            
            eventStartText.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
            eventStartText.textAlignment = NSTextAlignment.Center
            
            //eventLocalStartText.text = startLocalDate
            
            eventLocalStartText.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
            eventLocalStartText.textAlignment = NSTextAlignment.Center
            
            eventLocationText.text = eventObject["eventLocation"] as! String!
            eventLocationText.textAlignment = NSTextAlignment.Center
            eventLocationText.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
            
            
            
            eventDescriptionText.text = eventObject["eventDescription"] as! String!
            eventDescriptionText.textColor = UIColor.blackColor()
            
            
            
            var style = NSMutableParagraphStyle()
            style.lineSpacing = 5
            let attributes = [NSParagraphStyleAttributeName : style]
            
            eventDescriptionText.attributedText = NSAttributedString(string:eventDescriptionText.text, attributes:attributes)
            eventDescriptionText.font = UIFont(name: "AvenirNext-Regular", size: 13.0)
            eventDescriptionText.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            
            let by = "by"
            
            let host =  eventObject["senderName"] as! String
            
            hostName.text = "\(by) \(host)"
            hostNameBottom.text = eventObject["senderName"] as? String
            
        }
        else
        {
            invitationView.hidden = true
        }
        
        
        if((eventObject["isApproved"] as! Bool) == true)
        {
            streamView.hidden = false
        }
        else
        {
            streamView.hidden = true
        }

        currentStatus = currentSharedEvent["attendingStatus"] as! String
        statusToBeChanged = currentSharedEvent["attendingStatus"] as! String
        
        if (eventObject["isApproved"] as! Bool) == true && (eventObject["isRSVP"] as! Bool) == true
        {
            if currentStatus != ""
            {
                streamButtonClicked(UIButton())
            }
            
            invitationMessageView.hidden = false
            invitationStreamView.hidden = false
            postMessageView.hidden = false
            postStreamView.hidden = false
        }
        else if(eventObject["isApproved"] as! Bool) == true && (eventObject["isRSVP"] as! Bool) == false
        {
            invitationMessageView.hidden = true
            invitationStreamView.hidden = true
            postMessageView.hidden = true
            postStreamView.hidden = true
            
            invitationBottom.hidden = true
            streamBottom.hidden = true
            
            tableView.frame.size.height = self.view.frame.height - (tableView.frame.origin.y)
            
            postStreamView.frame.size.width = self.view.frame.width
            
            postStreamView.frame.origin.x = 0
        }
        else if(eventObject["isApproved"] as! Bool) == false && (eventObject["isRSVP"] as! Bool) == true
        {
            //invitationMessageView.hidden = true
            //invitationStreamView.hidden = true
            postMessageView.hidden = false
            postStreamView.hidden = true
            
            var originalPostMessageViewWidth = postMessageButtonInnerView.frame.width
            
            postMessageView.frame.size.width = self.view.frame.width
            postMessageButtonInnerView.frame.size.width = originalPostMessageViewWidth
            
            postMessageView.frame.origin.x = 0
            postMessageButtonInnerView.frame.origin.x = (postMessageView.frame.width/2) - (originalPostMessageViewWidth/2)
        }
        
        

        
        messagePopupView.hidden = true
        
        var noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        var noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        if currentStatus != ""
        {
            
            if currentStatus == "yes"
            {
                statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
                attendingStatusImageView.image = UIImage(named:"check-green.png")
            }
            
            if currentStatus == "online"
            {
                statusInfo.text = "I will follow online"
                attendingStatusImageView.image = UIImage(named:"web-globe.png")
            }
            
            if currentStatus == "maybe"
            {
                statusInfo.text = "I am not sure"
                attendingStatusImageView.image = UIImage(named:"questionmark.png")
            }
            
            if currentStatus == "no"
            {
                statusInfo.text = "I am not attending"
                attendingStatusImageView.image = UIImage(named:"cross-red.png")
            }
            
            
            openMeassageButtonView.hidden = false
            statusButtonsView.hidden = true
            statusInfoView.hidden = false
        }
        else
        {
            
            openMeassageButtonView.hidden = true
            statusButtonsView.hidden = false
            statusInfoView.hidden = true
        }
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        
        
        //senderName.text = eventObject["senderName"] as? String
        
        //styleLabelFont(senderName)
        
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        if((currentSharedEvent["eventTitle"] as? String) == "")
        {
            eventTitleLabel.text = "No Title"
        }
        else
        {
            var eventTitleString = (currentSharedEvent["eventTitle"] as? String)!
            
            
            currentSharedEvent["eventTitle"] = prefix(eventTitleString, 1).capitalizedString + suffix(eventTitleString, count(eventTitleString) - 1)
            eventTitleLabel.text = currentSharedEvent["eventTitle"] as? String
        }
        
        
        moviePlayer = MPMoviePlayerController()
        
        moviePlayer!.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        
        moviePlayer!.prepareToPlay()
        
        moviePlayer!.shouldAutoplay = false
        moviePlayer!.view.hidden = true
        self.view.addSubview(moviePlayer!.view)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "doneButtonClick:", name: MPMoviePlayerWillExitFullscreenNotification, object:nil)
        
        
        tableView.separatorColor = UIColor.clearColor()
        
        
        println(currentSharedEvent.objectId!)
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("refreshContent"), userInfo: nil, repeats: true)
        
    }
    
    func refreshContent()
    {
        
        var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentSharedEvent.objectId!])
        
        resultSetCount.next()
        
        var unreadMsgCount = resultSetCount.intForColumn("count")
        
        if unreadMsgCount == 0
        {
            unreadCount.hidden = true
        }
        else
        {
            unreadCount.hidden = false
            unreadCount.text = "\(unreadMsgCount)"
            unreadCount.textAlignment = .Center
            unreadCount.font = UIFont(name: "System-Bold", size: 11.0)
            
            var labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
            
            if labelWidth > 20
            {
                unreadCount.frame.size.width = labelWidth+4.0
            }
            
        }
        
        resultSetCount.close()
        
        if isEventStreamUpdated == true
        {
            if noOfStreamUpdates == 1
            {
                newUpdatesButton.setTitle("\(noOfStreamUpdates) new update", forState: UIControlState.Normal)
            }
            else
            {
                newUpdatesButton.setTitle("\(noOfStreamUpdates) new updates", forState: UIControlState.Normal)
            }
            
            //sendButton.setTitleColor(UIColor(red: 68.0/255, green: 185.0/255, blue: 227.0/255, alpha: 1.0), forState: UIControlState.Normal)
            newUpdatesButton.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 12.0)
        
            newUpdatesButton.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func styleTextFont(textView: UITextView)
    {
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
    }
    
    
    
    func styleLabelFont(textLabel: UILabel)
    {
        textLabel.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        textLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        
        var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentSharedEvent.objectId!])
        
        resultSetCount.next()
        
        var unreadMsgCount = resultSetCount.intForColumn("count")
        
        if unreadMsgCount == 0
        {
            unreadCount.hidden = true
        }
        else
        {
            unreadCount.hidden = false
            unreadCount.text = "\(unreadMsgCount)"
            unreadCount.textAlignment = .Center
            unreadCount.font = UIFont(name: "System-Bold", size: 11.0)
            
            var labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
            
            if labelWidth > 20
            {
                unreadCount.frame.size.width = labelWidth+4.0
            }
            
        }
        
        resultSetCount.close()

        
        if((currentSharedEvent["eventTitle"] as? String) == "")
        {
            eventTitleLabel.text = "No Title"
        }
        else
        {
            eventTitleLabel.text = currentSharedEvent["eventTitle"] as? String
        }
        
        mySharedEventData.removeAll()
        deleteData()
        refreshList()
        downloadData()
        updateData()
        
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func addCommentButtonClicked(sender : AnyObject){
        let addCommentVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddCommentsViewController") as! AddCommentsViewController
        
        addCommentVC.isShared = true
        
        self.navigationController?.pushViewController(addCommentVC, animated: true)
        
    }
    
    
    @IBAction func invitationButtonClicked(sender : AnyObject){
        
        invitationView.hidden=false
        streamView.hidden=true
    }
    
    @IBAction func streamButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        invitationView.hidden=true
        streamView.hidden=false
    }
    
    
    @IBAction func closeButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
        //isEventStreamUpdated = false
        isOnEventStream = false
        noOfStreamUpdates = 0
    }
    
    
    @IBAction func backButtonClicked(sender : AnyObject){
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func addToCalender(sender: AnyObject)
    {
        var eventStore : EKEventStore = EKEventStore()
        // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
        eventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: {
            granted, error in
            if (granted) && (error == nil) {
                println("granted \(granted)")
                println("error  \(error)")
                
                var event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = self.eventTitle
                let date = self.eventObject["eventStartDateTime"] as! NSDate
                
                event.startDate = date
                event.endDate = date
                
                event.notes = self.eventObject["eventDescription"] as! String!
                var latitude = self.eventObject["eventLatitude"] as! Double
                var longitude = self.eventObject["eventLongitude"] as! Double
                event.location = self.eventObject["eventLocation"] as! String
                event.calendar = eventStore.defaultCalendarForNewEvents
                eventStore.saveEvent(event, span: EKSpanThisEvent, error: nil)
                
                var refreshAlert = UIAlertController(title: "Event Saved", message: "Your event has been saved to calender.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                    
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        })
    }
    
    
    
    @IBAction func goToMapsButton(sender: AnyObject)
    {
        
        var latitude = eventObject["eventLatitude"] as! Double
        var longitude = eventObject["eventLongitude"] as! Double
        
        
        let openLink = NSURL(string : "http://maps.google.com/maps?q=\(latitude),\(longitude)")
        UIApplication.sharedApplication().openURL(openLink!)
    }
    
    
    func downloadData()
    {
        var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId = '\(currentSharedEvent.objectId!)' GROUP BY objectId ORDER BY eventImageId DESC", whereFields: [])
        
        var postObjectIds: Array<String>
        
        postObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                postObjectIds.append(resultSet.stringForColumn("objectId"))
            }
        }
        
        resultSet.close()
        
        
        var postObjectIdsString = "','".join(postObjectIds)
        
        println("Ids: \(postObjectIdsString)")
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(postObjectIdsString)'}) AND eventObjectId = '\(currentSharedEvent.objectId!)'")
        
        var query = PFQuery(className:"EventImages", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
        
        var resultSetLikes: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["*"], whereString: "eventObjectId = '\(currentSharedEvent.objectId!)'", whereFields: [])
        
        var likeObjectIds: Array<String>
        
        likeObjectIds = []
        
        if (resultSetLikes != nil) {
            while resultSetLikes.next() {
                likeObjectIds.append(resultSetLikes.stringForColumn("objectId"))
            }
        }
        
        resultSetLikes.close()
        
        
        var likeObjectIdsString = "','".join(likeObjectIds)
        
        println("Ids: \(likeObjectIdsString)")
        
        let likePredicate = NSPredicate(format: "NOT (objectId IN {'\(likeObjectIdsString)'}) AND eventObjectId = '\(currentSharedEvent.objectId!)'")
        
        var likeQuery = PFQuery(className:"PostLikes", predicate: likePredicate)
        
        ParseOperations.instance.fetchData(likeQuery, target: self, successSelector: "fetchAllLikesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllLikesError:", errorSelectorParameters:nil)
        
    }
    
    
    
    func updateData()
    {
        var invitationId = currentSharedEvent["invitationId"] as! String
        
        let textUpdatePredicate = NSPredicate(format: "objectId = '\(invitationId)' AND eventObjectId = '\(currentSharedEvent.objectId!)' AND isTextUpdated = true")
        
        var likeQuery = PFQuery(className:"Invitations", predicate: textUpdatePredicate)
        
        //ParseOperations.instance.fetchData(likeQuery, target: self, successSelector:  "fetchTextUpdatesSuccess:", successSelectorParameters: nil, errorSelector: "fetchTextUpdatesError:", errorSelectorParameters:nil)
    }
    
    func fetchTextUpdatesSuccess(timer:NSTimer)
    {
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if var fetchedobjects = objects
        {
            
            
            if fetchedobjects.count > 0
            {
                var i = 0
                
                for invitation in fetchedobjects
                {
                    fetchedobjects[i]["isTextUpdated"] = false
                    i++
                }
                
                PFObject.saveAllInBackground(fetchedobjects)
                
                let predicate = NSPredicate(format: "postType = 'text' AND eventObjectId = '\(currentSharedEvent.objectId!)'")
                
                var query = PFQuery(className:"EventImages", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
            }
            
        }
    }
    
    func fetchTextUpdatesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    func fetchAllLikesSuccess(timer:NSTimer)
    {
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if var fetchedobjects = objects {
            var i=0;
            for object in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventObjectId"] = object["eventObjectId"] as? String
                tblFields["postObjectId"] = object["postObjectId"] as? String
                tblFields["userObjectId"] = object["userObjectId"] as? String
                tblFields["objectId"] = object.objectId!
                tblFields["isUpdated"] = "1"
                
                
                var insertedId = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
            }
            
            self.refreshList()
        }
    }
    
    func fetchAllLikesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func deleteData()
    {
        let updatePredicate = NSPredicate(format: "eventObjectId = '\(currentSharedEvent.objectId!)'")
        
        var updateQuery = PFQuery(className:"EventImages", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchExistingPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchExistingPostsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchExistingPostsSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects
        {
            
            var i = 0
            
            var existingPostObjectIds: Array<String>
            existingPostObjectIds = []
            
            for postObject in fetchedobjects
            {
                existingPostObjectIds.append(postObject.objectId!)
            }
            
            var existingPostObjectIdsString = "','".join(existingPostObjectIds)
            
            
            
            var whereQuery = ""
            
            if existingPostObjectIdsString != ""
            {
                whereQuery = "eventObjectId = '\(currentSharedEvent.objectId!)' AND objectId NOT IN ('\(existingPostObjectIdsString)') AND objectId != ''"
            }
            else
            {
                whereQuery = "eventObjectId = '\(currentSharedEvent.objectId!)' AND objectId != ''"
            }
            
            var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["objectId"], whereString: whereQuery, whereFields: [])
            
            
            var nonExistingPostObjectIds: Array<String>
            nonExistingPostObjectIds = []
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    nonExistingPostObjectIds.append(resultSet.stringForColumn("objectId"))
                }
            }
            
            resultSet.close()
            
            var nonExistingPostObjectIdsString = "','".join(nonExistingPostObjectIds)
            
            if nonExistingPostObjectIdsString != ""
            {
                ModelManager.instance.deleteTableData("EventImages", whereString: "objectId IN ('\(nonExistingPostObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("PostLikes", whereString: "postObjectId IN ('\(nonExistingPostObjectIdsString)')", whereFields: [])
                
                refreshList()
            }

        }
    }
    
    
    func fetchExistingPostsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    

    
    
    func fetchAllPostsSuccess(timer:NSTimer)
    {
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        isSharedPostUpdated = false
        println("Successfully retrieved \(objects!.count) posts.")
        
        if var fetchedobjects = objects {
            mySharedEventData = fetchedobjects
            var i=0;
            for post in mySharedEventData
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["postData"] = post["postData"] as? String
                
                tblFields["isApproved"] = "0"
                tblFields["isRead"] = "1"
                
                var postHeight = post["postHeight"] as! CGFloat
                var postWidth = post["postWidth"] as! CGFloat
                
                tblFields["postHeight"] = "\(postHeight)"
                tblFields["postWidth"] = "\(postWidth)"
                tblFields["eventObjectId"] = post["eventObjectId"] as? String
                
                tblFields["eventFolder"] = post["eventFolder"] as? String
                
                tblFields["postType"] = post["postType"] as? String
                
                tblFields["objectId"] = post.objectId!
                tblFields["isPosted"] = "1"
                
                var date = ""
                
                if post.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.createdAt)!)
                    println(date)
                    tblFields["createdAt"] = date
                }
                
                if post.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.updatedAt)!)
                    println(date)
                    tblFields["updatedAt"] = date
                }
                
                
                var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "objectId = '\(post.objectId!)'", whereFields: [])
                
                resultSet.next()
                
                var noOfPosts = Int(resultSet.intForColumn("count"))
                
                resultSet.close()
                
                if noOfPosts > 0
                {
                    var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "objectId=?", whereFields: [post.objectId!])
                }
                else
                {
                    var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
                }
                
            }
            
            self.refreshList()
        }
    }
    
    func fetchAllPostsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    
    
    func doneButtonClick(sender: NSNotification){
        println("jiouiop")
        moviePlayer?.setFullscreen(false, animated: true)
        moviePlayer?.stop()
        moviePlayer?.view.hidden = true
    }
    
    
    func refreshList()
    {
        
        //isPostDataUpDated = false
        
        mySharedEventData.removeAll()
        mySharedRowHeights.removeAll()
        println("fetching....")
        
        var currentEventId = currentSharedEvent.objectId as String!
        
        
        var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId=? ORDER BY eventImageId DESC", whereFields: [currentEventId])
        
        mySharedEventData = []
        likeObjectIds = []
        var i = 0
        
        if (resultSet != nil) {
            while resultSet.next() {
                
                var userpost = PFObject(className: "EventImages")
                
                userpost["eventImageId"] = Int(resultSet.intForColumn("eventImageId"))
                userpost["postData"] = resultSet.stringForColumn("postData")
                userpost["eventFolder"] = resultSet.stringForColumn("eventFolder")
                
                
                userpost["postType"] = resultSet.stringForColumn("postType")
                userpost["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
                userpost["postHeight"] = resultSet.doubleForColumn("postHeight")
                userpost["postWidth"] = resultSet.doubleForColumn("postWidth")
                
                
                
                userpost["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                userpost["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                
                println(resultSet.stringForColumn("createdAt"))
                
                if(resultSet.stringForColumn("createdAt") != "" && resultSet.stringForColumn("updatedAt") != "" && resultSet.stringForColumn("createdAt") != nil && resultSet.stringForColumn("updatedAt") != nil)
                {
                    userpost.objectId = resultSet.stringForColumn("objectId")
                }
                else
                {
                    
                }
                
                
                var isPosted = resultSet.stringForColumn("isPosted")
                
                
                if isPosted == "0"
                {
                    userpost["isPosted"] = false
                }
                else
                {
                    userpost["isPosted"] = true
                }
                
                
                var isApproved = resultSet.stringForColumn("isApproved")
                
                if(isApproved != nil)
                {
                    if isApproved == "0"
                    {
                        userpost["isApproved"] = false
                    }
                    else
                    {
                        userpost["isApproved"] = true
                    }
                }
                else
                {
                    userpost["isApproved"] = false
                }
                
                
                userpost["isDownloading"] = true
                
                mySharedEventData.append(userpost)
                
                println(userpost["isPosted"]!)
                
                
                var rowHeight:CGFloat = 380.0
                
                var extraHeight:CGFloat = 0
                
                if(userpost["postType"] as! String == "text")
                {
                    //extraHeight = 36.0
                    var postText = userpost["postData"] as! String
                    
                    var senderMessageTemp = UITextView()
                    
                    senderMessageTemp.frame.size.width = self.view.frame.width-self.view.frame.width*(40.0/320)
                    //senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
                    
                    senderMessageTemp.text = postText
                    
                    var style = NSMutableParagraphStyle()
                    style.lineSpacing = 8
                    let attributes = [NSParagraphStyleAttributeName : style]
                    
                    
                    var attrs = [
                        NSFontAttributeName : UIFont.systemFontOfSize(20.0),
                        NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
                        NSUnderlineStyleAttributeName : 1]
                    
                    
                    senderMessageTemp.attributedText = NSAttributedString(string: postText, attributes:attributes)
                    senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20.0)
                    
                    let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
                    var frame = senderMessageTemp.frame
                    frame.size.height = contentSize.height
                    senderMessageTemp.frame = frame
                    
                    println(rowHeight)
                    
                    rowHeight = contentSize.height + (0.120625 * self.view.frame.height) + (58.0 * self.view.frame.height)/568
                }
                else
                {
                    println(userpost["postHeight"])
                    var rheight = userpost["postHeight"] as! CGFloat
                    var rwidth = userpost["postWidth"] as! CGFloat
                    
                    println("height: \(rheight)")
                    
                    rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+((52.0/568)*self.view.frame.height)
                }
                
                if i == 0
                {
                    //rowHeight = rowHeight
                }
                
                mySharedRowHeights.append(rowHeight+extraHeight)
                
                i++
            }
        }
        
        resultSet.close()
        println(mySharedRowHeights)
        
        for var j = 0; j < mySharedEventData.count; j++
        {
            
            var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["count(*) as count"], whereString: "postObjectId=? AND userObjectId=?", whereFields: [mySharedEventData[j].objectId!, currentUserId])
            
            resultSetCount.next()
            
            var userLikeCount = resultSetCount.intForColumn("count")
            resultSetCount.close()
            
            if userLikeCount > 0
            {
                
                var resultSetObjectId: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["*"], whereString: "postObjectId=? AND userObjectId=?", whereFields: [mySharedEventData[j].objectId!, currentUserId])
                
                resultSetObjectId.next()
                
                var userLikeObjectId = resultSetObjectId.stringForColumn("objectId")
                resultSetObjectId.close()
                
                likeObjectIds.append(userLikeObjectId)
            }
            else
            {
                likeObjectIds.append("")
            }
            
            
        }
        
        
        self.tableView.reloadData()
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["isRead"] = "1"
        
        var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventObjectId=?", whereFields: [currentSharedEvent.objectId!])
        if isUpdated {
            println("Record Updated Successfully")
            println("eventImage")
        } else {
            println("Record not Updated Successfully")
        }
        
        
    }
    
    
    func stringToDate(dateString: String)->NSDate
    {
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        var date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    
    
    func likeSelectedPost(sender : UIButton)
    {
        sender.enabled = false
        
        var likeObject: PFObject!
        likeObject = PFObject(className: "PostLikes")
        likeObject["eventObjectId"] = currentSharedEvent.objectId!
        likeObject["postObjectId"] = mySharedEventData[sender.tag].objectId!
        likeObject["userObjectId"] = currentUserId
        likeObject["isUpdated"] = true
        
        var postType = mySharedEventData[sender.tag]["postType"] as! String
        
        if postType == "text"
        {
           postType = "note"
        }
        
        if likeObjectIds[sender.tag] != ""
        {
            likeObject.objectId = likeObjectIds[sender.tag]
            ParseOperations.instance.deleteData(likeObject, target: self, successSelector: "deletePostLikeSuccess:", successSelectorParameters: nil, errorSelector: "deletePostLikeError:", errorSelectorParameters: likeObject)
        }
        else
        {
            ParseOperations.instance.saveData(likeObject, target: self, successSelector: "likePostSuccess:", successSelectorParameters: postType, errorSelector: "likePostError:", errorSelectorParameters: nil)
        }
        
    }
    
    
    func likePostSuccess(timer:NSTimer)
    {
        
        var object = timer.userInfo?.valueForKey("internal") as! PFObject
        
        var postType = timer.userInfo?.valueForKey("external") as! String
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["eventObjectId"] = object["eventObjectId"] as? String
        tblFields["postObjectId"] = object["postObjectId"] as? String
        tblFields["userObjectId"] = currentUserId
        tblFields["objectId"] = object.objectId!
        tblFields["isUpdated"] = "1"
        
        var insertedId = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
        if insertedId > 0 {
            println("Record inserted Successfully")
            println("eventImage")
        } else {
            println("Record not inserted Successfully")
        }
        
        
        var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        var eventTitle = currentSharedEvent["eventTitle"] as! String
        
        var notifMessage = "\(fullUserName) liked a \(postType) in your event, \(eventTitle)."
        var userObjectId = currentSharedEvent["eventCreatorObjectId"] as! String
        
        var notificationObject = PFObject(className: "Notifications")
        notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
        notificationObject["notificationImage"] = "profilePic.png"
        notificationObject["senderId"] = currentUserId
        notificationObject["receiverId"] = userObjectId
        notificationObject["notificationActivityMessage"] = notifMessage
        notificationObject["eventObjectId"] = currentSharedEvent.objectId!
        notificationObject["notificationType"] = "likedpost"
        
        
        
        
        notificationObject.saveInBackground()

        //var createdAt = ""
        //var updatedAt = ""
        
        var postObjectId = object["postObjectId"] as! String
        var isUpdated = object["isUpdated"] as! Bool
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        var createdAt = dateFormatter.stringFromDate((object.createdAt)!)
        
        var updatedAt = dateFormatter.stringFromDate((object.updatedAt)!)
        
        var data: Dictionary<String, String!> = [
            "alert" : "\(notifMessage)",
            "notifType" :  "likedpost",
            "eventObjectId": currentSharedEvent.objectId!,
            "userObjectId": currentUserId,
            "postObjectId": postObjectId,
            "objectId" : "\(object.objectId!)",
            "isUpdated" : "\(isUpdated)",
            "createdAt": "\(createdAt)",
            "updatedAt": "\(updatedAt)",
            "badge": "Increment",
            "sound" : "default"
        ]
        
        
        
        
        var eventType = "online"
        
        if currentSharedEvent["isRSVP"] as! Bool == true
        {
            eventType = "rsvp"
        }
        
       /* var eventFolder = currentSharedEvent["eventFolder"] as! String!
        var eventImage = currentSharedEvent["eventImage"] as! String!
        
        let emailPredicate = NSPredicate(format: "objectId IN {'\(userObjectId)'}")
        
        let emailQuery = PFUser.queryWithPredicate(emailPredicate)

        
        emailQuery!.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            println(users!.count)
            if let users = users as? [PFUser]
            {
                for user in users
                {
                    var contentLike = ContentLike()
                    
                    var emailMessage = contentLike.emailMessage(currentSharedEvent.objectId!, eventTitle: currentSharedEvent["eventTitle"] as! String, imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: eventType, guestName: fullUserName)
                    
                    var sendEmailObject = SendEmail()
                    
                    sendEmailObject.sendEmail(notifMessage, message: emailMessage, emails:[user.email!])
                    
                }
            }
        }*/

        
        var predicateString: String! = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = true"
        
        sendParsePush(predicateString, data: data)
        
        
        data["sound"] = ""
        
        predicateString = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = false"
        
        sendParsePush(predicateString, data: data)

        
        refreshList()
        
    }
    
    func likePostError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        var predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        var query = PFUser.queryWithPredicate(predicate)
        
        var push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                println(objects?.count)
                if var fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        var userObjectId = object.objectId!
                        
                        var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        var query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                println(objects?.count)
                                if var fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    var query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }
    
    
    func deletePostLikeSuccess(timer:NSTimer)
    {
        
        var object = timer.userInfo?.valueForKey("internal") as! PFObject
        
        var isDeleted = ModelManager.instance.deleteTableData("PostLikes", whereString: "objectId=?", whereFields: [object.objectId!])
        if isDeleted {
            println("Record deleted Successfully")
            println("eventImage")
        } else {
            println("Record not deleted Successfully")
        }
        
        refreshList()
        
        
    }
    
    func deletePostLikeError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        
        var likeObject: PFObject = timer.userInfo?.valueForKey("external") as! PFObject
        
        var isDeleted = ModelManager.instance.deleteTableData("PostLikes", whereString: "objectId=?", whereFields: [likeObject.objectId!])
        if isDeleted {
            println("Record deleted Successfully")
            println("eventImage")
        } else {
            println("Record not deleted Successfully")
        }
        
        refreshList()
        
    }
    
    func showImageInFullScreenView(sender: UITapGestureRecognizer) {
        
        let row = sender.view!.tag
        
        var postType = mySharedEventData[row]["postType"] as! String!
        
        if(postType == "image")
        {
            let manager = NSFileManager.defaultManager()
            
            var eventStreamImageFile = mySharedEventData[row]["postData"] as! String
            
            var eventStreamImagePath = "\(documentDirectory)/\(eventStreamImageFile)"
            
            if (manager.fileExistsAtPath(eventStreamImagePath))
            {
                var image = UIImage(named: eventStreamImagePath)
                
                var eventPhotoFullScreenVC = self.storyboard?.instantiateViewControllerWithIdentifier("EventPhotoFullScreenViewController") as! EventPhotoFullScreenViewController
                eventPhotoFullScreenVC.image = image
                self.navigationController?.pushViewController(eventPhotoFullScreenVC, animated: false)
            }
            else
            {
                var refreshAlert = UIAlertController(title: "Error", message: "Please wait while the image is downloading.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                    
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        }
    }
    
    func playSelectedVideo(sender : UIButton) {
        var videoUrl: String = (mySharedEventData[sender.tag]["postData"] as? String)!
        
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath("\(documentDirectory)/\(videoUrl)"))
        {
            moviePlayer?.contentURL = NSURL(fileURLWithPath: "\(documentDirectory)/\(videoUrl)")
            moviePlayer?.view.hidden = false
            moviePlayer!.setFullscreen(true, animated: true)
            //moviePlayer!.scalingMode = .AspectFill
            moviePlayer!.controlStyle = .Embedded
            moviePlayer!.play()
        }
        else
        {
            var refreshAlert = UIAlertController(title: "Error", message: "Video doesn't exist.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    func getFirstFrame(postImageView: UIImageView, videoURL: NSURL, viewTop: CGFloat)
    {
        var asset : AVAsset = AVAsset.assetWithURL(videoURL) as! AVAsset
        
        var assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        var error       : NSError? = nil
        var time        : CMTime = CMTimeMake(1, 30)
        var img         : CGImageRef = assetImgGenerate.copyCGImageAtTime(time, actualTime: nil, error: &error)
        var frameImg    : UIImage = UIImage(CGImage: img)!
        
        
        postImageView.image = frameImg
        
    }
    
    
    @IBAction func closeImagePreview(sender: AnyObject!)
    {
        selectedImageWrapper.hidden = true
    }
    
    
    @IBAction func attendingButtonClicked(sender: UIButton)
    {
        //updateStatus("yes")
        statusToBeChanged = "yes"
        
        messageTitle.text = "Awesome! Glad you can attend. Tell your host how many from your side will be attending the event."
        messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        noOfAdultsWrapper.hidden = false
        noOfChildWrapper.hidden = false
        
        adultsLabel.hidden = false
        childLabel.hidden = false
        
        messagePopupView.hidden = false
        
        
        var noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        var noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        invitationNoteView.frame.origin.y = (self.view.frame.height*259)/568
    }
    
    
    @IBAction func maybeButtonClicked(sender: UIButton)
    {
        //updateStatus("maybe")
        statusToBeChanged = "maybe"
        
        messageTitle.text = "You haven't decided yet. That's alright. Want to leave a message to the host?"
        messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        var noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        var noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        invitationNoteView.frame.origin.y = (self.view.frame.height*110)/568
        
        noOfAdultsWrapper.hidden = true
        noOfChildWrapper.hidden = true
        
        adultsLabel.hidden = true
        childLabel.hidden = true
        
        messagePopupView.hidden = false
    }
    
    
    @IBAction func notAttendingButtonClicked(sender: UIButton)
    {
        //updateStatus("no")
        statusToBeChanged = "no"
        
        messageTitle.text = "Bummer! It's sad you can't make it. Want to leave a message to the host?"
        messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        var noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        var noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        invitationNoteView.frame.origin.y = (self.view.frame.height*110)/568
        
        noOfAdultsWrapper.hidden = true
        noOfChildWrapper.hidden = true
        
        adultsLabel.hidden = true
        childLabel.hidden = true
        
        messagePopupView.hidden = false
    }
    
    
    @IBAction func onlineButtonClicked(sender: UIButton)
    {
        //updateStatus("online")
        statusToBeChanged = "online"
        
        messageTitle.text = "Great! You are going to follow the event online. Want to leave a message to the host?"
        messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        var noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        var noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        invitationNoteView.frame.origin.y = (self.view.frame.height*110)/568
        
        noOfAdultsWrapper.hidden = true
        noOfChildWrapper.hidden = true
        
        adultsLabel.hidden = true
        childLabel.hidden = true
        
        messagePopupView.hidden = false
        
    }
    
    
    @IBAction func goButtonClicked(sender: UIButton)
    {
        updateStatus(statusToBeChanged)
        
    }
    
    
    @IBAction func reselectStatusButtonClicked(sender: UIButton)
    {
        
        openMeassageButtonView.hidden = true
        
        statusButtonsView.hidden = false
        statusInfoView.hidden = true
    }
    
    
    @IBAction func editStatusInfoButtonClicked(sender: UIButton)
    {
        
        if currentStatus == "yes"
        {
            
            noOfAdultsWrapper.hidden = false
            noOfChildWrapper.hidden = false
            
            adultsLabel.hidden = false
            childLabel.hidden = false
            
            messagePopupView.hidden = false
            
            messageTitle.text = "Awesome! Glad you can attend. Tell your host how many from your side will be attending the event."
            messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            
            invitationNoteView.frame.origin.y = (self.view.frame.height*259)/568
        }
        else
        {
            noOfAdultsWrapper.hidden = true
            noOfChildWrapper.hidden = true
            
            adultsLabel.hidden = true
            childLabel.hidden = true
            
            messagePopupView.hidden = false
            
            messageTitle.text = "Your message to the host."
            messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            
            invitationNoteView.frame.origin.y = (self.view.frame.height*100)/568
        }
        
        
        var noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        var noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
    }
    
    
    @IBAction func closePopupButtonClicked(sender: UIButton)
    {
        messagePopupView.hidden = true
        
        
        if currentStatus != ""
        {
            
            if currentStatus == "yes"
            {
                var noOfAdults = currentSharedEvent["noOfAdults"] as! Int
                var noOfChilds = currentSharedEvent["noOfChilds"] as! Int
                
                statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
                attendingStatusImageView.image = UIImage(named:"check-green.png")
            }
            
            if currentStatus == "online"
            {
                statusInfo.text = "I will follow online"
                attendingStatusImageView.image = UIImage(named:"web-globe.png")
            }
            
            if currentStatus == "maybe"
            {
                statusInfo.text = "I am not sure"
                attendingStatusImageView.image = UIImage(named:"questionmark.png")
            }
            
            if currentStatus == "no"
            {
                statusInfo.text = "I will not attend"
                attendingStatusImageView.image = UIImage(named:"cross-red.png")
            }
            
            
            openMeassageButtonView.hidden = false
            statusButtonsView.hidden = true
            statusInfoView.hidden = false
        }
        else
        {
            
            openMeassageButtonView.hidden = true
            statusButtonsView.hidden = false
            statusInfoView.hidden = true
        }
        
        hideKeyBoard()
        
    }
    
    
    func updateStatus(status: String)
    {
        var invitationObject: PFObject!
        invitationObject = PFObject(className: "Invitations")
        invitationObject.objectId = currentSharedEvent["invitationId"] as? String
        invitationObject["attendingStatus"] = status
        invitationObject["isUpdated"] = true
        
        var noOfAdults = noOfAdultsText.text!.toInt()
        var noOfChilds = noOfChildText.text!.toInt()
        
        invitationObject["noOfAdults"] = noOfAdults
        invitationObject["noOfChilds"] = noOfChilds
        
        invitationObject["invitationNote"] = invitationNoteView.text!
        
        if ((noOfAdults! + noOfChilds!) > 0 && status == "yes") || (status != "yes")
        {
            ParseOperations.instance.saveData(invitationObject, target: self, successSelector: "updateStatusSuccess:", successSelectorParameters: status, errorSelector: "updateStatusError:", errorSelectorParameters: nil)
        }
        else
        {
            var refreshAlert = UIAlertController(title: "Error", message: "Please enter atleast one Adult or Child.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    func updateStatusSuccess(timer:NSTimer)
    {
        
        var invitation = timer.userInfo?.valueForKey("internal") as! PFObject
        var status = timer.userInfo?.valueForKey("external") as! String
        
        //println("Successfully retrieved \(object!.count) posts.")
        
        
        var noOfAdults = invitation["noOfAdults"] as! Int
        var noOfChilds = invitation["noOfChilds"] as! Int
        var invitationNote = invitation["invitationNote"] as! String
        
        var tblFields: Dictionary! = [String: String]()
       
        tblFields["attendingStatus"] = status
        tblFields["noOfAdults"] = "\(noOfAdults)"
        tblFields["noOfChilds"] = "\(noOfChilds)"
        
        tblFields["invitationNote"] = invitationNote
        
        var isUpdated = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitation.objectId!])
        if isUpdated {
            println("Record Updated Successfully")
            println("eventImage")
        } else {
            println("Record not Updated Successfully")
        }
        

        
        var userObjectId = currentSharedEvent["eventCreatorObjectId"] as! String

        
        var noOfChildNew = currentSharedEvent["noOfChilds"] as! Int
        var noOfAdultsNew = currentSharedEvent["noOfAdults"] as! Int
        
        let predicate = NSPredicate(format: "objectId IN {'\(userObjectId)'}")
        
        var query = PFQuery(className:"User", predicate: predicate)

        var eventType = "online"
        
        if currentSharedEvent["isRSVP"] as! Bool == true
        {
            eventType = "rsvp"
        }
        
        var eventFolder = currentSharedEvent["eventFolder"] as! String!
        var eventImage = currentSharedEvent["eventImage"] as! String!
        
        let emailPredicate = NSPredicate(format: "objectId IN {'\(userObjectId)'}")
        
        let emailQuery = PFUser.queryWithPredicate(emailPredicate)
        
        var currentStatusTemp = self.currentStatus
        
        emailQuery!.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            println(users!.count)
            if let users = users as? [PFUser]
            {
                for user in users
                {
                    self.fetchHostDataSuccess(user, attendees: [noOfAdults, noOfChilds, noOfAdultsNew, noOfChildNew], currentStatus: currentStatusTemp,invitation:invitation,invitationNote:invitationNote)
                }
            }
        }
        
        
        
        var notifMessage = ""
        
        if currentStatus == ""
        {
            notifMessage = "\(fullUserName) responded to the invitation for your event, \(eventTitle)."
            
        }
        else
        {
            if statusToBeChanged == "yes" && currentStatus == "yes"
            {
                if (currentSharedEvent["noOfAdults"] as! Int) != noOfAdults || (currentSharedEvent["noOfChilds"] as! Int) != noOfChilds
                {
                    notifMessage = "\(fullUserName) changed guest count for your event, \(eventTitle)."
                    
                }
            }
            else if statusToBeChanged != currentStatus
            {
                notifMessage = "\(fullUserName) changed status for your event, \(eventTitle)."
            }
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        //var createdAt = dateFormatter.stringFromDate((invitation.createdAt)!)
        
        //var updatedAt = dateFormatter.stringFromDate((invitation.updatedAt)!)

        var createdAt = ""
        
        var updatedAt = ""

        
        var eventObjectId = currentSharedEvent.objectId!
        
        var email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String

        var eventCreatorId = currentSharedEvent["eventCreatorObjectId"] as! String
        
        if notifMessage != ""
        {
            var data: Dictionary<String, String!> = [
                "alert" : "\(notifMessage)",
                "notifType" :  "invitationresponse",
                "userObjectId": "\(currentUserId)",
                "eventObjectId": "\(eventObjectId)",
                "objectId": "\(invitation.objectId!)",
                "invitedName": "\(fullUserName)",
                "attendingStatus" : "\(status)",
                "noOfChilds": "\(noOfChilds)",
                "noOfAdults": "\(noOfAdults)",
                "invitationNote": "\(invitationNote)",
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "isUpdated": "\(isUpdated)",
                "emailId": "\(email)",
                "badge": "Increment",
                "sound" : "default",
                "eventCreatorId" : "\(eventCreatorId)"
            ]
            
            println(data)
            
            
            var notificationObject = PFObject(className: "Notifications")
            notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
            notificationObject["notificationImage"] = "profilePic.png"
            notificationObject["senderId"] = currentUserId
            notificationObject["receiverId"] = userObjectId
            notificationObject["notificationActivityMessage"] = notifMessage
            notificationObject["eventObjectId"] = currentSharedEvent.objectId!
            notificationObject["notificationType"] = "invitationresponse"
            
            notificationObject.saveInBackground()


            var predicateString: String! = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = true"
            
            sendParsePush(predicateString, data: data)
            
            
            data["sound"] = ""
            
            predicateString = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = false"
            
            sendParsePush(predicateString, data: data)
           
        }
        
        
        currentSharedEvent["noOfAdults"] = noOfAdults
        currentSharedEvent["noOfChilds"] = noOfChilds
        
        currentSharedEvent["invitationNote"] = invitation["invitationNote"] as! String
        
        
        messagePopupView.hidden = true
        
        currentStatus = statusToBeChanged
        
        if currentStatus == "yes"
        {
            statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
            attendingStatusImageView.image = UIImage(named:"check-green.png")
        }
        
        if currentStatus == "online"
        {
            statusInfo.text = "I will follow online"
            attendingStatusImageView.image = UIImage(named:"web-globe.png")
        }
        
        if currentStatus == "maybe"
        {
            statusInfo.text = "I am not sure"
            attendingStatusImageView.image = UIImage(named:"questionmark.png")
        }
        
        if currentStatus == "no"
        {
            statusInfo.text = "I will not attend"
            attendingStatusImageView.image = UIImage(named:"cross-red.png")
        }
        
        
        openMeassageButtonView.hidden = false
        statusButtonsView.hidden = true
        statusInfoView.hidden = false
        
    }
    
    func updateStatusError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    
    
    func getAttendingStatus(userevent: PFObject) -> String
    {
        var statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        var attendingStatus = statusResultSet.stringForColumn("attdendingStatus")
        
        statusResultSet.close()
        
        return attendingStatus
    }
    
    
    func fetchHostDataSuccess(user:PFUser, attendees: [Int], currentStatus: String,invitation:PFObject,invitationNote:String)
    {
        
        
        var hostEmail = user.email!
                
        println("host email: \(hostEmail)")
        
        var email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        //var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        //var eventTitle = currentSharedEvent["eventTitle"] as! String
        
        var eventLatitude = currentSharedEvent["eventLatitude"] as! Double
        var eventLongitude = currentSharedEvent["eventLongitude"] as! Double
        
        var createdAt = ""
        var updatedAt = ""
        
        var localNotification = UILocalNotification()
        
        localNotification.fireDate = NSDate()
        
        var notifMessage = ""
        
        println(hostEmail)
        //
        var eventCreatorId = currentSharedEvent["eventCreatorObjectId"] as! String
        
        let data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "invitationresponse",
            "eventObjectId": currentSharedEvent.objectId!,
            "objectId": "\(invitation.objectId!)",
            "invitedName": "\(fullUserName)",
            "attendingStatus" : "\(currentStatus)",
            "noOfChilds": "\(attendees[1])",
            "noOfAdults": "\(attendees[0])",
            "invitationNote": "\(invitationNote)",
            "createdAt": "\(createdAt)",
            "updatedAt": "\(updatedAt)",
            "isUpdated": "\(isUpdated)",
            "emailId": "\(email)",
            "userObjectId": "\(currentUserId)",
            "eventCreatorId" : "\(eventCreatorId)"
        ]
        
        
        var urlString = String()
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                println(url!)
                
                urlString = url!
                
                if currentStatus == ""
                {
                    notifMessage = "\(self.fullUserName) responded to the invitation for your event, \(self.eventTitle)."
                    
                    if self.statusToBeChanged == "yes"
                    {
                        //
                        
                        var attending = Attending()
                        
                        var emailMessage = attending.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, adultsCount: attendees[0], childsCount: attendees[1], type: "rsvp",url:urlString)
                        
                        var sendEmailObject = SendEmail()
                        
                        
                        sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                        
                        
                    }
                    if self.statusToBeChanged == "online"
                    {
                        var online = OnlineOnly()
                        
                        var emailMessage = online.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:urlString)
                        
                        var sendEmailObject = SendEmail()
                        
                        
                        sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                        
                        
                    }
                    
                    if self.statusToBeChanged == "maybe"
                    {
                        var mayBe = Maybe()
                        
                        var emailMessage = mayBe.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:urlString)
                        
                        var sendEmailObject = SendEmail()
                        
                        //
                        sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                        
                        
                    }
                    if self.statusToBeChanged == "no"
                    {
                        var notAttending = NotAttending()
                        
                        var emailMessage = notAttending.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:urlString)
                        
                        var sendEmailObject = SendEmail()
                        
                        //
                        
                        
                        sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                        
                        
                    }
                    
                }
                else
                {
                    if self.statusToBeChanged == "yes" && currentStatus == "yes"
                    {
                        if attendees[2] != attendees[0] || attendees[3] != attendees[1]
                        {
                            notifMessage = "\(self.fullUserName) changed guest count for your event, \(self.eventTitle)."
                            
                            var guestListChange = GuestListChange()
                            
                            var emailMessage = guestListChange.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, oldAdultCount: attendees[2], oldChildsCount: attendees[3], newAdultCount: attendees[0], newChildCount: attendees[1], type: "rsvp",url:urlString)
                            
                            var sendEmailObject = SendEmail()
                            
                            sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                            
                            println(hostEmail)
                        }
                    }
                    else if self.statusToBeChanged != currentStatus
                    {
                        var prevoiusStatus = ""
                        var newStatus = ""
                        
                        if currentStatus == "yes"
                        {
                            prevoiusStatus = "Attending"
                        }
                        
                        if currentStatus == "no"
                        {
                            prevoiusStatus = "Not attending"
                        }
                        
                        if currentStatus == "maybe"
                        {
                            prevoiusStatus = "May be"
                        }
                        
                        if currentStatus == "online"
                        {
                            prevoiusStatus = "Online"
                        }
                        
                        
                        if self.statusToBeChanged == "yes"
                        {
                            newStatus = "Attending"
                        }
                        
                        if self.statusToBeChanged == "no"
                        {
                            newStatus = "Not attending"
                        }
                        
                        if self.statusToBeChanged == "maybe"
                        {
                            newStatus = "May be"
                        }
                        
                        if self.statusToBeChanged == "online"
                        {
                            newStatus = "Online"
                        }
                        
                        var guestResponseChange = GuestResponseChange()
                        
                        var emailMessage = guestResponseChange.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, prevoiusStatus: prevoiusStatus, guestName: self.fullUserName, newStatus: newStatus, type: "rsvp",url:urlString)
                        //
                        
                        
                        var sendEmailObject = SendEmail()
                        
                        
                        sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                        
                        
                    }
                }
                
                if self.statusToBeChanged == "yes"
                {
                    localNotification.alertBody = "Hooray! You are going to \(self.eventTitle)"
                    
                    //
                    var attendingInviteResponse = AttendingInviteResponseGuest()
                    
                    var attendingEmailMessage = attendingInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: self.dateStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), timeString: self.timeStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                    
                    var sendEmailObject = SendEmail()
                    
                    sendEmailObject.sendEmail("Hooray! You are attending an event.", message: attendingEmailMessage, emails: [email])
                    
                    
                }
                if self.statusToBeChanged == "online"
                {
                    localNotification.alertBody = "Cheers! You can’t make it to \(self.eventTitle), but you will follow the event online."
                    
                    var onlineInviteResponse = OnlineOnlyInviteResponseGuest()
                    
                    var onlineEmailMessage = onlineInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: self.dateStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), timeString: self.timeStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                    //
                    var sendEmailObject = SendEmail()
                    
                    sendEmailObject.sendEmail("You are going to follow this event online using the iOS app.", message: onlineEmailMessage, emails: [email])
                }
                if self.statusToBeChanged == "maybe"
                {
                    localNotification.alertBody = " Hmmm! You are not sure whether you can attend \(self.eventTitle). Don’t forget to change your status once you know more."
                    
                    var maybeInviteResponse = MaybeInviteResponseGuest()
                    
                    var maybeEmailMessage = maybeInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: self.dateStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), timeString: self.timeStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                    
                    var sendEmailObject = SendEmail()
                    
                    sendEmailObject.sendEmail("You are not sure you can attend this event.", message: maybeEmailMessage, emails:[email] )
                    
                }
                if self.statusToBeChanged == "no"
                {
                    localNotification.alertBody = "Bummer! You are not going to \(self.eventTitle). Don’t forget, you can change your status anytime before the event starts."
                    
                    var notAttendingInviteResponse = NotAttendingInviteResponseGuest()
                    
                    var notAttendingEmailMessage = notAttendingInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: self.dateStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), timeString: self.timeStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                    
                    var sendEmailObject = SendEmail()
                    
                    sendEmailObject.sendEmail("Bummer! You cannot attend this event.", message: notAttendingEmailMessage, emails:[email] )
                }
                
                UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                
            }
            
        })
    }
    
    func fetchHostDataError(timer:NSTimer)
    {
        
    }
    
    func dateStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitWeekday | .CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: sdate)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        let sweekday = scomponents.weekday
        
        var dateString = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        
        return dateString
    }
    
    
    func timeStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitWeekday | .CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        var timeString = "\(shour):\(sminute) \(sam)"
        
        return timeString
    }
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        var normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    
    private func playVideo(url: NSURL) {
        if let
            moviePlayer = MPMoviePlayerController(contentURL: url) {
                self.moviePlayer = moviePlayer
                moviePlayer.view.frame = CGRectMake(20, 20, 280, 400)
                moviePlayer.prepareToPlay()
                moviePlayer.shouldAutoplay = false
                moviePlayer.scalingMode = .AspectFill
                moviePlayer.controlStyle = .None
                self.view.addSubview(moviePlayer.view)
        } else {
            debugPrintln("Oops, something wrong when playing video.m4v")
        }
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewShouldReturn(textView: UITextView) -> Bool
    {
        textView.resignFirstResponder()
        return true
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if statusToBeChanged == "yes"
        {
            invitationNoteView.frame.size.height = invitationNoteViewHeight/2
        }
        else
        {
            invitationNoteView.frame.size.height = invitationNoteViewHeight
        }
        
        return true
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool
    {
        
        invitationNoteView.frame.size.height = invitationNoteViewHeight
        
        return true
    }
    
    
    @IBAction func hideKeyBoard()
    {
        noOfChildText.resignFirstResponder()
        noOfAdultsText.resignFirstResponder()
        invitationNoteView.resignFirstResponder()
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return mySharedEventData.count
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        
        let row = section.row
        
        var postType = mySharedEventData[row]["postType"] as! String!
        
        var extraHeight: CGFloat = 0.0
        
        if postType == "text"
        {
            //extraHeight = 10.0
        }
        return mySharedRowHeights[row] + extraHeight
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        
        let row = indexPath.row
        
        var postType = mySharedEventData[row]["postType"] as! String!
        
        var cellIdentifier: String! = "SharedEventPhotoCell1"
        
        
        var cell: SharedPhotosTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? SharedPhotosTableViewCell
        
        
        if (cell == nil)
        {
            cell = SharedPhotosTableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: cellIdentifier)
        }
        
        
        for view in cell!.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        var viewTop: CGFloat = 0
        
        if row == 0 && postType == "text"
        {
            viewTop = (0/568)*self.view.frame.height
        }
        
        var heightDiff: CGFloat = 0
        
        var postTextView = UITextView()
        var textBackgroundView = UIView()
        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 8
        let attributes = [NSParagraphStyleAttributeName : style]
        
        var attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(20),
            NSForegroundColorAttributeName : UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        if(postType == "text")
        {
            var postTextClickableView = UIView()
            
            postTextView.text = mySharedEventData[row]["postData"] as! String
            

            
            postTextView.attributedText = NSAttributedString(string:postTextView.text, attributes:attributes)
            postTextView.font = UIFont(name:"Tigerlily", size: 20.0)
            postTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            
            
            postTextView.editable = false
            postTextView.selectable = false
            postTextView.scrollEnabled = false
            
            var postText = mySharedEventData[row]["postData"] as! String
            
            
           /* var charCount: CGFloat = CGFloat(count(postText))
            
            var postTextHeight: CGFloat = 320
            
            var trowCount: CGFloat = (charCount/18) + 2
            
            if( trowCount < 11)
            {
                postTextHeight = trowCount*29
                heightDiff = 320-(trowCount*29)
            }*/
            
            var style = NSMutableParagraphStyle()
            style.lineSpacing = 8
            let attributes = [NSParagraphStyleAttributeName : style]
            
            
            var attrs = [
                NSFontAttributeName : UIFont.systemFontOfSize(20.0),
                NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
                NSUnderlineStyleAttributeName : 1]
            
            
            var senderMessageTemp = UITextView()
            
            
            
            senderMessageTemp.frame.size.width = self.view.frame.width-self.view.frame.width*(40.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
            
            senderMessageTemp.text = postText
            
            senderMessageTemp.attributedText = NSAttributedString(string:postText, attributes:attributes)
            
            senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20.0)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            postTextView.frame.size.width = cell!.contentView.frame.width-self.view.frame.width*(20.0/320)
            postTextView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop+((20.0/568)*self.view.frame.height), cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height)
            
            postTextView.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 0)
            
            postTextClickableView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop, cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height)
            
            
            textBackgroundView.frame = CGRectMake(0, viewTop, cell!.contentView.frame.width, contentSize.height+((20.0/568)*self.view.frame.height))
            
            textBackgroundView.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            cell!.contentView.addSubview(textBackgroundView)
            cell!.contentView.addSubview(postTextView)
            cell!.contentView.addSubview(postTextClickableView)
        }
        
        if row == 0
        {
            //heightDiff = heightDiff-45
        }
        
        var rheight = mySharedEventData[row]["postHeight"] as! CGFloat
        var rwidth = mySharedEventData[row]["postWidth"] as! CGFloat
        
        var postImageView = UIImageView()
        
        if(rheight>0 && rwidth>0)
        {
            
            postImageView.frame.size.width = self.tableView.frame.width
            postImageView.frame.size.height = (rheight/rwidth)*self.tableView.frame.width
            
            postImageView.frame.origin.x = 0
            postImageView.frame.origin.y = viewTop
            
            println(postImageView.frame.size.width)
            println(postImageView.frame.size.height)
            println(postImageView.frame.origin.x)
            println(postImageView.frame.origin.y)
            
        }
        if(postType == "image")
        {
            
            var imageName = mySharedEventData[row]["postData"] as! String
            
            var eventFolder = mySharedEventData[row]["eventFolder"] as! String
            
            var eventImagePath = "\(documentDirectory)/\(imageName)"
            
            println(eventImagePath)
            
            cell!.contentView.addSubview(postImageView)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventImagePath))
            {
                var image = UIImage(named: eventImagePath)

                postImageView.image = image

                //cell!.contentView.addSubview(postImageView)
            }
            else
            {
                /*var isDownloading = mySharedEventData[row]["isDownloading"] as! Bool
                println("isDownloading for \(row) is \(isDownloading)")
                
                if mySharedEventData[row]["isDownloading"] as! Bool == true
                {*/
                    //mySharedEventData[row]["isDownloading"] = false
                    
                    let s3BucketName = "eventnode1"
                    let fileName = imageName
                    
                    let downloadFilePath = documentDirectory.stringByAppendingPathComponent(fileName)
                    let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                    
                    let downloadRequest = AWSS3TransferManagerDownloadRequest()
                    downloadRequest.bucket = s3BucketName
                    downloadRequest.key  = "\(eventFolder)\(fileName)"
                    downloadRequest.downloadingFileURL = downloadingFileURL
                    
                    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                    
                    transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                        
                        if (task.error != nil){
                            if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                                switch (task.error.code) {
                                case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                    break;
                                case AWSS3TransferManagerErrorType.Paused.rawValue:
                                    break;
                                    
                                default:
                                    println("error downloading")
                                    break;
                                }
                            } else {
                                // Unknown error.
                                println("error downloading")
                            }
                        }
                        
                        if (task.result != nil) {
                            println("downloading successfull")
                            
                            var image = UIImage(named: "\(documentDirectory)/\(imageName)")
                            
                            postImageView.image = image
                            
                        }
                        
                        return nil
                    
                    })

                    
                //}
            }
        }
        
        var postPlayButton = UIButton()
        
        if(postType == "video")
        {
            
            var videoName = mySharedEventData[row]["postData"] as! String
            
            var eventFolder = mySharedEventData[row]["eventFolder"] as! String
            
            var eventVideoPath = "\(documentDirectory)/\(videoName)"
            

            
            postPlayButton.enabled = false
            
            cell!.contentView.addSubview(postImageView)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventVideoPath)) {
                postPlayButton.enabled = true
                
                postPlayButton.frame = postImageView.frame
                
                postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
                
                postPlayButton.tag = indexPath.row
                postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
                
                
                var videoUrl = NSURL(fileURLWithPath: eventVideoPath)
                self.getFirstFrame(postImageView, videoURL: videoUrl!, viewTop: viewTop)
                
                //cell!.contentView.addSubview(postPlayButton)
            }
            else
            {
                let s3BucketName = "eventnode1"
                let fileName = videoName
                
                let downloadFilePath = documentDirectory.stringByAppendingPathComponent(fileName)
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
                
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                println("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            println("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        
                        println("downloading successfull")
                        
                        postPlayButton.enabled = true
                        
                        postPlayButton.frame = postImageView.frame
                        
                        postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
                        postPlayButton.tag = indexPath.row
                        postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
                        
                        var videoUrl = NSURL(fileURLWithPath: eventVideoPath)
                        
                        self.getFirstFrame(postImageView, videoURL: videoUrl!, viewTop: viewTop)
                        
                    }
                    
                    return nil
                    
                })
            }
            
            
        }
        
        
        /*var pdate: NSDate!
        
        if(mySharedEventData[row].createdAt != nil)
        {
            pdate = mySharedEventData[row].createdAt
        }
        else
        {
            pdate = mySharedEventData[row]["dateCreated"] as! NSDate
        }
        
        
        println(pdate)
        
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: pdate!)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        var postDate = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        */
        
        var resultSetTotalCount: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["count(*) as count"], whereString: "postObjectId=?", whereFields: [mySharedEventData[row].objectId!])
        
        resultSetTotalCount.next()
        
        var totalLikeCount = resultSetTotalCount.intForColumn("count")
        resultSetTotalCount.close()
        
        
        var infoView = UIView()
        var likeButton = UIButton()
        
        var likeButtonWrapper = UIView()
        
        // Like Button Wrapper overlays postImageView entirely, so the click/tap is delivered to the wrapper instead of the image view.
        // Hence, adding gesture recognizer to wrapper. 
        // Ideally the wrapper should contain the image, like button and play button making it a contentWrapper as opposed to a likeButtonWrapper.
        var imageTapRecognizer = UITapGestureRecognizer(target: self, action: "showImageInFullScreenView:")
        likeButtonWrapper.addGestureRecognizer(imageTapRecognizer)
        likeButtonWrapper.tag = row
        likeButtonWrapper.userInteractionEnabled = true

        if likeObjectIds[row] != ""
        {
            likeButton.setImage(UIImage(named:"heart-circle.png"), forState: UIControlState.Normal)
        }
        else
        {
            likeButton.setImage(UIImage(named:"unlike.png"), forState: UIControlState.Normal)
        }
        
        
        likeButton.addTarget(self, action:"likeSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
        
        likeButton.tag = indexPath.row
        
        
        if rheight>0 && rwidth>0
        {
            println(postImageView.frame)
            
            //postImageView.frame.height-heightDiff+((10/568)*self.view.frame.height)
            likeButtonWrapper.frame = CGRectMake(0, 0, self.view.frame.width, postImageView.frame.height)
            likeButtonWrapper.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha:0)
            likeButton.frame = CGRectMake((postImageView.frame.width)-((75/568)*self.view.frame.height), (postImageView.frame.height)-((75/568)*self.view.frame.height), (45/568)*self.view.frame.height, (45/568)*self.view.frame.height)
            likeButton.alpha = 0.7
            
            infoView.frame = CGRectMake(0, postImageView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
        }
        else
        {
            //
            //likeButtonWrapper.frame = CGRectMake(0, (cell!.contentView.frame.width-heightDiff+((33/568)*self.view.frame.height))-((45/568)*self.view.frame.height)-((5/568)*self.view.frame.height), self.view.frame.width, (45/568)*self.view.frame.height)
            
            likeButtonWrapper.frame = CGRectMake(0, textBackgroundView.frame.height+viewTop, self.view.frame.width, (55/568)*self.view.frame.height)
            likeButtonWrapper.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            //likeButtonWrapper.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            likeButton.frame = CGRectMake(self.view.frame.width-((75/320)*self.view.frame.width), 0, (45/568)*self.view.frame.height, (45/568)*self.view.frame.height)
            
            infoView.frame = CGRectMake(0, textBackgroundView.frame.height + ((55/568)*self.view.frame.height) + viewTop, cell!.contentView.frame.width, (0.120625)*380)
        }

        if(postType == "video")
        {
            likeButtonWrapper.addSubview(postPlayButton)
        }
        
        likeButtonWrapper.addSubview(likeButton)
        
        println(likeButton.frame)
        println(infoView.frame)
        
        var deleteButton = UIButton()
        
        deleteButton.setTitle ("Delete", forState: UIControlState.Normal)
        deleteButton.titleLabel!.font =  UIFont(name:"AvenirNext-Medium" , size: 12)
        
        
        deleteButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        
        
        deleteButton.frame = CGRectMake((self.view.frame.width)*11/320, 0, self.view.frame.width/5,deleteButton.sizeThatFits(deleteButton.bounds.size).height)
        deleteButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        
        deleteButton.tag = indexPath.row
        
        deleteButton.addTarget(self, action:"deleteSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)

        // Add More Button
        var moreImageView = UIImageView()
        
        moreImageView.frame = CGRectMake(0, 12, 30, 8)
        
        moreImageView.image = UIImage(named:"more.png")
        
        var moreButton = UIButton()
        //shareButton.backgroundColor = UIColor.redColor()
        
        moreButton.addSubview(moreImageView)
        
//        moreButton.frame = CGRectMake(15, 0, 11, moreButton.sizeThatFits(moreButton.bounds.size).height)
        
        moreButton.frame = CGRectMake(self.view.frame.width - 15 - moreButton.sizeThatFits(moreButton.bounds.size).width, 0, 30,moreButton.sizeThatFits(moreButton.bounds.size).height)
        
        moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        
        moreButton.tag = indexPath.row
        
        moreButton.addTarget(self, action:"showMoreOptions:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // Like Button
        var likeButtonSmall = UIButton()
        
        likeButtonSmall.setTitle ("", forState: UIControlState.Normal)
        
        likeButtonSmall.frame = CGRectMake(15, 0, (0.120625)*380,(0.120625)*380)
        
        likeButtonSmall.tag = indexPath.row
        
        //likeButton.addTarget(self, action:"likeSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
        
        var postLikeText = UILabel()
        
        postLikeText.textAlignment = NSTextAlignment.Right
        postLikeText.text = "\(totalLikeCount) Loved it"
        
        
        postLikeText.textColor = UIColor.grayColor()
        postLikeText.backgroundColor = UIColor.clearColor()
        postLikeText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        postLikeText.textAlignment = .Right
        
        postLikeText.frame = CGRectMake(30, (deleteButton.frame.height/2)-(postLikeText.sizeThatFits(postLikeText.bounds.size).height/2), postLikeText.sizeThatFits(postLikeText.bounds.size).width, postLikeText.sizeThatFits(postLikeText.bounds.size).height)
        
        
        
        println("likeheight: \(postLikeText.frame.height)")
        
        var likeImageView = UIImageView()
        
        likeImageView.image = UIImage(named:"heart.png")
        
        likeImageView.frame = CGRectMake(15, (moreButton.frame.height/2)-(postLikeText.frame.height*3/8), postLikeText.frame.height*3/4, postLikeText.frame.height*3/4)
        
        infoView.addSubview(likeImageView)
        infoView.addSubview(likeButtonSmall)
        infoView.addSubview(postLikeText)
        infoView.addSubview(moreButton)
        
        cell!.contentView.addSubview(likeButtonWrapper)
        cell!.contentView.addSubview(infoView)
        
        //var sepImageView = UIImageView()
        var sepImageView = UIView()
        
        //sepImageView.frame = CGRectMake((11/320)*self.view.frame.width, (infoView.frame.origin.y+infoView.frame.height-((2/568)*self.view.frame.height)), (298/320)*self.view.frame.width,(1/568)*self.view.frame.height)
        //sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height-((2/568)*self.view.frame.height)), self.view.frame.width,(1/568)*self.view.frame.height)
        
        //sepImageView.image = UIImage(named:"sep-line.png")
        
        if(postType == "text")
        {
            //sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height+((18/568)*self.view.frame.height)), self.view.frame.width,(1/568)*self.view.frame.height)
            sepImageView.frame = CGRectMake(0.4*self.view.frame.width, (infoView.frame.origin.y+infoView.frame.height-((7/568)*self.view.frame.height)), 0.2*self.view.frame.width,1)
        }
        else
        {
            sepImageView.frame = CGRectMake(0.4*self.view.frame.width, (infoView.frame.origin.y+infoView.frame.height-((7/568)*self.view.frame.height)), 0.2*self.view.frame.width,1)
        }
        
        sepImageView.backgroundColor = UIColor(red: 155.0/255, green: 155.0/255, blue: 155.0/255, alpha: 1)
        
        cell!.contentView.addSubview(sepImageView)
        
        cell?.selectionStyle = .None
        return cell!
    }
    
    func showMoreOptions(sender: UIButton) {
        println("More button clicked")
        
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Share")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        println("ActionSheet clickedButtonAtIndex \(buttonIndex)")
        switch (buttonIndex){
        case 0:
            println("Cancel")
            // Nothing to do. Popup menu closes.
        case 1:
            println("Share")
            shareSelectedPost(actionSheet.tag)
        default:
            println("Default")
            // Should not happen here.
        }
    }
    
    // TODO(geetikak): Refactor common code in EventPhotosVC and StreamVC based on role and permission model.
    // This is to support share content to various social networks, mail, messages, etc.
    func shareSelectedPost(row: Int) {
        var postType = mySharedEventData[row]["postType"] as! String!
        
        println("Share Selected Post \(postType) \(row)")
        
        if(postType == "text") {
            var textPostToShare : String = mySharedEventData[row]["postData"] as! String
            var objectsToShare : [String] = [textPostToShare]
            
            let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
            self.navigationController!.presentViewController(activityVC,
                animated: true,
                completion: nil)
        }
        
        if(postType == "image" || postType == "video") {
            var fileName = mySharedEventData[row]["postData"] as! String
            
            var eventFolder = mySharedEventData[row]["eventFolder"] as! String
            
            var eventMediaFilePath = "\(documentDirectory)/\(fileName)"
            
            println(eventMediaFilePath)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventMediaFilePath)) {
                
                if (postType == "image") {
                    var shareImage = UIImage(named: eventMediaFilePath)
                    
                    var objectsToShare : [UIImage] = [shareImage!]
                    
                    let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
                    self.navigationController!.presentViewController(activityVC, animated: true, completion: nil)
                    
                } else {
                    var videoUrl = NSURL(fileURLWithPath: eventMediaFilePath)
                    
                    var objectsToShare : [NSURL] = [videoUrl!]
                    
                    let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
                    self.navigationController!.presentViewController(activityVC, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        // do something specific to cell content clicked.
    }
    
    
    @IBAction func UpdateButton(sender: UIButton)
    {
        sender.hidden = true
        isEventStreamUpdated = false
        noOfStreamUpdates = 0
        downloadData()
        updateData()
    }
}
