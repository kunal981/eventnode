//
//  OnlineOnlyReminderInviteResponseGuest.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class OnlineOnlyReminderInviteResponseGuest
{
    func emailMessage(eventId: String, eventTitle: String, dateString: String, timeString: String, locationString: String, hostName: String, latitude: String, longitude: String, type: String, url:String)-> String
    {
        
        var file = "Online_Only_Reminder_Invite_Response_Guest.html"
        
        var path = documentDirectory.stringByAppendingPathComponent(file)
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        println(base64String)
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!
        
        message = message.stringByReplacingOccurrencesOfString("RRRRRRosy's Baby Shower<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by Sharon Tucker", withString: "\(eventTitle)<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by \(hostName)", options: nil, range: nil)
        
        if type == "rsvp"
        {
            message = message.stringByReplacingOccurrencesOfString("April 300000000,2015 - 7:000000PM", withString: "\(dateString) - \(timeString)", options: nil, range: nil)
            
            message = message.stringByReplacingOccurrencesOfString("3333333444 Spectrum, Irvine, CA 92618", withString: "\(locationString)", options: nil, range: nil)
            
            message = message.stringByReplacingOccurrencesOfString("Goooooooooooogggggggleeeeeeeeeeeemaaaaappppssssssss", withString:"http://maps.google.com/maps?q=\(latitude),\(longitude)", options: nil, range: nil)
            
        }
        
        message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Change your Response</a></td>", withString: "<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Change your Response</a></td>", options: nil, range: nil)
        
        
        return message
    }
}