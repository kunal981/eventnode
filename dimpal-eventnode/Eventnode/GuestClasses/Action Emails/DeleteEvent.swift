//
//  DeleteEvent.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class DeleteEvent
{
func emailMessage(eventId: String, eventTitle: String, hostName: String,type: String, url:String)-> String
{

var file = "delete_event.html"

var path = documentDirectory.stringByAppendingPathComponent(file)



var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!

message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRRRRRRRRosy's Baby Shower", withString:"\(eventTitle)", options: nil, range: nil)

  if type == "rsvp"
  {
    message = message.stringByReplacingOccurrencesOfString("SSSSSSSSSSSSSSSSharon Tucker cancelled the event: <br><b>RRRRRRRRRosy's Baby Shower", withString: "\(hostName) cancelled the event: <br><b>\(eventTitle)", options: nil, range: nil)
  }
  else
  {
    
    message = message.stringByReplacingOccurrencesOfString("SSSSSSSSSSSSSSSSharon Tucker cancelled the event: <br><b>RRRRRRRRRosy's Baby Shower", withString: "\(hostName) deleted the event: <br><b>\(eventTitle)", options: nil, range: nil)
  }

   return message
  }
}