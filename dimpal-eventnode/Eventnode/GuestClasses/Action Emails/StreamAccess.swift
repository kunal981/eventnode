//
//  StreamAccess.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class StreamAccess
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, imageUrl:String, url:String )-> String
    {

        var file = "stream_access.html"

        var path = documentDirectory.stringByAppendingPathComponent(file)
        
        println(path)

        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        println(base64String)
    
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

        var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!

        message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRRRRRRosy's Baby Shower", withString: "\(eventTitle)", options: nil, range: nil)


        message = message.stringByReplacingOccurrencesOfString("SSSSSSSharon Tucker gave you access to photos/videos/notes for the event:", withString: "\(hostName) gave you access to photos/videos/notes for the event:", options: nil, range: nil)
        
        message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRosy's Baby Shower", withString: "\(eventTitle)", options: nil, range: nil)

        
        
        
        
        message = message.stringByReplacingOccurrencesOfString("<img src=\"http://dcy86hdr5o800.cloudfront.net/image1.jpg\">", withString:"<img src=\"\(imageUrl)\">" , options: nil, range: nil)
        
        message = message.stringByReplacingOccurrencesOfString("<b>Latest Content</b></span><br><img src=\"http://dcy86hdr5o800.cloudfront.net/image1.jpg\"></td>", withString:"<b>Latest Content</b></span><br><img src=\"\(imageUrl)\"></td>" , options: nil, range: nil)

        message = message.stringByReplacingOccurrencesOfString("<a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Event</a>", withString:"<a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\"href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Go to Event</a>", options: nil, range: nil)
    
    
    

        return message
    }
}