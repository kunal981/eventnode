//
//  InPersonReminder.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class InPersonReminder
{
    func emailMessage(eventId: String, eventTitle: String, dateString: String, timeString: String, locationString: String, hostName: String, type: String, imageUrl: String, longitude: String , latitude: String, url:String )-> String
{

var file = "In_Person_Reminder.html"

var path = documentDirectory.stringByAppendingPathComponent(file)

    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    println(base64String)
    
    let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    
    let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!

message = message.stringByReplacingOccurrencesOfString("<b>Sharon Tucker</b> invited you to <b>Rosy’s Baby Shower</b>", withString:"<b>\(hostName)</b> invited you to <b>\(eventTitle)</b>", options: nil, range: nil)

if type == "rsvp"
{
    message = message.stringByReplacingOccurrencesOfString("<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"http://dcy86hdr5o800.cloudfront.net/image1.jpg\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", withString:"<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\(imageUrl) width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", options: nil, range: nil)


    message = message.stringByReplacingOccurrencesOfString("Rosy's Baby Shower<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by Sharon Tucker", withString:"\(eventTitle)<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by \(hostName)", options: nil, range: nil)
    
    message = message.stringByReplacingOccurrencesOfString("April 30,2015 - 7:00PM", withString:"\(dateString) - \(timeString)", options: nil, range: nil)

    message = message.stringByReplacingOccurrencesOfString("3444 Spectrum, Irvine, CA 92618", withString:"\(locationString)", options: nil, range: nil)
    
     message = message.stringByReplacingOccurrencesOfString("Goooooooooooogggggggleeeeeeeeeeeemaaaaappppssssssss", withString:"http://maps.google.com/maps?q=\(latitude),\(longitude)", options: nil, range: nil)
    
   
    
    
     message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Respond to Invite</a></td>", withString:"<td height=\"30\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">Respond to Invite</a></td>", options: nil, range: nil)
    
    
    
    

}

return message
}
}