//
//  OnlineEventCreation.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class OnlineEventCreation
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, type: String, imageUrl: String, url:String)-> String
    {
        
        var file = "online_event_creation.html"
        
        var path = documentDirectory.stringByAppendingPathComponent(file)
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        println(base64String)
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!
        
        
        
        
            message = message.stringByReplacingOccurrencesOfString("Rosy's Baby Shower<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by Sharon Tucker", withString: "\(eventTitle)<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by \(hostName)", options: nil, range: nil)
            
            
            
            message = message.stringByReplacingOccurrencesOfString("deeeeeeeeeeeeppppppppppppplllllllliiiiiiiinnnnnnnnkkkkkkkk", withString:"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)", options: nil, range: nil)
            
            
            message = message.stringByReplacingOccurrencesOfString("<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"http://dcy86hdr5o800.cloudfront.net/image1.jpg\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", withString:"<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"\(imageUrl)\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", options: nil, range: nil)
            
            
            
       
        
        
        return message
    }
}