//
//  AddtextViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit


class AddtextViewController: UIViewController {

    @IBOutlet var evenNameText : UITextView!
    
    @IBOutlet var eventTitleText : UILabel!
    
    var currentUserId: String!
    
    @IBOutlet var loaderView : UIView!
    
    @IBOutlet var loaderSubView : UIView!
    
    var isNewEntry: Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.view.addSubview(wakeUpImageView)
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        eventTitleText.text = eventTextTitle as String
        eventTitleText.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        eventTitleText.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
    
        evenNameText.text = myNewPost["postData"] as! String
        
        if evenNameText.text == ""
        {
            isNewEntry = true
        }
        else
        {
            isNewEntry = false
        }
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        //indicator.frame = (loaderSubView.frame.width/2) - (indicator.frame.width/2)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        var loadingMessage = UILabel()
        loadingMessage.text = "Loading..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()

        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 8
        let attributes = [NSParagraphStyleAttributeName : style]
        
        var attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(20.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        evenNameText.attributedText = NSAttributedString(string:evenNameText.text, attributes:attributes)
        evenNameText.font = UIFont(name: "Tigerlily", size: 20.0)
        evenNameText.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        

        
        
        // Do any additional setup after loading the view.
        evenNameText.becomeFirstResponder()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func keyboardWillShow(sender: NSNotification) {
        evenNameText.frame = CGRectMake(15 , 90, evenNameText.frame.width, evenNameText.frame.height - 175)
    }
    func keyboardWillHide(sender: NSNotification) {
        evenNameText.frame = CGRectMake(15 , 90, evenNameText.frame.width, evenNameText.frame.height + 175)
    }
    
    
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventPhotosVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
        self.navigationController?.pushViewController(eventPhotosVC, animated: false)*/
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    

    
    
    @IBAction func viewTapped(sender : AnyObject) {
        evenNameText.resignFirstResponder()
    }
    
    @IBAction func saveButtonClicked(sender : AnyObject){

        if evenNameText.text != ""
        {
            self.loaderView.hidden=false
            
            myNewPost["postData"] = self.evenNameText.text as String
            myNewPost["postType"] = "text"
            
            myNewPost["postHeight"] = 0
            myNewPost["postWidth"] = 0
            
            myNewPost["eventFolder"] = "/"
            
            myNewPost["eventObjectId"] = currentEvent.objectId
            
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["postData"] = myNewPost["postData"] as? String
            tblFields["eventFolder"] = "nothing"
            tblFields["isApproved"] = "0"
            
            var postHeight = myNewPost["postHeight"] as! CGFloat
            tblFields["postHeight"] = "\(postHeight)"
            
            var postWidth = myNewPost["postWidth"] as! CGFloat
            tblFields["postWidth"] = "\(postWidth)"
            
            tblFields["eventObjectId"] = myNewPost["eventObjectId"] as? String
            
            var postType = myNewPost["postType"] as! String
            tblFields["postType"] = "\(postType)"
            
            var insertedId = 0
            
            if(myNewPost.objectId != nil)
            {
                var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "objectId=?", whereFields: [myNewPost.objectId!])
                //ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "objectId=?", whereFields: [myNewPost.objectId])
                
                //
                
                insertedId = myNewPost["eventImageId"] as! Int
                println(insertedId)
            }
            else
            {
                tblFields["objectId"] = ""
                insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
            }
            
            if insertedId>0
            {
                ParseOperations.instance.saveData(myNewPost, target: self, successSelector: "createPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createPostError:", errorSelectorParameters:myNewPost)
            }
            else
            {
                self.loaderView.hidden = true
                Util.invokeAlertMethod("", strBody: "Error saving record.", delegate: nil)
            }
        }
        else
        {
            Util.invokeAlertMethod("", strBody: "Text note cannot be blank.", delegate: nil)
        }
    }
    
    
    func createPostSuccess(timer:NSTimer)
    {
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var postId = timer.userInfo?.valueForKey("external") as! Int!
        
        self.loaderView.hidden=true
        isPostUpdated = true
        println(myNewPost.objectId)
        
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            println(date)
            tblFields["updatedAt"] = date
        }
        
        
        //objectId=?
        
        var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            println("Record Updated Successfully")
            println("eventImage")
            //Util.invokeAlertMethod("", strBody: "Record updated successfully.", delegate: nil)
        } else {
            println("Record not Updated Successfully")
            //Util.invokeAlertMethod("", strBody: "Error in updating record.", delegate: nil)
        }
        
        self.loaderView.hidden=true
        isPostUpdated = true
        println(myNewPost.objectId)
        
        let predicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)' AND isApproved = true")
        
        var query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: eventObject, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    func createPostError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        var eventObject: PFObject = timer.userInfo?.valueForKey("external") as! PFObject
        
        println("error")
    }
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        var postObject = timer.userInfo?.valueForKey("external") as! PFObject
        var notificationObjects = [PFObject]()
        var eventTitle = currentEvent["eventTitle"] as! String
        var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        var notificationObject = PFObject(className: "Notifications")
        
        println("Successfully retrieved \(objects!.count) events.")
        
        var notifMessage = "\(fullUserName) posted a new note to the event, \(eventTitle)"
        
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            
            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                    
                    
                    notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                    notificationObject["notificationImage"] = "profilePic.png"
                    notificationObject["senderId"] = currentUserId
                    notificationObject["receiverId"] = invitation["userObjectId"] as! String
                    notificationObject["notificationActivityMessage"] = notifMessage
                    notificationObject["eventObjectId"] = currentEvent.objectId!
                    notificationObject["notificationType"] = "postnewcontent"
                    
                    notificationObjects.append(notificationObject)
                }
                
                if isNewEntry == true
                {
                    fetchedobjects[i]["isEventStreamUpdated"] = true
                    fetchedobjects[i]["isTextUpdated"] = false
                }
                else
                {
                    fetchedobjects[i]["isEventStreamUpdated"] = false
                    fetchedobjects[i]["isTextUpdated"] = true
                }
                
                i++
                
            }
            
            PFObject.saveAllInBackground(fetchedobjects)
            
            
            PFObject.saveAllInBackground(notificationObjects)
            
            //notificationObject.saveInBackground()
            
            var eventCreatorObjectId = currentUserId
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //eventCreatorObjectId
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            
            var createdAt = dateFormatter.stringFromDate((postObject.createdAt)!)
            
            var updatedAt = dateFormatter.stringFromDate((postObject.updatedAt)!)
            
            var postData =  postObject["postData"] as! String
            var postType = postObject["postType"] as! String
            
            var data: Dictionary<String, String!> = [
                "alert" : "\(notifMessage)",
                "notifType" :  "postnewcontent",
                "objectId" :  postObject.objectId!,
                "eventObjectId" :  currentEvent.objectId!,
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "postHeight": "0",
                "postWidth": "0",
                "eventFolder": "nothing",
                "postData" : "\(postData)",
                "badge": "Increment",
                "sound" : "default",
                "postType": "\(postType)"
            ]
            
            if isNewEntry == true
            {
                
                var fetchedUserObjectIdsString = "','".join(fetchedUserObjectIds)
                
                var predicateString: String! = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = true"
                
                sendParsePush(predicateString, data: data)
                
                
                data["sound"] = ""
                
                predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = false"
                sendParsePush(predicateString, data: data)
                
            }
            
        }
    }
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        var predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        var query = PFUser.queryWithPredicate(predicate)
        
        var push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                println(objects?.count)
                if var fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        var userObjectId = object.objectId!
                        
                        var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        var query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                println(objects?.count)
                                if var fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    var query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }


}
