//
//  EventPhotoFullScreenViewController.swift
//  Eventnode
//
//  Created by Geetika Katragadda on 11/15/15.
//  Copyright © 2015 Empro Systems LLC. All rights reserved.
//

import UIKit

// Reference: http://www.raywenderlich.com/76436/use-uiscrollview-scroll-zoom-content-swift
class EventPhotoFullScreenViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    
    var image: UIImage!
    
    var imageView: UIImageView!
    
    var minZoomScale: CGFloat!
    
    // Default state of the image within the scrollView is zoomedIn.
    var isZoomedOut = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 1 Create imageView, set its frame to image size and starting at (0,0), add imageView to scrollView.
        imageView = UIImageView(image: image)
        imageView.backgroundColor = UIColor.blueColor()
        imageView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size:image.size)
        println("In EventPhotoFullScreenVC: imagesize: \(image.size)")
        scrollView.addSubview(imageView)
        
        // 2 Set scrollView content size to image size.
        scrollView.contentSize = image.size
        
        // 3 Set double tap gesture recognizer so as to implement complete zoom in and zoom out.
        var doubleTapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewDoubleTapped:")
        doubleTapRecognizer.numberOfTapsRequired = 2
        doubleTapRecognizer.numberOfTouchesRequired = 1
        scrollView.addGestureRecognizer(doubleTapRecognizer)
        
        // 4 Calculate minimum scale so that entire image fits within given scroll view boundary.
        let scrollViewFrame = scrollView.frame
        let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
        let minScale = min(scaleWidth, scaleHeight);
        scrollView.minimumZoomScale = minScale;
        
        // track minimum scale for usage later.
        minZoomScale = minScale
        
        // 5 Max zoom scale would be 1.0. Any more than that would lead to blurry zoom in.
        // Set default zoom scale to the min scale value so that entire image would show within given scrollView boundary.
        scrollView.maximumZoomScale = 1.0
        scrollView.zoomScale = minScale;
        
        // 6 Center imageView within ScrollView (otherwise it would show up top-left aligned)
        centerScrollViewContents()
    }
    
    func centerScrollViewContents() {
        let boundsSize = scrollView.bounds.size
        var contentsFrame = imageView.frame
        
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            contentsFrame.origin.y = 0.0
        }
        imageView.frame = contentsFrame
    }
    
    func scrollViewDoubleTapped(recognizer: UITapGestureRecognizer) {
        // 1 Get current point in view.
        let pointInView = recognizer.locationInView(imageView)
        
        // 2 Toggle double tap behavior to complete zoom in and zoom out behavior.
        var newZoomScale: CGFloat!
        if (isZoomedOut) {
            newZoomScale = minZoomScale
        } else {
            newZoomScale = 1.0
        }
        isZoomedOut = !isZoomedOut
        
        // 3 Calculate rectangle to zoom in for a smooth transition.
        let scrollViewSize = scrollView.bounds.size
        let w = scrollViewSize.width / newZoomScale
        let h = scrollViewSize.height / newZoomScale
        let x = pointInView.x - (w / 2.0)
        let y = pointInView.y - (h / 2.0)
        
        let rectToZoomTo = CGRectMake(x, y, w, h);
        
        // 4 Zoom in/out to rectangle.
        scrollView.zoomToRect(rectToZoomTo, animated: true)
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        centerScrollViewContents()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonClicked(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(false)
    }
}
