//
//  OnlineOnly.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class OnlineOnly
{
    func emailMessage(eventId: String, eventTitle: String, guestName:String ,type: String, url:String)-> String
{

var file = "Online_Only.html"

var path = documentDirectory.stringByAppendingPathComponent(file)

    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    println(base64String)
    
    let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
    
    let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

var message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!

message = message.stringByReplacingOccurrencesOfString("Chandra Kilaru is following the event Rosy’s Baby Shower", withString: "\(guestName) is following the event \(eventTitle)", options: nil, range: nil)


message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Event</a></td>", withString: "<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(eventPageBaseUrl)\(base64String)&url=\(base64UrlString)\" target=\"_blank\">View Response</a></td>", options: nil, range: nil)



return message
}
}