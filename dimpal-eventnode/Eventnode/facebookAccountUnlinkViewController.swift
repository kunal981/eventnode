//
//  facebookAccountUnlinkViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

var showLinkfacebookView: Bool! = true

class facebookAccountUnlinkViewController: UIViewController {
    
    @IBOutlet weak var loaderView: UIView!
    
    
  
    
    @IBOutlet weak var textView2: UITextView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emaillabel: UILabel!
    @IBOutlet weak var button: UIButton!
    override func viewDidLoad()
    {
        
        self.view.addSubview(wakeUpImageView)
        
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        var loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        loaderSubView.layer.cornerRadius = 10
        
        
        self.loaderView.hidden = true
        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        textView.attributedText = NSAttributedString(string:textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

        
//        var style2 = NSMutableParagraphStyle()
//        style.lineSpacing = 5
//        let attributes2 = [NSParagraphStyleAttributeName : style2]

        textView2.attributedText = NSAttributedString(string:textView2.text, attributes:attributes)
        textView2.font = UIFont(name: "AvenirNext-Medium", size: 10.0)
        textView2.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

        
        
        super.viewDidLoad()
        
        nameLabel.text = NSUserDefaults.standardUserDefaults().valueForKey("facebookName") as? String
        emaillabel.text = NSUserDefaults.standardUserDefaults().valueForKey("facebookEmail") as? String
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func unlinkButton(sender: AnyObject)
    {
        var user = PFUser.currentUser()
        
        
        
        if hasPassword
        {
            self.loaderView.hidden = false
            PFFacebookUtils.unlinkUserInBackground(user!, block: { (success:Bool, error:NSError?) -> Void in
                
                if let user = user
                {
                    isFacebookLogin = false
                    user["isFacebookLogin"] = false
                    user["facebookId"] = ""
                    println("unlink")
                    
                    user.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                        if success
                        {
                            self.loaderView.hidden = true
                            
                            isFacebookLogin = false
                            
                            showLinkfacebookView = false
                            
                            println("data is updated")
                            
                            NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isNormalLogin")
                            
                            self.navigationController?.popViewControllerAnimated(false)
                        }
                        else
                        {
                            
                            self.loaderView.hidden = true
                            
                            println(error?.localizedDescription)
                            
                            var refreshAlert = UIAlertController(title: "Error", message: "Couldn't unlink your Facebook account. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                                
                            }))
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                        }
                    })
                    
                }
                else
                {
                    self.loaderView.hidden = true
                    println(error?.localizedDescription)
                    var refreshAlert = UIAlertController(title: "Error", message: "Couldn't unlink your Facebook account. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                        
                    }))
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                    
                }
                
                
                
            })
        }
        else
        {
            
            var refreshAlert = UIAlertController(title: "Error", message: "You haven't created a password yet. Please create a password before unlinking your Facebook account.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    @IBAction func backButoon(sender: AnyObject)
    {
        showLinkfacebookView = false
        
        self.navigationController?.popViewControllerAnimated(true)
        
        //let linkedAccounts = self.storyboard!.instantiateViewControllerWithIdentifier("LinkedAccounts") as! LinkedAccountsViewController
        //self.navigationController?.pushViewController(linkedAccounts, animated: false)
        
    }
}
