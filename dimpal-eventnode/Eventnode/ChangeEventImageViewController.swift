//
//  ChangeEventImageViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import AssetsLibrary
import MobileCoreServices

class ChangeEventImageViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var perfectmsg : UILabel!
    @IBOutlet var leftrightlabel : UILabel!
    @IBOutlet var updownlabel : UILabel!
    @IBOutlet var imagewrap : UIScrollView!
    
    var timer = NSTimer()
    var adjustDirection: Int = 1
    var adjustSpeed: Double = 0.01
    
    var moveHorizontal: Bool! = false
    var moveVertical: Bool! = false
    
    var isImageEdited: Bool! = false;
    
    @IBOutlet var replaceButton : UIButton!
    @IBOutlet weak var imageToSave: UIImageView!
    @IBOutlet weak var eventTitle: UITextField!
    @IBOutlet weak var eventTitleLabel: UILabel!
    
    @IBOutlet var loaderView : UIView!
    @IBOutlet var loaderSubView : UIView!
    
    @IBOutlet weak var deleteEventButton: UIButton!
    @IBOutlet weak var eventDescriptionButton: UIButton!
    @IBOutlet weak var eventLocationButton: UIButton!
    @IBOutlet weak var eventDateTimeButton: UIButton!
    
    
    var imageData: UIImage!
    var newImage: UIImage!
    
    var currentUserId: String!
    
    @IBOutlet weak var croplayer: UIImageView!
    
    var wastouchbeganonlayer: Bool! = false
    var resize: Bool! = false
    
    var lastX: CGFloat! = 0
    var lastY: CGFloat! = 0
    
    var resizePosition: Int! = 1
    
    var orientation: UIImageOrientation = .Up
    var loadingMessage = UILabel()
    
    
    var eventLogoFile: String!
    var originalEventLogoFile: String!
    
    var eventLogoFileUrl: NSURL!
    var originalEventLogoFileUrl: NSURL!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        
        loadingMessage.text = "Updating Event"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        
        indicator.startAnimating()
        
        
        croplayer.alpha = 0.4
        wastouchbeganonlayer = false
        
        
        
        //eventTitleLabel.text = currentEvent["eventTitle"] as? String
        eventTitleLabel.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        eventTitleLabel.textAlignment = .Center
        eventTitleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15)
        
        eventTitle.text = currentEvent["eventTitle"] as? String
        eventTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

        eventTitle.textAlignment = .Center
        
        eventTitle.font = UIFont(name: "AvenirNext-DemiBold", size: 15)
        
        var imageData = currentEvent["originalEventImage"] as! String!
        
        println(imageData)
        
        var image = UIImage(named: "\(documentDirectory)/\(imageData)")
        self.imageData = image
        
        var x = currentEvent["frameX"] as! CGFloat
        var y = currentEvent["frameY"] as! CGFloat
        
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath("\(documentDirectory)/\(imageData)")) {
            println("file does exist")
        }
        else
        {
            println("file does not exist")
        }
        
        if(self.imageData.size.width/self.imageData.size.height == 4/3 )
        {
            self.perfectmsg.hidden=false
            self.view.viewWithTag(1)?.hidden = true
            self.view.viewWithTag(2)?.hidden = true
            self.view.viewWithTag(4)?.hidden = true
            self.view.viewWithTag(6)?.hidden = true
            self.view.viewWithTag(7)?.hidden = true
            self.view.viewWithTag(9)?.hidden = true
            
            self.moveHorizontal = false
            self.moveVertical = false
            
            self.imageToSave.frame.size.width = self.imagewrap.frame.width
            self.imageToSave.frame.size.height = self.imagewrap.frame.height
            
        }
        else
        {
            self.perfectmsg.hidden=true
            
            println(self.imageData.size.height)
            println(self.imageData.size.width)
            
            if(self.imageData.size.width<=self.imageData.size.height || (self.imageData.size.width/self.imageData.size.height < 4/3 && self.imageData.size.width/self.imageData.size.height > 1))
            {
                self.imageToSave.frame.size.width = self.imagewrap.frame.width
                self.imageToSave.frame.size.height = self.imagewrap.frame.width * (self.imageData.size.height/self.imageData.size.width)
                
                self.imageToSave.frame.origin.x = 0
                self.imageToSave.frame.origin.y = (self.imagewrap.frame.height/2)-(self.imageToSave.frame.size.height/2)
                
                self.view.viewWithTag(1)?.hidden = true
                self.view.viewWithTag(2)?.hidden = true
                self.view.viewWithTag(4)?.hidden = true
                self.view.viewWithTag(6)?.hidden = false
                self.view.viewWithTag(7)?.hidden = false
                self.view.viewWithTag(9)?.hidden = false
                
                self.moveHorizontal = false
                self.moveVertical = true
                
            }
            else
            {
                self.imageToSave.frame.size.width = self.imagewrap.frame.height * (self.imageData.size.width/self.imageData.size.height)
                self.imageToSave.frame.size.height = self.imagewrap.frame.height
                
                self.imageToSave.frame.origin.x = (self.imagewrap.frame.width/2)-(self.imageToSave.frame.size.width/2)
                self.imageToSave.frame.origin.y = 0
                
                self.view.viewWithTag(1)?.hidden = false
                self.view.viewWithTag(2)?.hidden = false
                self.view.viewWithTag(4)?.hidden = false
                self.view.viewWithTag(6)?.hidden = true
                self.view.viewWithTag(7)?.hidden = true
                self.view.viewWithTag(9)?.hidden = true
                
                self.moveHorizontal = true
                self.moveVertical = false
                
            }
        }
        
        self.imageToSave.image = self.imageData
        
        self.imageToSave.frame.origin.x =  currentEvent["frameX"] as! CGFloat
        self.imageToSave.frame.origin.y =  currentEvent["frameY"] as! CGFloat
        
        
    }
    
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool{
        
        println(string)
        
        //\u200B
        var _char = string.cStringUsingEncoding(NSUTF8StringEncoding)
        
        
        
        if(string=="")
        {
            println("sdsdfs__\(_char?.count)")
        }
        
        eventTitleLabel.text = eventTitle.text
        eventTitleLabel.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        eventTitleLabel.textAlignment = .Center
        eventTitleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15)
        
        if(string != "")
        {
            if count(eventTitle.text) < 30 {
                println("eerfre")
                isUpdated = true
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return true
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        eventTitle.resignFirstResponder()
        return true
    }
    
    
    
    func keyboardWillShow(sender: NSNotification) {
        
    }
    
    func keyboardWillHide(sender: NSNotification) {
        //        eventTitle.hidden=true
        //        eventTitleLabel.hidden=false
        //        eventTitle.resignFirstResponder()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func editEventTitleButtonClicked(sender : AnyObject){
        
        //        eventTitle.hidden=false
        //        eventTitleLabel.hidden=true
        //        eventTitle.becomeFirstResponder()
    }
    
    
    @IBAction func updateImage(sender : AnyObject){
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            showLoader("Loading")
            var imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {
        NSLog("Success.")
        
        self.loaderView.hidden=true
        
        if(imagewrap.frame.height>image.size.height || imagewrap.frame.width>image.size.width){
            var refreshAlert = UIAlertController(title: "Error", message: "Photo is too small. Please select a different photo.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            dispatch_async(dispatch_get_main_queue(), {
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            })
        }
        else
        {
            
            let data = UIImageJPEGRepresentation(self.correctlyOrientedImage(image), 0.5)
            
            
            
            imageData = image
            
            
            isUpdated = true
            
            isImageEdited = true;
            
            if(self.imageData.size.width/self.imageData.size.height == 4/3 )
            {
                self.perfectmsg.hidden=false
                self.view.viewWithTag(1)?.hidden = true
                self.view.viewWithTag(2)?.hidden = true
                self.view.viewWithTag(4)?.hidden = true
                self.view.viewWithTag(6)?.hidden = true
                self.view.viewWithTag(7)?.hidden = true
                self.view.viewWithTag(9)?.hidden = true
                
                self.moveHorizontal = false
                self.moveVertical = false
                
                self.imageToSave.frame.size.width = self.imagewrap.frame.width
                self.imageToSave.frame.size.height = self.imagewrap.frame.height
                self.imageToSave.frame.origin.x = 0
                self.imageToSave.frame.origin.y = 0
                
            }
            else
            {
                self.perfectmsg.hidden=true
                
                println(self.imageData.size.height)
                println(self.imageData.size.width)
                
                if(self.imageData.size.width<=self.imageData.size.height || (self.imageData.size.width/self.imageData.size.height < 4/3 && self.imageData.size.width/self.imageData.size.height > 1))
                {
                    self.imageToSave.frame.size.width = self.imagewrap.frame.width
                    self.imageToSave.frame.size.height = self.imagewrap.frame.width * (self.imageData.size.height/self.imageData.size.width)
                    
                    self.imageToSave.frame.origin.x = 0
                    self.imageToSave.frame.origin.y = (self.imagewrap.frame.height/2)-(self.imageToSave.frame.size.height/2)
                    
                    self.view.viewWithTag(1)?.hidden = true
                    self.view.viewWithTag(2)?.hidden = true
                    self.view.viewWithTag(4)?.hidden = true
                    self.view.viewWithTag(6)?.hidden = false
                    self.view.viewWithTag(7)?.hidden = false
                    self.view.viewWithTag(9)?.hidden = false
                    
                    self.moveHorizontal = false
                    self.moveVertical = true
                    
                }
                else
                {
                    self.imageToSave.frame.size.width = self.imagewrap.frame.height * (self.imageData.size.width/self.imageData.size.height)
                    self.imageToSave.frame.size.height = self.imagewrap.frame.height
                    
                    self.imageToSave.frame.origin.x = (self.imagewrap.frame.width/2)-(self.imageToSave.frame.size.width/2)
                    self.imageToSave.frame.origin.y = 0
                    
                    self.view.viewWithTag(1)?.hidden = false
                    self.view.viewWithTag(2)?.hidden = false
                    self.view.viewWithTag(4)?.hidden = false
                    self.view.viewWithTag(6)?.hidden = true
                    self.view.viewWithTag(7)?.hidden = true
                    self.view.viewWithTag(9)?.hidden = true
                    
                    self.moveHorizontal = true
                    self.moveVertical = false
                    
                }
            }
            self.imageToSave.image = self.imageData
            
        }
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.loaderView.hidden=true
        NSLog("picker cancel.")
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func adjustPhoto(touches: NSSet!){
        
        let touch = touches.allObjects[0] as! UITouch
        let touchLocation = touch.locationInView(self.view)
        
        
        if(moveHorizontal == true)
        {
            var diffX: CGFloat = touchLocation.x-lastX
            lastX = touchLocation.x
            
            if(diffX>0)
            {
                if(imageToSave.frame.origin.x >= 0)
                {
                    imageToSave.frame.origin.x = 0
                }
                else
                {
                    imageToSave.frame.origin.x = imageToSave.frame.origin.x+diffX
                }
            }
            
            if(diffX<0)
            {
                if(((-1)*imageToSave.frame.origin.x) >= (imageToSave.frame.width-imagewrap.frame.width))
                {
                    imageToSave.frame.origin.x = (imagewrap.frame.width-imageToSave.frame.width)
                }
                else
                {
                    imageToSave.frame.origin.x = imageToSave.frame.origin.x+diffX
                }
            }
        }
        
        if(moveVertical == true)
        {
            
            var diffY: CGFloat = touchLocation.y-lastY
            lastY = touchLocation.y
            
            if(diffY>0)
            {
                if(imageToSave.frame.origin.y >= 0)
                {
                    imageToSave.frame.origin.y = 0
                }
                else
                {
                    imageToSave.frame.origin.y = imageToSave.frame.origin.y+diffY
                }
            }
            
            
            if(diffY<0)
            {
                if(((-1)*imageToSave.frame.origin.y) >= (imageToSave.frame.height-imagewrap.frame.height))
                {
                    imageToSave.frame.origin.y = (imagewrap.frame.height-imageToSave.frame.height)
                }
                else
                {
                    imageToSave.frame.origin.y = imageToSave.frame.origin.y+diffY
                }
            }
        }
    }
    
    
    
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject)
    {
         self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func uploadEventWithImage(timer: NSTimer)
    {
        var eventId = timer.userInfo as! Int
        
        if MyReachability.isConnectedToNetwork()
        {
            var imageFolder = currentEvent["eventFolder"] as! String
            
            var imageName = currentEvent["eventImage"] as! String
            
            
            
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            
            let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            
            
            uploadRequest.bucket = "eventnodepublicpics"
            uploadRequest.key =  "\(self.currentUserId)/eventProfileImages/\(eventLogoFile)"
            uploadRequest.body = eventLogoFileUrl
            
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
            
            var deleteRequest = AWSS3DeleteObjectRequest()
            deleteRequest.bucket = "eventnodepublicpics"
            deleteRequest.key = "\(imageFolder)\(imageName)"
            
            var s3 = AWSS3.defaultS3()
            
            
            /*s3.deleteObject(deleteRequest).continueWithBlock {
                (task: AWSTask!) -> AnyObject! in
                
                if(task.error != nil){
                    self.upload(uploadRequest, isOriginal: false, insertedId: eventId)
                    
                }else{
                    var imageOriginalName = currentEvent["originalEventImage"] as! String
                    
                    var deleteRequestOriginal = AWSS3DeleteObjectRequest()
                    deleteRequestOriginal.bucket = "eventnodepublicpics"
                    deleteRequestOriginal.key = "\(imageFolder)\(imageOriginalName)"
                    
                    var s3Original = AWSS3.defaultS3()
                    
                    s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                        (task: AWSTask!) -> AnyObject! in
                        
                        if(task.error != nil){
                            
                            self.upload(uploadRequest, isOriginal: false, insertedId: eventId)
                            
                        }else{*/
                            
                            self.upload(uploadRequest, isOriginal: false, insertedId: eventId)
                            
                        /*}
                        return nil
                    }
                    
                }
                return nil
            }*/
            
        }
        else
        {
            isPostUpdated = true
            
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    
    
    
    
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage
    {
        if image.imageOrientation == UIImageOrientation.Up
        {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        var normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int){
        
        self.loaderView.hidden = true
        
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    func updateEvent(){
        
        ParseOperations.instance.saveData(currentEvent, target: self, successSelector: "updateEventSuccess:", successSelectorParameters: nil, errorSelector: "updateEventError:", errorSelectorParameters:currentEvent)
    }
    
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                            println("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                        println("upload() failed: [\(error)]")
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                    println("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                println("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    
                    if(isOriginal == true){
                        
                        println("Original image uploaded. Creating event now...")
                        
                        currentEvent["eventImage"] = self.eventLogoFile
                        
                        currentEvent["originalEventImage"] = self.originalEventLogoFile
                        currentEvent["frameX"] = self.imageToSave.frame.origin.x
                        currentEvent["frameY"] = self.imageToSave.frame.origin.y
                        
                        currentEvent["eventFolder"] = "\(self.currentUserId)/eventProfileImages/"
                        
                        self.updateEvent()
                        
                    }
                    else
                    {
                        println("Cropped image uploaded. Uploading original image now...")
                        
                        
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        
                        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        
                        uploadRequest.bucket = "eventnodepublicpics"
                        uploadRequest.key =  "\(self.currentUserId)/eventProfileImages/\(self.originalEventLogoFile)"
                        
                        uploadRequest.body = self.originalEventLogoFileUrl
                        
                        
                        println(self.eventLogoFileUrl.relativePath)
                        println(self.eventLogoFileUrl.relativeString)
                        println(self.eventLogoFileUrl.path)
                        
                        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                        
                        self.upload(uploadRequest, isOriginal: true, insertedId: insertedId)
                    }
                    
                })
            }
            return nil
        }
    }
    
    
    
    
    
    func showLoader(message: String )
    {
        self.loadingMessage.text = "\(message)"
        self.loadingMessage.textColor = UIColor.whiteColor()
        
        self.loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.loadingMessage.numberOfLines = 2
        self.loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        self.loadingMessage.textAlignment = .Center
        
        
        
        self.loaderView.hidden = false
    }
    
    
    
    func updateEventSuccess(timer:NSTimer)
    {
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        self.loaderView.hidden=true
        
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            println(date)
            tblFields["updatedAt"] = date
        }
        
        isUpdated = false
        
        
        var isUpdated1 = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [currentEvent["eventId"] as! Int])
        
        var data = [
            "eventObjectId": eventObject.objectId!,
            "emailId": "noemail",
            "eventCreatorId": "\(currentUserId)"
        ]
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                println(url!)
                tblFields["socialSharingURL"] = url! as String
                var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [currentEvent["eventId"] as! Int])
                
                eventObject["socialSharingURL"] = url! as String
                
                eventObject.saveInBackground()
            }
            
        })

        
        println(currentEvent.objectId)

        let predicate = NSPredicate(format: "eventObjectId IN {'\(currentEvent.objectId!)'}")
        
        var query = PFQuery(className:"Invitations", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
       
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventInvitationsError:", errorSelectorParameters: nil)
        
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    
    func updateEventError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        
        self.navigationController?.popViewControllerAnimated(false)
        
        println("error")
    }
    
    
    func fetchEventInvitationsSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedObjects = objects
        {
            var i = 0
            
            for object in fetchedObjects
            {
                fetchedObjects[i]["isEventUpdated"] = true
                i++
            }
            
            PFObject.saveAllInBackground(fetchedObjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    
                }
                else
                {
                    
                }
            }
        }
        
    }
    
    
    
    func fetchEventInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    

    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        
        currentEvent["eventTitle"] = eventTitle.text
        
        eventTitle.hidden=true
        eventTitleLabel.hidden=false
        eventTitle.resignFirstResponder()
        
        var taps = touches as NSSet
        let touch = taps.allObjects[0] as! UITouch
        let touchLocation = touch.locationInView(self.view)
        lastX = touchLocation.x
        lastY = touchLocation.y
        
        if CGRectContainsPoint(imagewrap.frame, touchLocation) {
            wastouchbeganonlayer = true
        }
        else
        {
            wastouchbeganonlayer = false
        }
        
    }
    
    
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        if(wastouchbeganonlayer == true){
            isUpdated = true
            isImageEdited = true
            adjustPhoto(touches)
        }
    }
    
    
    //    @IBAction func saveImage(sender: AnyObject) {
    //
    //    }
    
    
    //    @IBAction func descriptionButtonClicked(sender : AnyObject){
    //
    //        let eventDescriptionVC = self.storyboard!.instantiateViewControllerWithIdentifier("UpdateDescriptionViewController") as! UpdateDescriptionViewController
    //        self.navigationController?.pushViewController(eventDescriptionVC, animated: false)
    //    }
    //
    
    //    @IBAction func locationButtonClicked(sender : AnyObject){
    //
    //        let addLocationVC = self.storyboard!.instantiateViewControllerWithIdentifier("UpdateLocationViewController") as! UpdateLocationViewController
    //        addLocationVC.eventTitle = eventTitle.text
    //        self.navigationController?.pushViewController(addLocationVC, animated: false)
    //    }
    
    //    @IBAction func dateTimeButtonClicked(sender : AnyObject){
    //        
    //        let addDateTimeVC = self.storyboard!.instantiateViewControllerWithIdentifier("UpdateEventDateTimeViewController") as! UpdateEventDateTimeViewController
    //        self.navigationController?.pushViewController(addDateTimeVC, animated: false)
    //    }
    
    
    @IBAction func saveButton(sender: AnyObject)
    {
        if(isUpdated == true)
        {
            
            
            currentEvent["eventTitle"] = eventTitle.text
            
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["eventTitle"] = eventTitle.text
            
            
            tblFields["isPosted"] = "0"
            
            if(currentEvent["isRSVP"] as? Bool == true)
            {
                tblFields["isRSVP"] = "1"
                
                var date = ""
                if currentEvent["eventStartDateTime"] != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
                    println(date)
                    tblFields["eventStartDateTime"] = date
                }
                
                if currentEvent["eventEndDateTime"] != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((currentEvent["eventEndDateTime"] as? NSDate)!)
                    println(date)
                    tblFields["eventEndDateTime"] = date
                    println(tblFields["eventEndDateTime"])
                }
                
                tblFields["eventDescription"] = currentEvent["eventDescription"] as? String
                
                var eventLatitude = currentEvent["eventLatitude"] as! Double
                var eventLongitude = currentEvent["eventLongitude"] as! Double
                
                tblFields["eventLatitude"] = "\(eventLatitude)"
                tblFields["eventLongitude"] = "\(eventLongitude)"
                
                tblFields["eventLocation"] = currentEvent["eventLocation"] as? String
                
            }
            else
            {
                tblFields["isRSVP"] = "0"
            }
            
            
            if(isImageEdited == true)
            {
                
                orientation = UIImageOrientation.Up
                
                var originalWidth  = imageData.size.width
                var originalHeight = imageData.size.height
                
                var imageHeight = imageToSave.frame.size.height
                var imageWidth = imageToSave.frame.size.width
                
                var cropHeight: CGFloat = originalHeight
                var cropWidth: CGFloat = originalWidth
                var posX: CGFloat = 0
                var posY: CGFloat = 0
                
                if(moveHorizontal == true){
                    cropHeight = originalHeight
                    cropWidth = originalHeight*(4/3)
                    var widthFactor: CGFloat = originalWidth/imageWidth
                    posX = imageToSave.frame.origin.x*widthFactor*(-1)
                    posY = 0
                }
                
                if(moveVertical == true){
                    cropHeight = originalWidth*(3/4)
                    cropWidth = originalWidth
                    var heightFactor: CGFloat = originalHeight/imageHeight
                    posX = 0
                    posY = imageToSave.frame.origin.y*heightFactor*(-1)
                }
                
                println(posX)
                println(posY)
                
                var originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(imageData!), 0.5)
                
                imageData = UIImage(data: originaldata)
                
                var cropSquare = CGRectMake(posX, posY, cropWidth, cropHeight)
                
                var cgimg = CGImageCreateWithImageInRect(imageData.CGImage, cropSquare);
                
                
                newImage = UIImage(CGImage: cgimg, scale:1, orientation:orientation)
                
                var cropdata = UIImageJPEGRepresentation(self.correctlyOrientedImage(newImage!), 0.5)
                
                newImage = UIImage(data: cropdata)
                
                
                var date = NSDate()
                let currentTimeStamp = String(Int64(date.timeIntervalSince1970*1000))
                
                originalEventLogoFile = "\(currentTimeStamp)_\(currentUserId)_originaleventlogo.png"
                
                eventLogoFile = "\(currentTimeStamp)_\(currentUserId)_eventlogo.png"
                
                
                self.originalEventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(self.originalEventLogoFile))
                
                var originalImagedata = UIImageJPEGRepresentation(self.correctlyOrientedImage(self.imageData!), 0.5)
                
                originalImagedata.writeToURL(self.originalEventLogoFileUrl!, atomically: true)
                
                
                eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(eventLogoFile))
                
                cropdata = UIImageJPEGRepresentation(self.correctlyOrientedImage(newImage!), 0.5)
                
                
                var result = cropdata.writeToURL(eventLogoFileUrl!, atomically: true)
                
                
                showLoader("Updating Event")
                
                
                
                
                var eventId = currentEvent["eventId"] as! Int
                tblFields["eventImage"] = self.eventLogoFile
                tblFields["originalEventImage"] = self.originalEventLogoFile
                var frameX = self.imageToSave.frame.origin.x
                var frameY = self.imageToSave.frame.origin.y
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = self.currentUserId
                
                tblFields["eventFolder"] = currentEvent["eventFolder"] as? String
                
                var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [currentEvent.objectId!])
                
                if isUpdated
                {
                    
                    var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("uploadEventWithImage:"), userInfo: currentEvent["eventId"] as! Int, repeats: false)
                    
                }
                else
                {
                    self.loaderView.hidden = true
                    Util.invokeAlertMethod("", strBody: "Error in updating record.", delegate: nil)
                }
                
            }
            else
            {
                self.navigationController?.popViewControllerAnimated(false)
            }
            
        }
        else
        {
            self.navigationController?.popViewControllerAnimated(false)
        }
        

    }
}
