//
//  SettingsViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/24/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MessageUI

class SettingsViewController: UIViewController, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func eventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func sharedEventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
    @IBAction func logoutButtonClicked(sender : AnyObject){
        var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: " isPosted=0", whereFields: [])
        
        resultSetCount.next()
        
        var eventCount = resultSetCount.intForColumn("count")
        
        resultSetCount.close()
        
        var resultSetPostCount: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: " isPosted=0", whereFields: [])
        
        resultSetPostCount.next()
        
        var postCount = resultSetPostCount.intForColumn("count")
        
        resultSetPostCount.close()
        
        
        if(eventCount>0 || postCount>0)
        {
            var refreshAlert = UIAlertController(title: "Pending Changes", message: "You have pending changes that need to be uploaded. If you logout now you will lose those changes.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Logout", style: .Default, handler: { (action: UIAlertAction!) in
                self.logout()
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
        else
        {
            logout()
        }
    }
    
    
    func logout()
    {
        NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isLoggedIn")
        
        PFUser.logOut()
        
        var isDeleted = ModelManager.instance.deleteTableData("Events", whereString: " isPosted=0", whereFields: [])
        if isDeleted {
            println("Record deleted successfully.")
        } else {
            println("Error in deleting record.")
        }
        
        
        var isDeleted1 = ModelManager.instance.deleteTableData("EventImages", whereString: " isPosted=0", whereFields: [])
        
        if isDeleted1 {
            println("Record deleted successfully.")
        } else {
            println("Error in deleting record.")
        }
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func manageStorageButtonClicked(sender : AnyObject){
        let manageStorageVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageStorageViewController") as! ManageStorageViewController
        self.navigationController?.pushViewController(manageStorageVC, animated: false)
    }
    
    @IBAction func notificationsButtonClicked(sender : AnyObject){
        let notificationsVC = self.storyboard!.instantiateViewControllerWithIdentifier("NotificationsViewController") as! NotificationsViewController
        self.navigationController?.pushViewController(notificationsVC, animated: false)
    }
    
    @IBAction func aboutButtonClicked(sender : AnyObject){
        let aboutVC = self.storyboard!.instantiateViewControllerWithIdentifier("AboutViewController") as! AboutViewController
        self.navigationController?.pushViewController(aboutVC, animated: false)
    }
    
    @IBAction func feedbackButtonClicked(sender : AnyObject){
        /*let feedbackVC = self.storyboard!.instantiateViewControllerWithIdentifier("FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(feedbackVC, animated: false)*/
        var emailTitle = "Eventnode Feedback V1.0"
        var messageBody = ""
        var toRecipents = ["support@eventnode.co"]
        var mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        
        self.presentViewController(mc, animated: true, completion: nil)
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError) {
        var refreshAlert1 = UIAlertController(title: "Mail Sent", message: "Your request has been sent successfully", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert1.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            //println("Handle Ok logic here")
        }))
        
        var refreshAlert2 = UIAlertController(title: "Mail Couldn't be Sent", message: "Your request couldn't be processed at this time", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert2.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            //println("Handle Ok logic here")
        }))
        var showAlert = true
        var alertView: UIAlertController!
        
        switch result.value {
        case MFMailComposeResultCancelled.value:
            println("Mail cancelled")
            showAlert = false
        case MFMailComposeResultSaved.value:
            println("Mail saved")
            showAlert = false
        case MFMailComposeResultSent.value:
            alertView = refreshAlert1
        case MFMailComposeResultFailed.value:
            alertView = refreshAlert2
        default:
            showAlert = false
            break
        }
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if showAlert
        {
            presentViewController(alertView, animated: true, completion: nil)
        }
    }
    

}
