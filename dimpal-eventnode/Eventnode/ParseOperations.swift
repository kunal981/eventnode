//
//  ParseOperations.swift
//  eventnode
//
//  Created by mrinal khullar on 6/30/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation

let parseInstance = ParseOperations()

class ParseOperations: NSObject
{
    class var instance: ParseOperations {

        return parseInstance
    }
    
    func fetchData(query:PFQuery, target: AnyObject!, successSelector: String, successSelectorParameters: AnyObject!, errorSelector: String, errorSelectorParameters: AnyObject!)
    {
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                var finalSuccessParameters: Dictionary! = [String: AnyObject]()
                
                if(successSelectorParameters != nil)
                {
                    finalSuccessParameters["external"] = successSelectorParameters
                }
                
                
                finalSuccessParameters["internal"] = objects
                
                var timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: target, selector: Selector(successSelector), userInfo: finalSuccessParameters, repeats: false)
            }
            else
            {
                var finalErrorParameters: Dictionary! = [String: AnyObject]()
                
                if(errorSelectorParameters != nil)
                {
                    finalErrorParameters["external"] = errorSelectorParameters
                }
                
                finalErrorParameters["internal"] = error
                
                
                var timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: target, selector: Selector(errorSelector), userInfo: finalErrorParameters, repeats: false)
            }
        }
    }
    
    func saveData(eventObject:PFObject, target: AnyObject!, successSelector: String, successSelectorParameters: AnyObject!, errorSelector: String, errorSelectorParameters: AnyObject!)
    {
        eventObject.saveEventually {
            (success: Bool, error: NSError?) -> Void in
            if (success)
            {
                var finalSuccessParameters: Dictionary! = [String: AnyObject]()
                
                if(successSelectorParameters != nil)
                {
                    finalSuccessParameters["external"] = successSelectorParameters
                }
                
                finalSuccessParameters["internal"] = eventObject
                
                var timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: target, selector: Selector(successSelector), userInfo: finalSuccessParameters, repeats: false)
            }
            else
            {
                var finalErrorParameters: Dictionary! = [String: AnyObject]()
                
                if(errorSelectorParameters != nil)
                {
                    finalErrorParameters["external"] = errorSelectorParameters
                }
                
                finalErrorParameters["internal"] = error
                
                
                var timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: target, selector: Selector(errorSelector), userInfo: finalErrorParameters, repeats: false)
            }
        }
    }
    
    func updateData(eventObject:PFObject, target: AnyObject!, successSelector: String, successSelectorParameters: AnyObject!, errorSelector: String, errorSelectorParameters: AnyObject!)
    {
        eventObject.saveEventually() {
            (success: Bool, error: NSError?) -> Void in
            if (success)
            {
                var finalSuccessParameters: Dictionary! = [String: AnyObject]()
                
                if(successSelectorParameters != nil)
                {
                    finalSuccessParameters["external"] = successSelectorParameters
                }
                
                finalSuccessParameters["internal"] = eventObject
                
                var timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: target, selector: Selector(successSelector), userInfo: finalSuccessParameters, repeats: false)
            }
            else
            {
                var finalErrorParameters: Dictionary! = [String: AnyObject]()
                
                if(errorSelectorParameters != nil)
                {
                    finalErrorParameters["external"] = errorSelectorParameters
                }
                
                finalErrorParameters["internal"] = error
                
                
                var timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: target, selector: Selector(errorSelector), userInfo: finalErrorParameters, repeats: false)
            }
        }
    }
    
    func deleteData(eventObject:PFObject, target: AnyObject!, successSelector: String, successSelectorParameters: AnyObject!, errorSelector: String, errorSelectorParameters: AnyObject!)
    {
        var pfObject: PFObject = eventObject
        
        eventObject.deleteInBackgroundWithBlock{
        (success: Bool, error: NSError?) -> Void in
            if (success)
            {
                var finalSuccessParameters: Dictionary! = [String: AnyObject]()
                
                if(successSelectorParameters != nil)
                {
                    finalSuccessParameters["external"] = successSelectorParameters
                }
                
                finalSuccessParameters["internal"] = pfObject
                
                var timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: target, selector: Selector(successSelector), userInfo: finalSuccessParameters, repeats: false)
            }
            else
            {
                var finalErrorParameters: Dictionary! = [String: AnyObject]()
                
                if(errorSelectorParameters != nil)
                {
                    finalErrorParameters["external"] = errorSelectorParameters
                }
                
                finalErrorParameters["internal"] = error

                
                var timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: target, selector: Selector(errorSelector), userInfo: finalErrorParameters, repeats: false)
            }
        }
    }
    
}