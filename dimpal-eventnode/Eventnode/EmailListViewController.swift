//
//  EmailListViewController.swift
//  eventnode
//
//  Created by brst on 7/7/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//


//var style = NSMutableParagraphStyle()
//style.lineSpacing = 40
//let attributes = [NSParagraphStyleAttributeName : style]
//textView.attributedText = NSAttributedString(string: yourText, attributes:attributes)
//
import UIKit



var linkedEmails = [PFObject]()
class EmailListViewController: UIViewController,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate
{
    
    @IBOutlet weak var textView2: UITextView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var emailListtableView: UITableView!
    private weak var saveAction : UIAlertAction?
    private weak var textFieldEmail : UITextField?
    private var validEmail = false
    var emailData = NSString()
    var emailList: Array<PFObject>! = []
    var user = PFUser.currentUser()
    var currentUserId: String!
    var currentUserEmail: String!
    var emailObjectId:String!
    var emailObjectEmail:String!
    
    var indexData = NSInteger()
    
    @IBOutlet var loaderView : UIView!
    
    @IBOutlet var loaderSubView : UIView!
    
    // var linkedEmails = [PFObject]()
    
    override func viewDidLoad()
    {
        
        self.view.addSubview(wakeUpImageView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        var loadingMessage = UILabel()
        loadingMessage.text = "Updating..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        loadingMessage.textAlignment = .Center
        loaderSubView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        
        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        textView.attributedText = NSAttributedString(string:textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        
//        var style2 = NSMutableParagraphStyle()
//        style.lineSpacing = 3
//        let attributes2 = [NSParagraphStyleAttributeName : style2]
        
        
        
        textView2.attributedText = NSAttributedString(string:textView2.text, attributes:attributes)
        textView2.font = UIFont(name: "AvenirNext-Medium", size: 10.0)
        textView2.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

        
        loaderView.hidden = true
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        currentUserEmail = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        
        emailListtableView.separatorColor = UIColor.whiteColor()
        
        println("\(PFUser.currentUser())")
        
        fetchLoggedInUserDetails()
        
        super.viewDidLoad()
    }
    

    func fetchLoggedInUserDetails()
    {
        
        var userQuery = PFUser.query()
        userQuery?.whereKey("objectId", equalTo:currentUserId)
        userQuery?.findObjectsInBackgroundWithBlock({ (objects:[AnyObject]?, error:NSError?) -> Void in
             if error == nil
             {
                self.loaderView.hidden = true
                
                var linkEmail = PFObject(className: "LinkedAccounts")
                
                ///Users/brst981/Desktop/eventnode_backup/19 october 2015/eventnode/Eventnode/EmailListViewController.swift:109:20: '_??' is not convertible to 'Void'
                if var users = objects
                {
                    for user in users
                    {
                        var currentUser = user as! PFUser
                        
                        linkEmail["emailId"] = self.currentUserEmail
                        linkEmail["userObjectId"] = self.currentUserId!
                        
                        if var verifiedStatus = currentUser["emailVerified"] as? Bool
                        {
                            linkEmail["isEmailVerified"] = verifiedStatus
                        }
                        else
                        {
                            linkEmail["isEmailVerified"] = false
                        }
                        
                        
                        self.emailList = []
                        self.emailList.append(linkEmail)
                        
                        var linkedQuery = PFQuery(className: "LinkedAccounts")
                        linkedQuery.whereKey("userObjectId", equalTo: self.currentUserId!)
                        
                        ParseOperations.instance.fetchData(linkedQuery, target: self, successSelector: "fetchLinkedAccountSuccess:", successSelectorParameters: nil, errorSelector:"fetchLinkedAccountError:", errorSelectorParameters: nil)
                        
                    }
                }
            }
            else
            {
                self.loaderView.hidden = true
                
                var refreshAlert = UIAlertController(title: "Error", message: "Please check your internet connection.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                    
                    self.loaderView.hidden = true
                    
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
                
            }
            
        })
        
        
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func AddMoreEmail(sender: AnyObject)
    {
        
        saveAction?.enabled = false
        alertEmail()
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func emailFetchedSuccess(timer:NSTimer)
    {
        var fetchedEmail = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if fetchedEmail!.count == 0
        {
            let predicate = NSPredicate(format: "(emailId = '\(self.emailData)' AND isEmailVerified = true) OR (emailId = '\(self.emailData)' AND userObjectId = '\(currentUserId)')")
            var linkedQuery = PFQuery(className: "LinkedAccounts", predicate: predicate)
            //linkedQuery.whereKey("emailId", equalTo: self.emailData)
            ParseOperations.instance.fetchData(linkedQuery, target: self, successSelector: "linkedAccountFetchedSuccess:", successSelectorParameters: nil, errorSelector:"linkedAccountFetchedError:", errorSelectorParameters: nil)
        }
        else
        {
            
            self.loaderView.hidden = true

            
            var refreshAlert = UIAlertController(title: "Error", message: "This email address is already in use. Please choose a different email address.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
                
            }))
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
        
    }
    
    func emailFetchedError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        var refreshAlert = UIAlertController(title: "Error", message: "Oops! Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
    
    func emailSaved(timer:NSTimer)
    {
        //self.loaderView.hidden = true
        
        var emailObject: PFObject = timer.userInfo?.valueForKey("internal") as! PFObject
       
        var emailId = emailObject["emailId"] as! String
        
        println(emailObject.objectId!)
        
        let plainData = (emailObject.objectId!).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        println(base64String)
        
        var verificationLink = "http://web.eventnode.co/index.php/event/verify-linked-account?accessTokenId=\(base64String)"
        
        var verificationMessage = "Hi,<br/><br/>You are being asked to confirm the email address \(emailId) with eventnode<br/><br/>Click here to confirm it:<br/><a href='\(verificationLink)' target='_blank'>\(verificationLink)</a><br/><br/>Thanks,<br/><br/>Eventnode team<br/>support@eventnode.co"
        
        var sendVerificationEmail = SendVerificationEmail()
        
        sendVerificationEmail.sendEmail("Please verify your e-mail for eventnode", message: verificationMessage, emails: [emailId])
        
        fetchLoggedInUserDetails()
        //emailList
    }

    func emailSavedError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        self.loaderView.hidden = true
        println("errrrrrooooooor")

        var refreshAlert = UIAlertController(title:"Error", message: "Oops! Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title:"Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
    func fetchLinkedAccountSuccess(timer:NSTimer)
    {
        var linkedAccounts: Array<PFObject>! = timer.userInfo?.valueForKey("internal") as! [PFObject]
        self.loaderView.hidden = true
        
        for email in linkedAccounts
        {
            emailList.append(email)
        }
        
        emailListtableView.reloadData()
        
    }
    
    func fetchLinkedAccountError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        self.loaderView.hidden = true
        println("errrrrrooooooor")
        
        var refreshAlert = UIAlertController(title: "Error", message: "Oops! Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
    
    func unlinkAccountSuccess(timer:NSTimer)
    {
        
        //self.loaderView.hidden = true
        
        var linkedQuery = PFQuery(className: "LinkedAccounts")
        linkedQuery.whereKey("userObjectId", equalTo: currentUserId!)
        
        fetchLoggedInUserDetails()
        
        var refreshAlert = UIAlertController(title: "Success", message: "Email unlinked successfully", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    func unlinkAccountError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        self.loaderView.hidden = true
        var refreshAlert = UIAlertController(title: "Error", message: "Oops! Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    func linkedAccountFetchedSuccess(timer:NSTimer)
    {
        
        var fetchedEmail = timer.userInfo?.valueForKey("internal") as? [PFObject]
        if fetchedEmail?.count == 0
        {
            
            var linkEmail = PFObject(className: "LinkedAccounts")
            
            linkEmail["emailId"] = self.emailData
            linkEmail["userObjectId"] = currentUserId!
            linkEmail["isEmailVerified"] = false
            
            
            ParseOperations.instance.saveData(linkEmail, target: self, successSelector:"emailSaved:", successSelectorParameters: nil, errorSelector:"emailSavedError:", errorSelectorParameters:nil)
            self.loaderView.hidden = true
            
        }
        else
        {
            var refreshAlert = UIAlertController(title: "Error", message: "This email address is already in use. Please choose a different email address.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func linkedAccountFetchedError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("email is not saved in linked: \(error) \(error.userInfo!)")
        
        var refreshAlert = UIAlertController(title: "Error", message: "Oops! Something went wrong. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return emailList.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! EmailtableViewCell
        println("\(cell.frame.height)")
        
        cell.selectionStyle = .None
        
        cell.email.text = emailList[indexPath.row]["emailId"] as? String
        cell.email.adjustsFontSizeToFitWidth = true
        
        cell.emaildelete.tag = indexPath.row
        
        cell.emailDeleteWrapper.tag = indexPath.row
        
        if emailList[indexPath.row]["isEmailVerified"] as? Bool == true
        {
            if indexPath.row > 0
            {
                cell.emailstatus.setTitle("Verified", forState: UIControlState.Normal)
                //cell.emailstatus.addTarget(self, action:"emailVerification", forControlEvents: UIControlEvents.TouchUpInside)
            }
            else
            {
                cell.emailstatus.setTitle("Primary, Verified", forState: UIControlState.Normal)
                
                cell.emailstatus.tag = indexPath.row
                
                //cell.emailstatus.addTarget(self, action:"primaryVerifyEmail:", forControlEvents: UIControlEvents.TouchUpInside)
                cell.emaildelete.hidden = true
            }
            
            
            cell.emailstatus.setTitleColor(UIColor(red: 65/255, green: 117/255, blue: 5/255, alpha: 1.0), forState: UIControlState.Normal)
            
            cell.emailstatus.enabled = true
        }
        else
        {
            
            if indexPath.row > 0
            {
                cell.emailstatus.setTitle("Verify Email", forState: UIControlState.Normal)
                
                cell.emailstatus.tag = indexPath.row
                
                cell.emailstatus.addTarget(self, action:"verifyEmail:", forControlEvents: UIControlEvents.TouchUpInside)

                //cell.emailstatus.setTitleColor(UIColor(red: 121.0/255, green: 181.0/255, blue: 224.0/255, alpha: 1.0),forState: UIControlState.Normal)
            }
            else
            {
                cell.emailstatus.setTitle("Verify Email", forState: UIControlState.Normal)

                cell.emailstatus.addTarget(self, action:"primaryVerifyEmail:", forControlEvents: UIControlEvents.TouchUpInside)
                
                cell.emaildelete.hidden = true
                cell.emailstatus.frame.size.width = 120
            }
            
        }
        
        return cell
    }
    
    @IBAction func unlinkEmail(sender: UIButton)
    {
    
        self.loaderView.hidden = false
        
        ParseOperations.instance.deleteData(emailList[sender.tag], target: self, successSelector: "unlinkAccountSuccess:", successSelectorParameters: nil, errorSelector: "unlinkAccountError:", errorSelectorParameters: nil)
        
    }
    
    
    func primaryVerifyEmail(sender:UIButton)
    {
        
        self.loaderView.hidden = false
        
        let user = PFUser.currentUser()
        let email = user!.email
        user!.email = ""
        user!.saveInBackgroundWithBlock { result, error in
            if let e = error {
                // Handle the error
                
                self.loaderView.hidden = true
                
                var refreshAlert = UIAlertController(title: "Error", message: "Something went wrong. Could not send verification email", preferredStyle: UIAlertControllerStyle.Alert)
                
                return
            }
            user!.email = email
            user!.saveInBackgroundWithBlock {result, error in
                if let e = error {
                    
                    
                    self.loaderView.hidden = true
                    
                    var refreshAlert = UIAlertController(title: "Error", message: "Something went wrong. Could not send verification email", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    // If you have an error here you're screwed, as your user now has a blank email address
                    return
                }
                else
                {
                    self.loaderView.hidden = true
                    
                    var refreshAlert = UIAlertController(title: "Alert", message: "Verification Email Sent", preferredStyle: UIAlertControllerStyle.Alert)
                }
            }
        }
        
        /*if emailList[sender.tag]["isEmailVerified"] as? Bool == false
        {
            self.loaderView.hidden = false
            
            var emailId = emailList[sender.tag]["emailId"] as? String
            
            var user = PFUser.currentUser()
            user!.email = emailId
            
            user!.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                if success
                {
                    self.loaderView.hidden = true
                    
                    var refreshAlert = UIAlertController(title: "Alert", message: "Verification Email Sent", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    
                    
                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                        
                        
                        
                    }))
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                    
                    
                }
                else
                {
                    self.loaderView.hidden = true
                    
                    var refreshAlert = UIAlertController(title: "Error", message: "Error in Sending Verification Email", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    
                    
                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                        
                        
                        
                    }))
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                    
                    
                }
                
            }
            
        }*/
    }
    
    func verifyEmail(sender:UIButton)
    {

        if emailList[sender.tag]["isEmailVerified"] as? Bool == false
        {
            
            self.loaderView.hidden = false
            
            var emailString = emailList[sender.tag] as PFObject
            
            
            var emailId = emailString["emailId"] as! String
            println(emailId)
            
            
            
            var linkedQuery = PFQuery(className: "LinkedAccounts")
            linkedQuery.whereKey("emailId", equalTo: emailId)
            
            linkedQuery.getFirstObjectInBackgroundWithBlock { (Object:PFObject?, Error:NSError?) -> Void in
                
                if Error != nil
                {
                    self.loaderView.hidden = true
                    println(Error)
                }
                else
                {
                    
                    var data = Object!.objectId!
                    
                    let plainData = (Object!.objectId!).dataUsingEncoding(NSUTF8StringEncoding)
                    
                    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                    println(base64String)
                    
                    var verificationLink = "http://web.eventnode.co/index.php/event/verify-linked-account?accessTokenId=\(base64String)"
                    
                    var verificationMessage = "Hi,<br/><br/>You are being asked to confirm the email address \(emailId) with eventnode<br/><br/>Click here to confirm it:<br/><a href='\(verificationLink)' target='_blank'>\(verificationLink)</a><br/><br/>Thanks,<br/><br/>Eventnode team<br/>support@eventnode.co"
                    
                    var sendVerificationEmail = SendVerificationEmail()
                    
                    sendVerificationEmail.sendEmail("Please verify your e-mail for eventnode", message: verificationMessage, emails: [emailId])
                    
                    self.loaderView.hidden = true
                    
                    var refreshAlert = UIAlertController(title: "Alert", message: "Verification Email Sent", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                        
                        
                        
                    }))
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                    
                    
                }
            }
        }
        
    }
    
    
    func alertEmail()
    {
        var alert = UIAlertController(title: "Add New Email",
            message: "Enter the email address you wish to add.",
            preferredStyle: .Alert)
        
        
        let cancelAction = UIAlertAction(title: "Cancel",
            style: .Default) { (action: UIAlertAction!) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler {
            (textFieldEmail: UITextField!) in
            textFieldEmail.placeholder = "Enter a valid email address"
            textFieldEmail.keyboardType = .EmailAddress
            textFieldEmail.delegate = self
            self.textFieldEmail = textFieldEmail
        }
        
        let saveAction = UIAlertAction(title: "Save",
            style: .Default) { (action: UIAlertAction!) -> Void in
                
                let emailTextField = (alert.textFields![0] as! UITextField).text
                
                self.emailData = emailTextField
                
                //self.emailListtableView.reloadData()
                
                self.loaderView.hidden = false
                
                var userQuery = PFUser.query()
                userQuery?.whereKey("email", equalTo:self.emailData)
                ParseOperations.instance.fetchData(userQuery!, target: self, successSelector: "emailFetchedSuccess:", successSelectorParameters: nil, errorSelector:"emailFetchedError:", errorSelectorParameters: nil)
        }
        
        saveAction.enabled = false
        self.saveAction = saveAction
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        presentViewController(alert,animated: true,completion: nil)
        
    }
    
    func isValidEmail(emailid: NSString)->Bool
    {
        
        var isValid = true
        
        if !emailid.containsString(" ")
        {
            var atRateSplitArray = emailid.componentsSeparatedByString("@")
            
            if(atRateSplitArray.count>=2)
            {
                for component in atRateSplitArray
                {
                    if component as! String == ""
                    {
                        isValid = false
                    }
                }
                
                if(isValid)
                {
                    var dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                    
                    if(dotSplitArray.count>=2)
                    {
                        for component in dotSplitArray
                        {
                            if component as! String == ""
                            {
                                isValid = false
                            }
                        }
                    }
                    else
                    {
                        isValid = false
                    }
                }
            }
            else
            {
                isValid = false
            }
            
        }
        else
        {
            isValid = false
        }
        
        return isValid
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let newText = NSString(string: textField.text).stringByReplacingCharactersInRange(range, withString: string)
        
        if textField == self.textFieldEmail {
            validEmail = isValidEmail(newText)
        }
        
        self.saveAction?.enabled = validEmail
        
        
        return true
    }
    
}