//
//  EventPreviewViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/29/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//


import UIKit
import MapKit
import EventKit
class EventPreviewViewController: UIViewController, UITableViewDelegate {
    
    
    //var imageData: UIImage!
    
    @IBOutlet weak var secondSubView: UIView!
    @IBOutlet weak var hostNameBottom: UILabel!
    @IBOutlet weak var hostName: UILabel!
    @IBOutlet weak var eventLocationText: UITextView!
    
    
    @IBOutlet weak var eventHeaderTitle: UILabel!
    
    @IBOutlet weak var eventTitleText: UITextView!
    @IBOutlet weak var eventStartText: UITextView!
    @IBOutlet weak var eventEndText: UITextView!
    
    @IBOutlet weak var eventLocalStartText: UITextView!
    
    
    @IBOutlet weak var notefromhost: UILabel!
    
    @IBOutlet weak var eventDescriptionText: UITextView!
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var attendEvent: UIButton!
    @IBOutlet weak var notAttendEvent: UIButton!
    @IBOutlet weak var notSure: UIButton!
    @IBOutlet weak var followOnline: UIButton!
    
    var eventTitle: String!
    var eventObject: PFObject!
    var eventStore : EKEventStore = EKEventStore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        subView.layer.cornerRadius = 3
        subView.layer.borderWidth = 0.3
        subView.backgroundColor = UIColor(red: 242/255, green:  242/255, blue: 242/255, alpha: 1.0)
        
        subView.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
        
        
        secondSubView.layer.cornerRadius = 3
        secondSubView.layer.borderWidth = 0.3
        secondSubView.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
        
        
        /*var firstLine : UIView! = UIView(frame: CGRectMake(156, 3, 1, 27))
        firstLine.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1.0)
        self.secondSubView.addSubview(firstLine)*/
        
        //        var secondLine : UIView! = UIView(frame: CGRectMake(, 0, 5, 32))
        //        firstLine.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1.0)
        //        self.secondSubView.addSubview(secondLine)
        //
        //        var thirdLine : UIView! = UIView(frame: CGRectMake(244, 0, 5, 32))
        //        firstLine.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1.0)
        //        self.secondSubView.addSubview(thirdLine)
        
        
        
        
        
        
        //       attendEvent.layer.borderWidth = 0.5
        //        attendEvent.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
        //        notAttendEvent.layer.borderWidth = 0.5
        //        notAttendEvent.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
        //        notSure.layer.borderWidth = 0.5
        //        notSure.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
        //        followOnline.layer.borderWidth = 0.5
        //        followOnline.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
        
        
        
        
        
        // Do any additional setup after loading the view.
        
        
        var latitude = eventObject["eventLatitude"] as! Double
        var longitude = eventObject["eventLongitude"] as! Double
        var eventLocation = eventObject["eventLocation"] as! String
        println(eventObject["eventStartDateTime"] as! NSDate)
        
        //
        
        
        var eventStartDate = eventObject["eventStartDateTime"] as! NSDate
        
        var startTimeStamp = Int64(eventStartDate.timeIntervalSince1970)
        var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
        
        var eventTimezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
        
        var timeStampToBeShown = Int64(startTimeStamp-timezoneOffset+eventTimezoneOffset)
        
        var sdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))
        
        //tblFields["eventTimezoneOffset"] = timezoneOffset
        
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        
        var startDate = "\(monthsArray[smonth-1]) \(sday), \(syear) @ \(shour):\(sminute) \(sam)"
        
        
        
        let ecomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: eventStartDate)
        
        var eam = "AM"
        var ehour: Int!
        if(ecomponents.hour > 12)
        {
            if(ecomponents.hour > 12)
            {
                ehour = ecomponents.hour-12
            }
            else
            {
                ehour = 12
            }
            
            eam = "PM"
        }
        else
        {
            ehour = ecomponents.hour
            eam = "AM"
            if(ecomponents.hour==0){
                ehour = 12
            }
        }
        
        var eminute = "\(ecomponents.minute)"
        let eday = ecomponents.day
        let emonth = ecomponents.month
        let eyear = ecomponents.year
        
        if(ecomponents.minute<10)
        {
            eminute = "0\(eminute)"
        }
        
        var startLocalDate = "\(monthsArray[emonth-1]) \(eday) @ \(ehour):\(eminute) \(eam) (Your Time)"
        
        
        println(startDate)
       // println(endDate)
        println(eventTitle)
        
        
        
        eventTitleText.text = eventTitle
        eventTitleText.textAlignment = NSTextAlignment.Center
        eventTitleText.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
        
        
        eventStartText.text = startDate
        
        eventStartText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        eventStartText.textAlignment = NSTextAlignment.Center
        
        
        //eventLocalStartText.text = startLocalDate
        
        eventLocalStartText.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        eventLocalStartText.textAlignment = NSTextAlignment.Center
        
        eventLocationText.text = eventObject["eventLocation"] as! String!
        eventLocationText.textAlignment = NSTextAlignment.Center
        eventLocationText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        
        
        eventDescriptionText.text = eventObject["eventDescription"] as! String!
        eventDescriptionText.textColor = UIColor.blackColor()
        
        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        eventDescriptionText.attributedText = NSAttributedString(string:eventDescriptionText.text, attributes:attributes)
        eventDescriptionText.font = UIFont(name: "AvenirNext-Regular", size: 15.0)
        eventDescriptionText.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        
        
        let by = "by"
        
        let host =  NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        hostName.text = "\(by) \(host)"
        hostNameBottom.text = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as? String
        
        
        
        
        
        
        let artwork = Artwork(title: eventTitle,
            locationName: eventLocation,
            discipline: "Sculpture",
            coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        
    }
    
    func styleTextFont(textView: UITextView)
    {
        textView.textColor = UIColor.blackColor()
        textView.font = UIFont(name: "AveniNext-Medium", size: 13.0)
    }
    
    
    
    func styleLabelFont(textLabel: UILabel)
    {
        textLabel.textColor = UIColor.whiteColor()
        textLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    @IBAction func closeAdjustPhotoButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventDetailsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventDetailsVC.imageData=imageData
        self.navigationController?.pushViewController(eventDetailsVC, animated: false)*/
        
        self.navigationController?.popViewControllerAnimated(false)
        
    }
    
    @IBAction func closeButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    
    
    @IBAction func addToCalender(sender: AnyObject)
    {
        var eventStore : EKEventStore = EKEventStore()
        // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
        eventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: {
            granted, error in
            if (granted) && (error == nil) {
                println("granted \(granted)")
                println("error  \(error)")
                
                var event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = self.eventTitle
                
                let startDate = self.eventObject["eventStartDateTime"] as! NSDate
                
                // Calendar event needs both start and end date, so set it to the same value: start Date.
                event.startDate = startDate
                event.endDate = startDate
                
                event.notes = self.eventObject["eventDescription"] as! String!
                
                var latitude = self.eventObject["eventLatitude"] as! Double
                var longitude = self.eventObject["eventLongitude"] as! Double
                
                event.location = self.eventObject["eventLocation"] as! String
                event.calendar = eventStore.defaultCalendarForNewEvents
                eventStore.saveEvent(event, span: EKSpanThisEvent, error: nil)
                
                var refreshAlert = UIAlertController(title: "Event Saved", message: "Your event has been saved to the calender.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                    
                    
                }))
                
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
                
                
                
                
            }
        })
    }
    
    
    
    @IBAction func goToMapsButton(sender: AnyObject)
    {
        
        var latitude = eventObject["eventLatitude"] as! Double
        var longitude = eventObject["eventLongitude"] as! Double
        
        
        let openLink = NSURL(string : "http://maps.google.com/maps?q=\(latitude),\(longitude)")
        UIApplication.sharedApplication().openURL(openLink!)
    }
    
    
}
