//
//  EventPhotosViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import AVFoundation
import MediaPlayer
//import Fabric
//import Crashlytics

var myNewPost:PFObject!

var isPostUpdated:Bool! = true

var myEventData = [PFObject]()
var myRowHeights = [CGFloat]()

var eventTextTitle = "Add Text"

class EventPhotosViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate {

    @IBOutlet var tableView : UITableView!
    
    @IBOutlet weak var selectedImageWrapper: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    
    @IBOutlet weak var chatButton: UIButton!

    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var unreadCount: UILabel!
    
    @IBOutlet var loaderView : UIView!
    
    @IBOutlet var loaderSubView : UIView!

    var postHiddenTextView = UITextView()
    
    var newMedia: Bool = true
    var moviePlayer : MPMoviePlayerController?
    

    var moviePlayers = [Int: MPMoviePlayerController]()
    var currentScrollTop = 0
    var hideNow = 0
    var currentUserId: String!
    
    var eventLogoFile: String!
   
    var eventLogoFileUrl: NSURL!
    
    var currentEventObject: PFObject!
    
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        unreadCount.hidden = true
        unreadCount.layer.masksToBounds = true
        unreadCount.layer.cornerRadius = 10

        
        //chatButton.frame.origin.y = headerView.frame.origin.y + ((headerView.frame.height/2)-(chatButton.frame.height/2))
        
        /*let button = UIButton()
        button.frame = CGRectMake(20, 50, 100, 30)
        button.setTitle("Crash", forState: UIControlState.Normal)
        button.addTarget(self, action: "crashButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        view.addSubview(button)*/
        
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        
        var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentEvent.objectId!])
        
        resultSetCount.next()
        
        var unreadMsgCount = resultSetCount.intForColumn("count")
        
        if unreadMsgCount == 0
        {
            unreadCount.hidden = true
        }
        else
        {
            unreadCount.hidden = false
            unreadCount.text = "\(unreadMsgCount)"
            unreadCount.textAlignment = .Center
            unreadCount.font = UIFont(name: "System-Bold", size: 11.0)
            
            var labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
            
            if labelWidth > 20
            {
                unreadCount.frame.size.width = labelWidth+4.0
            }
            
        }
        
        resultSetCount.close()
        
        
        if((currentEvent["eventTitle"] as? String) == "")
        {
            eventTitle.text = "No Title"
        }
        else
        {
            var eventTitleString = (currentEvent["eventTitle"] as? String)!
            
            
            currentEvent["eventTitle"] = prefix(eventTitleString, 1).capitalizedString + suffix(eventTitleString, count(eventTitleString) - 1)
            eventTitle.text = currentEvent["eventTitle"] as? String
        }

        
        if let isLikesReloaded = NSUserDefaults.standardUserDefaults().objectForKey("isLikesReloaded") as? String
        {
            if isLikesReloaded == "false"
            {
                ModelManager.instance.deleteTableData("PostLikes", whereString: "1", whereFields: [])
                NSUserDefaults.standardUserDefaults().setObject("true", forKey: "isLikesReloaded")
            }
        }
        else
        {
            ModelManager.instance.deleteTableData("PostLikes", whereString: "1", whereFields: [])
        }

        
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        var loadingMessage = UILabel()
        loadingMessage.text = "Loading..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)

        indicator.startAnimating()
        
        moviePlayer = MPMoviePlayerController()

        moviePlayer!.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
    
        moviePlayer!.prepareToPlay()
        
        moviePlayer!.shouldAutoplay = false
        moviePlayer!.view.hidden = true
        self.view.addSubview(moviePlayer!.view)
        
       
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "doneButtonClick:", name: MPMoviePlayerWillExitFullscreenNotification, object: nil)
        
        postHiddenTextView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.width)
        
        postHiddenTextView.hidden = true
        
        self.view.addSubview(postHiddenTextView)
        
        tableView.separatorColor = UIColor.clearColor()
        
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("refreshContent"), userInfo: nil, repeats: true)
       
        println(currentEvent)
        
        currentEventObject = currentEvent
        
    }
    
    
    override func viewDidAppear(animated: Bool) {

        super.viewDidAppear(animated)
        
        if((currentEvent["eventTitle"] as? String) == "")
        {
            eventTitle.text = "No Title"
        }
        else
        {
            eventTitle.text = currentEvent["eventTitle"] as? String
        }
        
        deleteData()
        
        if(isPostUpdated == true ){
            
            myEventData.removeAll()
            refreshList()
        }
        
        downloadData()
    }
    
    
    
    @IBAction func crashButtonTapped(sender: AnyObject)
    {
        //var arrayndex = ["1","2","3","4"]
        
        //arrayndex.insert("4", atIndex: 7)
        
       // Crashlytics.sharedInstance().crash()
    }
    
    
    func doneButtonClick(sender: NSNotification){
        println("jiouiop")
        moviePlayer?.setFullscreen(false, animated: true)
        moviePlayer?.stop()
        moviePlayer?.view.hidden = true
    }
    
    
    func refreshList()
    {
        
        isPostDataUpDated = false
        
        myEventData.removeAll()
        myRowHeights.removeAll()
        println("fetching....")
        
        var currentEventId = currentEvent.objectId as String!
        
        /*var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "eventObjectId=?", whereFields: [currentEventId])
        
        resultSetCount.next()
        
        var postCount = resultSetCount.intForColumn("count")
        
        resultSetCount.close()
        
        if(postCount>0)
        {*/
            var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId=? GROUP BY objectId ORDER BY eventImageId DESC", whereFields: [currentEventId])

            myEventData = []

            isPostUpdated = false
            //println("Successfully retrieved \(postCount) posts.")
            
            var i = 0
            
            if (resultSet != nil) {
                while resultSet.next() {
                    
                    var userpost = PFObject(className: "EventImages")
                    
                    userpost["eventImageId"] = Int(resultSet.intForColumn("eventImageId"))
                    
                    var eventImageId = Int(resultSet.intForColumn("eventImageId"))
                    
                    userpost["postData"] = resultSet.stringForColumn("postData")
                    userpost["eventFolder"] = resultSet.stringForColumn("eventFolder")
                    
                    userpost["postType"] = resultSet.stringForColumn("postType")
                    userpost["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
                    userpost["postHeight"] = resultSet.doubleForColumn("postHeight")
                    userpost["postWidth"] = resultSet.doubleForColumn("postWidth")
                    
                    //userpost["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                    //userpost["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                    
                    println(resultSet.stringForColumn("createdAt"))
                    
                    if(resultSet.stringForColumn("createdAt") != "" && resultSet.stringForColumn("updatedAt") != "" && resultSet.stringForColumn("createdAt") != nil && resultSet.stringForColumn("updatedAt") != nil)
                    {
                        userpost.objectId = resultSet.stringForColumn("objectId")
                    }
                    else
                    {
                        
                    }

                    
                    var isPosted = resultSet.stringForColumn("isPosted")
                    
                    userpost["isUploading"] = false
                    
                    if isPosted == "0"
                    {
                        userpost["isPosted"] = false
                        
                        if var newEventImageId = myNewPost["eventImageId"] as? Int
                        {
                            if newEventImageId == eventImageId
                            {
                                userpost["isUploading"] = true
                            }
                        }
                    }
                    else
                    {
                        userpost["isPosted"] = true
                    }
                    
                    

                    
                    /*var isApproved = resultSet.stringForColumn("isApproved")
                    
                    if(isApproved != nil)
                    {
                        if isApproved == "0"
                        {
                            userpost["isApproved"] = false
                        }
                        else
                        {
                            userpost["isApproved"] = true
                        }
                    }
                    else
                    {
                        userpost["isApproved"] = false
                    }*/

                    

                    
                    myEventData.append(userpost)
                    
                    //println(userpost["isPosted"]!)
                    
                    
                    var rowHeight:CGFloat = 380.0
                    
                    if(userpost["postType"] as! String == "text")
                    {
                        var postText = userpost["postData"] as! String
                        
                        /*var charCount: CGFloat = CGFloat(count(postText))

                        var trowCount: CGFloat = (charCount/18)+2*/

                        var senderMessageTemp = UITextView()

                        senderMessageTemp.frame.size.width = self.view.frame.width-self.view.frame.width*(40.0/320)
                        senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)

                        senderMessageTemp.text = postText

                        //senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20)

                        var style = NSMutableParagraphStyle()
                        style.lineSpacing = 8
                        let attributes = [NSParagraphStyleAttributeName : style]


                        var attrs = [
                            NSFontAttributeName : UIFont.systemFontOfSize(20.0),
                            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
                            NSUnderlineStyleAttributeName : 1]


                        senderMessageTemp.attributedText = NSAttributedString(string: postText, attributes:attributes)
                        senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20.0)

                        let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
                        var frame = senderMessageTemp.frame
                        frame.size.height = contentSize.height
                        senderMessageTemp.frame = frame

                        println(rowHeight)

                        rowHeight = senderMessageTemp.frame.height + (0.120625 * self.view.frame.height)+((20.0/568)*self.view.frame.height)
                        //rowHeight = senderMessageTemp.frame.height + 500
                        /*if( trowCount < 11)
                        {
                            rowHeight = 380.0 - (320.0-(trowCount*29.0))
                        }*/
                    }
                    else
                    {
                        println(userpost["postHeight"])
                        var rheight = userpost["postHeight"] as! CGFloat
                        var rwidth = userpost["postWidth"] as! CGFloat

                        println("height: \(rheight)")

                        rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+((45.0/568)*self.view.frame.height)
                    }

                    if i == 0
                    {
                        rowHeight = rowHeight+((30.0/568)*self.view.frame.height)
                    }

                    myRowHeights.append(rowHeight)

                    i++
                }
            }

            resultSet.close()
            println(myRowHeights)
            self.tableView.reloadData()
        /*}
        else
        {
            var query = PFQuery(className:"EventImages")
            
            var currentEventId = currentEvent.objectId as String!
            println(currentEventId)
            
            query.whereKey("eventObjectId", equalTo:"\(currentEventId)")
            query.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
            
            
        }*/
    }
    
    
    func downloadData()
    {
        var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId = '\(currentEvent.objectId!)' GROUP BY objectId ORDER BY eventImageId DESC", whereFields: [])
        
        var postObjectIds: Array<String>
        
        postObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                postObjectIds.append(resultSet.stringForColumn("objectId"))
            }
        }
        
        resultSet.close()
        
        
        var postObjectIdsString = "','".join(postObjectIds)
        
        println("Ids: \(postObjectIdsString)")
        
        var postObjectIdsStringPredicate = ""
        
        if postObjectIdsString == ""
        {
            postObjectIdsStringPredicate = "eventObjectId = '\(currentEvent.objectId!)'"
        }
        else
        {
            postObjectIdsStringPredicate = "NOT (objectId IN {'\(postObjectIdsString)'}) AND eventObjectId = '\(currentEvent.objectId!)'"
        }
        
        let predicate = NSPredicate(format: postObjectIdsStringPredicate)
        
        var query = PFQuery(className:"EventImages", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
        
        var resultSetLikes: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["*"], whereString: "eventObjectId = '\(currentEvent.objectId!)'", whereFields: [])
        
        var likeObjectIds: Array<String>
        
        likeObjectIds = []
        
        if (resultSetLikes != nil) {
            while resultSetLikes.next() {
                likeObjectIds.append(resultSetLikes.stringForColumn("objectId"))
            }
        }
        
        resultSetLikes.close()
        
        
        var likeObjectIdsString = "','".join(likeObjectIds)
        
        println("Ids: \(likeObjectIdsString)")
        
        let likePredicate = NSPredicate(format: "NOT (objectId IN {'\(likeObjectIdsString)'}) AND eventObjectId = '\(currentEvent.objectId!)'")
        
        var likeQuery = PFQuery(className:"PostLikes", predicate: likePredicate)
        
        ParseOperations.instance.fetchData(likeQuery, target: self, successSelector: "fetchAllLikesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllLikesError:", errorSelectorParameters:nil)

    }
    
    
    func deleteData()
    {
        let updatePredicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)'")
        
        var updateQuery = PFQuery(className:"EventImages", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchExistingPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchExistingPostsError:", errorSelectorParameters:nil)
    }


    func fetchExistingPostsSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects
        {
            
            var i = 0
            
            var existingPostObjectIds: Array<String>
            existingPostObjectIds = []
            
            for postObject in fetchedobjects
            {
                existingPostObjectIds.append(postObject.objectId!)
            }
            
            
            var existingPostObjectIdsString = "','".join(existingPostObjectIds)
            
            var whereQuery = ""
            
            if existingPostObjectIdsString != ""
            {
                whereQuery = "eventObjectId = '\(currentEvent.objectId!)' AND objectId NOT IN ('\(existingPostObjectIdsString)') AND objectId != ''"
            }
            else
            {
                whereQuery = "eventObjectId = '\(currentEvent.objectId!)' AND objectId != ''"
            }
            
            var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["objectId"], whereString: whereQuery, whereFields: [])
            
            
            var nonExistingPostObjectIds: Array<String>
            nonExistingPostObjectIds = []
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    nonExistingPostObjectIds.append(resultSet.stringForColumn("objectId"))
                }
            }
            
            resultSet.close()
            
            var nonExistingPostObjectIdsString = "','".join(nonExistingPostObjectIds)
            
            if nonExistingPostObjectIdsString != ""
            {
                ModelManager.instance.deleteTableData("EventImages", whereString: "objectId IN ('\(nonExistingPostObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventLikes", whereString: "postObjectId IN ('\(nonExistingPostObjectIdsString)')", whereFields: [])
                
                refreshList()
            }
           
        }
    }
    
    
    func fetchExistingPostsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func fetchAllLikesSuccess(timer:NSTimer)
    {
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if var fetchedobjects = objects {
            var i=0;
            for object in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventObjectId"] = object["eventObjectId"] as? String
                tblFields["postObjectId"] = object["postObjectId"] as? String
                tblFields["userObjectId"] = currentUserId
                tblFields["objectId"] = object.objectId!
                tblFields["isUpdated"] = "1"
                
                
                var insertedId = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
            }
            
            self.refreshList()
        }
    }
    
    func fetchAllLikesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func refreshContent()
    {
        var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentEvent.objectId!])
        
        resultSetCount.next()
        
        var unreadMsgCount = resultSetCount.intForColumn("count")
        
        if unreadMsgCount == 0
        {
            unreadCount.hidden = true
        }
        else
        {
            unreadCount.hidden = false
            unreadCount.text = "\(unreadMsgCount)"
            unreadCount.textAlignment = .Center
            unreadCount.font = UIFont(name: "System-Bold", size: 11.0)
            
            var labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
            
            if labelWidth > 20
            {
                unreadCount.frame.size.width = labelWidth+4.0
            }
        }

        resultSetCount.close()


        if isPostDataUpDated
        {
            refreshList()
        }
    }
    
    func stringToDate(dateString: String)->NSDate
    {
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        var date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
         self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func addCommentButtonClicked(sender : AnyObject)
    {
        
        println(currentEvent)
        
        let addCommentVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddCommentsViewController") as! AddCommentsViewController
        
        addCommentVC.isShared = false
        
        self.navigationController?.pushViewController(addCommentVC, animated: true)
        
    }
    
    
    @IBAction func addTextButtonClicked(sender : AnyObject)
    {
        
        myNewPost = PFObject(className:"EventImages")
        myNewPost["postData"] = ""
        
        eventTextTitle = "Add Text"
        
        let addTextVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddtextViewController") as! AddtextViewController
        self.navigationController?.pushViewController(addTextVC, animated: false)
    }
    
    @IBAction func inviteFriendsButtonClicked(sender : AnyObject)
    {
        if currentEvent["isRSVP"] as! Bool == true
        {
            let addTextVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageGuestViewController") as! ManageGuestViewController
            
            self.navigationController?.pushViewController(addTextVC, animated: false)
            
 
        }
        else
        {
            let manageFreinds = self.storyboard!.instantiateViewControllerWithIdentifier("ManageFreindsViewController") as! ManageFreindsViewController
            
            self.navigationController?.pushViewController(manageFreinds, animated: false)
            

        }
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject)
    {
        let EventSettingsView = self.storyboard!.instantiateViewControllerWithIdentifier("EventSettingsViewController") as! EventSettingsViewController
        self.navigationController?.pushViewController(EventSettingsView, animated: false)
    }
    
    
    @IBAction func capture(sender : UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
            println("Button capture")
            
            var imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage,kUTTypeMovie]
            imag.allowsEditing = false
            imag.videoMaximumDuration = 30
            
            newMedia = true
            
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func showMoreOptions(sender: UIButton) {
        println("More button clicked")
        
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete", otherButtonTitles: "Share")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        println("ActionSheet clickedButtonAtIndex \(buttonIndex)")
        switch (buttonIndex){
        case 0:
            println("Delete")
            deleteSelectedPost(actionSheet.tag)
        case 1:
            println("Cancel")
            // Nothing to do. Popup menu closes.
        case 2:
            println("Share")
            shareSelectedPost(actionSheet.tag)
        default:
            println("Default")
            // Should not happen here.
        }
    }
    
    // This is to support share content to various social networks, mail, messages, etc.
    func shareSelectedPost(row: Int) {
        var postType = myEventData[row]["postType"] as! String!
        
        println("Share Selected Post \(postType) \(row)")
        
        if(postType == "text") {
            var textPostToShare : String = myEventData[row]["postData"] as! String
            var objectsToShare : [String] = [textPostToShare]
            
            let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
            self.navigationController!.presentViewController(activityVC,
                animated: true,
                completion: nil)
        }
        
        if(postType == "image" || postType == "video") {
            var fileName = myEventData[row]["postData"] as! String
            
            var eventFolder = myEventData[row]["eventFolder"] as! String
            
            var eventMediaFilePath = "\(documentDirectory)/\(fileName)"
                
            println(eventMediaFilePath)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventMediaFilePath)) {
            
                if (postType == "image") {
                    var shareImage = UIImage(named: eventMediaFilePath)
                
                    var objectsToShare : [UIImage] = [shareImage!]
              
                    let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
                    self.navigationController!.presentViewController(activityVC, animated: true, completion: nil)

                } else {
                    var videoUrl = NSURL(fileURLWithPath: eventMediaFilePath)
                    
                    var objectsToShare : [NSURL] = [videoUrl!]
                    
                    let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
                    self.navigationController!.presentViewController(activityVC, animated: true, completion: nil)
                }
            }
                
        }
    }
    
    func deleteSelectedPost(row: Int)
    {
        self.loaderView.hidden = true
        
        if( myEventData[row]["isPosted"] as! Bool == false && myEventData[row].objectId == nil)
        {
            var isDeleted = ModelManager.instance.deleteTableData("EventImages", whereString: "eventImageId=?", whereFields: [myEventData[row]["eventImageId"]!])
            
            myEventData.removeAtIndex(row)
            myRowHeights.removeAtIndex(row)
            
            tableView.reloadData()
            
            println("dwde")
        }
        else
        {
            var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("deletePostFromParse:"), userInfo: myEventData[row], repeats: false)
        }
    }
    
    func deletePostFromParse(timer: NSTimer)
    {
        if MyReachability.isConnectedToNetwork()
        {
            var eventToBeDeleted: PFObject = timer.userInfo as! PFObject
            ParseOperations.instance.deleteData(eventToBeDeleted, target: self, successSelector: "deletePostSuccess:", successSelectorParameters: nil, errorSelector: "deletePostError:", errorSelectorParameters:nil)
        }
        else
        {
            var refreshAlert = UIAlertController(title: "Error", message: "Post cannot be deleted as you seem to be offline. Please check your networks settings.", preferredStyle: UIAlertControllerStyle.Alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                self.loaderView.hidden=true
            }))

            self.presentViewController(refreshAlert, animated: true, completion: nil)
            tableView.reloadData()
        }
    }


    func likeSelectedPost(sender : UIButton)
    {
        
    }
    
    
    func deletePostSuccess(timer:NSTimer)
    {
        
        var eventToBeDeleted: PFObject = timer.userInfo?.valueForKey("internal") as! PFObject
        
        var isDeleted = ModelManager.instance.deleteTableData("EventImages", whereString: "eventImageId=?", whereFields: [eventToBeDeleted["eventImageId"]!])
        
        refreshList()
        
        if eventToBeDeleted["postType"] as! String != "text"
        {
            var eventFolder = eventToBeDeleted["eventFolder"] as! String
            var eventFile = eventToBeDeleted["postData"] as! String
            
            var deleteRequest = AWSS3DeleteObjectRequest()
            
            
            deleteRequest.bucket = "eventnode1"
            deleteRequest.key = "\(eventFolder)\(eventFile)"
            
            var s3 = AWSS3.defaultS3()
            
            
            
            s3.deleteObject(deleteRequest).continueWithBlock {
                (task: AWSTask!) -> AnyObject! in
                
                if(task.error != nil){
                    println("not deleted cropped")
                    
                }else{
                    
                    println("deleted cropped")
                }
                return nil
            }
        }
    }
    
    func deletePostError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        var refreshAlert = UIAlertController(title: "Error", message: "Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.loaderView.hidden=true
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
        tableView.reloadData()
        
        println("error occured \(error.description)")
    }


    func playSelectedVideo(sender : UIButton) {
        var videoUrl: String = (myEventData[sender.tag]["postData"] as? String)!
        
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath("\(documentDirectory)/\(videoUrl)"))
        {
            moviePlayer?.contentURL = NSURL(fileURLWithPath: "\(documentDirectory)/\(videoUrl)")
            moviePlayer?.view.hidden = false
            moviePlayer!.setFullscreen(true, animated: true)
            //moviePlayer!.scalingMode = .AspectFill
            moviePlayer!.controlStyle = .Embedded
            moviePlayer!.play()
        }
        else
        {
            var refreshAlert = UIAlertController(title: "Error", message: "Video doesn't exist.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                self.loaderView.hidden=true
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func addMediaButtonClicked(sender : AnyObject){

        // TODO(geetikak, dimpal): Why are we not checking for SavedPhotosAlbum ?
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum){
            println("Button capture")
            self.loaderView.hidden = false
            
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.mediaTypes = [kUTTypeImage,kUTTypeMovie]
            imagePicker.allowsEditing = false
            imagePicker.videoMaximumDuration = 30
            
            self.presentViewController(imagePicker, animated: true,
                completion: nil)
            newMedia = false
            
        }
        else
        {
            NSLog("failed")
            loaderView.hidden=true
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
    {
        NSLog("Success.")
        self.loaderView.hidden = true
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        NSLog(mediaType)
        self.dismissViewControllerAnimated(true, completion: nil)

        var uploadAllow: Bool = true
        
        var date = NSDate()
        let currentTimeStamp = String(Int64(date.timeIntervalSince1970*1000))
        
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        
        
        if mediaType == (kUTTypeImage as! String)
        {
            println("image")
            
            var imageData = info[UIImagePickerControllerOriginalImage] as? UIImage
            
            if newMedia
            {
                UIImageWriteToSavedPhotosAlbum(imageData, self, Selector(), UnsafeMutablePointer<Void>())
            }
            
            eventLogoFile = "\(currentTimeStamp)_\(currentUserId)_eventstreamphoto.png"
            
            myNewPost = PFObject(className:"EventImages")
            myNewPost["postData"] = eventLogoFile
            myNewPost["postHeight"] = imageData?.size.height
            myNewPost["postWidth"] = imageData?.size.width
            myNewPost["postType"] = "image"
            myNewPost["eventObjectId"] = currentEvent.objectId!
            
            myNewPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
            
            
            eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(eventLogoFile))
            
            var cropdata = UIImageJPEGRepresentation(self.correctlyOrientedImage(imageData!), 0.3)
            
            var result = cropdata.writeToURL(eventLogoFileUrl!, atomically: true)
        }
        
        
        if mediaType == (kUTTypeMovie as! String)
        {
            println("movie")
            
            println(info)
            
             var videoURL = NSURL()
            
            if var videoURLTemp = info[UIImagePickerControllerMediaURL] as? NSURL
            {
                videoURL = videoURLTemp
            }
            else
            {
             
                if var videoURLTemp = info[UIImagePickerControllerReferenceURL] as? NSURL
                {
                    videoURL = videoURLTemp
                }
                else
                {
                    uploadAllow = false
                    self.loaderView.hidden = true
                    var refreshAlert = UIAlertController(title: "Error", message: "The video you selected needs to be fully downloaded into your device before it can be added.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                        
                    }))
                    
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                }
            }
            
            if uploadAllow == true
            {
                var videoURLString = videoURL.relativePath                
                
                var splitExt = videoURLString!.componentsSeparatedByString(".")
                
                var ext = splitExt[splitExt.count-1]
                
                println(ext)
                
                var assetOptions = [AVURLAssetPreferPreciseDurationAndTimingKey : 1]
                var videoAsset = AVURLAsset(URL: videoURL, options: assetOptions)
                var error:NSError?
                
                var videoAssetReader=AVAssetReader(asset: videoAsset, error: &error)
                
                var duration = CMTimeGetSeconds(videoAsset.duration)
                println(videoAsset.duration)
                println(duration)
                
                if(duration<=31)
                {
                    if newMedia
                    {
                        UISaveVideoAtPathToSavedPhotosAlbum(videoURLString, self, Selector(), UnsafeMutablePointer<Void>())
                    }
                    
                    if var videoDataTemp: NSData = NSData(contentsOfURL: videoURL)
                    {
                        var videoData: NSData = NSData(contentsOfURL: videoURL)!
                        
                        eventLogoFile = "\(currentTimeStamp)_\(currentUserId)_eventstreamvideo.\(ext)"
                        eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(eventLogoFile))
                        
                        var result = videoData.writeToURL(eventLogoFileUrl!, atomically: true)
                        
                        
                        var videoFrame = getVideoFrame(eventLogoFileUrl)
                        
                        
                        myNewPost = PFObject(className:"EventImages")
                        myNewPost["postData"] = eventLogoFile
                        myNewPost["postHeight"] = videoFrame.height
                        myNewPost["postWidth"] = videoFrame.width
                        
                        myNewPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
                        
                        myNewPost["postType"] = "video"
                        myNewPost["eventObjectId"] = currentEvent.objectId!
                    }
                    else
                    {
                        
                        uploadAllow = false
                        self.loaderView.hidden = true
                        var refreshAlert = UIAlertController(title: "Error", message: "The video you selected needs to be fully downloaded into your device before it can be added.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                            
                        }))
                        
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                    }
                }
                else
                {
                    uploadAllow = false
                    self.loaderView.hidden = true
                    var refreshAlert = UIAlertController(title: "Error", message: "Videos must be 30 seconds or less.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                        
                    }))
                    
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                }
                
            }
            
        }
        
        if uploadAllow
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["postData"] = myNewPost["postData"] as? String
            
            var postHeight = myNewPost["postHeight"] as! CGFloat
            tblFields["postHeight"] = "\(postHeight)"
            
            var postWidth = myNewPost["postWidth"] as! CGFloat
            tblFields["postWidth"] = "\(postWidth)"
            
            tblFields["eventObjectId"] = myNewPost["eventObjectId"] as? String
            
            var postType = myNewPost["postType"] as! String
            tblFields["postType"] = "\(postType)"
            
            tblFields["eventFolder"] = myNewPost["eventFolder"] as? String
            tblFields["isPosted"] = "0"
            tblFields["objectId"] = ""
            
            var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
            if insertedId>0
            {
                refreshList()
                
                myNewPost["eventImageId"] = insertedId
                myNewPost["isUploading"] = true
                
                let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                
                uploadRequest.bucket = "eventnode1"
                println(self.currentUserId)
                println(currentEvent.objectId!)
                println(eventLogoFile)
                println(eventLogoFileUrl)
                uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(eventLogoFile)"
                uploadRequest.body = eventLogoFileUrl
                upload(uploadRequest, insertedId: insertedId, postToBeUploaded: myNewPost)
            }
            else
            {
                self.loaderView.hidden = true
                Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
            }

        }

        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
          self.loaderView.hidden = true
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        var normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int, postToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
                            })
                            break;
                            
                        default:
                            println("upload() failed: [\(error)]")
                            self.loaderView.hidden=true
                            self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
                            break;
                        }
                    } else {
                        println("upload() failed: [\(error)]")
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
                    }
                } else {
                    println("upload() failed: [\(error)]")
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
                }
            }
            
            if let exception = task.exception {
                println("upload() failed: [\(exception)]")
                self.loaderView.hidden=true
                self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in

                    println("file uploaded. creating post now....")
                    
                    ParseOperations.instance.saveData(postToBeUploaded, target: self, successSelector: "createPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createPostError:", errorSelectorParameters:postToBeUploaded)
                    
                })
            }
            return nil
        }
    }
    
    func fetchAllPostsSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        isPostUpdated = false
        println("Successfully retrieved \(objects!.count) posts.")

        if var fetchedobjects = objects {

            self.loaderView.hidden=true
            var i=0;
            for post in fetchedobjects
            {
                
               /* var rowHeight:CGFloat = 380.0
                
                if(post["postType"] as! String == "text")
                {
                    var postText = post["postData"] as! String
                    
                    /*var charCount: CGFloat = CGFloat(count(postText))
                    
                    var trowCount: CGFloat = (charCount/18)+2*/
                    
                    var senderMessageTemp = UITextView()
                    
                    senderMessageTemp.frame.size.width = self.view.frame.width-20
                    senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
                    
                    senderMessageTemp.text = postText
                    
                    senderMessageTemp.font = UIFont(name: "AvenirNext-Medium", size: 12)
                    
                    let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
                    var frame = senderMessageTemp.frame
                    frame.size.height = contentSize.height
                    senderMessageTemp.frame = frame
                    
                    println(rowHeight)
                    
                    rowHeight = contentSize.height + (0.120625 * self.view.frame.height)
                }
                else
                {
                    var rheight = post["postHeight"] as! CGFloat
                    var rwidth = post["postWidth"] as! CGFloat
                    
                    rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+60.0
                }
                
                if i == 0
                {
                    rowHeight = rowHeight+30.0
                }
                */
                //myEventData[i]["isPosted"] = true
                
                //myRowHeights.append(rowHeight)

                var tblFields: Dictionary! = [String: String]()

                tblFields["postData"] = post["postData"] as? String

                tblFields["isApproved"] = "0"

                var postHeight = post["postHeight"] as! CGFloat
                var postWidth = post["postWidth"] as! CGFloat

                tblFields["postHeight"] = "\(postHeight)"
                tblFields["postWidth"] = "\(postWidth)"
                tblFields["eventObjectId"] = post["eventObjectId"] as? String

                tblFields["eventFolder"] = post["eventFolder"] as? String

                tblFields["postType"] = post["postType"] as? String

                tblFields["objectId"] = post.objectId
                tblFields["isPosted"] = "1"

                var date = ""

                if post.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.createdAt)!)
                    println(date)
                    tblFields["createdAt"] = date
                }

                if post.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.updatedAt)!)
                    println(date)
                    tblFields["updatedAt"] = date
                }
                
                var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
                
                if insertedId>0
                {
                    //myEventData[i]["eventImageId"] = insertedId
                    println("Record inserted at \(insertedId).")
                }
                else
                {
                    self.loaderView.hidden = true
                    println("Error in inserting record.")
                }
                
                i++;

                
            }
            
            //self.tableView.reloadData()
            self.refreshList()
        }
    }
    
    func fetchAllPostsError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
    }
    
    func createPostSuccess(timer:NSTimer)
    {
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var postId = timer.userInfo?.valueForKey("external") as! Int!
        
        self.loaderView.hidden=true
        isPostUpdated = true
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            println(date)
            tblFields["updatedAt"] = date
        }
        
        
        var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            println("Record Updated Successfully")
            println("eventImage")
        } else {
            println("Record not Updated Successfully")
        }

        
        let predicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)' AND isApproved = true")
        
        var query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: eventObject, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)

        
        self.refreshList()
    }
    
    func createPostError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        var eventObject: PFObject = timer.userInfo?.valueForKey("external") as! PFObject
        
        println("error")
        
        var refreshAlert = UIAlertController(title: "Error", message: "There’s bad or no internet connection. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
        

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            self.refreshList()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction!) in
            self.loaderView.hidden=false
            if eventObject["postType"] as! String == "text"
            {
                ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createPostSuccess:", successSelectorParameters: eventObject["eventImageId"] as! Int, errorSelector: "createPostError:", errorSelectorParameters:eventObject)
            }
            else
            {
                var imageName = eventObject["postData"] as! String
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                var eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(imageName))
                
                let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                
                uploadRequest.bucket = "eventnode1"
                uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(imageName)"
                uploadRequest.body = eventLogoFileUrl
                self.upload(uploadRequest, insertedId: eventObject["eventImageId"] as! Int, postToBeUploaded: eventObject)
            }
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        var postObject = timer.userInfo?.valueForKey("external") as! PFObject
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            var eventFolder = currentEvent["eventFolder"] as! String!
            var eventImage = currentEvent["eventImage"] as! String!
            
            var eventType = "online"
            if currentEvent["isRSVP"] as! Bool == true
            {
                eventType = "rsvp"
            }
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            var notificationObjects = [PFObject]()
            
            var fetchedUserEmailIds: Array<String>
            fetchedUserEmailIds = []
            
            var eventTitle = currentEvent["eventTitle"] as! String
            var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            var postType = postObject["postType"] as! String
            
            var notifMessage = ""
            
            if postType == "video"
            {
                notifMessage = "\(fullUserName) posted a new video to the event, \(eventTitle)"
            }
            else
            {
                notifMessage = "\(fullUserName) posted a new photo to the event, \(eventTitle)"
            }
            
            var eventCreatorId = ""
            
            var i = 0
            
            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    
                    fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                    fetchedUserEmailIds.append(invitation["emailId"] as! String)
                    
                    var notificationObject = PFObject(className: "Notifications")
                    notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                    notificationObject["notificationImage"] = "profilePic.png"
                    notificationObject["senderId"] = currentUserId
                    notificationObject["receiverId"] = invitation["userObjectId"] as! String
                    notificationObject["notificationActivityMessage"] = notifMessage
                    notificationObject["eventObjectId"] = currentEvent.objectId!
                    notificationObject["notificationType"] = "postnewcontent"
                    
                    notificationObjects.append(notificationObject)
                }
                
                fetchedobjects[i]["isEventStreamUpdated"] = true
                fetchedobjects[i]["isTextUpdated"] = false
                
                i++
                
            }


            PFObject.saveAllInBackground(fetchedobjects)


//            if postType == "image"
//            {
//                var newContent = NewContent()
//                
//                var emailMessage = newContent.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: eventType)
//                
//                
//                var sendEmailObject = SendEmail()
//                
//                sendEmailObject.sendEmail("\(fullUserName) posted a new photo for the event, \(eventTitle)", message: emailMessage, emails: fetchedUserEmailIds)
//            }
            
            var eventCreatorObjectId = currentUserId
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //eventCreatorObjectId

            PFObject.saveAllInBackground(notificationObjects)


            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            
            var createdAt = dateFormatter.stringFromDate((postObject.createdAt)!)
            
            var updatedAt = dateFormatter.stringFromDate((postObject.updatedAt)!)
            
            var postData =  postObject["postData"] as! String
            
            var postHeight = postObject["postHeight"] as! Double
            var postWidth = postObject["postWidth"] as! Double
            var postFolder = postObject["eventFolder"] as! String

            eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
            
            var data: Dictionary<String, String!> = [
                "alert" : "\(notifMessage)",
                "notifType" :  "postnewcontent",
                "objectId" :  postObject.objectId!,
                "eventObjectId" :  currentEvent.objectId!,
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "postHeight": "\(postHeight)",
                "postWidth": "\(postWidth)",
                "eventFolder": "\(postFolder)",
                "postData" : "\(postData)",
                "postType": "\(postType)",
                "badge": "Increment",
                "sound": "default",
                "eventCreatorId" : "\(eventCreatorId)"
            ]
            
            var fetchedUserObjectIdsString = "','".join(fetchedUserObjectIds)
            
            
            var predicateString: String! = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = true"
            
            sendParsePush(predicateString, data: data)
            
            
            data["sound"] = ""
            
            predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = false"
            sendParsePush(predicateString, data: data)
            
            
        }
    }
    
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }

    
    
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        var predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        var query = PFUser.queryWithPredicate(predicate)
        
        var push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                println(objects?.count)
                if var fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        var userObjectId = object.objectId!
                        
                        var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        var query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                println(objects?.count)
                                if var fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    var query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }
    
    
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int, postToBeUploaded: PFObject)
    {
        var refreshAlert = UIAlertController(title: "Error", message: "There’s bad or no internet connection. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
            self.refreshList()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction!) in
            self.loaderView.hidden=false
            if postToBeUploaded["postType"] as! String == "text"
            {
                ParseOperations.instance.saveData(postToBeUploaded, target: self, successSelector: "createPostSuccess:", successSelectorParameters: postToBeUploaded["eventImageId"] as! Int, errorSelector: "createPostError:", errorSelectorParameters:postToBeUploaded)
            }
            else
            {
                var imageName = postToBeUploaded["postData"] as! String
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                var eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(imageName))
                
                let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                
                uploadRequest.bucket = "eventnode1"
                uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(imageName)"
                uploadRequest.body = eventLogoFileUrl
                self.upload(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
            }
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    private func playVideo(url: NSURL) {
        if let
            moviePlayer = MPMoviePlayerController(contentURL: url) {
                self.moviePlayer = moviePlayer
                
                moviePlayer.view.frame = CGRectMake(self.view.frame.size.width*(20/320),self.view.frame.size.height*(20/568), self.view.frame.size.width*(280/320),self.view.frame.size.height*(400/568))
                
               
                moviePlayer.prepareToPlay()
                moviePlayer.shouldAutoplay = false
                moviePlayer.scalingMode = .AspectFill
                moviePlayer.controlStyle = .None
                self.view.addSubview(moviePlayer.view)
        } else {
            debugPrintln("Ops, something wrong when playing video.m4v")
        }
    }
    

    func scrollViewDidScroll(_scrollView: UIScrollView){
        var newScroll = Int(_scrollView.contentOffset.y)
        if(currentScrollTop <= newScroll){
            if(hideNow > 65){
                headerView.hidden = true
                
                UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.headerView.frame.origin.y = -70
                    }, completion: nil)
                
            }
            hideNow+=Int(_scrollView.contentOffset.y)
        }
        else
        {
            hideNow=0
            headerView.hidden = false
            
            UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.headerView.frame.origin.y = 20
                }, completion: nil)
            
        }
        
        currentScrollTop = Int(_scrollView.contentOffset.y)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return myEventData.count
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {

        let row = section.row
        
        return myRowHeights[row]
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        

        let row = indexPath.row
        
        var postType = myEventData[row]["postType"] as! String!
        
        var cellIdentifier: String! = "EventPhotoCell1"

 
        var cell: EventPhotosTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? EventPhotosTableViewCell
        
        
        if (cell == nil)
        {
            cell = EventPhotosTableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: cellIdentifier)
        }
        
        
        for view in cell!.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        var viewTop: CGFloat = 0
        
        if row == 0
        {
            viewTop = (30.0/568)*self.view.frame.height
        }
        
        var heightDiff: CGFloat = 0

        var postTextView = UITextView()
        var textBackgroundView = UIView()
        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 8
        let attributes = [NSParagraphStyleAttributeName : style]
        
        
        var attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(20.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]

        
        if(postType == "text")
        {
            var postTextClickableView = UIView()
            
            postTextView.text = myEventData[row]["postData"] as! String

            postTextView.attributedText = NSAttributedString(string:postTextView.text, attributes:attributes)
            postTextView.font = UIFont(name: "Tigerlily", size: 20.0)
            postTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

            postTextView.editable = false
            postTextView.selectable = false
            postTextView.scrollEnabled = false

            
            var postText = myEventData[row]["postData"] as! String
            
            
            var charCount: CGFloat = CGFloat(count(postText))
            
            var postTextHeight: CGFloat = 320
            /*
            var trowCount: CGFloat = (charCount/18) + 2
            
            if( trowCount < 11)
            {
                postTextHeight = trowCount*29
                heightDiff = 320-(trowCount*29)
            }
            */
            /*var charCount: CGFloat = CGFloat(count(postText))
            
            var trowCount: CGFloat = (charCount/18)+2*/
            
            var style = NSMutableParagraphStyle()
            style.lineSpacing = 8
            let attributes = [NSParagraphStyleAttributeName : style]
            
            
            var attrs = [
                NSFontAttributeName : UIFont.systemFontOfSize(20.0),
                NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
                NSUnderlineStyleAttributeName : 1]
            
            var senderMessageTemp = UITextView()
            
            senderMessageTemp.frame.size.width = cell!.contentView.frame.width-self.view.frame.width*(40.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
            
            senderMessageTemp.text = postText
            
            senderMessageTemp.attributedText = NSAttributedString(string:myEventData[row]["postData"] as! String, attributes:attributes)
            
            //self.view.addSubview(senderMessageTemp)
            
            
            
            //senderMessageTemp.hidden = true
            
            
            senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            postTextView.frame.size.width = cell!.contentView.frame.width-self.view.frame.width*(20.0/320)
            postTextView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop+((20.0/568)*self.view.frame.height), cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height)
            
            postTextView.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 0)
            
            postTextClickableView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop, cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height)
            
            
            
            textBackgroundView.frame = CGRectMake(0, viewTop, cell!.contentView.frame.width, contentSize.height+((40.0/568)*self.view.frame.height))
            
            textBackgroundView.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            cell!.contentView.addSubview(textBackgroundView)
            cell!.contentView.addSubview(postTextView)
            cell!.contentView.addSubview(postTextClickableView)
        }
        
        if row == 0
        {
            if(postType == "text")
            {
                heightDiff = heightDiff-((35.0/568)*self.view.frame.height)
            }
            else
            {
                heightDiff = heightDiff-((30.0/568)*self.view.frame.height)
            }
        }
        
        var rheight = myEventData[row]["postHeight"] as! CGFloat
        var rwidth = myEventData[row]["postWidth"] as! CGFloat
        
        var postImageView = UIImageView()
        
        if(rheight>0 && rwidth>0)
        {
            
            postImageView.frame.size.width = self.tableView.frame.width
            postImageView.frame.size.height = (rheight/rwidth)*self.tableView.frame.width
        
            postImageView.frame.origin.x = 0
            postImageView.frame.origin.y = viewTop
        }
        if(postType == "image")
        {
            
            var imageName = myEventData[row]["postData"] as! String
            
            var eventFolder = myEventData[row]["eventFolder"] as! String
            
            var eventImagePath = "\(documentDirectory)/\(imageName)"
            
            println(eventImagePath)
            
            cell!.contentView.addSubview(postImageView)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventImagePath)) {
                
                var image = UIImage(named: eventImagePath)
                
                postImageView.image = image
                
                //cell!.contentView.addSubview(postImageView)
                
            }
            else
            {
                let s3BucketName = "eventnode1"
                let fileName = imageName
            
                let downloadFilePath = documentDirectory.stringByAppendingPathComponent(fileName)
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
            
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
            
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
               /* transferManager.download(downloadRequest).continueWithSuccessBlock({
                    (task: AWSTask!) -> AWSTask! in
                    dispatch_async(dispatch_get_main_queue(), {
                        var image = UIImage(named: "\(documentDirectory)/\(imageName)")
                        
                        var postImageView = UIImageView()
                        
                        postImageView.image = image
                        
                        cell!.contentView.addSubview(postImageView)
                    })
                    return nil
                })*/
                
                
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                println("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            println("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        println("downloading successfull")
                        
                        var image = UIImage(named: "\(documentDirectory)/\(imageName)")
                        
                        //var postImageView = UIImageView()
                        
                        postImageView.image = image
                        
                        //cell!.contentView.addSubview(postImageView)
                    }
                    
                    return nil
                    
                })
            }
        }


        if(postType == "video")
        {
           
            cellIdentifier = "EventPhotoCell3"
            
            var videoName = myEventData[row]["postData"] as! String

            var eventFolder = myEventData[row]["eventFolder"] as! String

            var eventVideoPath = "\(documentDirectory)/\(videoName)"
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventVideoPath)) {
                
                var postPlayButton = UIButton()
                
                postPlayButton.frame = postImageView.frame
                
                postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)

                postPlayButton.tag = indexPath.row
                postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
                
                
                var videoUrl = NSURL(fileURLWithPath: eventVideoPath)
                self.getFirstFrame(postImageView, videoURL: videoUrl!, viewTop: viewTop)
                cell!.contentView.addSubview(postImageView)
                cell!.contentView.addSubview(postPlayButton)
            }
            else
            {
                let s3BucketName = "eventnode1"
                let fileName = videoName
                
                let downloadFilePath = documentDirectory.stringByAppendingPathComponent(fileName)
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                println("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            println("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        println("downloading successfull")
                        
                        
                        var postPlayButton = UIButton()
                        
                        postPlayButton.frame = postImageView.frame
                        
                        postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
                        postPlayButton.tag = indexPath.row
                        postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
                        
                        var videoUrl = NSURL(fileURLWithPath: eventVideoPath)
                        
                        self.getFirstFrame(postImageView, videoURL: videoUrl!, viewTop: viewTop)
                        cell!.contentView.addSubview(postImageView)
                        cell!.contentView.addSubview(postPlayButton)
                    }
                    
                    return nil
                    
                })
            }
        }
        
        /*var pdate: NSDate!
        
        if(myEventData[row].createdAt != nil)
        {
            pdate = myEventData[row].createdAt
        }
        else
        {
            pdate = myEventData[row]["dateCreated"] as! NSDate
        }
        
        println(pdate)
        
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: pdate!)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        var postDate = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        */
        var infoView = UIView()
        
        if rheight>0 && rwidth>0
        {
            infoView.frame = CGRectMake(0, postImageView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
        }
        else
        {
            infoView.frame = CGRectMake(0, textBackgroundView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
        }
        
        // Add More Options Button
        var moreOptionsImageView = UIImageView()
    
        moreOptionsImageView.frame = CGRectMake(0, 12, 30, 8)
        
        moreOptionsImageView.image = UIImage(named:"more.png")
        
        var moreButton = UIButton()
        //moreButton.backgroundColor = UIColor.blueColor()
        
        moreButton.addSubview(moreOptionsImageView)

        moreButton.frame = CGRectMake(self.view.frame.width - 15 - moreButton.sizeThatFits(moreButton.bounds.size).width, 0, 30,moreButton.sizeThatFits(moreButton.bounds.size).height)
        moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        
        moreButton.tag = indexPath.row
        
        moreButton.addTarget(self, action:"showMoreOptions:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // Add Like Button
        var likeButton = UIButton()
        
        likeButton.setTitle ("", forState: UIControlState.Normal)

        likeButton.frame = CGRectMake(15, 0, (0.120625)*380,infoView.frame.height)
        
        likeButton.tag = indexPath.row
        
        likeButton.addTarget(self, action:"likeSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)

        var totalLikeCount = 0
        
        if var postObjId = myEventData[row].objectId
        {
            var resultSetTotalCount: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["count(*) as count"], whereString: "postObjectId=?", whereFields: [myEventData[row].objectId!])
            
            resultSetTotalCount.next()
            
            totalLikeCount = Int(resultSetTotalCount.intForColumn("count"))
            resultSetTotalCount.close()
        }
        
        var postLikeText = UILabel()

        //postLikeText.frame = CGRectMake(self.view.frame.size.width*(244/320), (infoView.frame.height/2) - (self.view.frame.size.height*(15.5/568)), self.view.frame.size.width*(68/320),self.view.frame.size.height*(31/568))

        postLikeText.textAlignment = NSTextAlignment.Right
        postLikeText.text = "\(totalLikeCount) Loved it"


        postLikeText.textColor = UIColor.grayColor()
        postLikeText.backgroundColor = UIColor.clearColor()
        postLikeText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        postLikeText.textAlignment = .Right
        
        postLikeText.frame = CGRectMake(30, (moreButton.frame.height/2)-(postLikeText.sizeThatFits(postLikeText.bounds.size).height/2), postLikeText.sizeThatFits(postLikeText.bounds.size).width, postLikeText.sizeThatFits(postLikeText.bounds.size).height)


        
        println("likeheight: \(postLikeText.frame.height)")
        
        var likeImageView = UIImageView()
        
        likeImageView.image = UIImage(named:"heart.png")
        
        likeImageView.frame = CGRectMake(15, (moreButton.frame.height/2)-(postLikeText.frame.height*3/8), postLikeText.frame.height*3/4,postLikeText.frame.height*3/4)
        
        infoView.addSubview(likeImageView)
        infoView.addSubview(likeButton)
        infoView.addSubview(postLikeText)
        
        infoView.addSubview(moreButton)

        if myEventData[indexPath.row][ "isPosted"] as! Bool == false
        {
            var uploadButton = UIButton()
            var uploadButtonLayer = UIButton()
            uploadButton.frame = CGRectMake(self.view.frame.size.width*(137/320),(moreButton.frame.height/2)-(self.view.frame.size.height*(7/568)), self.view.frame.size.width*(14/320),self.view.frame.size.height*(14/568))
            
            uploadButtonLayer.frame = CGRectMake(self.view.frame.size.width*(130/320),self.view.frame.size.height*(0/568), self.view.frame.size.width*(30/320),self.view.frame.size.height*(30/568))
            
            uploadButton.setImage(UIImage(named: "upload.png"), forState: UIControlState.Normal)
            
            uploadButton.tag = indexPath.row
            uploadButtonLayer.tag = indexPath.row
            
            uploadButton.addTarget(self, action:"uploadPost:",forControlEvents: UIControlEvents.TouchUpInside)
            uploadButtonLayer.addTarget(self, action:"uploadPost:",forControlEvents: UIControlEvents.TouchUpInside)
            
            var notUploadedButton = UIButton()
            var notUploadedButtonLayer = UIButton()
            
            notUploadedButton.frame = CGRectMake(self.view.frame.size.width*(175/320),(moreButton.frame.height/2)-(self.view.frame.size.height*(7/568)), self.view.frame.size.width*(14/320),self.view.frame.size.height*(14/568))
            
            
            notUploadedButtonLayer.frame = CGRectMake(self.view.frame.size.width*(175/320),self.view.frame.size.height*(0/568), self.view.frame.size.width*(30/320),self.view.frame.size.height*(30/568))
            
            
            notUploadedButton.setImage(UIImage(named: "not_uploaded.png"), forState: UIControlState.Normal)
            
            notUploadedButton.tag = indexPath.row
            notUploadedButtonLayer.tag = indexPath.row
            
            notUploadedButton.addTarget(self, action:"notUploaded:",forControlEvents: UIControlEvents.TouchUpInside)
            notUploadedButtonLayer.addTarget(self, action:"notUploaded:",forControlEvents: UIControlEvents.TouchUpInside)
            
            infoView.addSubview(uploadButton)
            infoView.addSubview(uploadButtonLayer)
            
            infoView.addSubview(notUploadedButton)
            infoView.addSubview(notUploadedButtonLayer)
        }

        cell!.contentView.addSubview(infoView)

        //var sepImageView = UIImageView()
        var sepImageView = UIView()
        
        //sepImageView.frame = CGRectMake((11/320)*self.view.frame.width, (infoView.frame.origin.y+infoView.frame.height-((2/568)*self.view.frame.height)), (298/320)*self.view.frame.width,(1/568)*self.view.frame.height)
        
        if(postType == "text")
        {
            //sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height+((18/568)*self.view.frame.height)), self.view.frame.width,(1/568)*self.view.frame.height)
            sepImageView.frame = CGRectMake(0.4*self.view.frame.width, (infoView.frame.origin.y+infoView.frame.height) + ((myRowHeights[row] - (infoView.frame.origin.y+infoView.frame.height))/2)-((11/568)*self.view.frame.height), 0.2*self.view.frame.width,1)
        }
        else
        {
            sepImageView.frame = CGRectMake(0.4*self.view.frame.width, (infoView.frame.origin.y+infoView.frame.height-((7/568)*self.view.frame.height)), 0.2*self.view.frame.width,1)
        }

        
        //sepImageView.image = UIImage(named:"sep-line.png")
        
        sepImageView.backgroundColor = UIColor(red: 155.0/255, green: 155.0/255, blue: 155.0/255, alpha: 1)


        cell!.contentView.addSubview(sepImageView)

        cell?.selectionStyle = .None
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let row = indexPath.row
        
        var postType = myEventData[row]["postType"] as! String!
        
        if(postType == "image")
        {
            let manager = NSFileManager.defaultManager()
            
            var eventStreamImageFile = myEventData[row]["postData"] as! String
            
            var eventStreamImagePath = "\(documentDirectory)/\(eventStreamImageFile)"
            
            if (manager.fileExistsAtPath(eventStreamImagePath))
            {
                var image = UIImage(named: eventStreamImagePath)
                var eventPhotoFullScreenVC = self.storyboard?.instantiateViewControllerWithIdentifier("EventPhotoFullScreenViewController") as! EventPhotoFullScreenViewController
                eventPhotoFullScreenVC.image = image
                self.navigationController?.pushViewController(eventPhotoFullScreenVC, animated: false)
            }
            else
            {
                var refreshAlert = UIAlertController(title: "Error", message: "Please wait while the image is downloading.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                    
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        }
        
        if(postType == "text")
        {
            
            myNewPost = myEventData[row]
            
            eventTextTitle = "Edit Text"
            
            let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddtextViewController") as! AddtextViewController
            self.navigationController?.pushViewController(eventPhototsVC, animated: true)
        }
        
    }
    
    func uploadPost(sender: UIButton)
    {
        
        sender.enabled = false
        
        if myNewPost["isUploading"] as? Bool == false
        {
            if myEventData[sender.tag]["postType"] as! String == "text"
            {
                ParseOperations.instance.saveData(myEventData[sender.tag], target: self, successSelector: "createPostSuccess:", successSelectorParameters: myEventData[sender.tag]["eventImageId"] as! Int, errorSelector: "createPostError:", errorSelectorParameters:myEventData[sender.tag])
            }
            else
            {
                var imageName = myEventData[sender.tag]["postData"] as! String
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                var eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(imageName))
                
                let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                
                uploadRequest.bucket = "eventnode1"
                uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(imageName)"
                uploadRequest.body = eventLogoFileUrl
                upload(uploadRequest, insertedId: myEventData[sender.tag]["eventImageId"] as! Int, postToBeUploaded: myEventData[sender.tag])
            }
        }
        
    }

    func notUploaded(sender: UIButton)
    {
        self.loaderView.hidden = true
        
        var refreshAlert = UIAlertController(title: "Not Uploaded to Cloud", message: "If you just created this and there’s internet connection, then it’s uploading in the background. If not, try uploading manually.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func closeImagePreview(sender: AnyObject!)
    {
        selectedImageWrapper.hidden = true
    }
    
    func getFirstFrame(postImageView: UIImageView, videoURL: NSURL, viewTop: CGFloat)
    {
        var asset : AVAsset = AVAsset.assetWithURL(videoURL) as! AVAsset
        
        var assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        var error       : NSError? = nil
        var time        : CMTime = CMTimeMake(1, 30)
        var img         : CGImageRef = assetImgGenerate.copyCGImageAtTime(time, actualTime: nil, error: &error)
        var frameImg    : UIImage = UIImage(CGImage: img)!

        
        postImageView.image = frameImg

    }
    
    
    func getVideoFrame(videoURL: NSURL)->CGSize
    {
        var asset : AVAsset = AVAsset.assetWithURL(videoURL) as! AVAsset
        
        var assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        var error       : NSError? = nil
        var time        : CMTime = CMTimeMake(1, 30)
        var img         : CGImageRef = assetImgGenerate.copyCGImageAtTime(time, actualTime: nil, error: &error)
        var frameImg    : UIImage = UIImage(CGImage: img)!
        
        
        return frameImg.size

    }
    
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
}
