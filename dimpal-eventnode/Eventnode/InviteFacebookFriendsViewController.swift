//
//  InviteFacebookFriendsViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 7/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class InviteFacebookFriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var eventnodeContactsTable: UITableView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorMessage: UITextView!
    
    var registeredContacts: Array<NSDictionary>!
    var registeredContactsOriginal: Array<NSDictionary>!
    
    var registeredInviteStatus: Array<Bool>!
    var registeredInviteStatusOriginal: Array<Bool>!
    
    var contactFbIds: Array<String> = []
    
    var contactDetails: Array<NSDictionary> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        contactFbIds = []
        contactDetails = []
        
        registeredContacts = []
        registeredInviteStatus = []
        
        registeredContactsOriginal = []
        registeredInviteStatusOriginal = []
        
        
        eventnodeContactsTable.separatorColor = UIColor.clearColor()
        
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        errorMessage.attributedText = NSAttributedString(string:errorMessage.text, attributes:attributes)
        errorMessage.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        errorMessage.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        errorMessage.textAlignment = .Center
        
        if facebookFriends.count == 0
        {
            errorView.hidden = false
        }
        else
        {
            errorView.hidden = true
            
            for friend in facebookFriends {
                var valueDict = friend as NSDictionary
                var name = valueDict.valueForKey("name") as! String
                var id = valueDict.valueForKey("id") as! String
                
                contactFbIds.append(id)
                //self.contactDetails.append(emailDetail)
            }
            self.reloadContacts()
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func reloadContacts()
    {
        
        //println(currentUserId)
        
        var existingFriends: Array<NSDictionary>!
        
        existingFriends = []
        
        registeredContacts = []
        registeredInviteStatus = []
        
        registeredContactsOriginal = []
        registeredInviteStatusOriginal = []
        
        var query = PFUser.query()
        query?.whereKey("facebookId", containedIn: contactFbIds)
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                println("Successfully retrieved \(objects!.count) contacts.")
                
                if let objects = objects as? [PFUser] {
                    
                    if objects.count == 0
                    {
                        self.errorView.hidden = false
                    }
                    else
                    {
                        self.errorView.hidden = true
                    }
                    
                    for object in objects {
                        if let emailVerified = object["emailVerified"] as? Bool
                        {
                            /*if(emailVerified)
                            {*/
                            var existingEmailId: NSDictionary! = ["userObjectId": object.objectId!, "email": object.email!, "facebookId": object["facebookId"]!]
                            existingFriends.append(existingEmailId)
                            //}
                            println(object.email!)
                            
                        }
                    }
                    
                    for emailDetail in facebookFriends
                    {
                        
                        //println(email)
                        var existingEmailIdDetails: NSDictionary!
                        var emailCount = 0
                        var emailStatus = false
                        for email in existingFriends
                        {
                            if emailDetail["id"] as! String == email["facebookId"] as! String
                            {
                                emailStatus = true
                                
                                existingEmailIdDetails = ["userObjectId": email["userObjectId"] as! String, "email": email["email"] as! String,  "contactName":emailDetail["name"] as! String, "facebookId": emailDetail["id"] as! String, "contactIndex":self.registeredContacts.count]
                                
                                var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["count(*) as count"], whereString: "userObjectId = ? AND eventObjectId = ? ", whereFields: [email["userObjectId"] as! String, currentEvent.objectId as String!])
                                
                                resultSetCount.next()
                                
                                emailCount = Int(resultSetCount.intForColumn("count"))
                                
                                resultSetCount.close()
                                
                                break
                            }
                        }
                        
                        
                        if emailStatus == true
                        {
                            //foundIndex = emailDetail["contactIndex"] as! Int
                            
                            self.registeredContacts.append(existingEmailIdDetails)
                            self.registeredContactsOriginal.append(existingEmailIdDetails)
                            
                            if emailCount>0
                            {
                                self.registeredInviteStatus.append(true)
                                self.registeredInviteStatusOriginal.append(true)
                            }
                            else
                            {
                                self.registeredInviteStatus.append(false)
                                self.registeredInviteStatusOriginal.append(false)
                            }
                            //
                            
                            //existingEmailIndexes.append(foundIndex)
                            
                        }
                        
                        
                        
                    }
                    
                    self.eventnodeContactsTable.reloadData()
                    
                }
            } else {
                println("Error: \(error!) \(error!.userInfo!)")
            }
        }
        
    }
    
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return registeredContacts.count
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        return 60
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let row = indexPath.row
        
        var cellIdentifier: String! = ""
        
        cellIdentifier = "FacebookFriendsTableViewCell"
        
        var cell: FacebookFriendsTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? FacebookFriendsTableViewCell
        
        
        tableView.separatorColor = UIColor.clearColor()
        
        
        for view in cell!.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        var imageView = UIImageView()
        imageView.frame = CGRectMake(16, 16, 28, 28)
        
        var fbId = registeredContacts[row]["facebookId"] as! String
        
        println(fbId)
        //var url:NSURL = NSURL(string:"https://graph.facebook.com/\(fbId)/picture?type=square")!
        //var data:NSData = NSData(contentsOfURL: url)!
        
        if let url = NSURL(string: "https://graph.facebook.com/\(fbId)/picture?type=square") {
            if let data = NSData(contentsOfURL: url){
                imageView.contentMode = UIViewContentMode.ScaleAspectFit
                imageView.image = UIImage(data: data)
            }
        }
        
        imageView.layer.masksToBounds = true;
        imageView.layer.cornerRadius = 14
        
        var nameLabel = UILabel()
        nameLabel.frame = CGRectMake(52, 20, self.view.frame.width-140, 20)
        //nameLabel.numberOfLines = 2
        nameLabel.text = registeredContacts[row]["contactName"] as? String
        nameLabel.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        nameLabel.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        nameLabel.adjustsFontSizeToFitWidth = true

        
        var sendButton = UIButton()
        sendButton.frame = CGRectMake(self.view.frame.width-140+60+27, 17, 26, 26)
        
        
        if registeredInviteStatus[row]
        {
            sendButton.setImage(UIImage(named: "check-box.png"), forState: UIControlState.Normal)
        }
        else
        {
            sendButton.setImage(UIImage(named: "checkbox.png"), forState: UIControlState.Normal)
        }
        
        
        sendButton.addTarget(self, action:"sendInvitationToParseUser:",forControlEvents: UIControlEvents.TouchUpInside)
        sendButton.tag = row
        
        
        cell?.contentView.addSubview(imageView)
        cell?.contentView.addSubview(nameLabel)
        //cell?.contentView.addSubview(emailLabel)
        cell?.contentView.addSubview(sendButton)
        
        //check-round-grey.png
        //check-box.png
        
        //cell?.textLabel!.text = registeredContacts[row]["contactName"] as? String
        
        cell?.selectionStyle = .None
        
        return cell!
        
    }
    
    func sendInvitationToParseUser(sender: UIButton)
    {
        println("parse")
        sender.enabled = false
        if !registeredInviteStatus[sender.tag]
        {
            var inviteObject: PFObject = PFObject(className: "Invitations")
            
            var userObjectId = registeredContacts[sender.tag]["userObjectId"] as! String
            var email = registeredContacts[sender.tag]["email"] as! String
            
            
            inviteObject["invitedName"] = registeredContacts[sender.tag]["contactName"] as? String
            inviteObject["eventObjectId"] = currentEvent.objectId!
            inviteObject["isApproved"] = true
            inviteObject["userObjectId"] = userObjectId
            inviteObject["emailId"] = email
            inviteObject["attendingStatus"] = ""
            inviteObject["invitationType"] = "facebook"

            inviteObject["isUpdated"] = false
            inviteObject["noOfChilds"] = 0
            inviteObject["noOfAdults"] = 0
            inviteObject["invitationNote"] = ""
            inviteObject["isEventUpdated"] = false
            
            inviteObject["isEventStreamUpdated"] = false
            inviteObject["isTextUpdated"] = false
            
            var suppliedParameters: Dictionary<String, String>! = Dictionary()
            
            suppliedParameters["invitationType"] = "parse"
            suppliedParameters["invitationNo"] = "\(sender.tag)"
            
            
            ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "createInvitationSuccess:", successSelectorParameters: suppliedParameters, errorSelector: "createInvitationError:", errorSelectorParameters:"parse")
        }
    }
    
    func createInvitationSuccess(timer: NSTimer)
    {
        var invitation = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var recievedParameters: Dictionary<String,String> = (timer.userInfo?.valueForKey("external") as? Dictionary)!
        
        var type = recievedParameters["invitationType"] as String!
        
        var invitationNo = recievedParameters["invitationNo"] as String!
        
        println(type)
        
        var email = invitation["emailId"] as! String
        
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = invitation.objectId!
        tblFields["invitedName"] = invitation["invitedName"] as? String
        tblFields["userObjectId"] = invitation["userObjectId"] as? String
        tblFields["attendingStatus"] = ""
        tblFields["invitationType"] = "facebook"
        tblFields["needsContentApprovel"] = "0"
        
        var date = ""
        
        if invitation.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((invitation.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
        }
        
        if invitation.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((invitation.updatedAt)!)
            println(date)
            tblFields["updatedAt"] = date
        }
        
        tblFields["isPosted"] = "1"
        tblFields["emailId"] = "\(email)"
        tblFields["eventObjectId"] = "\(currentEvent.objectId!)"
        
        tblFields["isApproved"] = "1"
        
        tblFields["isUpdated"] = "1"
        tblFields["noOfChilds"] = "0"
        tblFields["noOfAdults"] = "0"
        tblFields["invitationNote"] = ""
        tblFields["isEventUpdated"] = "0"
        
        
        
        var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
        
        //if insertedId>0
        
        var inviteCode = currentEvent.objectId!
        var eventTitle = currentEvent["eventTitle"] as! String
        var dateString = ""
        var timeString = ""
        var locationString = ""
        var hostName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        var eventType = "online"
        
        if currentEvent["isRSVP"] as! Bool
        {
            
            let sdate = currentEvent["eventStartDateTime"] as! NSDate
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components(.CalendarUnitWeekday | .CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: sdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour >= 12)
            {
                if(scomponents.hour > 12)
                {
                    shour = scomponents.hour-12
                }
                else
                {
                    shour = 12
                }
                
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            let sweekday = scomponents.weekday
            
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            
            eventType = "rsvp"
            
            dateString = "\(weekDaysArray[sweekday-1]) \(monthsArray[smonth-1]) \(sday), \(syear)"
            timeString = "\(shour):\(sminute) \(sam)"
            
            println(sweekday)
            
            locationString = currentEvent["eventLocation"] as! String!
        }
        
        var message = ""
        

        var inviteUserEmail = InviteEmailToUser()
        
        var eventFolder = currentEvent["eventFolder"] as! String
        var eventImage = currentEvent["eventImage"] as! String
        
        var userObjectId = invitation["userObjectId"] as! String
        
        var notifMessage = ""
        var currentUserId =  NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        
        
        if eventType == "rsvp"
        {
            notifMessage = "\(hostName) invited you to the event, \(eventTitle). Please respond to the invitation."
        }
        else
        {
            notifMessage = "\(hostName) shared the event, \(eventTitle) with you. Check it out."
        }
        
        var invitedName = invitation["invitedName"] as! String
        var isApproved = invitation["isApproved"] as! Bool
        //var userObjectId = invitation["userObjectId"] as! String
        var emailId = invitation["emailId"] as! String
        var attendingStatus = invitation["attendingStatus"] as! String
        var invitationType = invitation["invitationType"] as! String
        var isUpdated = invitation["isUpdated"] as! Bool
        var noOfChilds = invitation["noOfChilds"] as! Int
        var noOfAdults = invitation["noOfAdults"] as! Int
        var invitationNote = invitation["invitationNote"] as! String
        var isEventUpdated = invitation["isEventUpdated"] as! Bool
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        var createdAt = dateFormatter.stringFromDate((invitation.createdAt)!)
        
        var updatedAt = dateFormatter.stringFromDate((invitation.updatedAt)!)
        
        var eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        
        var data: Dictionary<String, String!> = [
            "alert" : "\(notifMessage)",
            "notifType" :  "invitation",
            "objectId" :  invitation.objectId!,
            "eventObjectId": currentEvent.objectId!,
            "invitedName" : "\(invitedName)",
            "isUpdated" : "\(isUpdated)",
            "isEventUpdated": "\(isEventUpdated)",
            "isApproved": "\(isApproved)",
            "userObjectId" : "\(userObjectId)",
            "emailId": "\(emailId)",
            "attendingStatus" : "\(attendingStatus)",
            "invitationType" : "\(invitationType)",
            "noOfChilds": "\(noOfChilds)",
            "noOfAdults": "\(noOfAdults)",
            "invitationNote": "\(invitationNote)",
            "createdAt": "\(createdAt)",
            "updatedAt": "\(updatedAt)",
            "badge": "Increment",
            "sound" : "default",
            "eventCreatorId" : "\(eventCreatorId)"
        ]
        
        var urlString = String()
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
            
            if error == nil
            {
                println(url!)
                
                urlString = url!
                
                if eventType == "rsvp"
                {
                    
                    var eventLatitude = currentEvent["eventLatitude"] as! Double
                    var eventLongitude = currentEvent["eventLongitude"] as! Double
                    
                    var inviteUserEmail = InPerson()
                    message = inviteUserEmail.emailMessage(inviteCode, eventTitle: eventTitle, dateString: dateString, timeString: timeString, locationString: locationString, hostName: hostName, type: "rsvp", latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                    
                }
                else
                {
                    var inviteUserEmail = OnlineOnlyInviteEmailGuest()
                    
                    message = inviteUserEmail.emailMessage(inviteCode, eventTitle:eventTitle, hostName:hostName, type:"online", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)",url:urlString)
                }
                
                
                var sendNonInvitationEmailsObject = SendNonInvitationEmails()
                
                sendNonInvitationEmailsObject.sendEmail("\(hostName) invited you to the event, \(eventTitle)", message: message, emails: [email])
                
            }
            
        })
        
        
        //message = inviteUserEmail.emailMessage(inviteCode, eventTitle: eventTitle, dateString: dateString, timeString: timeString, locationString: locationString, hostName: hostName, type: eventType)

        
        
        if invitationNo.toInt()! >= 0
        {
            registeredInviteStatus[invitationNo.toInt()!] = true
            registeredInviteStatusOriginal[registeredContacts[invitationNo.toInt()!]["contactIndex"] as! Int] = true
        }
        eventnodeContactsTable.reloadData()

        
        

        
        var notificationObject = PFObject(className: "Notifications")
        notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
        notificationObject["notificationImage"] = "profilePic.png"
        notificationObject["senderId"] = currentUserId
        notificationObject["receiverId"] = userObjectId
        notificationObject["notificationActivityMessage"] = notifMessage
        notificationObject["eventObjectId"] = currentEvent.objectId!
        notificationObject["notificationType"] = "invitation"
        
        
        notificationObject.saveInBackground()
        
        
        var predicateString: String! = "objectId = '\(userObjectId)' AND inviteNotification = true AND allowSound = true"
        
        sendParsePush(predicateString, data: data)
        
        
        data["sound"] = ""
        
        predicateString = "objectId = '\(userObjectId)' AND inviteNotification = true AND allowSound = false"
        sendParsePush(predicateString, data: data)
        
        //let message = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)!
        
        //println("text: \(text2)")

        
        /*var sns = AWSSES.defaultSES()
        
        var messageBody = AWSSESContent()
        var subject = AWSSESContent()
        var body = AWSSESBody()
        
        
        
        subject.data = "\(hostName) invited you to the event, \(eventTitle)"
        
        //messageBody.data = "\(senderName) invited you to the event, \(eventTitle). Your invitation code is \(invitation.objectId!)"
        
        messageBody.data = message
        
        //body.text = messageBody
        body.html = messageBody
        
        var theMessage = AWSSESMessage()
        theMessage.subject = subject
        
        theMessage.body = body
        
        //email = "dimpal1990@gmail.com"
        
        var destination = AWSSESDestination()
        destination.toAddresses = [email]
        
        var send = AWSSESSendEmailRequest()
        send.source = "noreply@eventnode.co"
        send.destination = destination
        send.message = theMessage
        send.returnPath = "noreply@eventnode.co"
        
        sns.sendEmail(send).continueWithBlock {(task: AnyObject!) -> AWSTask! in
            
            if task.error != nil
            {
                println(task.error.debugDescription)
            }
            else
            {
                println("success")
            }
            
            return nil
        }*/
    }
    
    func createInvitationError(timer: NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("error occured \(error.description)")
    }
    
    
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        var predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        var query = PFUser.queryWithPredicate(predicate)
        
        var push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                println(objects?.count)
                if var fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        var userObjectId = object.objectId!
                        
                        var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        var query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                println(objects?.count)
                                if var fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    var query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }
    
    
    @IBAction func backButton(sender: AnyObject)
    {
        showLinkfacebookView = false
        
        self.navigationController?.popViewControllerAnimated(true)
        
        //let linkedAccounts = self.storyboard!.instantiateViewControllerWithIdentifier("LinkedAccounts") as! LinkedAccountsViewController
        //self.navigationController?.pushViewController(linkedAccounts, animated: false)
        
    }
    
}
