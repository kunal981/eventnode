//
//  AddCommentViewController.swift
//  Eventnode
//
//  Created by brst on 9/1/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class AddCommentViewController: UIViewController {
    
    var currentUserId = ""
    
    var messageIds = [String]()
    
    var chatsArray = [PFObject]();
    
    var senderName = ["fhn","aman","dsd","zfgdcfd","zfddzx","abhi"]
    
    //var senderId = ["vbnbj","cWKoulXs4G","h5RUuNIIJi","cWKoulXs4G","jhjmn","cWKoulXs4G"]
    
    var eventObject: PFObject!
    
    var rowHeights = [CGFloat]()
    
    var senderMessage = ["Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt."]
    
    var profileImage = ["girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg"]
    
    var objectId = ["1","1","2","3","4","5"]
    
    
    @IBOutlet weak var wrapperViewChat: UIView!
    @IBOutlet weak var addCommentTableView: UITableView!
    
    @IBOutlet weak var chatHeadBottom: UIView!
    @IBOutlet weak var enterTextMessage: UITextView!
    
    
    var textMessage:String = String()
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        println("add comments view")
        
        self.view.addSubview(wakeUpImageView)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        
        addCommentTableView.separatorColor = UIColor.clearColor()
        
        
        //com.eventnode.iospush
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
            println("current User Id = \(currentUserId)")
        }
        
        
        //        for (var i = 0; i < senderName.count; i++)
        //        {
        //            var chat = PFObject(className: "EventComments")
        //
        //            chat["senderName"] = senderName[i]
        //
        //            chat["senderId"] = senderId[i]
        //
        //            chat["senderMessage"] = senderMessage[i]
        //
        //            chat["profileImage"] = profileImage[i]
        //
        //            chatsArray.append(chat)
        //        }
        
        //addCommentTableView.reloadData()
        
        
        refreshList()
        
        println(messageIds.count)
        
        var messageIdsString = ""
        if messageIds.count > 0
        {
            messageIdsString = "','".join(messageIds)
            
            println("message ids string = \(messageIdsString)")
        }
        else
        {
            enterTextMessage.becomeFirstResponder()
        }
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(messageIdsString)'}) AND eventObjectId = '\(eventObject.objectId!)'")
        
        var query = PFQuery(className:"EventComments", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagesError:", errorSelectorParameters:nil)
        
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("refreshList"), userInfo: nil, repeats: true)
        
    }
    
    
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func fetchAllMessagesSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = message.objectId!
                tblFields["messageText"] = message["messageText"] as? String
                tblFields["senderObjectId"] = message["senderObjectId"] as? String
                tblFields["eventObjectId"] = message["eventObjectId"] as? String
                tblFields["senderName"] = message["senderName"] as? String
                //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                
                var date = ""
                
                if message.createdAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.createdAt)!)
                    println(date)
                    tblFields["createdAt"] = date
                }
                
                if message.updatedAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.updatedAt)!)
                    println(date)
                    tblFields["updatetAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                
                var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                
                fetchedEventObjectIds.append(message["eventObjectId"] as! String)
                
            }
            
            refreshList()
        }
    }
    
    
    
    func fetchAllMessagesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    //MARK: - sendMessage()
    @IBAction func sendMessage(sender: AnyObject)
    {
        var messageEntered:NSString = self.enterTextMessage.text
        
        if (messageEntered.length > 0)
        {
            textMessage = self.enterTextMessage.text!
            
            println(textMessage)
            
            self.enterTextMessage.text = ""
            
            var chat = PFObject(className: "EventComments")
            
            
            chat["senderObjectId"] = currentUserId
            
            chat["eventObjectId"] = eventObject.objectId!
            
            chat["messageText"] = "\(textMessage)"
            
            chat["profileImage"] = "girl.jpeg"
            
            var senderName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            
            chat["senderName"] = senderName
            
            chat["timeString"] = "just now"
            
            var senderMessageTemp = UITextView()
            
            senderMessageTemp.frame.size.width = self.view.frame.width*(215.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(45.0/568)
            
            senderMessageTemp.text = chat["messageText"] as! String
            
            senderMessageTemp.font = UIFont(name: "AvenirNext-Medium", size: 12)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            
            var rowHeight: CGFloat = 0
            
            if contentSize.height > self.view.frame.height*(25.0/568)
            {
                rowHeight = contentSize.height - (self.view.frame.height*(25.0/568))
            }
            
            println(rowHeight)
            
            rowHeights.append(rowHeight)
            
            chatsArray.append(chat)
            
            addCommentTableView.reloadData()
            adjustTableHeight()
            adjustTableY()
            
            ParseOperations.instance.saveData(chat, target: self, successSelector: "sendMessageSuccess:", successSelectorParameters: nil, errorSelector: "sendMessageError:", errorSelectorParameters:nil)
            
        }
        
    }
    
    
    func sendMessageSuccess(timer: NSTimer)
    {
        var message = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = message.objectId!
        tblFields["messageText"] = message["messageText"] as? String
        tblFields["senderObjectId"] = message["senderObjectId"] as? String
        tblFields["eventObjectId"] = message["eventObjectId"] as? String
        tblFields["senderName"] = message["senderName"] as? String
        //tblFields["eventCommentId"] = message["eventCommentId"] as? String
        
        var date = ""
        
        if message.createdAt != nil
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((message.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
        }
        
        if message.updatedAt != nil
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((message.updatedAt)!)
            println(date)
            tblFields["updatetAt"] = date
        }
        
        tblFields["isPosted"] = "1"
        
        var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
        
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(eventObject.objectId!)'")
        
        var query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: message, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        
        refreshList()
        
        
        //respondtableView.reloadData()
        //repondTableView2.reloadData()
    }
    
    func sendMessageError(timer: NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("error occured \(error.description)")
    }
    
    
    
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        var payloadData = timer.userInfo?.valueForKey("external") as! PFObject
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            ///Users/brst981/Desktop/my projects/Eventnode Git/Eventnode/AddCommentsViewController.swift:317:34: Cannot invoke 'fetchData' with an argument list of type '(PFQuery, target: AddCommentsViewController, successSelector: String, successSelectorParameters: [String : String?], errorSelector: String, errorSelectorParameters: nil)'
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            
            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                }
            }
            
            var eventCreatorObjectId = eventObject["eventCreatorObjectId"] as! String
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //eventCreatorObjectId
            
            var createdAt = ""
            
            if payloadData.createdAt != nil
            {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                createdAt = dateFormatter.stringFromDate((payloadData.createdAt)!)
            }
            
            var updatedAt = ""
            if payloadData.updatedAt != nil
            {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                updatedAt = dateFormatter.stringFromDate((payloadData.updatedAt)!)
            }
            
            
            var objectId = payloadData.objectId!
            var messageText = payloadData["messageText"] as! String
            var senderObjectId = payloadData["senderObjectId"] as! String
            var eventObjectId = payloadData["eventObjectId"] as! String
            var senderName = payloadData["senderName"] as! String
            
            var eventTitle = eventObject["eventTitle"] as! String
            
            
            /*,
            "objectId" : objectId,
            "messageText" : messageText,
            "senderObjectId" : senderObjectId,
            "eventObjectId" : eventObjectId,
            "senderName" : senderName,
            "createdAt" : createdAt,
            "updatedAt" :  updatedAt,
            "notifType" :  "groupchat"*/
            
            var data: Dictionary<String, String!> = [
                "alert" : "\(senderName) sent you a new message in \(eventTitle)",
                "objectId" : "\(objectId)",
                "messageText" : "\(messageText)",
                "senderObjectId" : "\(senderObjectId)",
                "eventObjectId" : "\(eventObjectId)",
                "senderName" : "\(senderName)",
                "createdAt" : "\(createdAt)",
                "updatedAt" :  "\(updatedAt)",
                "badge": "Increment",
                "sound" : "default",
                "notifType" :  "groupchat"
                
            ]
            
            
            var fetchedUserObjectIdsString = "','".join(fetchedUserObjectIds)
            
            var predicateString: String! = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND allowSound = true"
            
            sendParsePush(predicateString, data: data)
            
            
            data["sound"] = ""
            
            predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND allowSound = false"
            sendParsePush(predicateString, data: data)
            
        }
    }
    
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        var predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        var query = PFUser.queryWithPredicate(predicate)
        
        var push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                println(objects?.count)
                if var fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        var userObjectId = object.objectId!
                        
                        var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        var query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                println(objects?.count)
                                if var fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    var predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    var query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }

    
    
    
    func adjustTableHeight()
    {
        var totalHeight:CGFloat = 0
        for var i = 0; i < rowHeights.count; i++
        {
            totalHeight += (rowHeights[i] + (70*self.view.frame.height/568))
        }
        
        
        println("totalHeight: \(totalHeight)")
        
        //if totalHeight < ((404/568)*self.view.frame.height)
        if totalHeight < (wrapperViewChat.frame.origin.y - (chatHeadBottom.frame.origin.y + chatHeadBottom.frame.height))
        {
            addCommentTableView.frame.size.height = totalHeight
        }
        else
        {
            addCommentTableView.frame.size.height = (wrapperViewChat.frame.origin.y - (chatHeadBottom.frame.origin.y + chatHeadBottom.frame.height))
        }
        
        if totalHeight > addCommentTableView.frame.height
        {
            addCommentTableView.contentOffset.y = addCommentTableView.contentSize.height - addCommentTableView.frame.height
        }
        
    }
    
    func adjustTableY()
    {
        
        addCommentTableView.frame.origin.y = wrapperViewChat.frame.origin.y - addCommentTableView.frame.height
        
        //addCommentTableView.frame.origin.y = ((429/568)*self.view.frame.height) - totalHeight + ((74/568)*self.view.frame.height)
        //addCommentTableView.frame.origin.y = ((74/568)*self.view.frame.height)
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: - UITableViewDelegates and DataSource() Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return chatsArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return (100*self.view.frame.height/568) + rowHeights[indexPath.row]
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! AddCommentTableViewCell
        
        addCommentTableView.separatorColor = UIColor.clearColor()
        
        
        for view in cell.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        
        var wrapperView:UIView = UIView()
        
        var senderName_lbl:UILabel = UILabel()
        
        var senderImage:UIImageView = UIImageView()
        
        var senderMessage: UITextView = UITextView()
        
        var wrapperMessage:UIView = UIView()
        
        var messageTime:UILabel = UILabel()
        
        
        cell.contentView.addSubview(wrapperView)
        
        wrapperView.addSubview(wrapperMessage)
        
        wrapperView.addSubview(senderImage)
        
        wrapperMessage.addSubview(senderMessage)
        
        wrapperMessage.addSubview(senderName_lbl)
        
        wrapperMessage.addSubview(messageTime)
        
        
        println("current User Id1 = \(currentUserId)")
        
        if currentUserId == chatsArray[indexPath.row]["senderObjectId"] as! String
        {
            senderMessage.editable = false
            //cell.senderMessage.selectable = false
            senderMessage.scrollEnabled = false
            
            println("current User Id = \(currentUserId)")
            
            wrapperView.frame = CGRectMake(0, 0, cell.contentView.frame.width, (70*self.view.frame.height/568) + rowHeights[indexPath.row])
            
            wrapperMessage.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            senderMessage.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            senderName_lbl.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            
            //            wrapperMessage.backgroundColor = UIColor.blackColor()
            //
            //            senderMessage.backgroundColor = UIColor.blueColor()
            //
            //            senderName_lbl.backgroundColor = UIColor.redColor()
            
            
            wrapperMessage.frame = CGRectMake(self.view.frame.width*(33/320), self.view.frame.height*(0.0/568), self.view.frame.width*(235.0/320), self.view.frame.height*(senderMessage.frame.height/568))
            
            
            senderName_lbl.frame = CGRectMake(self.view.frame.height*(10.0/568), self.view.frame.height*(10.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
            
            senderName_lbl.textAlignment = NSTextAlignment.Right
            
            
            senderName_lbl.font = UIFont(name: "AvenirNext-DemiBold", size: 12)
            
            senderName_lbl.text = chatsArray[indexPath.row]["senderName"] as? String
            
            
            senderMessage.frame = CGRectMake( self.view.frame.height*(10.0/568), self.view.frame.height*(27.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(45.0/568))
            
            senderMessage.textAlignment = NSTextAlignment.Right
            senderMessage.font = UIFont(name: "AvenirNext-Regular", size: 12)
            
            senderMessage.text = chatsArray[indexPath.row]["messageText"] as! String
            
            messageTime.text = chatsArray[indexPath.row]["timeString"] as? String
            
            messageTime.font = UIFont(name: "AvenirNext-DemiBold", size: 11)
            
            
            messageTime.textAlignment = NSTextAlignment.Right
            
            senderImage.frame = CGRectMake(self.view.frame.width*(268.0/320), self.view.frame.height*(0.0/568), self.view.frame.width*(38.0/320), self.view.frame.height*(38.0/568))
            
            senderImage.image = UIImage(named: "boy.jpeg")
            
        }
        else
        {
            
            wrapperView.frame = CGRectMake(0, 0, cell.contentView.frame.width, (70*self.view.frame.height/568) + rowHeights[indexPath.row])
            
            senderImage.frame = CGRectMake(self.view.frame.width*(8/320), self.view.frame.height*(5.0/568), self.view.frame.width*(38.0/320), self.view.frame.height*(38.0/568))
            
            senderImage.image = UIImage(named: "girl.jpeg")
            
            wrapperMessage.frame = CGRectMake(self.view.frame.width*(57/320), self.view.frame.height*(0.0/568), self.view.frame.width*(235.0/320), self.view.frame.height*(senderMessage.frame.height/568))
            
            wrapperMessage.backgroundColor = UIColor(red: 206.0/255, green: 206.0/255, blue: 206.0/255, alpha: 1.0)
            
            
            
            senderName_lbl.backgroundColor = UIColor(red: 206.0/255, green: 206.0/255, blue: 206.0/255, alpha: 1.0)
            
            senderName_lbl.frame = CGRectMake( self.view.frame.height*(10.0/568), self.view.frame.height*(10.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
            
            senderMessage.textAlignment = NSTextAlignment.Left
            
            senderName_lbl.font = UIFont(name: "AvenirNext-DemiBold", size: 12)
            
            senderName_lbl.text = chatsArray[indexPath.row]["senderName"] as? String
            
            senderMessage.frame = CGRectMake( self.view.frame.height*(10.0/568), self.view.frame.height*(27.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(45.0/568))
            
            senderMessage.font = UIFont(name: "AvenirNext-Regular", size: 12)
            
            senderMessage.text = chatsArray[indexPath.row]["messageText"] as! String
            
            println(chatsArray[indexPath.row]["messageText"] as! String)
            
            
            messageTime.text = chatsArray[indexPath.row]["timeString"] as? String
            
            messageTime.font = UIFont(name: "AvenirNext-Medium", size: 10)
            messageTime.textAlignment = NSTextAlignment.Left
            
            senderMessage.backgroundColor = UIColor(red: 206.0/255, green: 206.0/255, blue: 206.0/255, alpha: 1.0)
            
            
            senderMessage.editable = false
            //cell.senderMessage.selectable = false
            senderMessage.scrollEnabled = false
            
        }
        
        
        let contentSize = senderMessage.sizeThatFits(senderMessage.bounds.size)
        var frame = senderMessage.frame
        frame.size.height = contentSize.height
        senderMessage.frame = frame
        
        wrapperMessage.frame.size.height = frame.height + self.view.frame.height*(61.0/568)
        wrapperMessage.layer.cornerRadius = 5
        wrapperMessage.layer.masksToBounds = true
        
        messageTime.frame = CGRectMake(self.view.frame.height*(10.0/568), self.view.frame.height*(30.0/568) + contentSize.height, self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
        
        println("SenderMessage Height = \(senderMessage.frame)")
        
        return cell
        
    }
    
    
    func refreshList()
    {
        
        chatsArray = []
        
        //messageIds = []
        
        var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["*"], whereString: "eventObjectId = '\(eventObject.objectId!)' ORDER BY eventCommentId ASC", whereFields: [])
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                var chat = PFObject(className: "EventComments")
                
                println(resultSet.stringForColumn("messageText"))
                
                chat["senderObjectId"] = resultSet.stringForColumn("senderObjectId")
                
                chat.objectId = resultSet.stringForColumn("objectId")
                
                println(resultSet.stringForColumn("objectId"))
                
                messageIds.append(resultSet.stringForColumn("objectId"))
                
                var textMessage = resultSet.stringForColumn("messageText")
                
                chat["messageText"] = "\(textMessage)"
                
                chat["profileImage"] = "girl.jpeg"
                
                // TODO(Dimpal): Fix this. App is crashing here. senderName is nil.
                println(resultSet.stringForColumn("senderName"))
                
                chat["senderName"] = resultSet.stringForColumn("senderName")
                
                var date = NSDate()
                println("date is = \(date)")
                
                
                let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
                println("current Time Stamp = \(currentTimeStamp)")
                
                
                var dateCreated = stringToDate(resultSet.stringForColumn("dateCreated"))
                println("Date Created = \(dateCreated)")
                
                
                let createdTimeStamp = Int64(dateCreated.timeIntervalSince1970*1000)
                println("Created Time Stamp = \(createdTimeStamp)")
                
                var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
                
                var timeDiff = Int64(currentTimeStamp - createdTimeStamp) - timezoneOffset
                println(timeDiff)
                
                
                var nYears = timeDiff / (1000*60*60*24*365)
                
                println(nYears)
                
                var nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
                
                println(nMonths)
                
                var nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
                println(nDays)
                
                var nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
                println(nHours)
                
                
                var nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
                println(nMinutes)
                
                var timeMsg = ""
                
                if nYears > 0
                {
                    timeMsg = "about \(nYears) years ago"
                }
                else if nMonths > 0
                {
                    timeMsg = "about \(nMonths) months ago"
                }
                else if nDays > 0
                {
                    timeMsg = "about \(nDays) days ago"
                }
                else if nHours > 0
                {
                    timeMsg = "about \(nHours) hours ago"
                }
                else if nMinutes > 0
                {
                    timeMsg = "about \(nMinutes) minutes ago"
                }
                else
                {
                    timeMsg = "just now"
                }
                
                
                chat["timeString"] = timeMsg
                
                
                var senderMessageTemp = UITextView()
                
                senderMessageTemp.frame.size.width = self.view.frame.width*(215.0/320)
                senderMessageTemp.frame.size.height = self.view.frame.height*(45.0/568)
                
                senderMessageTemp.text = chat["messageText"] as! String
                
                senderMessageTemp.font = UIFont(name: "AvenirNext-Medium", size: 12)
                
                let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
                var frame = senderMessageTemp.frame
                frame.size.height = contentSize.height
                senderMessageTemp.frame = frame
                
                var rowHeight: CGFloat = 0
                
                if contentSize.height > self.view.frame.height*(25.0/568)
                {
                    rowHeight = contentSize.height - (self.view.frame.height*(25.0/568))
                }
                
                println(rowHeight)
                
                rowHeights.append(rowHeight)
                
                chatsArray.append(chat)
            }
            
            addCommentTableView.reloadData()
            adjustTableHeight()
            adjustTableY()
        }
        resultSet.close()
        
    }
    
    
    
    //MARK: - stringToDate()
    func stringToDate(dateString: String)->NSDate
    {
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        println("date formatter = \(dateFormatter)")
        
        var date = dateFormatter.dateFromString(dateString)
        
        println("date  = \(date)")
        
        return date!
    }
    
    
    
    //MARK: -  viewTapped()
    @IBAction func viewTapped(sender: AnyObject)
    {
        enterTextMessage.resignFirstResponder()
    }
    
    
    
    //MARK: -  keyboardWillShow()
    func keyboardWillShow(sender: NSNotification)
    {
        //        wrapperViewChat.frame.origin.y -= (253.0/568)*self.view.frame.height
        //        addCommentTableView.frame.origin.y -= (253.0/568)*self.view.frame.height
        
        wrapperViewChat.frame.origin.y -= 253.0
        addCommentTableView.frame.origin.y -= 253.0
        adjustTableHeight()
        adjustTableY()
    }
    
    
    
    //MARK: -  keyboardWillHide()
    func keyboardWillHide(sender: NSNotification)
    {
        
        //        wrapperViewChat.frame.origin.y += (253.0/568)*self.view.frame.height
        //        addCommentTableView.frame.origin.y += (253.0/568)*self.view.frame.height
        
        wrapperViewChat.frame.origin.y += 253.0
        addCommentTableView.frame.origin.y += 253.0
        
        adjustTableHeight()
        adjustTableY()
    }
    
    
    
    //MARK: -  closeEventDetailsButtonClicked()
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventPhotosVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
        self.navigationController?.pushViewController(eventPhotosVC, animated: false)*/
        self.navigationController?.popViewControllerAnimated(false)
    }
}
