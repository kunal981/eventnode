//
//  InvitationViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/29/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MapKit

class InvitationViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var textView: UITextView!
    
    var matchingItems: [MKMapItem] = [MKMapItem]()
    let regionRadius: CLLocationDistance = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        let artwork = Artwork(title: "King David Kalakaua",
            locationName: "Waikiki Gateway Park",
            discipline: "Sculpture",
            coordinate: CLLocationCoordinate2D(latitude: 21.283921, longitude: -157.831661))
        
        mapView.zoomEnabled = false
        mapView.scrollEnabled = false
        mapView.userInteractionEnabled = false
        
        
        mapView.addAnnotation(artwork)
        let nextLocation = CLLocation(latitude: 21.283921, longitude: -157.831661)
        centerMapOnLocation(nextLocation)
        
        let contentSize = textView.sizeThatFits(textView.bounds.size)
        var frame = textView.frame
        frame.size.height = contentSize.height
        textView.frame = frame
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    // MARK: - Navigation

    @IBAction func streamButtonClicked(sender : AnyObject){
        
        let streamVC = self.storyboard!.instantiateViewControllerWithIdentifier("StreamViewController") as! StreamViewController
        
        self.navigationController?.pushViewController(streamVC, animated: false)
    }
    
    
    @IBAction func backButtonClicked(sender : AnyObject){
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
