//
//  ModelManager.swift
//  eventnode
//
//  Created by mrinal khullar on 4/29/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//


import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var database: FMDatabase? = nil
    
    class var instance: ModelManager {
        sharedInstance.database = FMDatabase(path: Util.getPath("EventNodeDb.sqlite"))
        var path = Util.getPath("EventNodeDb.sqlite")

        return sharedInstance
    }

    func addTableData(tblName: String, primaryKey: String , tblFields: Dictionary<String, String>) -> Int {
        sharedInstance.database!.open()

        var fieldValues = [String]()

        var fieldColumns = ""
        var fieldValuePlace = ""

        var i = 0

        for (column, value) in tblFields
        {
            if i > 0
            {
                fieldColumns = "\(fieldColumns), "
                fieldValuePlace = "\(fieldValuePlace), "
            }
            
            fieldColumns = "\(fieldColumns)\(column)"
            fieldValuePlace = "\(fieldValuePlace)?"
            
            fieldValues.append("\(value)")
            i++
        }

        if fieldColumns != ""
        {
            fieldColumns = "\(fieldColumns), "
        }

        if fieldValuePlace != ""
        {
            fieldValuePlace = "\(fieldValuePlace), "
        }

        fieldColumns = "\(fieldColumns) dateCreated, dateUpdated"

        fieldValuePlace = "\(fieldValuePlace) datetime('now'), datetime('now')"
        
        var stringtobeinserted = "','".join(fieldValues)

        println("INSERT INTO \(tblName) (\(primaryKey), \(fieldColumns)) VALUES (NULL, '\(stringtobeinserted)',  datetime('now'), datetime('now'))")

        println(fieldValues)

        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO \(tblName) (\(primaryKey), \(fieldColumns)) VALUES (NULL, \(fieldValuePlace))", withArgumentsInArray: fieldValues as [AnyObject])

        var insertedId = -1

        if(isInserted)
        {
            insertedId = Int(sharedInstance.database!.lastInsertRowId())            
        }


        sharedInstance.database!.close()
        return insertedId
    }

    func updateTableData(tblName: String, tblFields: Dictionary<String, String>, whereString: String, whereFields: NSArray ) -> Bool {
        sharedInstance.database!.open()
        
        var fieldValues = [String]()
        
        var fieldColumnsValuePlace = ""
        
        var i = 0
        
        for (column, value) in tblFields
        {
            if i > 0
            {
                fieldColumnsValuePlace = "\(fieldColumnsValuePlace), "
            }
            
            fieldColumnsValuePlace = "\(fieldColumnsValuePlace)\(column)=?"
            
            fieldValues.append("\(value)")
            i++
        }
        
        if fieldColumnsValuePlace != ""
        {
            fieldColumnsValuePlace = "\(fieldColumnsValuePlace), "
        }

        
        fieldColumnsValuePlace = "\(fieldColumnsValuePlace) dateUpdated=datetime('now')"
        
        var finalWhereString = "1"
        
        
        if(whereString != "")
        {
            finalWhereString = whereString
            
            for j in 0..<whereFields.count
            {
                fieldValues.append("\(whereFields[j])")
            }
        }
 
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE \(tblName) SET \(fieldColumnsValuePlace) WHERE \(finalWhereString)", withArgumentsInArray: fieldValues as [AnyObject])
        
        sharedInstance.database!.close()
        
        return isUpdated
    }
    
    func deleteTableData(tblName: String, whereString: String, whereFields: NSArray ) -> Bool {
        sharedInstance.database!.open()
        
        var fieldValues = [String]()
        
        
        var finalWhereString = "1"
        
        
        if(whereString != "")
        {
            finalWhereString = whereString
            
            for j in 0..<whereFields.count
            {
                fieldValues.append("\(whereFields[j])")
            }
        }
        
        println("DELETE FROM \(tblName) WHERE \(finalWhereString)")
        println(fieldValues)

        let isDeleted = sharedInstance.database!.executeUpdate("DELETE FROM \(tblName) WHERE \(finalWhereString)", withArgumentsInArray: fieldValues as [AnyObject])
        
        if(isDeleted)
        {
            println("deleted")
        }
        else
        {
            println("not deleted")
        }
        
        sharedInstance.database!.close()
        
        return isDeleted
    }

    func addColumnToTable(tblName: String, columnName: String, colType: String, defaultValue: String) -> Bool
    {
        sharedInstance.database!.open()
        
        var defaultVal = ""
        
        if defaultValue != ""
        {
            defaultVal = " DEFAULT \(defaultValue)"
        }
        
        let isAdded = sharedInstance.database!.executeUpdate("ALTER TABLE \(tblName) ADD COLUMN \(columnName) \(colType)\(defaultVal)", withArgumentsInArray: [])
        
        if(isAdded)
        {
            println("added")
        }
        else
        {
            println("not added")
        }
        
        sharedInstance.database!.close()
        
        return isAdded
    }
    
    func getTableData(tblName: String, selectColumns: NSArray, whereString: String, whereFields: NSArray )->FMResultSet {
        
        sharedInstance.database!.open()

        var fieldValues = [String]()
        
        
        var finalWhereString = "1"
        
        
        if(whereString != "")
        {
           finalWhereString = whereString
           
           for j in 0..<whereFields.count
           {
               fieldValues.append("\(whereFields[j])")
           }
        }
        
        var finalSelectColumnString = "*"
        
        if(selectColumns.count>0)
        {
            finalSelectColumnString = selectColumns.componentsJoinedByString(", ")
        }
        
        println("SELECT \(finalSelectColumnString) FROM \(tblName) WHERE \(finalWhereString)")
        
        println(fieldValues)
        
        var resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT \(finalSelectColumnString) FROM \(tblName) WHERE \(finalWhereString)", withArgumentsInArray: fieldValues as [AnyObject])

        return resultSet
    }
    
}
