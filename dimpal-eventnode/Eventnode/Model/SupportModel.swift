//
//  SupportModel.swift
//  Eventnode
//
//  Created by Chandra Kilaru on 10/9/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation

let supportModel = SupportModel()

class SupportModel: NSObject
{
    let domain: String = "eventnode.freshdesk.com"
    let appKey: String = "eventnode-2-8d10a86d4ca6d5830dc8618c4b910ed1"
    let appSecret: String = "df4fdc3902308844c051982f3b52f7e1d3b72143"
    
    override init() {
        var config: MobihelpConfig = MobihelpConfig(domain: self.domain,
            withAppKey: self.appKey,
            andAppSecret: self.appSecret)
        config.feedbackType = FEEDBACK_TYPE.NAME_AND_EMAIL_REQUIRED
        config.enableAutoReply = true // Enable Auto Reply
        config.appStoreId = "id1049409726"
        config.launchCountForAppReviewPrompt = 10 // Show review Automatically after 10 launches.
        Mobihelp.sharedInstance().initWithConfig(config)
    }
    
    class var instance: SupportModel {
        return supportModel
    }
    
    func initialize() {
        println("Initialize already complete")
    }
    
    func initializeWithUsernameAndEmail(userName: String, email: String) {
        Mobihelp.sharedInstance().userName = userName
        Mobihelp.sharedInstance().emailAddress = email
    }
}