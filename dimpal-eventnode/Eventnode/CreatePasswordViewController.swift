//
//  CreatePasswordViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class CreatePasswordViewController: UIViewController {

    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    @IBOutlet var loaderView : UIView!
    
    @IBOutlet var loaderSubView : UIView!
    
    @IBOutlet var wakeUpView : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        var loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        loadingMessage.textAlignment = .Center
        loaderSubView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        
        
        self.loaderView.hidden = true
        newPasswordTextField.secureTextEntry = true
        confirmPassword.secureTextEntry = true
        

        
        let paddingViewForNewPassword = UIView(frame: CGRectMake(0, 0, 15, self.newPasswordTextField.frame.height))
        newPasswordTextField.leftView = paddingViewForNewPassword
        newPasswordTextField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewForConfirmPassword = UIView(frame: CGRectMake(0, 0, 15, self.confirmPassword.frame.height))
        confirmPassword.leftView = paddingViewForConfirmPassword
        confirmPassword.leftViewMode = UITextFieldViewMode.Always

        
        newPasswordTextField.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        newPasswordTextField.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        confirmPassword.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        confirmPassword.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
//        newPasswordTextField.attributedPlaceholder = NSAttributedString(string: "New Password",attributes:[NSForegroundColorAttributeName: UIColor.blackColor(),UIFont():(name: "AvenirNext-Medium", size: 13.0)])
        
        let newPasswordFontStyle = UIFont(name: "AvenirNext-Medium", size: 13)!
       
        let newPasswordAttributes = [NSForegroundColorAttributeName: UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0), NSFontAttributeName : newPasswordFontStyle]
       
        newPasswordTextField.attributedPlaceholder = NSAttributedString(string: "New Password",
            attributes:newPasswordAttributes)
       
        
        let confirmPasswordFontStyle = UIFont(name: "AvenirNext-Medium", size: 13)!
        
        let confirmPasswordAttributes = [NSForegroundColorAttributeName: UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0), NSFontAttributeName : confirmPasswordFontStyle]
        
        confirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
            attributes:confirmPasswordAttributes)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        newPasswordTextField.resignFirstResponder()
        confirmPassword.resignFirstResponder()
        
    }
    
    
    
    @IBAction func createpassword(sender: AnyObject)
    {
        
        // wakeUpView.hidden = false
        
        
        //var username = NSUserDefaults.standardUserDefaults().valueForKey("username") as! NSString
        
        if count(newPasswordTextField.text) >= 7
        {
            
            if newPasswordTextField.text != "" && confirmPassword.text != ""
            {
                if newPasswordTextField.text == confirmPassword.text
                {
                    loaderView.hidden = false
                    PFUser.currentUser()?.password = newPasswordTextField.text!
                    PFUser.currentUser()?["isFacebookLogin"] =  true
                    PFUser.currentUser()?["hasPassword"] =  true
                    PFUser.currentUser()?.saveInBackgroundWithBlock({
                        (success:Bool, error:NSError?) -> Void in
                        if success
                        {
                            
                            self.loaderView.hidden = true
                            hasPassword = true
                            println(PFUser.currentUser()?.password)
                            
                            NSUserDefaults.standardUserDefaults().setObject(self.newPasswordTextField.text, forKey: "password")
                            
                            var refreshAlert = UIAlertController(title: "Success", message: "Password created successfully", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            self.loginInBackground()
                            
                            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                                self.navigationController?.popViewControllerAnimated(true)
                            }))
                            self.presentViewController(refreshAlert, animated: true, completion: nil)
                            
                            
                        }
                        else
                        {
                            self.loaderView.hidden = true
                            
                            var alert = UIAlertView()
                            alert.title = "Alert"
                            alert.message = error?.localizedDescription
                            alert.addButtonWithTitle("Ok")
                            alert.show()
                        }
                        
                    })
                }
                else
                {
                    
                    var alert = UIAlertView()
                    alert.title = "Alert"
                    alert.message = "New password and confirm password must be same."
                    alert.addButtonWithTitle("Ok")
                    alert.show()
                }
            }
            else
            {
                var alert = UIAlertView()
                alert.title = "Alert"
                alert.message = "Please fill all the required fields."
                alert.addButtonWithTitle("Ok")
                alert.show()
            }
        }
        else
        {
            var alert = UIAlertView()
            alert.title = "Alert"
            alert.message = "Password must contain atleast 7 characters."
            alert.addButtonWithTitle("Ok")
            alert.show()
        }
        
        
    }
    
    func loginInBackground()
    {
        var email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        println(email)
        var newPasword = NSUserDefaults.standardUserDefaults().objectForKey("password") as! String
        println(newPasword)
        
        PFUser.logInWithUsernameInBackground(email, password:newPasword)
            {
                (user: PFUser?, error: NSError?) -> Void in
                if user != nil {
                    println("logInsuccessfull")
                    
                } else
                {
                    println("user not exist")
                }
        }
        
    }
    
    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if buttonIndex == 0
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
