//
//  AdjustPhotoViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/24/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import AssetsLibrary
import MobileCoreServices

class AdjustPhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var perfectmsg : UILabel!
    @IBOutlet var leftrightlabel : UILabel!
    @IBOutlet var updownlabel : UILabel!
    @IBOutlet var imagewrap : UIScrollView!
    
    @IBOutlet var button : UIButton!
    
    @IBOutlet var imageToSave: UIImageView!
    var imageData: UIImage!
    
    var timer = NSTimer()

    
    @IBOutlet var loaderSubView : UIView!
    @IBOutlet var loaderView : UIView!
    
    var loadingMessage = UILabel()
    
    @IBOutlet weak var croplayer: UIImageView!

    var wastouchbeganonlayer: Bool! = false

    var moveHorizontal: Bool! = false
    var moveVertical: Bool! = false
    
    var lastX: CGFloat! = 0
    var lastY: CGFloat! = 0
    
    
    var orientation: UIImageOrientation = .Up
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.view.addSubview(wakeUpImageView)
        
        println(eventTitle)
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)

        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        indicator.startAnimating()
        
        println(imageData.size.height)
        println(imageData.size.width)
        
        if(imagewrap.frame.height>imageData.size.height || imagewrap.frame.width>imageData.size.width)
        {
            var refreshAlert = UIAlertController(title: "Error", message: "Photo is too small. Please select a different photo.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                self.navigationController?.popViewControllerAnimated(false)
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
        else
        {

            if(imageData.size.width/imageData.size.height == 4/3 )
            {
                perfectmsg.hidden=false
                self.view.viewWithTag(1)?.hidden = true
                self.view.viewWithTag(2)?.hidden = true
                self.view.viewWithTag(4)?.hidden = true
                self.view.viewWithTag(6)?.hidden = true
                self.view.viewWithTag(7)?.hidden = true
                self.view.viewWithTag(9)?.hidden = true
                
                moveHorizontal = false
                moveVertical = false
                
                imageToSave.frame.size.width = imagewrap.frame.width
                imageToSave.frame.size.height = imagewrap.frame.height
                
            }
            else
            {
                perfectmsg.hidden=true

                println(imageData.size.height)
                println(imageData.size.width)
                
                if(imageData.size.width<=imageData.size.height || (imageData.size.width/imageData.size.height < 4/3 && imageData.size.width/imageData.size.height > 1))
                {
                    imageToSave.frame.size.width = imagewrap.frame.width
                    imageToSave.frame.size.height = imagewrap.frame.width * (imageData.size.height/imageData.size.width)
                    
                    imageToSave.frame.origin.x = 0
                    imageToSave.frame.origin.y = (imagewrap.frame.height/2)-(imageToSave.frame.size.height/2)
                    
                    self.view.viewWithTag(1)?.hidden = true
                    self.view.viewWithTag(2)?.hidden = true
                    self.view.viewWithTag(4)?.hidden = true
                    self.view.viewWithTag(6)?.hidden = false
                    self.view.viewWithTag(7)?.hidden = false
                    self.view.viewWithTag(9)?.hidden = false
                    
                    moveHorizontal = false
                    moveVertical = true
                    
                }
                else
                {
                    imageToSave.frame.size.width = imagewrap.frame.height * (imageData.size.width/imageData.size.height)
                    imageToSave.frame.size.height = imagewrap.frame.height
                    
                    imageToSave.frame.origin.x = (imagewrap.frame.width/2)-(imageToSave.frame.size.width/2)
                    imageToSave.frame.origin.y = 0

                    self.view.viewWithTag(1)?.hidden = false
                    self.view.viewWithTag(2)?.hidden = false
                    self.view.viewWithTag(4)?.hidden = false
                    self.view.viewWithTag(6)?.hidden = true
                    self.view.viewWithTag(7)?.hidden = true
                    self.view.viewWithTag(9)?.hidden = true
                    
                    moveHorizontal = true
                    moveVertical = false
                    
                }
            }

            imageToSave.image = imageData
        }


        croplayer.alpha = 0.4
        wastouchbeganonlayer = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func showLoader(message: String)
    {
        loadingMessage.text = "\(message)"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        loaderView.hidden = false
    }
    
    
    @IBAction func updateImage(sender : AnyObject){
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            showLoader("Loading")
            var imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {

        println(image.size.height)
        println(image.size.width)
        loaderView.hidden = true
        if(imagewrap.frame.height>image.size.height || imagewrap.frame.width>image.size.width){
            var refreshAlert = UIAlertController(title: "Error", message: "Photo is too small. Please select a different photo.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                self.loaderView.hidden = true
            }))
            
            dispatch_async(dispatch_get_main_queue(), {
               self.presentViewController(refreshAlert, animated: true, completion: nil)
            })
        }
        else
        {
            let data = UIImagePNGRepresentation(image)
            
            imageData = image
            
            if(imageData.size.width/imageData.size.height == 4/3 )
            {
                perfectmsg.hidden=false
                self.view.viewWithTag(1)?.hidden = true
                self.view.viewWithTag(2)?.hidden = true
                self.view.viewWithTag(4)?.hidden = true
                self.view.viewWithTag(6)?.hidden = true
                self.view.viewWithTag(7)?.hidden = true
                self.view.viewWithTag(9)?.hidden = true
                
                moveHorizontal = false
                moveVertical = false
                
                imageToSave.frame.origin.x = 0
                imageToSave.frame.origin.y = 0
                
                
                imageToSave.frame.size.width = imagewrap.frame.width
                imageToSave.frame.size.height = imagewrap.frame.height
                
            }
            else
            {
                perfectmsg.hidden=true
                
                if(imageData.size.width<=imageData.size.height || (imageData.size.width/imageData.size.height < 4/3 && imageData.size.width/imageData.size.height > 1))
                {
                    imageToSave.frame.size.width = imagewrap.frame.width
                    imageToSave.frame.size.height = imagewrap.frame.width * (imageData.size.height/imageData.size.width)
                    
                    imageToSave.frame.origin.x = 0
                    imageToSave.frame.origin.y = (imagewrap.frame.height/2)-(imageToSave.frame.size.height/2)
                    
                    self.view.viewWithTag(1)?.hidden = true
                    self.view.viewWithTag(2)?.hidden = true
                    self.view.viewWithTag(4)?.hidden = true
                    self.view.viewWithTag(6)?.hidden = false
                    self.view.viewWithTag(7)?.hidden = false
                    self.view.viewWithTag(9)?.hidden = false
                    
                    moveHorizontal = false
                    moveVertical = true
                    
                }
                else
                {
                    imageToSave.frame.size.width = imagewrap.frame.height * (imageData.size.width/imageData.size.height)
                    imageToSave.frame.size.height = imagewrap.frame.height
                    
                    imageToSave.frame.origin.x = (imagewrap.frame.width/2)-(imageToSave.frame.size.width/2)
                    imageToSave.frame.origin.y = 0
                    
                    self.view.viewWithTag(1)?.hidden = false
                    self.view.viewWithTag(2)?.hidden = false
                    self.view.viewWithTag(4)?.hidden = false
                    self.view.viewWithTag(6)?.hidden = true
                    self.view.viewWithTag(7)?.hidden = true
                    self.view.viewWithTag(9)?.hidden = true
                    
                    
                    
                    moveHorizontal = true
                    moveVertical = false
                    
                }
            }
            
            imageToSave.image = imageData
        }
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        NSLog("picker cancel.")
        loaderView.hidden = true
        picker .dismissViewControllerAnimated(true, completion: nil)
    }


    @IBAction func closeAdjustPhotoButtonClicked(sender : AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    @IBAction func saveButtonClicked(sender : AnyObject){
        
        orientation = imageData.imageOrientation
        
        orientation = UIImageOrientation.Up
        
        var originalWidth  = imageData.size.width
        var originalHeight = imageData.size.height
        
        var imageHeight = imageToSave.frame.size.height
        var imageWidth = imageToSave.frame.size.width

        var cropHeight: CGFloat = originalHeight
        var cropWidth: CGFloat = originalWidth
        var posX: CGFloat = 0
        var posY: CGFloat = 0
        
        if(moveHorizontal == true){
            cropHeight = originalHeight
            cropWidth = originalHeight*(4/3)
            var widthFactor: CGFloat = originalWidth/imageWidth
            posX = imageToSave.frame.origin.x*widthFactor*(-1)
            posY = 0
        }
        
        if(moveVertical == true){
            cropHeight = originalWidth*(3/4)
            cropWidth = originalWidth
            var heightFactor: CGFloat = originalHeight/imageHeight
            posX = 0
            posY = imageToSave.frame.origin.y*heightFactor*(-1)
            
            println("cy:\(posY)")
            println("cx:\(posX)")
            
            println("ch:\(cropHeight)")
            println("cw:\(cropWidth)")
        }
        
        var originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(imageData!), 0.3)
        
        imageData = UIImage(data: originaldata)
        
        var cropSquare = CGRectMake(posX, posY, cropWidth, cropHeight)

        var cgimg = CGImageCreateWithImageInRect(imageData.CGImage, cropSquare);


        var newImage = UIImage(CGImage: cgimg, scale:1, orientation:orientation)
        
        var cropdata = UIImageJPEGRepresentation(self.correctlyOrientedImage(newImage!), 0.3)
        
        newImage = UIImage(data: cropdata)
        
        
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventVC.imageData = newImage
        eventVC.originalImageData = imageData
        eventVC.frameX = imageToSave.frame.origin.x
        eventVC.frameY = imageToSave.frame.origin.y
        eventVC.isAfterImage = true
        
        self.navigationController?.pushViewController(eventVC, animated: true)
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            println("dfrr")
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        var normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        var taps = touches as NSSet
        let touch = taps.allObjects[0] as! UITouch
        let touchLocation = touch.locationInView(self.view)
        lastX = touchLocation.x
        lastY = touchLocation.y

        if CGRectContainsPoint(imagewrap.frame, touchLocation) {
            wastouchbeganonlayer = true
        }
        else
        {
            wastouchbeganonlayer = false
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {

        if(wastouchbeganonlayer == true){
            adjustPhoto(touches)
        }
    }

    func adjustPhoto(touches: NSSet!){
        
        let touch = touches.allObjects[0] as! UITouch
        let touchLocation = touch.locationInView(self.view)

        if(moveHorizontal == true)
        {
            var diffX: CGFloat = touchLocation.x-lastX
            lastX = touchLocation.x

            if(diffX>0)
            {
                if(imageToSave.frame.origin.x >= 0)
                {
                    imageToSave.frame.origin.x = 0
                }
                else
                {
                    imageToSave.frame.origin.x = imageToSave.frame.origin.x+diffX
                }
            }

            if(diffX<0)
            {
                if(((-1)*imageToSave.frame.origin.x) >= (imageToSave.frame.width-imagewrap.frame.width))
                {
                    imageToSave.frame.origin.x = (imagewrap.frame.width-imageToSave.frame.width)
                }
                else
                {
                    imageToSave.frame.origin.x = imageToSave.frame.origin.x+diffX
                }
            }
        }
        
        if(moveVertical == true)
        {

            var diffY: CGFloat = touchLocation.y-lastY
            lastY = touchLocation.y

            if(diffY>0)
            {
                if(imageToSave.frame.origin.y >= 0)
                {
                    imageToSave.frame.origin.y = 0
                }
                else
                {
                    imageToSave.frame.origin.y = imageToSave.frame.origin.y+diffY
                }
            }
        

            if(diffY<0)
            {
                if(((-1)*imageToSave.frame.origin.y) >= (imageToSave.frame.height-imagewrap.frame.height))
                {
                    imageToSave.frame.origin.y = (imagewrap.frame.height-imageToSave.frame.height)
                }
                else
                {
                    imageToSave.frame.origin.y = imageToSave.frame.origin.y+diffY
                }
            }
        }
    }

}
