//
//  SendEmail.swift
//  Eventnode
//
//  Created by brst on 9/4/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class SendNonInvitationEmails
{
    func sendEmail(emailSubject: String, message: String, emails: [String])
    {
        var sns = AWSSES.defaultSES()
        
        var messageBody = AWSSESContent()
        var subject = AWSSESContent()
        var body = AWSSESBody()
        
        
        
        subject.data = emailSubject
        
        //messageBody.data = "\(senderName) invited you to the event, \(eventTitle). Your invitation code is \(invitation.objectId!)"
        
        messageBody.data = message
        
        //body.text = messageBody
        body.html = messageBody
        
        var theMessage = AWSSESMessage()
        theMessage.subject = subject
        
        theMessage.body = body
        
        //email = "dimpal1990@gmail.com"
        
        var destination = AWSSESDestination()
        destination.toAddresses = emails
        
        println(emails)
        
        var username = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        println(username)
        var send = AWSSESSendEmailRequest()
        send.source = "\(username)<noreply@eventnode.co>"
        send.destination = destination
        send.message = theMessage
        send.returnPath = "noreply@eventnode.co"
        
        
        
        sns.sendEmail(send).continueWithBlock {(task: AnyObject!) -> AWSTask! in
            
            if task.error != nil
            {
                println(task.error.debugDescription)
            }
            else
            {
                println("success")
            }

            return nil
            
        }
    }
}