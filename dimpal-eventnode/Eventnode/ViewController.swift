//
//  ViewController.swift
//  TipCalculator
//
//  Created by mrinal khullar on 4/22/15.
//  Copyright (c) 2015 TBI LLC. All rights reserved.
//

import UIKit
import MobileCoreServices

var isFacebookLogin: Bool = false
var hasPassword: Bool = false

var facebookFriends: Array<NSDictionary!> = []

var showInviteCodePopup: Bool = false

class ViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var facebookLoginButton : UIButton!
    //@IBOutlet var facebookSignupButton : UIButton!
    
    @IBOutlet weak var registerWithemail: UIButton!
    //@IBOutlet var facebookSignupLabel : UILabel!
    @IBOutlet var tAndCText : UITextView!
    //@IBOutlet var loginView : UIView!
    
    @IBOutlet var loaderView : UIView!
    
    @IBOutlet var loaderSubView : UIView!
    
    @IBOutlet var wakeUpView : UIImageView!
    var resultdict = NSDictionary()
    var nsdefault = NSUserDefaults.standardUserDefaults()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //var user: PFUser = PFUser.currentUser()!
        
        //println(user["fullUserName"] as! String)
        
        self.view.addSubview(wakeUpImageView)
        
        facebookLoginButton.hidden=true
        //facebookSignupButton.hidden=true
        //facebookSignupLabel.hidden=true
        tAndCText.hidden=true
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        //indicator.frame = (loaderSubView.frame.width/2) - (indicator.frame.width/2)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        var loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        self.wakeUpView.hidden = false
        self.loaderView.hidden = false
        
        //registerWithemail.backgroundColor = UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0)
        
        
        // TODO(geetikak): What does isNormalLogin signify ?
        if var isNormalLogin = NSUserDefaults.standardUserDefaults().objectForKey("isNormalLogin") as? String
        {
            var isLoggedIn = "No"
            
            if var loggedInStatus = NSUserDefaults.standardUserDefaults().objectForKey("isLoggedIn") as? String
            {
                isLoggedIn = loggedInStatus
            }
            
            if isNormalLogin == "Yes" && isLoggedIn == "Yes"
            {
                var email = ""
                var password = ""

                if var savedEmail = NSUserDefaults.standardUserDefaults().objectForKey("email") as? String
                {
                    email = savedEmail
                }
                
                if var savedPassword = NSUserDefaults.standardUserDefaults().objectForKey("password") as? String
                {
                    password = savedPassword
                }
                
                doNormalLogin(email, password: password)
            }
            else
            {
                // TODO(geetikak): what is the point of calling loginAutomatically ? It takes care of only FB user.
                // but this code path is reachable when isNormalLogin is true and isLoggedIn is false.
                // Is that a scenario that we need to handle ?
                loginAutomaticaly()
            }
        }
        else
        {
            loginAutomaticaly()
        }
        
        
                
        
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            var imag = UIImagePickerController()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func viewTapped(sender : AnyObject) {
        //totalTextField.resignFirstResponder()
    }
    
    
    
    
    
    func doNormalLogin(email: String, password: String)
    {
        PFUser.logInWithUsernameInBackground(email, password: password)
            {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user
                {
                    
                    hasPassword = true
                    
                    println("user found")
                    
                    println(user["emailVerified"] as! Bool)
                    
                    NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                    
                    
                    NSUserDefaults.standardUserDefaults().setObject(email, forKey: "email")
                    NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                    NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isNormalLogin")
                    NSUserDefaults.standardUserDefaults().setObject(password, forKey: "password")
                    NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                    
                    
                    let s3BucketName = "eventnodepublicpics"
                    let fileName = "profilePic.png"
                    
                    let downloadFilePath = documentDirectory.stringByAppendingPathComponent(fileName)
                    let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                    
                    let downloadRequest = AWSS3TransferManagerDownloadRequest()
                    downloadRequest.bucket = s3BucketName
                    println("\(user.objectId!)/profilePic/profilePic.png")
                    downloadRequest.key  = "\(user.objectId!)/profilePic/profilePic.png"
                    downloadRequest.downloadingFileURL = downloadingFileURL
                    
                    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                    
                    
                    transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                        
                        if (task.error != nil){
                            if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                                switch (task.error.code) {
                                case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                    break;
                                case AWSS3TransferManagerErrorType.Paused.rawValue:
                                    break;
                                    
                                default:
                                    println("error downloading")
                                    break;
                                }
                            } else {
                                // Unknown error.
                                println("error downloading")
                            }
                        }
                        
                        if (task.result != nil) {

                            println("downloading successfull")
                            
                        }
                        
                        return nil
                        
                    })
                    
                    
                    isFacebookLogin = user["isFacebookLogin"] as! Bool
                    
                    // TODO(geetikak): Should we log a user logged in metric here ?
                    
                    self.facebookLoginButton.hidden=false
                    self.tAndCText.hidden=false
                    
                    if isFacebookLogin
                    {
                        self.fetchFacebookFriends()
                    }
                    
                    
                    self.updateDeviceToken(user.objectId!)
                    
                    self.navigateToNextScreen(user["fullUserName"] as! String)
                    
                    println("\(PFUser.currentUser())")
                }
                else
                {
                    self.loaderView.hidden = true
                    self.wakeUpView.hidden = true
                    self.facebookLoginButton.hidden=false
                    self.tAndCText.hidden=false
                    println("Could not login");
                }
                
        }
        
    }
    
    
    func checkExistance(email: String, facebookId: String, firstName: String, lastName: String, userObject: PFUser, isSignedUp: Bool )
    {
        println(email)
        let query = PFUser.query()
        query!.whereKey("email", equalTo: email)
        query!.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            println(users!.count)
            if let users = users as? [PFObject]
            {
                println(users)
                if users.count == 0 || self.isValidEmail(userObject.username!)
                {
                    let query = PFQuery(className: "LinkedAccounts")
                    query.whereKey("email", equalTo: email)
                    query.findObjectsInBackgroundWithBlock {
                        (users: [AnyObject]?, error: NSError?) -> Void in
                        
                        if let users = users as? [PFObject]
                        {
                            println(users)
                            if users.count == 0
                            {
                                var fbId = facebookId
                                println("\(fbId)")
                                
                                
                                var emailData = email
                                
                                var usernameData = firstName
                                var lastnameData = lastName
                                
                                
                                let s3BucketName = "eventnodepublicpics"
                                let fileName = "profilePic.png"
                                
                                let downloadFilePath = documentDirectory.stringByAppendingPathComponent(fileName)
                                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                                
                                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                                downloadRequest.bucket = s3BucketName
                                println("\(userObject.objectId!)/profilePic/profilePic.png")
                                downloadRequest.key  = "\(userObject.objectId!)/profilePic/profilePic.png"
                                downloadRequest.downloadingFileURL = downloadingFileURL
                                
                                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                                
                                
                                transferManager.download(downloadRequest).continueWithBlock {
                                    (task: AWSTask!) -> AnyObject! in
                                    
                                    if task.error != nil
                                    {
                                        println("Error downloading")
                                        println(task.error.description)
                                        
                                        var url:NSURL = NSURL(string:"https://graph.facebook.com/\(fbId)/picture?type=square")!
                                        var data:NSData = NSData(contentsOfURL: url)!
                                        
                                        
                                        let fileManager = NSFileManager.defaultManager()
                                        
                                        var filePathToWrite = "\(documentDirectory)/profilePic.png"
                                        
                                        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
                                        
                                        self.uploadProfilePic()
                                        
                                    }
                                    else
                                    {
                                        println("successfull")
                                    }
                                    
                                    return nil
                                }
                                
                                var user: PFUser = userObject
                                
                                user["isFacebookLogin"] =  true
                                user["facebookId"] =  fbId
                                
                                if var hasSetPassword = user["hasPassword"] as? Bool
                                {
                                    user["hasPassword"] =  hasSetPassword
                                    hasPassword = hasSetPassword
                                    if !hasPassword
                                    {
                                        user.email = emailData
                                        user.username = emailData
                                    }
                                }
                                else
                                {
                                    user["hasPassword"] =  false
                                    hasPassword = false
                                    user.email = emailData
                                    user.username = emailData
                                }
                                
                                
                                // user["emailVerified"] = true
                                //println("\(isFacebookLogin)")
                                
                                println("\(user.email)")
                                
                                user["fullUserName"] = "\(usernameData) \(lastnameData)"
                                
                                user.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                                    if success
                                    {
                                        if isSignedUp
                                        {
                                            self.updateDeviceToken(user.objectId!)
                                            
                                            var localNotification = UILocalNotification()
                                            
                                            localNotification.fireDate = NSDate()
                                            
                                            localNotification.alertBody = "Welcome to eventnode, we love that you chose us. Don’t wait for a special occasion, everyday can be an event with eventnode."
                                            
                                            // localNotification.applicationIconBadgeNumber = 100
                                            
                                            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                                        }
                                        
                                        self.setFacebookUser(user, usernameData: usernameData, lastnameData: lastnameData, emailData: emailData)
                                        
                                    }
                                    else
                                    {
                                        self.loaderView.hidden = true
                                        self.wakeUpView.hidden = true
                                        
                                        println(error?.localizedDescription)
                                    }
                                })
                            }
                            else
                            {
                                self.deleteUser(userObject, userToLogin: users[0] as! PFUser, usernameData: firstName, lastnameData: lastName, emailData: email)
                            }
                        }
                    }
                    
                    
                }
                else
                {
                    self.deleteUser(userObject, userToLogin: users[0] as! PFUser, usernameData: firstName, lastnameData: lastName, emailData: email)
                }
            }
            else
            {
                self.loaderView.hidden = true
                self.wakeUpView.hidden = true
            }
            
        }
    }
    
    func deleteUser(user: PFUser, userToLogin: PFUser, usernameData: String, lastnameData: String, emailData: String)
    {
        println(user)
        
        PFFacebookUtils.unlinkUserInBackground(user, block: { (success:Bool, error:NSError?) -> Void in
            println("deleted")
            
            /*if(userToLogin["isFacebookLogin"] as? Bool == true)
            {
                if var hasSetPassword = userToLogin["hasPassword"] as? Bool
                {
                    userToLogin["hasPassword"] =  hasSetPassword
                    hasPassword = hasSetPassword
                }
                else
                {
                    userToLogin["hasPassword"] =  false
                    hasPassword = false
                }
                self.setFacebookUser(userToLogin, usernameData: usernameData, lastnameData: lastnameData, emailData: emailData)
            }
            else
            {*/
                var refreshAlert = UIAlertController(title: "Error", message: "There's an existing Eventnode account using your facebook email address. Either link your facebook account with that Eventnode account or change the email address in that Eventnode account.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                    self.loaderView.hidden=true
                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            /*}*/
        });
    }
    
    
    func setFacebookUser(user: PFUser, usernameData: String, lastnameData: String, emailData: String)
    {
        isFacebookLogin = true
        //hasPassword = (user["hasPassword"] as? Bool)!
        
        println("data is updated")
        
        NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
        NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
        NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
        NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
        
        if hasPassword
        {
            NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
            NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setObject(emailData, forKey: "email")
            NSUserDefaults.standardUserDefaults().setObject("\(usernameData) \(lastnameData)", forKey: "fullUserName")
        }
        NSUserDefaults.standardUserDefaults().setObject(emailData, forKey: "facebookEmail")
        NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "facebookName")
        
        self.fetchFacebookFriends()
        
        self.updateDeviceToken(user.objectId!)

        self.navigateToNextScreen(user["fullUserName"] as! String)
    }
    
    func navigateToNextScreen(userName: String) {
        countMyEvents()
    }
    
    func countMyEvents() {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {

        let myEventsPredicate = NSPredicate(format: "eventCreatorObjectId = '\(currentUserId)'")
        
        var myEventsQuery = PFQuery(className:"Events", predicate: myEventsPredicate)
        
        myEventsQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(myEventsQuery, target: self, successSelector: "fetchMyEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchMyEventsError:", errorSelectorParameters:nil)
        }
    }
    
    func fetchMyEventsSuccess(timer:NSTimer) {
        
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        if (objects!.count > 0) {
            
            self.loaderView.hidden = true
            self.wakeUpView.hidden = true
            
            var myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            self.navigationController?.pushViewController(myEventsVC, animated: false)
        } else {
            countMySharedEvents()
        }
    }
    
    func fetchMyEventsError(timer:NSTimer) {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        println("Unable to fetch myEvents count. What to do here ?")
        checkOfflineData()
    }
    
    func countMySharedEvents() {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
        let mySharedEventsPredicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
        
        var mySharedEventsQuery = PFQuery(className:"Invitations", predicate: mySharedEventsPredicate)
        
        mySharedEventsQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(mySharedEventsQuery, target: self, successSelector: "fetchMySharedEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchMySharedEventsError:", errorSelectorParameters:nil)
        }
    }
    
    func fetchMySharedEventsSuccess(timer:NSTimer) {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        println("Successfully retrieved \(objects!.count) events.")
        
        self.loaderView.hidden = true
        self.wakeUpView.hidden = true
        
        if (objects!.count > 0) {
            var mySharedEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
            self.navigationController?.pushViewController(mySharedEventsVC, animated: false)
        } else {
            print("Navigating to OnboardingVC")
            let onboardingVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
            //onboardingVC.firstName = userName
            self.navigationController?.pushViewController(onboardingVC, animated: false)
        }
    }
    
    func fetchMySharedEventsError(timer:NSTimer) {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        println("Error: \(error) \(error.userInfo!)")
        println("Unable to fetch mySharedEvents count. What to do here ?")
        checkOfflineData()
    }
    
    
    func checkOfflineData()
    {
        self.loaderView.hidden = true
        self.wakeUpView.hidden = true
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            var resultSetEventCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "eventCreatorObjectId = ? ", whereFields: [currentUserId])
            
            resultSetEventCount.next()
            
            var totalEventCount = Int(resultSetEventCount.intForColumn("count"))
            resultSetEventCount.close()
            
            if totalEventCount > 0
            {
                var myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                self.navigationController?.pushViewController(myEventsVC, animated: false)
            }
            else
            {
                var resultSetSharedEventCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "objectId IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = ?  )", whereFields: [currentUserId])
                
                resultSetSharedEventCount.next()
                
                var totalSharedEventCount = Int(resultSetSharedEventCount.intForColumn("count"))
                resultSetSharedEventCount.close()
                if totalSharedEventCount > 0 {
                    var mySharedEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
                    self.navigationController?.pushViewController(mySharedEventsVC, animated: false)
                } else {
                    print("Navigating to OnboardingVC")
                    let onboardingVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
                    //onboardingVC.firstName = userName
                    self.navigationController?.pushViewController(onboardingVC, animated: false)
                }
            }
            
        }
    }
    

    func loginAutomaticaly()
    {
        
        let accessToken = FBSDKAccessToken.currentAccessToken() // Use existing access token.
        
        if(accessToken != nil)
        {
            println(accessToken)
            PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken, block: {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user {
                    var user_id = user.objectId!
                    println("\(user_id)")

                    if var currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
                    {
                        println(currentUserId)
                        if var isLoggedIn = NSUserDefaults.standardUserDefaults().objectForKey("isLoggedIn") as? String
                        {
                            println(isLoggedIn)
                            if isLoggedIn == "Yes"
                            {
                                isFacebookLogin = true
                                
                                // TODO(geetikak): Confirm this is always existing user and remove the isNew condition.
                                if (user.isNew) {
                                    AnalyticsModel.instance.identifyNewUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                                } else {
                                    AnalyticsModel.instance.identifyExistingUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                                }
                                SupportModel.instance.initializeWithUsernameAndEmail((user["fullUserName"] as? String)!, email: user.email!)
                                
                                if var password = user["hasPassword"] as? Bool
                                {
                                    // Why would FB user have password ? It's a feature. You can turn it on in EventNode Settings.
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                                    NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
                                    NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
                                    
                                    NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
                                    NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                                    
                                    hasPassword = password
                                    
                                    self.fetchFacebookFriends()
                                    
                                    self.updateDeviceToken(user.objectId!)
                                    
                                    self.navigateToNextScreen(user["fullUserName"] as! String)
                                }
                                else
                                {
                                    
                                    var graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                                    
                                    var connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                                        if error != nil {
                                            println(error)
                                        } else
                                        {
                                            
                                            self.resultdict = result as! NSDictionary
                                            
                                            var fbId = self.resultdict.valueForKey("id") as! String
                                            println("\(fbId)")
                                            println(self.resultdict)
                                            
                                            
                                            var emailData = self.resultdict.valueForKey("email") as! String
                                            
                                            var usernameData = self.resultdict.valueForKey("first_name") as! String
                                            var lastnameData = self.resultdict.valueForKey("last_name") as! String
                                            
                                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user ,isSignedUp: false)
                                            
                                        }
                                    })
                                    
                                    connection.start()
                                    
                                }
                                
                            }
                        }
                    }
                    self.facebookLoginButton.hidden=false
                    //self.facebookSignupButton.hidden=false
                    //self.facebookSignupLabel.hidden=false
                    self.tAndCText.hidden=false
                    
                    /*let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(homeVC, animated: true)*/
                    
                } else {
                    println("Uh oh. There was an error logging in.")
                    self.loaderView.hidden = true
                    self.wakeUpView.hidden = true
                    self.facebookLoginButton.hidden=false
                    //self.facebookSignupButton.hidden=false
                    //self.facebookSignupLabel.hidden=false
                    self.tAndCText.hidden=false
                }
            })
        }
        else
        {
            self.loaderView.hidden = true
            self.wakeUpView.hidden = true
            self.facebookLoginButton.hidden=false
            //facebookSignupButton.hidden=false
            //facebookSignupLabel.hidden=false
            self.tAndCText.hidden=false
        }
        
    }
    

    func updateDeviceToken(userObjectId: String!)
    {
        let installation = PFInstallation.currentInstallation()
        var user: PFUser = PFUser.currentUser()!
        
        if var allowSound = user["allowSound"] as? Bool
        {
            installation["allowSound"] = allowSound
        }
        else
        {
            user["allowSound"] = true
            installation["allowSound"] = true
            
            user.saveInBackground()
        }

        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = user["inviteNotification"] as! Bool
        installation["hostActivityNotification"] = user["inviteNotification"] as! Bool
        installation["guestActivityNotification"] = user["inviteNotification"] as! Bool

        installation.saveInBackground()
    }

    
    func updateNotificationSettingsWithDeviceToken(userObjectId: String!)
    {
        var installation = PFInstallation.currentInstallation()
        installation["userObjectId"] = userObjectId
        installation["inviteNotification"] = true
        installation["hostActivityNotification"] = true
        installation["guestActivityNotification"] = true
        installation["allowSound"] = true
        
        var user: PFUser = PFUser.currentUser()!
        
        user["inviteEmail"] = true
        user["hostActivityEmail"] = true
        user["guestActivityEmail"] = true
        user["inviteNotification"] = true
        user["hostActivityNotification"] = true
        user["guestActivityNotification"] = true
        user["allowSound"] = true
        

        installation.saveInBackground()
        user.saveInBackground()
        
    }
    
    
    
    
    
    @IBAction func loginButtonClicked(sender : AnyObject){
        
        /*let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)*/
        //NSLog("sdd")
        self.wakeUpView.hidden = true
        self.loaderView.hidden = false
        
        
        let accessToken = FBSDKAccessToken.currentAccessToken() // Use existing access token.
        
        if(accessToken != nil)
        {
            // TODO(geetikak): Understand why there would be an access token at this point ?
            // If we did, we should have been automatically logged in.
            println(accessToken)
            PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken, block: {
                (user: PFUser?, error: NSError?) -> Void in
                if let user = user {
                    var user_id = user.objectId!
                    println("\(user_id)")
                    //self.loaderView.hidden = true
                    //self.wakeUpView.hidden = true
                    if var currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
                    {
                        println(currentUserId)
                        
                        isFacebookLogin = true
                        
                        // TODO(geetikak): Confirm that this is always an existing user and cannot be a new FB User Signup.
                        if (user.isNew) {
                            AnalyticsModel.instance.identifyNewUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                        } else {
                            AnalyticsModel.instance.identifyExistingUserForAnalytics(user.email!, isFbUser: true, name: (user["fullUserName"] as? String)!, user: user)
                        }
                        SupportModel.instance.initializeWithUsernameAndEmail((user["fullUserName"] as? String)!, email: user.email!)
                        
                        //pX2x64NbCa
                        if var password = user["hasPassword"] as? Bool
                        {
                            
                            NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                            NSUserDefaults.standardUserDefaults().setObject(user.username!, forKey: "currentUserName")
                            
                            NSUserDefaults.standardUserDefaults().setObject(user.email!, forKey: "email")
                            NSUserDefaults.standardUserDefaults().setObject(user["fullUserName"] as! String, forKey: "fullUserName")
                            
                            NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                            NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isNormalLogin")
                            
                            hasPassword = password

                            
                            self.fetchFacebookFriends()
                            
                            self.updateDeviceToken(user.objectId!)
                            
                            self.navigateToNextScreen(user["fullUserName"] as! String)
                        }
                        else
                        {
                            
                            
                            var graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                            
                            var connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                            connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                                if error != nil {
                                    println(error)
                                } else
                                {
                                    
                                    self.resultdict = result as! NSDictionary
                                    
                                    var fbId = self.resultdict.valueForKey("id") as! String
                                    println("\(fbId)")
                                    println(self.resultdict)
                                    
                                    
                                    var emailData = self.resultdict.valueForKey("email") as! String
                                    
                                    var usernameData = self.resultdict.valueForKey("first_name") as! String
                                    var lastnameData = self.resultdict.valueForKey("last_name") as! String
                                    
                                    self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user , isSignedUp: false)
                                    
                                }
                            })
                            
                            connection.start()
                            
                        }
                        
                    }
                    else
                    {
                        println("Uh oh. There was an error logging in.")
                        self.openFacebookLogin()
                    }
                    
                } else {
                    println("Uh oh. There was an error logging in.")
                    self.openFacebookLogin()
                }
            })
        }
        else
        {
            self.openFacebookLogin()
        }
        
    }
    
    func openFacebookLogin(){
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile", "email", "user_friends"], block: {
            (user: PFUser?, error: NSError?) -> Void in
            
            if let user = user {
                if user.isNew {
                    
                    
                    println("User signed up and logged in through Facebook!")
                    
                    self.updateNotificationSettingsWithDeviceToken(user.objectId!)
                    
                    showInviteCodePopup = true
                    
                    // TODO(geetikak): The next piece of code until connection.start() seems to be same for the else() condition. Refactor and Simplify.
                    var graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                    
                    var connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                        if error != nil {
                            //handle error
                            println(error)
                        } else
                        {
                            
                            self.resultdict = result as! NSDictionary
                            
                            var fbId = self.resultdict.valueForKey("id") as! String
                            println("\(fbId)")
                            println(self.resultdict)
                            
                            
                            var emailData = self.resultdict.valueForKey("email") as! String
                            
                            var usernameData = self.resultdict.valueForKey("first_name") as! String
                            var lastnameData = self.resultdict.valueForKey("last_name") as! String
                            
                            
                            
                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user, isSignedUp: true )
                            
                            // New FB User SignUp.
                            AnalyticsModel.instance.identifyNewUserForAnalytics(emailData, isFbUser: true, name: usernameData + " " + lastnameData, user: user)
                            SupportModel.instance.initializeWithUsernameAndEmail(usernameData + " " + lastnameData, email: emailData)
                        }
                    })
                    connection.start()
                }
                else
                {
                    var graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
                    
                    var connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
                    connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
                        if error != nil {
                            println(error)
                        } else
                        {
                            self.resultdict = result as! NSDictionary
                            
                            var fbId = self.resultdict.valueForKey("id") as! String
                            println("\(fbId)")
                            println(self.resultdict)
                            
                            
                            var emailData = self.resultdict.valueForKey("email") as! String
                            
                            var usernameData = self.resultdict.valueForKey("first_name") as! String
                            var lastnameData = self.resultdict.valueForKey("last_name") as! String
                            
                            self.checkExistance(emailData, facebookId: fbId, firstName: usernameData, lastName: lastnameData, userObject: user, isSignedUp:false)
                            
                            AnalyticsModel.instance.identifyExistingUserForAnalytics(emailData, isFbUser: true, name: usernameData + " " + lastnameData, user: user)
                            SupportModel.instance.initializeWithUsernameAndEmail(usernameData + " " + lastnameData, email: emailData)
                        }
                    })
                    
                    connection.start()
                }
            }
            else
            {
                self.loaderView.hidden = true
                self.wakeUpView.hidden = true
                println("Uh oh. The user cancelled the Facebook login.")
                NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isLoggedIn")
            }
        })
    }
    
    
    
    
    func isValidEmail(emailid: NSString)->Bool
    {
        
        var isValid = true
        
        if !emailid.containsString(" ")
        {
            var atRateSplitArray = emailid.componentsSeparatedByString("@")
            
            if(atRateSplitArray.count>=2)
            {
                for component in atRateSplitArray
                {
                    if component as! String == ""
                    {
                        isValid = false
                    }
                }
                
                if(isValid)
                {
                    var dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                    
                    if(dotSplitArray.count>=2)
                    {
                        for component in dotSplitArray
                        {
                            if component as! String == ""
                            {
                                isValid = false
                            }
                        }
                    }
                    else
                    {
                        isValid = false
                    }
                }
            }
            else
            {
                isValid = false
            }
            
        }
        else
        {
            isValid = false
        }
        
        return isValid
    }
    
    
    func fetchFacebookFriends()
    {
        var graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me/friends?fields=name,email", parameters: nil)
        
        var connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
        connection.addRequest(graphRequest, completionHandler: { (innerConnection, result, error) -> Void in
            if error != nil {
                //handle error
                println(error)
            } else
            {
                //println(result)
                facebookFriends = result.valueForKey("data") as! Array<NSDictionary!>
                println(facebookFriends)
            }
        })
        
        connection.start()
    }
    
    func uploadProfilePic()
    {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        var profilePicUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent("profilePic.png"))
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        
        
        
        
        let pickedImage = UIImage(named: documentDirectory.stringByAppendingPathComponent("profilePic.png"))
        
        //profileImageView.image = pickedImage
        
        let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(pickedImage!), 0.5)
        
        var error:NSErrorPointer = NSErrorPointer()
        
        var currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        //NSFileManager.defaultManager().removeItemAtPath(documentDirectory.stringByAppendingPathComponent("profilePic.png"), error: error)
            uploadRequest.bucket = "eventnodepublicpics"
            uploadRequest.key =  "\(currentUserId)/profilePic/profilePic.png"
            uploadRequest.body = profilePicUrl
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead

            transferManager.upload(uploadRequest)

    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage
    {
        
        if image.imageOrientation == UIImageOrientation.Up
        {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        var normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }

    
    /*
    {"facebook":
    {
    "access_token":"CAAJASOaPZBL0BAIG69wspO3vPxoWb6XlF6aAuux5x8ZBP4Hg821F15aQsjVIjd5S7teY9ViFhnrmdeM5BnBrusZCc9z5DpZBejC2WtdxsejEiFniaz8meGMHycSpcAVM8rLbGtBZCguHQfPwnahXfyISKgzKuRlCCkq6RDnn3pCT10ED3fr2HOtkLNXNmkW7IJELjzTZBxZCAME8HqwLEUBVwBDQTubbR31Tbby0cd1x0YZAwj1w1m7j","expiration_date":"2015-09-25T08:27:26.453Z","id":"1648646342030855"
    }
    }
    */
    
    @IBAction func termsAndConditionsButtonClicked(sender : AnyObject)
    {
        let termsAndCondition = self.storyboard!.instantiateViewControllerWithIdentifier("TermsAndConditions") as! TermsAndConditionViewController
        self.navigationController?.pushViewController(termsAndCondition, animated: false)
    }
    
    
    @IBAction func registerWithEmail(sender: AnyObject)
    {
        let registerView = self.storyboard!.instantiateViewControllerWithIdentifier("RegistrationView") as! RegistrationViewController
        self.navigationController?.pushViewController(registerView, animated: true)
    }
    
    @IBAction func LoginView(sender: AnyObject)
    {
        let logInView = self.storyboard!.instantiateViewControllerWithIdentifier("logInWithEmail") as! LogInWithEmailViewController
        println("log in button pressed")
        self.navigationController?.pushViewController(logInView, animated: true)
    }
}

