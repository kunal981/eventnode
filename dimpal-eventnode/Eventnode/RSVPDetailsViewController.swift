//
//  RSVPDetailsViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 5/22/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class RSVPDetailsViewController: UIViewController {
    
    @IBOutlet var loaderView : UIView!
    @IBOutlet var loaderSubView : UIView!
    
    @IBOutlet var previewButtton : UIButton!

    
    var imageData: UIImage!

    var frameX: CGFloat!
    var frameY: CGFloat!
    var originalImageData: UIImage!
    var eventTitle: String!
    
    
    var currentUserId: String!
    var fullUserName: String!
    
    var eventLogoFile: String!
    var originalEventLogoFile: String!
    
    var eventLogoFileUrl: NSURL!
    var originalEventLogoFileUrl: NSURL!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        var loadingMessage = UILabel()
        loadingMessage.text = "Creating Event..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        
        indicator.startAnimating()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){

        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func descriptionButtonClicked(sender : AnyObject){
        
        let eventDescriptionVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDescriptionViewController") as! EventDescriptionViewController

        self.navigationController?.pushViewController(eventDescriptionVC, animated: true)
    }
    
    
    @IBAction func locationButtonClicked(sender : AnyObject){
        
        let addLocationVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddLocationViewController") as! AddLocationViewController
        addLocationVC.imageData=imageData
        addLocationVC.eventTitle = eventTitle
        self.navigationController?.pushViewController(addLocationVC, animated: true)
    }
    
    @IBAction func dateTimeButtonClicked(sender : AnyObject){
        
        let dateTimeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDateTimeViewController") as! EventDateTimeViewController
        dateTimeVC.imageData=imageData
        self.navigationController?.pushViewController(dateTimeVC, animated: true)
    }
    
    @IBAction func previewButtonClicked(sender : AnyObject){
        
        let eventPreviewVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPreviewViewController") as! EventPreviewViewController
        //eventPreviewVC.imageData=imageData
        eventPreviewVC.eventTitle = eventTitle
        eventPreviewVC.eventObject = newEvent
        self.navigationController?.pushViewController(eventPreviewVC, animated: true)
    }
    
    @IBAction func createButtonClicked(sender : AnyObject){
        
        let replaced = eventTitle.stringByReplacingOccurrencesOfString(" ", withString: "", options: nil, range: nil)
        
        if(eventTitle == "" || replaced == "")
        {
            var refreshAlert = UIAlertController(title: "Error", message: "Please enter the name of the event.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
        else
        {
            
            myEventData = [PFObject]()
            
            var eventDesc = newEvent["eventDescription"] as! String
            var eventLat = newEvent["eventLatitude"] as! Double
            var eventLong = newEvent["eventLongitude"] as! Double
            
            var startDate: NSDate = newEvent["eventStartDateTime"] as! NSDate
            var endDate: NSDate = newEvent["eventEndDateTime"] as! NSDate
            
            var startDateString = String(Int64(startDate.timeIntervalSince1970*1000))
            var endDateString = String(Int64(endDate.timeIntervalSince1970*1000))
            
            println(startDateString)
            println(endDateString)
            
            var haveError: Bool = false
            var errorElements: Array<String>!
            
            errorElements = []
            
            if(eventDesc == "")
            {
                errorElements.append("description")
                haveError = true
            }
            
            if(startDateString>=endDateString)
            {
                errorElements.append("start date, end date")
                haveError = true
            }
            
            if(eventLat == 0 || eventLong == 0)
            {
                errorElements.append("location")
                haveError = true
            }

            if(haveError)
            {

                var errorString = ", ".join(errorElements)

                errorString = "Please enter \(errorString)"

                println(errorString)

                var refreshAlert = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.Alert)

                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in

                }))
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
            else
            {
                var date = NSDate()
                let currentTimeStamp = String(Int64(date.timeIntervalSince1970*1000))

                loaderView.hidden=false

                originalEventLogoFile = "\(currentTimeStamp)_\(currentUserId)_originaleventlogo.png"

                eventLogoFile = "\(currentTimeStamp)_\(currentUserId)_eventlogo.png"


                self.originalEventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(self.originalEventLogoFile))
                let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(self.originalImageData!), 0.5)
                
                originaldata.writeToURL(self.originalEventLogoFileUrl!, atomically: true)
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                eventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(eventLogoFile))
                let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()

                uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                
                var cropdata = UIImageJPEGRepresentation(self.correctlyOrientedImage(imageData!), 0.5)

                var result = cropdata.writeToURL(eventLogoFileUrl!, atomically: true)

                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = ""
                tblFields["eventTitle"] = self.eventTitle
                tblFields["eventImage"] = self.eventLogoFile
                tblFields["originalEventImage"] = self.originalEventLogoFile
                tblFields["senderName"] = self.fullUserName
                
                
                var frameX = self.frameX
                var frameY = self.frameY
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = self.currentUserId
                
                tblFields["eventFolder"] = "\(self.currentUserId)/eventProfileImages/"

                tblFields["isRSVP"] = "1"
                    
                var dateRSVP = ""
                if newEvent["eventStartDateTime"] != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    dateRSVP = dateFormatter.stringFromDate((newEvent["eventStartDateTime"] as? NSDate)!)
                    println(dateRSVP)
                    tblFields["eventStartDateTime"] = dateRSVP
                }
                    
                if newEvent["eventEndDateTime"] != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    dateRSVP = dateFormatter.stringFromDate((newEvent["eventEndDateTime"] as? NSDate)!)
                    println(dateRSVP)
                    tblFields["eventEndDateTime"] = dateRSVP
                    println(tblFields["eventEndDateTime"])
                }
                    
                tblFields["eventDescription"] = newEvent["eventDescription"] as? String
                
                var eventLatitude = newEvent["eventLatitude"] as! Double
                var eventLongitude = newEvent["eventLongitude"] as! Double
                
                tblFields["eventLatitude"] = "\(eventLatitude)"
                tblFields["eventLongitude"] = "\(eventLongitude)"
                
                tblFields["eventLocation"] = newEvent["eventLocation"] as? String

                
                
                
                var insertedId = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                println(insertedId)
                if insertedId>0
                {
                    
                    var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("uploadEvent:"), userInfo: insertedId, repeats: false)
                }
                else
                {
                    self.loaderView.hidden = true
                    Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
                }

            }
        }
    }
    
    
    func uploadEvent(timer: NSTimer)
    {
        var insertedId = timer.userInfo as! Int
        
        if MyReachability.isConnectedToNetwork()
        {
            let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            uploadRequest.bucket = "eventnodepublicpics"
            uploadRequest.key =  "\(currentUserId)/eventProfileImages/\(eventLogoFile)"
            uploadRequest.body = eventLogoFileUrl
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
            upload(uploadRequest, isOriginal: false, insertedId: insertedId)
        }
        else
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        var normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
   
    func createFirstPost(eventObject:PFObject, insertedId: Int){
        
          ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createFirstPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createFirstPostError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    func createEvent(eventObject:PFObject, insertedId: Int)
    {
        
         ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createEventSuccess:", successSelectorParameters: insertedId, errorSelector: "createEventError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    
    func dateStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitWeekday | .CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: sdate)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        let sweekday = scomponents.weekday
        
        var dateString = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        
        return dateString
    }
    
    
    func timeStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitWeekday | .CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        var timeString = "\(shour):\(sminute) \(sam)"
        
        return timeString
    }

    
    
    func createEventSuccess(timer:NSTimer)
    {
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var eventId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var updatedAt = ""
        var createdAt = ""
        var date = ""
        
        
        var email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String

        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
            createdAt = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            println(date)
            tblFields["updatedAt"] = date
            updatedAt = date
        }        
        
        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
        if isUpdated {
            
            var localNotification = UILocalNotification()
            
            localNotification.fireDate = NSDate()
            
            localNotification.alertBody = "Congrats you just created an event. Don’t forget to invite your friends & guests."
            
            var eventLatitude = eventObject["eventLatitude"] as! Double
            var eventLongitude = eventObject["eventLongitude"] as! Double
            var email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
            var eventFolder = eventObject["eventFolder"] as! String!
            var eventImage = eventObject["eventImage"] as! String!
            
            
            
           
//            var eventCreate = EventCreationSuccess()
//            
//            var emailMessage = eventCreate.emailMessage(eventObject.objectId!, eventTitle: eventTitle, dateString: dateStringFromNSDate(eventObject["eventStartDateTime"] as! NSDate), timeString: timeStringFromNSDate(eventObject["eventStartDateTime"] as! NSDate), locationString: eventObject["eventLocation"]as! String, hostName: fullUserName, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: "rsvp")
//            
//            var sendEmail = SendEmail()
//            
//            sendEmail.sendEmail(" Success! You just created an event.", message: emailMessage, emails:[email])
            
            // localNotification.applicationIconBadgeNumber = 100
            
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)

        }
        else
        {

        }

        
        isUpdated = true

        println(eventObject.objectId)
        
        currentEvent = eventObject;
        
        println(self.originalImageData!.size.height)
        
        var tblFieldsPost: Dictionary! = [String: String]()
        
        tblFieldsPost["objectId"] = ""
        tblFieldsPost["postData"] = "\(self.originalEventLogoFile)"
        tblFieldsPost["isApproved"] = "0"
        tblFieldsPost["postHeight"] = "\(self.originalImageData!.size.height)"
        tblFieldsPost["postWidth"] = "\(self.originalImageData!.size.width)"
        tblFieldsPost["postType"] = "image"
        tblFieldsPost["eventObjectId"] = "\(currentEvent.objectId!)"
        tblFieldsPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
        
        
        var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFieldsPost)
        if insertedId>0
        {
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            self.originalEventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(self.originalEventLogoFile))
            let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            
            let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(self.originalImageData!), 0.5)

            originaldata.writeToURL(self.originalEventLogoFileUrl!, atomically: true)
            uploadRequest.bucket = "eventnode1"
            uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId)/\(self.originalEventLogoFile)"
            uploadRequest.body = self.originalEventLogoFileUrl
            
           
            self.uploadFirstPost(uploadRequest, insertedId: insertedId)
        }
        else
        {
            self.loaderView.hidden = true
            Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
        }
        
    }
    
    func createEventError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    
    
    func createFirstPostSuccess(timer:NSTimer)
    {
        self.loaderView.hidden=true
        isPostUpdated = true
        
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        var postId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            println(date)
            tblFields["createdAt"] = date
        }

        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            println(date)
            tblFields["updatedAt"] = date
        }




        var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {

        } else {

        }



        let inviteFriendsVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFriendsFirstViewController") as! InviteFriendsFirstViewController
        
        inviteFriendsVC.isFromCreated = true
        
        self.navigationController?.pushViewController(inviteFriendsVC, animated: true)
    }
    
    func createFirstPostError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
        
    }
    
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int){
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    
    
    func internetErrorForFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int){
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    
    func uploadFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            println("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                        println("upload() failed: [\(error)]")
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                    println("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                println("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    myNewPost = PFObject(className:"EventImages")
                    myNewPost["postData"] = self.originalEventLogoFile
                    myNewPost["postHeight"] = self.originalImageData!.size.height
                    myNewPost["postWidth"] = self.originalImageData!.size.width
                    myNewPost["postType"] = "image"
                    myNewPost["eventObjectId"] = currentEvent.objectId!
                    myNewPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
                    
                    self.createFirstPost(myNewPost, insertedId: insertedId)
                    
                })
            }
            return nil
        }
    }
    
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                            println("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                        println("upload() failed: [\(error)]")
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                    println("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                println("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    
                    if(isOriginal == true){
                        
                        println("original image uploaded. creating event now....")
                        var eventDetails = PFObject(className:"Events")
                        eventDetails["eventTitle"] = self.eventTitle
                        eventDetails["eventImage"] = self.eventLogoFile
                        eventDetails["originalEventImage"] = self.originalEventLogoFile
                        eventDetails["frameX"] = self.frameX
                        eventDetails["frameY"] = self.frameY
                        eventDetails["eventCreatorObjectId"] = self.currentUserId
                        eventDetails["senderName"] = self.fullUserName
                        
                        
                        eventDetails["eventFolder"] = "\(self.currentUserId)/eventProfileImages/"
                        
                        eventDetails["isRSVP"] = true
                        eventDetails["eventDescription"] = newEvent["eventDescription"]
                        eventDetails["eventLatitude"] = newEvent["eventLatitude"]
                        eventDetails["eventLongitude"] = newEvent["eventLongitude"]
                        eventDetails["eventStartDateTime"] = newEvent["eventStartDateTime"]
                        eventDetails["eventEndDateTime"] = newEvent["eventEndDateTime"]
                        eventDetails["eventLocation"] = newEvent["eventLocation"]

                        eventDetails["isUpdated"] = false
                        
                        self.createEvent(eventDetails, insertedId: insertedId)
                    }
                    else
                    {
                        println("cropped image uploaded. uploading original image now....")
                        
                        
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        self.originalEventLogoFileUrl = NSURL(fileURLWithPath: documentDirectory.stringByAppendingPathComponent(self.originalEventLogoFile))
                        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        
                        let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(self.originalImageData!), 0.5)

                        originaldata.writeToURL(self.originalEventLogoFileUrl!, atomically: true)
                        uploadRequest.bucket = "eventnodepublicpics"
                        uploadRequest.key =  "\(self.currentUserId)/eventProfileImages/\(self.originalEventLogoFile)"
                        uploadRequest.body = self.originalEventLogoFileUrl

                        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                        
                        println(self.eventLogoFileUrl.relativePath)
                        println(self.eventLogoFileUrl.relativeString)
                        println(self.eventLogoFileUrl.path)
                        
                        self.upload(uploadRequest, isOriginal: true, insertedId: insertedId)
                    }
                    
                })
            }
            return nil
        }
    }
    
}
